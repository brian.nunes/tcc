﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Odbc;
using System.Data;

namespace softwarecordenacao
{
    class SQLDeletes
    {
        static public string DeletarCurso(string idCurso)
        {
            try
            {
                //pesquisa todas as classes relacionadas a esse curso
                sql.pesquisar("SELECT * FROM tab_classe WHERE fk_curso  = " + idCurso);

                //cria variável para contagem
                int k = 0;

                //conta
                while (sql.guarda.Read())
                {
                    k++;
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //cria vetor para armazenar código dessas classes
                string[] codigos = new string[k];

                //pesquisa todas as aulas relacionadas a esta tabela
                sql.pesquisar("SELECT * FROM tab_classe WHERE fk_curso  = " + idCurso);

                //zera variavel para usar como auxiliar
                k = 0;

                //armazena codigo por codigo
                while (sql.guarda.Read())
                {
                    codigos[k] = sql.guarda[0].ToString();
                    k++;
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //cria laço de repetição para excluir todas essas classes
                for (k = 0; k < codigos.Length; k++)
                {
                    DeletarTurma(codigos[k]);
                }

                //deleta as relações com matéria desse curso
                sql.executarcomando("DELETE FROM relacao_materia_curso WHERE fk_curso = '" + idCurso + "'", " delete");

                //deleta o registro do curso
                sql.executarcomando("DELETE FROM tab_curso WHERE id_curso = '" + idCurso + "'", " delete");

                return "Professor deletado com sucesso!";
            }
            catch
            {
                return "Houve um problema!";
            }
        }

        static public string DeletarProf(string idProf)
        {
            try
            {
                //deleta as aulas desse professor
                DeletarAula(idProf, "fk_prof");

                //deleta as notificações desse professor
                sql.executarcomando("DELETE FROM tab_notificacao WHERE fk_prof = '" + idProf + "'", " delete");

                //deleta os dias desse professor
                sql.executarcomando("DELETE FROM tab_dia WHERE fk_prof = '" + idProf + "'", " delete");

                //deleta as relações com classes
                sql.executarcomando("DELETE FROM relacao_prof_classe_permanente WHERE fk_prof = '" + idProf + "'", " delete");
                sql.executarcomando("DELETE FROM relacao_prof_classe_temporaria WHERE fk_prof = '" + idProf + "'", " delete");

                //deleta as relações com matérias
                sql.executarcomando("DELETE FROM relacao_prof_materia_permanente WHERE fk_prof = '" + idProf + "'", " delete");
                sql.executarcomando("DELETE FROM relacao_prof_materia_temporaria WHERE fk_prof = '" + idProf + "'", " delete");

                //deleta o registro do professor
                sql.executarcomando("DELETE FROM tab_prof WHERE id_prof = '" + idProf + "'", " delete");

                return "Professor deletado com sucesso!";
            }
            catch
            {
                return "Houve um problema!";
            }

        }

        static public string DeletarMateria(string idMateria)
        {
            try
            {
                //deleta as aulas dessa turma
                DeletarAula(idMateria, "fk_sigla_materia");

                //deleta a matéria nos cursos que ela foi alocada
                sql.executarcomando("DELETE FROM relacao_materia_curso WHERE fk_materia = '" + idMateria + "'", " delete");

                //deleta as relações com professores
                sql.executarcomando("DELETE FROM relacao_prof_materia_permanente WHERE fk_materia = '" + idMateria + "'", " delete");
                sql.executarcomando("DELETE FROM relacao_prof_materia_temporaria WHERE fk_materia = '" + idMateria + "'", " delete");

                //deleta a matéria
                sql.executarcomando("DELETE FROM tab_materia WHERE id_materia = '" + idMateria + "'", " delete");

                return "Turma deletada com sucesso!";
            }
            catch
            {
                return "Houve um problema!";
            }
        }

        static public string DeletarTurma(string idTurma)
        {
            try
            {
                //deleta as aulas dessa turma
                DeletarAula(idTurma, "fk_classe");

                //deleta os dias dessa turma
                sql.executarcomando("DELETE FROM tab_dia WHERE fk_classe = '" + idTurma + "'", " delete");

                //deleta as relações com professores
                sql.executarcomando("DELETE FROM relacao_prof_classe_permanente WHERE fk_classe = '" + idTurma + "'", " delete");
                sql.executarcomando("DELETE FROM relacao_prof_classe_temporaria WHERE fk_classe = '" + idTurma + "'", " delete");

                //deleta o registro da turma
                sql.executarcomando("DELETE FROM tab_classe WHERE id_classe = '" + idTurma + "'", " delete");

                return "Turma deletada com sucesso!";
            }
            catch
            {
                return "Houve um problema!";
            }
        }

        static public string DeletarAula(string ID, string coluna)
        {
            try
            {
                //pesquisa todas as aulas relacionadas a esta tabela
                sql.pesquisar("SELECT * FROM tab_aula WHERE " + coluna + " = " + ID);

                //cria variável para contagem
                int k = 0;

                //conta
                while(sql.guarda.Read())
                {
                    k++;
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //cria vetor para armazenar código dessas aulas
                string[] codigos = new string[k];

                //pesquisa todas as aulas relacionadas a esta tabela
                sql.pesquisar("SELECT * FROM tab_aula WHERE " + coluna + " = " + ID);

                //zera variavel para usar como auxiliar
                k = 0;

                //armazena codigo por codigo
                while (sql.guarda.Read())
                {
                    codigos[k] = sql.guarda[0].ToString();
                    k++;
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //cria laço de repetição para substituir todas as aulas de cada um desses códigos e deleta-las
                for(k = 0; k < codigos.Length; k++)
                {
                    sql.executarcomando("UPDATE `tab_dia` SET `730` = 1 WHERE `tab_dia`.`730` = " + codigos[k] + ";", " atualização");
                    sql.executarcomando("UPDATE `tab_dia` SET `820` = 1 WHERE `tab_dia`.`820` = " + codigos[k] + ";", " atualização");
                    sql.executarcomando("UPDATE `tab_dia` SET `910` = 1 WHERE `tab_dia`.`910` = " + codigos[k] + ";", " atualização");
                    sql.executarcomando("UPDATE `tab_dia` SET `1020` = 1 WHERE `tab_dia`.`1020` = " + codigos[k] + ";", " atualização");
                    sql.executarcomando("UPDATE `tab_dia` SET `1110` = 1 WHERE `tab_dia`.`1110` = " + codigos[k] + ";", " atualização");
                    sql.executarcomando("UPDATE `tab_dia` SET `1330` = 1 WHERE `tab_dia`.`1330` = " + codigos[k] + ";", " atualização");
                    sql.executarcomando("UPDATE `tab_dia` SET `1420` = 1 WHERE `tab_dia`.`1420` = " + codigos[k] + ";", " atualização");
                    sql.executarcomando("UPDATE `tab_dia` SET `1530` = 1 WHERE `tab_dia`.`1530` = " + codigos[k] + ";", " atualização");
                    sql.executarcomando("UPDATE `tab_dia` SET `1620` = 1 WHERE `tab_dia`.`1620` = " + codigos[k] + ";", " atualização");
                    sql.executarcomando("DELETE FROM tab_aula WHERE id_aula = " + codigos[k], " delete");
                }


                return "Aulas deletadas com sucesso!";
            }
            catch
            {
                return "Houve algum erro!";
            }
        }

        static public string DeletarAulaProfTurma(string idProf, string idTurma)
        {
            try
            {
                //pesquisa todas as aulas relacionadas a esta tabela
                sql.pesquisar("SELECT * FROM tab_aula WHERE fk_prof = " + idProf + " AND fk_classe = " + idTurma);

                //cria variável para contagem
                int k = 0;

                //conta
                while (sql.guarda.Read())
                {
                    k++;
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //cria vetor para armazenar código dessas aulas
                string[] codigos = new string[k];

                //pesquisa todas as aulas relacionadas a esta tabela
                sql.pesquisar("SELECT * FROM tab_aula WHERE fk_prof = " + idProf + " AND fk_classe = " + idTurma);

                //zera variavel para usar como auxiliar
                k = 0;

                //armazena codigo por codigo
                while (sql.guarda.Read())
                {
                    codigos[k] = sql.guarda[0].ToString();
                    k++;
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //cria laço de repetição para substituir todas as aulas de cada um desses códigos e deleta-las
                for (k = 0; k < codigos.Length; k++)
                {
                    sql.executarcomando("UPDATE `tab_dia` SET `730` = 1 WHERE `tab_dia`.`730` = " + codigos[k] + ";", " atualização");
                    sql.executarcomando("UPDATE `tab_dia` SET `820` = 1 WHERE `tab_dia`.`820` = " + codigos[k] + ";", " atualização");
                    sql.executarcomando("UPDATE `tab_dia` SET `910` = 1 WHERE `tab_dia`.`910` = " + codigos[k] + ";", " atualização");
                    sql.executarcomando("UPDATE `tab_dia` SET `1020` = 1 WHERE `tab_dia`.`1020` = " + codigos[k] + ";", " atualização");
                    sql.executarcomando("UPDATE `tab_dia` SET `1110` = 1 WHERE `tab_dia`.`1110` = " + codigos[k] + ";", " atualização");
                    sql.executarcomando("UPDATE `tab_dia` SET `1330` = 1 WHERE `tab_dia`.`1330` = " + codigos[k] + ";", " atualização");
                    sql.executarcomando("UPDATE `tab_dia` SET `1420` = 1 WHERE `tab_dia`.`1420` = " + codigos[k] + ";", " atualização");
                    sql.executarcomando("UPDATE `tab_dia` SET `1530` = 1 WHERE `tab_dia`.`1530` = " + codigos[k] + ";", " atualização");
                    sql.executarcomando("UPDATE `tab_dia` SET `1620` = 1 WHERE `tab_dia`.`1620` = " + codigos[k] + ";", " atualização");
                    sql.executarcomando("DELETE FROM tab_aula WHERE id_aula = " + codigos[k], " delete");
                }


                return "Aulas deletadas com sucesso!";
            }
            catch
            {
                return "Houve algum erro!";
            }
        }
    }
}
