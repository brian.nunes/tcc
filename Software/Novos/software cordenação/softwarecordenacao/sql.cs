﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Odbc;
using System.Data;

namespace softwarecordenacao
{
    class sql
    {
        static public string conexao = "Driver={MySql ODBC 3.51 Driver};Server=localhost;Database=tempus_bd;uid=root;";
        static public DataSet dados = new DataSet();
        static public OdbcConnection olecon = new OdbcConnection(conexao);
        static public OdbcDataReader guarda;

        static public string filtrar(string comando, string arq)
        {
            OdbcDataAdapter adapterx2 = new OdbcDataAdapter(comando, olecon);
            DataSet dataset2 = new DataSet(arq);
            dados.Clear();
            try
            {
                olecon.Open();
                adapterx2.Fill(dados, arq);
                olecon.Close();
                return "Dados coletados";
            }
            catch
            {
                olecon.Close();
                return ("A conexão não foi possível");
            }
        }

        static public string executarcomando(string comando, string operacao)
        {
            try
            {
                olecon.Open();
                OdbcCommand novo = new OdbcCommand(comando, olecon);
                novo.ExecuteNonQuery();
                olecon.Close();
                return (operacao + " com sucesso");
            }
            catch
            {
                olecon.Close();
                return ("Não foi possível a conexão ou erro de inclusão");
            }
        }

        static public void pesquisar(string comando)
        {
            olecon.Open();
            OdbcCommand cmdx = new OdbcCommand(comando, olecon);
            guarda = cmdx.ExecuteReader();
        }
    }
}
