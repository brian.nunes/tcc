﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace softwarecordenacao.Formularios
{
    public partial class CadastrarProfessor : Form
    {
        public CadastrarProfessor()
        {
            InitializeComponent();
        }

        private void pbconfirmarcadastro_Click(object sender, EventArgs e)
        {
            //confere se foi tudo preenchido
            if(txtnome.Text != "" && txtsigla.Text != "" && txtpreferencia.Text != "" && txtsenha.Text != "" && txtconfirmarsenha.Text != "")
            {
                //pesquisa se há uma sigla igual cadastrada ou id
                sql.pesquisar("select * from tab_prof where sigla ='" + txtsigla.Text + "' OR id_prof = '" + txtidprof.Text + "';");
                if(sql.guarda.Read())
                {
                    //avisa se for o caso
                    MessageBox.Show("Sigla de professor ou ID já cadastradado!", "*** Erro ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //fecha as conexões
                    sql.guarda.Close();
                    sql.olecon.Close();
                }
                else
                {
                    //fecha as conexões
                    sql.guarda.Close();
                    sql.olecon.Close();
                    //confere se as senhas batem
                    if (txtsenha.Text == txtconfirmarsenha.Text)
                    {
                        try
                        {
                            //faz a inserção no banco de dados
                            sql.executarcomando("INSERT INTO `tab_prof` (`id_prof`, `nome`, `sigla`, `senha`, `preferencia`, `num_aula_disponivel`) VALUES ('" + txtidprof.Text + "', '" + txtnome.Text + "', '" + txtsigla.Text + "', '" + txtsenha.Text + "', " + txtpreferencia.Text + ", 0)", "Inserção");
                            //da o feed back ao usuario
                            MessageBox.Show("Cadastro feito com sucesso!", "*** Cadastro ***", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            //prepara o form para um novo cadastro
                            txtnome.Clear();
                            txtidprof.Clear();
                            txtpreferencia.Clear();
                            txtsenha.Clear();
                            txtsigla.Clear();
                            txtconfirmarsenha.Clear();
                            txtidprof.Focus();
                        }
                        catch
                        {
                            //mensagem de erro caso ocorra algum erro coma inserção do cadastro
                            MessageBox.Show("Cadastro não pode ser efetuado!", "*** Erro ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        //avisa ao usuario que as senhas não coincidem
                        MessageBox.Show("As senhas não conferem!", "*** Erro ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
            {
                //avisa ao usuario que existem campos não preenchidos
                MessageBox.Show("Preencha todos os campos!", "*** Erro ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CadastrarProfessor_Load(object sender, EventArgs e)
        {

        }
    }
}
