﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace softwarecordenacao.Formularios
{
    public partial class AlterarProfessor : Form
    {
        public AlterarProfessor()
        {
            InitializeComponent();
        }

        private void pbconfirmaralteracoes_Click(object sender, EventArgs e)
        {
            //confere se foi tudo preenchido
            if (txtnomeprof.Text != "" && txtsigla.Text != "" && numpref.Text != "" && txtidprofessor.Text != "")
            {
                //confere se o campo de senha foi preenchido
                if (txtsenha.Text != "")
                {
                    //confere se as senhas são iguais
                    if (txtsenha.Text == txtconfirmasenha.Text)
                    {
                        //fecha as conexões
                        sql.guarda.Close();
                        sql.olecon.Close();
                            try
                            {
                                //faz a atualização no banco de dados
                                sql.executarcomando("UPDATE `tab_prof` SET `nome` = '"+txtnomeprof.Text+"', `senha` = '"+txtsenha.Text+"', `preferencia` = '"+numpref.Value.ToString()+"' WHERE `tab_prof`.`id_prof` = " + txtidprofessor.Text, " atualização");
                                //da o feed back ao usuario
                                DialogResult YN = MessageBox.Show("Alteração feita com sucesso! \nDeseja limpar o formulário?", "*** Alteração ***", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                //testa a resposta do usuário
                                if (YN == DialogResult.Yes)
                                {
                                    //prepara o form para um novo cadastro
                                    button3_Click(sender, e);
                                }
                            }
                            catch
                            {
                                //mensagem de erro caso ocorra algum erro coma inserção do cadastro
                                MessageBox.Show("Alteração não pode ser efetuada!", "*** Erro ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                    }
                    else
                    {
                        //avisa ao usuario que as senhas não coincidem
                        MessageBox.Show("As senhas não conferem!", "*** Erro ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                //caso não tenha sido preenchido segue a alteração sem a parte da senha
                else
                {
                    //fecha as conexões
                    sql.guarda.Close();
                    sql.olecon.Close();
                    try
                    {
                        //faz a atualização no banco de dados
                        sql.executarcomando("UPDATE `tab_prof` SET `nome` = '" + txtnomeprof.Text + "', `preferencia` = '" + numpref.Value.ToString() + "' WHERE `tab_prof`.`id_prof` = " + txtidprofessor.Text, " atualização");
                        //da o feed back ao usuario
                        DialogResult YN = MessageBox.Show("Alteração feita com sucesso! \nDeseja limpar o formulário?", "*** Alteração ***", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        //testa a resposta do usuário
                        if (YN == DialogResult.Yes)
                        {
                            //prepara o form para um novo cadastro
                            button3_Click(sender, e);
                        }
                    }
                    catch
                    {
                        //mensagem de erro caso ocorra algum erro coma inserção do cadastro
                        MessageBox.Show("Alteração não pode ser efetuada!", "*** Erro ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
            {
                //avisa ao usuario que existem campos não preenchidos
                MessageBox.Show("Preencha todos os campos! (Os de senhas são opcionais)", "*** Erro ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void pblocalizar_Click(object sender, EventArgs e)
        {
            //testa se o campo foi preenchiddo
            if (txtsigla.Text != "" && txtsigla.Text != "ine" && txtsigla.Text != "NDA" && txtsigla.Text != "IND")
            {
                try
                {
                    //pesquisa o professor buscado
                    sql.pesquisar("SELECT * FROM tab_prof WHERE sigla = '" + txtsigla.Text + "'");

                    //testa se encontrou
                    if (sql.guarda.Read())
                    {
                        //ativa todas as entradas de dados
                        txtnomeprof.Enabled = true;
                        txtsenha.Enabled = true;
                        txtconfirmasenha.Enabled = true;
                        numpref.Enabled = true;

                        //desativa a da sigla do prof
                        txtsigla.Enabled = false;

                        //preencha as mesmas com os dados do professor
                        txtidprofessor.Text = sql.guarda[0].ToString();
                        txtnomeprof.Text = sql.guarda[1].ToString();
                        numpref.Value = Convert.ToInt32(sql.guarda[4]);
                    }
                    else
                    {
                        //avisa se o prof não foi encontrado
                        MessageBox.Show("Professor não encontrado", "*** ERRO ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch
                {
                    //informa que há um erro de conexão com o banco de dados
                    MessageBox.Show("Conexão falha.", "*** ERRO ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                //pede ao usuário para preencher
                MessageBox.Show("Escreva uma sigla válida.", "*** ERRO ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            //fecha as conexões
            sql.guarda.Close();
            sql.olecon.Close();
        }

        private void pblimparformulario_Click(object sender, EventArgs e)
        {
            //limpa todos os campos
            txtconfirmasenha.Clear();
            txtidprofessor.Clear();
            txtnomeprof.Clear();
            txtsenha.Clear();
            txtsigla.Clear();
            numpref.Value = 0;

            //foca no campo da sigla
            txtsigla.Focus();

            //abre a textbox para a sigla do professor
            txtsigla.Enabled = true;

            //fecha todas as outras textbox
            txtconfirmasenha.Enabled = false;
            txtidprofessor.Enabled = false;
            txtnomeprof.Enabled = false;
            txtsenha.Enabled = false;
            numpref.Enabled = false;
        }

        private void pbdeletar_Click(object sender, EventArgs e)
        {
            if (txtnomeprof.Enabled == true)
            {
                //pergunta ao usuário se ele tem certeza do ato
                DialogResult YN = MessageBox.Show("Deseja mesmo excluir este registro?", "*** Exclusão ***", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                //confere a resposta
                if (YN == DialogResult.Yes)
                {
                    //fecha as conexões
                    sql.guarda.Close();
                    sql.olecon.Close();

                    SQLDeletes.DeletarProf(txtidprofessor.Text);

                    //retorna ao usuário o sucesso da exclusão
                    DialogResult SN = MessageBox.Show("Exclusão feita com sucesso! \nDeseja limpar o formulário?", "*** Alteração ***", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    //testa a resposta do usuário
                    if (SN == DialogResult.Yes)
                    {
                        //prepara o form para um novo cadastro
                        button3_Click(sender, e);
                    }
                }
            }
            else
            {
                //pede ao usuário para localizar
                MessageBox.Show("Localize um professor primeiro.", "*** ERRO ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
