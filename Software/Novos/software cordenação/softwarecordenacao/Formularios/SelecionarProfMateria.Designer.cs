﻿namespace softwarecordenacao
{
    partial class SelecionarProfMateria
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SelecionarProfMateria));
            this.pbcancelar = new System.Windows.Forms.PictureBox();
            this.pbconfirmar = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pbsetadir = new System.Windows.Forms.PictureBox();
            this.pbsetaesq = new System.Windows.Forms.PictureBox();
            this.lbPerm = new System.Windows.Forms.ListBox();
            this.lbTemp = new System.Windows.Forms.ListBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.cbmateria = new System.Windows.Forms.ComboBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbcancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbconfirmar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsetadir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsetaesq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.SuspendLayout();
            // 
            // pbcancelar
            // 
            this.pbcancelar.Image = ((System.Drawing.Image)(resources.GetObject("pbcancelar.Image")));
            this.pbcancelar.Location = new System.Drawing.Point(75, 674);
            this.pbcancelar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbcancelar.Name = "pbcancelar";
            this.pbcancelar.Size = new System.Drawing.Size(194, 72);
            this.pbcancelar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbcancelar.TabIndex = 33;
            this.pbcancelar.TabStop = false;
            this.pbcancelar.Click += new System.EventHandler(this.pbcancelar_Click);
            // 
            // pbconfirmar
            // 
            this.pbconfirmar.Image = ((System.Drawing.Image)(resources.GetObject("pbconfirmar.Image")));
            this.pbconfirmar.Location = new System.Drawing.Point(458, 674);
            this.pbconfirmar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbconfirmar.Name = "pbconfirmar";
            this.pbconfirmar.Size = new System.Drawing.Size(198, 72);
            this.pbconfirmar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbconfirmar.TabIndex = 32;
            this.pbconfirmar.TabStop = false;
            this.pbconfirmar.Click += new System.EventHandler(this.pbconfirmar_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.DarkGreen;
            this.pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(162, -3);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(455, 207);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 31;
            this.pictureBox3.TabStop = false;
            // 
            // pbsetadir
            // 
            this.pbsetadir.Enabled = false;
            this.pbsetadir.Image = ((System.Drawing.Image)(resources.GetObject("pbsetadir.Image")));
            this.pbsetadir.Location = new System.Drawing.Point(312, 462);
            this.pbsetadir.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbsetadir.Name = "pbsetadir";
            this.pbsetadir.Size = new System.Drawing.Size(98, 38);
            this.pbsetadir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbsetadir.TabIndex = 30;
            this.pbsetadir.TabStop = false;
            this.pbsetadir.Click += new System.EventHandler(this.pbsetadir_Click);
            // 
            // pbsetaesq
            // 
            this.pbsetaesq.Enabled = false;
            this.pbsetaesq.Image = ((System.Drawing.Image)(resources.GetObject("pbsetaesq.Image")));
            this.pbsetaesq.Location = new System.Drawing.Point(314, 546);
            this.pbsetaesq.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbsetaesq.Name = "pbsetaesq";
            this.pbsetaesq.Size = new System.Drawing.Size(98, 38);
            this.pbsetaesq.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbsetaesq.TabIndex = 29;
            this.pbsetaesq.TabStop = false;
            this.pbsetaesq.Click += new System.EventHandler(this.pbsetaesq_Click);
            // 
            // lbPerm
            // 
            this.lbPerm.Enabled = false;
            this.lbPerm.ForeColor = System.Drawing.Color.DarkGreen;
            this.lbPerm.FormattingEnabled = true;
            this.lbPerm.HorizontalScrollbar = true;
            this.lbPerm.ItemHeight = 20;
            this.lbPerm.Location = new System.Drawing.Point(420, 408);
            this.lbPerm.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lbPerm.Name = "lbPerm";
            this.lbPerm.Size = new System.Drawing.Size(272, 244);
            this.lbPerm.TabIndex = 28;
            // 
            // lbTemp
            // 
            this.lbTemp.Enabled = false;
            this.lbTemp.ForeColor = System.Drawing.Color.DarkGreen;
            this.lbTemp.FormattingEnabled = true;
            this.lbTemp.HorizontalScrollbar = true;
            this.lbTemp.ItemHeight = 20;
            this.lbTemp.Location = new System.Drawing.Point(44, 408);
            this.lbTemp.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lbTemp.Name = "lbTemp";
            this.lbTemp.Size = new System.Drawing.Size(258, 244);
            this.lbTemp.TabIndex = 27;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(42, 251);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(140, 49);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 41;
            this.pictureBox1.TabStop = false;
            // 
            // cbmateria
            // 
            this.cbmateria.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbmateria.FormattingEnabled = true;
            this.cbmateria.Location = new System.Drawing.Point(190, 258);
            this.cbmateria.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbmateria.Name = "cbmateria";
            this.cbmateria.Size = new System.Drawing.Size(502, 28);
            this.cbmateria.TabIndex = 40;
            this.cbmateria.SelectedIndexChanged += new System.EventHandler(this.cbmateria_SelectedIndexChanged);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.DarkGreen;
            this.pictureBox2.Location = new System.Drawing.Point(-3, -3);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(171, 208);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 42;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.DarkGreen;
            this.pictureBox4.Location = new System.Drawing.Point(615, -3);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(130, 208);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 43;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(44, 342);
            this.pictureBox5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(260, 57);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 44;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(420, 342);
            this.pictureBox6.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(274, 57);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 45;
            this.pictureBox6.TabStop = false;
            // 
            // SelecionarProfMateria
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(744, 758);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.cbmateria);
            this.Controls.Add(this.pbcancelar);
            this.Controls.Add(this.pbconfirmar);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pbsetadir);
            this.Controls.Add(this.pbsetaesq);
            this.Controls.Add(this.lbPerm);
            this.Controls.Add(this.lbTemp);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "SelecionarProfMateria";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Selecionar professor por matéria";
            this.Load += new System.EventHandler(this.SelecionarProfMateria_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbcancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbconfirmar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsetadir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsetaesq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pbcancelar;
        private System.Windows.Forms.PictureBox pbconfirmar;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pbsetadir;
        private System.Windows.Forms.PictureBox pbsetaesq;
        private System.Windows.Forms.ListBox lbPerm;
        private System.Windows.Forms.ListBox lbTemp;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ComboBox cbmateria;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
    }
}