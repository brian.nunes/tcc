﻿namespace softwarecordenacao.Formularios
{
    partial class AlterarTurma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AlterarTurma));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pbconfirmaralteracoes = new System.Windows.Forms.PictureBox();
            this.txtletra = new System.Windows.Forms.TextBox();
            this.txtidturma = new System.Windows.Forms.TextBox();
            this.cbcurso = new System.Windows.Forms.ComboBox();
            this.numano = new System.Windows.Forms.NumericUpDown();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pblimparformulario = new System.Windows.Forms.PictureBox();
            this.pbdeletar = new System.Windows.Forms.PictureBox();
            this.pblocalizar = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbconfirmaralteracoes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numano)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pblimparformulario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbdeletar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pblocalizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.DarkGreen;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(142, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(388, 177);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 33;
            this.pictureBox1.TabStop = false;
            // 
            // pbconfirmaralteracoes
            // 
            this.pbconfirmaralteracoes.Image = ((System.Drawing.Image)(resources.GetObject("pbconfirmaralteracoes.Image")));
            this.pbconfirmaralteracoes.Location = new System.Drawing.Point(452, 583);
            this.pbconfirmaralteracoes.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbconfirmaralteracoes.Name = "pbconfirmaralteracoes";
            this.pbconfirmaralteracoes.Size = new System.Drawing.Size(201, 78);
            this.pbconfirmaralteracoes.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbconfirmaralteracoes.TabIndex = 32;
            this.pbconfirmaralteracoes.TabStop = false;
            this.pbconfirmaralteracoes.Click += new System.EventHandler(this.pbconfirmaralteracoes_Click);
            // 
            // txtletra
            // 
            this.txtletra.Location = new System.Drawing.Point(164, 329);
            this.txtletra.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtletra.Name = "txtletra";
            this.txtletra.Size = new System.Drawing.Size(487, 26);
            this.txtletra.TabIndex = 31;
            // 
            // txtidturma
            // 
            this.txtidturma.Enabled = false;
            this.txtidturma.Location = new System.Drawing.Point(164, 398);
            this.txtidturma.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtidturma.Name = "txtidturma";
            this.txtidturma.Size = new System.Drawing.Size(487, 26);
            this.txtidturma.TabIndex = 38;
            // 
            // cbcurso
            // 
            this.cbcurso.Enabled = false;
            this.cbcurso.FormattingEnabled = true;
            this.cbcurso.Location = new System.Drawing.Point(164, 465);
            this.cbcurso.Name = "cbcurso";
            this.cbcurso.Size = new System.Drawing.Size(487, 28);
            this.cbcurso.TabIndex = 39;
            // 
            // numano
            // 
            this.numano.Location = new System.Drawing.Point(164, 263);
            this.numano.Maximum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numano.Name = "numano";
            this.numano.Size = new System.Drawing.Size(324, 26);
            this.numano.TabIndex = 44;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.DarkGreen;
            this.pictureBox2.Location = new System.Drawing.Point(-4, 0);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(158, 177);
            this.pictureBox2.TabIndex = 47;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.DarkGreen;
            this.pictureBox3.Location = new System.Drawing.Point(526, 0);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(144, 177);
            this.pictureBox3.TabIndex = 48;
            this.pictureBox3.TabStop = false;
            // 
            // pblimparformulario
            // 
            this.pblimparformulario.Image = ((System.Drawing.Image)(resources.GetObject("pblimparformulario.Image")));
            this.pblimparformulario.Location = new System.Drawing.Point(234, 583);
            this.pblimparformulario.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pblimparformulario.Name = "pblimparformulario";
            this.pblimparformulario.Size = new System.Drawing.Size(196, 78);
            this.pblimparformulario.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pblimparformulario.TabIndex = 49;
            this.pblimparformulario.TabStop = false;
            this.pblimparformulario.Click += new System.EventHandler(this.pblimparformulario_Click);
            // 
            // pbdeletar
            // 
            this.pbdeletar.Image = ((System.Drawing.Image)(resources.GetObject("pbdeletar.Image")));
            this.pbdeletar.Location = new System.Drawing.Point(12, 583);
            this.pbdeletar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbdeletar.Name = "pbdeletar";
            this.pbdeletar.Size = new System.Drawing.Size(200, 78);
            this.pbdeletar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbdeletar.TabIndex = 50;
            this.pbdeletar.TabStop = false;
            this.pbdeletar.Click += new System.EventHandler(this.pbdeletar_Click);
            // 
            // pblocalizar
            // 
            this.pblocalizar.Image = ((System.Drawing.Image)(resources.GetObject("pblocalizar.Image")));
            this.pblocalizar.Location = new System.Drawing.Point(495, 237);
            this.pblocalizar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pblocalizar.Name = "pblocalizar";
            this.pblocalizar.Size = new System.Drawing.Size(153, 71);
            this.pblocalizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pblocalizar.TabIndex = 51;
            this.pblocalizar.TabStop = false;
            this.pblocalizar.Click += new System.EventHandler(this.pblocalizar_Click);
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(27, 255);
            this.pictureBox7.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(114, 46);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 52;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(27, 322);
            this.pictureBox8.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(114, 49);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 53;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox9.Image")));
            this.pictureBox9.Location = new System.Drawing.Point(15, 389);
            this.pictureBox9.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(138, 48);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox9.TabIndex = 58;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox10.Image")));
            this.pictureBox10.Location = new System.Drawing.Point(27, 455);
            this.pictureBox10.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(114, 49);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox10.TabIndex = 59;
            this.pictureBox10.TabStop = false;
            // 
            // AlterarTurma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(670, 680);
            this.Controls.Add(this.pictureBox10);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pblocalizar);
            this.Controls.Add(this.pbdeletar);
            this.Controls.Add(this.pblimparformulario);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.numano);
            this.Controls.Add(this.cbcurso);
            this.Controls.Add(this.txtidturma);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pbconfirmaralteracoes);
            this.Controls.Add(this.txtletra);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "AlterarTurma";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Alterar turma";
            this.Load += new System.EventHandler(this.AlterarTurma_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbconfirmaralteracoes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numano)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pblimparformulario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbdeletar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pblocalizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pbconfirmaralteracoes;
        private System.Windows.Forms.TextBox txtletra;
        private System.Windows.Forms.TextBox txtidturma;
        private System.Windows.Forms.ComboBox cbcurso;
        private System.Windows.Forms.NumericUpDown numano;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pblimparformulario;
        private System.Windows.Forms.PictureBox pbdeletar;
        private System.Windows.Forms.PictureBox pblocalizar;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox10;
    }
}