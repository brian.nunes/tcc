﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace softwarecordenacao.Formularios
{
    public partial class PreencherHorarios : Form
    {
        string cod_prof, cod_materia, cod_turma, cod_aula, aulasrestantes = "0";
        string[,] aulas;
        Boolean status;
        public PreencherHorarios()
        {
            InitializeComponent();
        }

        private void cbmateria_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbmateria.SelectedIndex != -1)
            {
                //ativa cbturma e a limpa
                cbturma.Enabled = true;
                cbturma.Items.Clear();
                cbturma.Text = "";

                txtsala.Text = "";
                txtsala.Enabled = false;

                limpaCB();

                //seleciona o código da materia
                string materia = cbmateria.Text;
                string codigo = "";
                string letra = "";
                int index = 0;
                while (letra != " ")
                {
                    codigo += letra;
                    letra = materia.Substring(index, 1);
                    index++;
                }
                cod_materia = codigo;

                //pesquisa a quantidade de aulas dessa matéria
                sql.pesquisar("SELECT num_aulas_semanais FROM tab_materia WHERE id_materia = " + cod_materia);

                if(sql.guarda.Read())
                {
                    //instancia array que vai receber o dia e hora de cada aula
                    aulas = new string[Convert.ToInt32(sql.guarda[0].ToString()), 2];
                    //salva na variavel
                    aulasrestantes = sql.guarda[0].ToString();
                    //poe na label
                    lblaulasrest.Text = aulasrestantes;
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //pesquisa todas as turmas relacionadas ao prof e a materia
                sql.pesquisar("SELECT * FROM relacao_prof_classe_permanente WHERE fk_prof = " + cod_prof + " AND fk_materia = " + cod_materia);

                //cria variável para contagem
                int k = 0;

                //conta
                while (sql.guarda.Read())
                {
                    k++;
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //cria vetor para armazenar código dessas matérias
                string[] codigos = new string[k];

                //pesquisa todas as matérias relacionadas ao curso
                sql.pesquisar("SELECT * FROM relacao_prof_classe_permanente WHERE fk_prof = " + cod_prof + " AND fk_materia = " + cod_materia);

                //zera variavel para usar como auxiliar
                k = 0;

                //armazena codigo por codigo
                while (sql.guarda.Read())
                {
                    codigos[k] = sql.guarda[1].ToString();
                    k++;
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //cria laço de repetição para coloca-las na cbturma
                for (k = 0; k < codigos.Length; k++)
                {
                    //pesquisa a matéria
                    sql.pesquisar("SELECT * FROM tab_classe WHERE id_classe = " + codigos[k]);

                    //se achar
                    if (sql.guarda.Read())
                    {
                        //escreve na combobox
                        cbturma.Items.Add(sql.guarda[0].ToString() + " - " + sql.guarda[1].ToString()+ sql.guarda[2].ToString());
                    }

                    //fecha as conexões
                    sql.guarda.Close();
                    sql.olecon.Close();
                }
            }
            else
            {
                cod_materia = null;
            }
        }

        private void cbturma_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cbturma.SelectedIndex != -1)
            {
                //desativa todas as checkbox e as limpa
                #region ativando
                cb1aulaseg.Enabled = false;
                cb1aulater.Enabled = false;
                cb1aulaquar.Enabled = false;
                cb1aulaquin.Enabled = false;
                cb1aulasex.Enabled = false;

                cb2aulaseg.Enabled = false;
                cb2aulater.Enabled = false;
                cb2aulaquar.Enabled = false;
                cb2aulaquin.Enabled = false;
                cb2aulasex.Enabled = false;

                cb3aulaseg.Enabled = false;
                cb3aulater.Enabled = false;
                cb3aulaquar.Enabled = false;
                cb3aulaquin.Enabled = false;
                cb3aulasex.Enabled = false;

                cb4aulaseg.Enabled = false;
                cb4aulater.Enabled = false;
                cb4aulaquar.Enabled = false;
                cb4aulaquin.Enabled = false;
                cb4aulasex.Enabled = false;

                cb5aulaseg.Enabled = false;
                cb5aulater.Enabled = false;
                cb5aulaquar.Enabled = false;
                cb5aulaquin.Enabled = false;
                cb5aulasex.Enabled = false;

                cb6aulaseg.Enabled = false;
                cb6aulater.Enabled = false;
                cb6aulaquar.Enabled = false;
                cb6aulaquin.Enabled = false;
                cb6aulasex.Enabled = false;

                cb7aulaseg.Enabled = false;
                cb7aulater.Enabled = false;
                cb7aulaquar.Enabled = false;
                cb7aulaquin.Enabled = false;
                cb7aulasex.Enabled = false;

                cb8aulaseg.Enabled = false;
                cb8aulater.Enabled = false;
                cb8aulaquar.Enabled = false;
                cb8aulaquin.Enabled = false;
                cb8aulasex.Enabled = false;

                cb9aulaseg.Enabled = false;
                cb9aulater.Enabled = false;
                cb9aulaquar.Enabled = false;
                cb9aulaquin.Enabled = false;
                cb9aulasex.Enabled = false;
                #endregion
                #region limpando
                cb1aulaseg.Checked = false;
                cb1aulater.Checked = false;
                cb1aulaquar.Checked = false;
                cb1aulaquin.Checked = false;
                cb1aulasex.Checked = false;

                cb2aulaseg.Checked = false;
                cb2aulater.Checked = false;
                cb2aulaquar.Checked = false;
                cb2aulaquin.Checked = false;
                cb2aulasex.Checked = false;

                cb3aulaseg.Checked = false;
                cb3aulater.Checked = false;
                cb3aulaquar.Checked = false;
                cb3aulaquin.Checked = false;
                cb3aulasex.Checked = false;

                cb4aulaseg.Checked = false;
                cb4aulater.Checked = false;
                cb4aulaquar.Checked = false;
                cb4aulaquin.Checked = false;
                cb4aulasex.Checked = false;

                cb5aulaseg.Checked = false;
                cb5aulater.Checked = false;
                cb5aulaquar.Checked = false;
                cb5aulaquin.Checked = false;
                cb5aulasex.Checked = false;

                cb6aulaseg.Checked = false;
                cb6aulater.Checked = false;
                cb6aulaquar.Checked = false;
                cb6aulaquin.Checked = false;
                cb6aulasex.Checked = false;

                cb7aulaseg.Checked = false;
                cb7aulater.Checked = false;
                cb7aulaquar.Checked = false;
                cb7aulaquin.Checked = false;
                cb7aulasex.Checked = false;

                cb8aulaseg.Checked = false;
                cb8aulater.Checked = false;
                cb8aulaquar.Checked = false;
                cb8aulaquin.Checked = false;
                cb8aulasex.Checked = false;

                cb9aulaseg.Checked = false;
                cb9aulater.Checked = false;
                cb9aulaquar.Checked = false;
                cb9aulaquin.Checked = false;
                cb9aulasex.Checked = false;
                #endregion

                //ativa a txt de numaro da sala
                txtsala.Enabled = true;

                //seleciona o código da turma
                string materia = cbturma.Text;
                string codigo = "";
                string letra = "";
                int index = 0;
                while (letra != " ")
                {
                    codigo += letra;
                    letra = materia.Substring(index, 1);
                    index++;
                }
                cod_turma = codigo;

                //pesquisa código da aula desse prof nessa matéria para essa turma
                sql.pesquisar("select * from tab_aula where fk_prof = " + cod_prof + " and fk_classe = " + cod_turma + " and fk_sigla_materia = " + cod_materia);

                //grava o código que achar na variável
                if (sql.guarda.Read())
                {
                    cod_aula = sql.guarda[0].ToString();
                    txtsala.Text = sql.guarda[1].ToString();
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                #region att a tabela pra disp da sala
                //pesquisa o horario de segunda da turma
                sql.pesquisar("select * from tab_dia where dia_semana = 2 and fk_classe = '" + cod_turma + "'");

                while(sql.guarda.Read())
                {
                    //disponibiliza as check box em aulas vagas, e as marca se ja é essa aula (e subtrai na label)
                    if (sql.guarda[2].ToString() == cod_aula)
                    {
                        cb1aulaseg.Enabled = true;
                        cb1aulaseg.Checked = true;
                    }
                    else if (sql.guarda[2].ToString() == "1")
                    {
                        cb1aulaseg.Enabled = true;
                    }

                    if (sql.guarda[3].ToString() == cod_aula)
                    {
                        cb2aulaseg.Enabled = true;
                        cb2aulaseg.Checked = true;
                    }
                    else if (sql.guarda[3].ToString() == "1")
                    {
                        cb2aulaseg.Enabled = true;
                    }

                    if (sql.guarda[4].ToString() == cod_aula)
                    {
                        cb3aulaseg.Enabled = true;
                        cb3aulaseg.Checked = true;
                    }
                    else if (sql.guarda[4].ToString() == "1")
                    {
                        cb3aulaseg.Enabled = true;
                    }

                    if (sql.guarda[5].ToString() == cod_aula)
                    {
                        cb4aulaseg.Enabled = true;
                        cb4aulaseg.Checked = true;
                    }
                    else if (sql.guarda[5].ToString() == "1")
                    {
                        cb4aulaseg.Enabled = true;
                    }

                    if (sql.guarda[6].ToString() == cod_aula)
                    {
                        cb5aulaseg.Enabled = true;
                        cb5aulaseg.Checked = true;
                    }
                    else if (sql.guarda[6].ToString() == "1")
                    {
                        cb5aulaseg.Enabled = true;
                    }

                    if (sql.guarda[7].ToString() == cod_aula)
                    {
                        cb6aulaseg.Enabled = true;
                        cb6aulaseg.Checked = true;
                    }
                    else if (sql.guarda[7].ToString() == "1")
                    {
                        cb6aulaseg.Enabled = true;
                    }

                    if (sql.guarda[8].ToString() == cod_aula)
                    {
                        cb7aulaseg.Enabled = true;
                        cb7aulaseg.Checked = true;
                    }
                    else if (sql.guarda[8].ToString() == "1")
                    {
                        cb7aulaseg.Enabled = true;
                    }

                    if (sql.guarda[9].ToString() == cod_aula)
                    {
                        cb8aulaseg.Enabled = true;
                        cb8aulaseg.Checked = true;
                    }
                    else if (sql.guarda[9].ToString() == "1")
                    {
                        cb8aulaseg.Enabled = true;
                    }

                    if (sql.guarda[10].ToString() == cod_aula)
                    {
                        cb9aulaseg.Enabled = true;
                        cb9aulaseg.Checked = true;
                    }
                    else if (sql.guarda[10].ToString() == "1")
                    {
                        cb9aulaseg.Enabled = true;
                    }
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //pesquisa o horario de terça da turma
                sql.pesquisar("select * from tab_dia where dia_semana = 3 and fk_classe = '" + cod_turma + "'");

                while (sql.guarda.Read())
                {
                    //disponibiliza as check box em aulas vagas, e as marca se ja é essa aula (e subtrai na label)
                    if (sql.guarda[2].ToString() == cod_aula)
                    {
                        cb1aulater.Enabled = true;
                        cb1aulater.Checked = true;
                    }
                    else if (sql.guarda[2].ToString() == "1")
                    {
                        cb1aulater.Enabled = true;
                    }

                    if (sql.guarda[3].ToString() == cod_aula)
                    {
                        cb2aulater.Enabled = true;
                        cb2aulater.Checked = true;
                    }
                    else if (sql.guarda[3].ToString() == "1")
                    {
                        cb2aulater.Enabled = true;
                    }

                    if (sql.guarda[4].ToString() == cod_aula)
                    {
                        cb3aulater.Enabled = true;
                        cb3aulater.Checked = true;
                    }
                    else if (sql.guarda[4].ToString() == "1")
                    {
                        cb3aulater.Enabled = true;
                    }

                    if (sql.guarda[5].ToString() == cod_aula)
                    {
                        cb4aulater.Enabled = true;
                        cb4aulater.Checked = true;
                    }
                    else if (sql.guarda[5].ToString() == "1")
                    {
                        cb4aulater.Enabled = true;
                    }

                    if (sql.guarda[6].ToString() == cod_aula)
                    {
                        cb5aulater.Enabled = true;
                        cb5aulater.Checked = true;
                    }
                    else if (sql.guarda[6].ToString() == "1")
                    {
                        cb5aulater.Enabled = true;
                    }

                    if (sql.guarda[7].ToString() == cod_aula)
                    {
                        cb6aulater.Enabled = true;
                        cb6aulater.Checked = true;
                    }
                    else if (sql.guarda[7].ToString() == "1")
                    {
                        cb6aulater.Enabled = true;
                    }

                    if (sql.guarda[8].ToString() == cod_aula)
                    {
                        cb7aulater.Enabled = true;
                        cb7aulater.Checked = true;
                    }
                    else if (sql.guarda[8].ToString() == "1")
                    {
                        cb7aulater.Enabled = true;
                    }

                    if (sql.guarda[9].ToString() == cod_aula)
                    {
                        cb8aulater.Enabled = true;
                        cb8aulater.Checked = true;
                    }
                    else if (sql.guarda[9].ToString() == "1")
                    {
                        cb8aulater.Enabled = true;
                    }

                    if (sql.guarda[10].ToString() == cod_aula)
                    {
                        cb9aulater.Enabled = true;
                        cb9aulater.Checked = true;
                    }
                    else if (sql.guarda[10].ToString() == "1")
                    {
                        cb9aulater.Enabled = true;
                    }
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //pesquisa o horario de quarta da turma
                sql.pesquisar("select * from tab_dia where dia_semana = 4 and fk_classe = '" + cod_turma + "'");

                while (sql.guarda.Read())
                {
                    //disponibiliza as check box em aulas vagas, e as marca se ja é essa aula (e subtrai na label)
                    if (sql.guarda[2].ToString() == cod_aula)
                    {
                        cb1aulaquar.Enabled = true;
                        cb1aulaquar.Checked = true;
                    }
                    else if (sql.guarda[2].ToString() == "1")
                    {
                        cb1aulaquar.Enabled = true;
                    }

                    if (sql.guarda[3].ToString() == cod_aula)
                    {
                        cb2aulaquar.Enabled = true;
                        cb2aulaquar.Checked = true;
                    }
                    else if (sql.guarda[3].ToString() == "1")
                    {
                        cb2aulaquar.Enabled = true;
                    }

                    if (sql.guarda[4].ToString() == cod_aula)
                    {
                        cb3aulaquar.Enabled = true;
                        cb3aulaquar.Checked = true;
                    }
                    else if (sql.guarda[4].ToString() == "1")
                    {
                        cb3aulaquar.Enabled = true;
                    }

                    if (sql.guarda[5].ToString() == cod_aula)
                    {
                        cb4aulaquar.Enabled = true;
                        cb4aulaquar.Checked = true;
                    }
                    else if (sql.guarda[5].ToString() == "1")
                    {
                        cb4aulaquar.Enabled = true;
                    }

                    if (sql.guarda[6].ToString() == cod_aula)
                    {
                        cb5aulaquar.Enabled = true;
                        cb5aulaquar.Checked = true;
                    }
                    else if (sql.guarda[6].ToString() == "1")
                    {
                        cb5aulaquar.Enabled = true;
                    }

                    if (sql.guarda[7].ToString() == cod_aula)
                    {
                        cb6aulaquar.Enabled = true;
                        cb6aulaquar.Checked = true;
                    }
                    else if (sql.guarda[7].ToString() == "1")
                    {
                        cb6aulaquar.Enabled = true;
                    }

                    if (sql.guarda[8].ToString() == cod_aula)
                    {
                        cb7aulaquar.Enabled = true;
                        cb7aulaquar.Checked = true;
                    }
                    else if (sql.guarda[8].ToString() == "1")
                    {
                        cb7aulaquar.Enabled = true;
                    }

                    if (sql.guarda[9].ToString() == cod_aula)
                    {
                        cb8aulaquar.Enabled = true;
                        cb8aulaquar.Checked = true;
                    }
                    else if (sql.guarda[9].ToString() == "1")
                    {
                        cb8aulaquar.Enabled = true;
                    }

                    if (sql.guarda[10].ToString() == cod_aula)
                    {
                        cb9aulaquar.Enabled = true;
                        cb9aulaquar.Checked = true;
                    }
                    else if (sql.guarda[10].ToString() == "1")
                    {
                        cb9aulaquar.Enabled = true;
                    }
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //pesquisa o horario de quinta da turma
                sql.pesquisar("select * from tab_dia where dia_semana = 5 and fk_classe = '" + cod_turma + "'");

                while (sql.guarda.Read())
                {
                    //disponibiliza as check box em aulas vagas, e as marca se ja é essa aula (e subtrai na label)
                    if (sql.guarda[2].ToString() == cod_aula)
                    {
                        cb1aulaquin.Enabled = true;
                        cb1aulaquin.Checked = true;
                    }
                    else if (sql.guarda[2].ToString() == "1")
                    {
                        cb1aulaquin.Enabled = true;
                    }

                    if (sql.guarda[3].ToString() == cod_aula)
                    {
                        cb2aulaquin.Enabled = true;
                        cb2aulaquin.Checked = true;
                    }
                    else if (sql.guarda[3].ToString() == "1")
                    {
                        cb2aulaquin.Enabled = true;
                    }

                    if (sql.guarda[4].ToString() == cod_aula)
                    {
                        cb3aulaquin.Enabled = true;
                        cb3aulaquin.Checked = true;
                    }
                    else if (sql.guarda[4].ToString() == "1")
                    {
                        cb3aulaquin.Enabled = true;
                    }

                    if (sql.guarda[5].ToString() == cod_aula)
                    {
                        cb4aulaquin.Enabled = true;
                        cb4aulaquin.Checked = true;
                    }
                    else if (sql.guarda[5].ToString() == "1")
                    {
                        cb4aulaquin.Enabled = true;
                    }

                    if (sql.guarda[6].ToString() == cod_aula)
                    {
                        cb5aulaquin.Enabled = true;
                        cb5aulaquin.Checked = true;
                    }
                    else if (sql.guarda[6].ToString() == "1")
                    {
                        cb5aulaquin.Enabled = true;
                    }

                    if (sql.guarda[7].ToString() == cod_aula)
                    {
                        cb6aulaquin.Enabled = true;
                        cb6aulaquin.Checked = true;
                    }
                    else if (sql.guarda[7].ToString() == "1")
                    {
                        cb6aulaquin.Enabled = true;
                    }

                    if (sql.guarda[8].ToString() == cod_aula)
                    {
                        cb7aulaquin.Enabled = true;
                        cb7aulaquin.Checked = true;
                    }
                    else if (sql.guarda[8].ToString() == "1")
                    {
                        cb7aulaquin.Enabled = true;
                    }

                    if (sql.guarda[9].ToString() == cod_aula)
                    {
                        cb8aulaquin.Enabled = true;
                        cb8aulaquin.Checked = true;
                    }
                    else if (sql.guarda[9].ToString() == "1")
                    {
                        cb8aulaquin.Enabled = true;
                    }

                    if (sql.guarda[10].ToString() == cod_aula)
                    {
                        cb9aulaquin.Enabled = true;
                        cb9aulaquin.Checked = true;
                    }
                    else if (sql.guarda[10].ToString() == "1")
                    {
                        cb9aulaquin.Enabled = true;
                    }
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //pesquisa o horario de sexta da turma
                sql.pesquisar("select * from tab_dia where dia_semana = 6 and fk_classe = '" + cod_turma + "'");

                while (sql.guarda.Read())
                {
                    //disponibiliza as check box em aulas vagas, e as marca se ja é essa aula (e subtrai na label)
                    if (sql.guarda[2].ToString() == cod_aula)
                    {
                        cb1aulasex.Enabled = true;
                        cb1aulasex.Checked = true;
                    }
                    else if (sql.guarda[2].ToString() == "1")
                    {
                        cb1aulasex.Enabled = true;
                    }

                    if (sql.guarda[3].ToString() == cod_aula)
                    {
                        cb2aulasex.Enabled = true;
                        cb2aulasex.Checked = true;
                    }
                    else if (sql.guarda[3].ToString() == "1")
                    {
                        cb2aulasex.Enabled = true;
                    }

                    if (sql.guarda[4].ToString() == cod_aula)
                    {
                        cb3aulasex.Enabled = true;
                        cb3aulasex.Checked = true;
                    }
                    else if (sql.guarda[4].ToString() == "1")
                    {
                        cb3aulasex.Enabled = true;
                    }

                    if (sql.guarda[5].ToString() == cod_aula)
                    {
                        cb4aulasex.Enabled = true;
                        cb4aulasex.Checked = true;
                    }
                    else if (sql.guarda[5].ToString() == "1")
                    {
                        cb4aulasex.Enabled = true;
                    }

                    if (sql.guarda[6].ToString() == cod_aula)
                    {
                        cb5aulasex.Enabled = true;
                        cb5aulasex.Checked = true;
                    }
                    else if (sql.guarda[6].ToString() == "1")
                    {
                        cb5aulasex.Enabled = true;
                    }

                    if (sql.guarda[7].ToString() == cod_aula)
                    {
                        cb6aulasex.Enabled = true;
                        cb6aulasex.Checked = true;
                    }
                    else if (sql.guarda[7].ToString() == "1")
                    {
                        cb6aulasex.Enabled = true;
                    }

                    if (sql.guarda[8].ToString() == cod_aula)
                    {
                        cb7aulasex.Enabled = true;
                        cb7aulasex.Checked = true;
                    }
                    else if (sql.guarda[8].ToString() == "1")
                    {
                        cb7aulasex.Enabled = true;
                    }

                    if (sql.guarda[9].ToString() == cod_aula)
                    {
                        cb8aulasex.Enabled = true;
                        cb8aulasex.Checked = true;
                    }
                    else if (sql.guarda[9].ToString() == "1")
                    {
                        cb8aulasex.Enabled = true;
                    }

                    if (sql.guarda[10].ToString() == cod_aula)
                    {
                        cb9aulasex.Enabled = true;
                        cb9aulasex.Checked = true;
                    }
                    else if (sql.guarda[10].ToString() == "1")
                    {
                        cb9aulasex.Enabled = true;
                    }
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();
                #endregion

                #region att a tabela para a disp do prof
                //pesquisa o horario de segunda do prof
                sql.pesquisar("select * from tab_dia where dia_semana = 2 and fk_prof = '" + cod_prof + "'");

                while (sql.guarda.Read())
                {
                    //indisponibiliza as aulas q o prof estiver indisponível
                    if (sql.guarda[2].ToString() != "1" && sql.guarda[2].ToString() != cod_aula)
                    {
                        cb1aulaseg.Enabled = false;
                    }

                    if (sql.guarda[3].ToString() != "1" && sql.guarda[3].ToString() != cod_aula)
                    {
                        cb2aulaseg.Enabled = false;
                    }

                    if (sql.guarda[4].ToString() != "1" && sql.guarda[4].ToString() != cod_aula)
                    {
                        cb3aulaseg.Enabled = false;
                    }

                    if (sql.guarda[5].ToString() != "1" && sql.guarda[5].ToString() != cod_aula)
                    {
                        cb4aulaseg.Enabled = false;
                    }

                    if (sql.guarda[6].ToString() != "1" && sql.guarda[6].ToString() != cod_aula)
                    {
                        cb5aulaseg.Enabled = false;
                    }

                    if (sql.guarda[7].ToString() != "1" && sql.guarda[7].ToString() != cod_aula)
                    {
                        cb6aulaseg.Enabled = false;
                    }

                    if (sql.guarda[8].ToString() != "1" && sql.guarda[8].ToString() != cod_aula)
                    {
                        cb7aulaseg.Enabled = false;
                    }

                    if (sql.guarda[9].ToString() != "1" && sql.guarda[9].ToString() != cod_aula)
                    {
                        cb8aulaseg.Enabled = false;
                    }

                    if (sql.guarda[10].ToString() != "1" && sql.guarda[10].ToString() != cod_aula)
                    {
                        cb9aulaseg.Enabled = false;
                    }
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //pesquisa o horario de terça do prof
                sql.pesquisar("select * from tab_dia where dia_semana = 3 and fk_prof = '" + cod_prof + "'");

                while (sql.guarda.Read())
                {
                    //indisponibiliza as aulas q o prof estiver indisponível
                    if (sql.guarda[2].ToString() != "1" && sql.guarda[2].ToString() != cod_aula)
                    {
                        cb1aulater.Enabled = false;
                    }

                    if (sql.guarda[3].ToString() != "1" && sql.guarda[3].ToString() != cod_aula)
                    {
                        cb2aulater.Enabled = false;
                    }

                    if (sql.guarda[4].ToString() != "1" && sql.guarda[4].ToString() != cod_aula)
                    {
                        cb3aulater.Enabled = false;
                    }

                    if (sql.guarda[5].ToString() != "1" && sql.guarda[5].ToString() != cod_aula)
                    {
                        cb4aulater.Enabled = false;
                    }

                    if (sql.guarda[6].ToString() != "1" && sql.guarda[6].ToString() != cod_aula)
                    {
                        cb5aulater.Enabled = false;
                    }

                    if (sql.guarda[7].ToString() != "1" && sql.guarda[7].ToString() != cod_aula)
                    {
                        cb6aulater.Enabled = false;
                    }

                    if (sql.guarda[8].ToString() != "1" && sql.guarda[8].ToString() != cod_aula)
                    {
                        cb7aulater.Enabled = false;
                    }

                    if (sql.guarda[9].ToString() != "1" && sql.guarda[9].ToString() != cod_aula)
                    {
                        cb8aulater.Enabled = false;
                    }

                    if (sql.guarda[10].ToString() != "1" && sql.guarda[10].ToString() != cod_aula)
                    {
                        cb9aulater.Enabled = false;
                    }
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //pesquisa o horario de quarta do prof
                sql.pesquisar("select * from tab_dia where dia_semana = 4 and fk_prof = '" + cod_prof + "'");

                while (sql.guarda.Read())
                {
                    //indisponibiliza as aulas q o prof estiver indisponível
                    if (sql.guarda[2].ToString() != "1" && sql.guarda[2].ToString() != cod_aula)
                    {
                        cb1aulaquar.Enabled = false;
                    }

                    if (sql.guarda[3].ToString() != "1" && sql.guarda[3].ToString() != cod_aula)
                    {
                        cb2aulaquar.Enabled = false;
                    }

                    if (sql.guarda[4].ToString() != "1" && sql.guarda[4].ToString() != cod_aula)
                    {
                        cb3aulaquar.Enabled = false;
                    }

                    if (sql.guarda[5].ToString() != "1" && sql.guarda[5].ToString() != cod_aula)
                    {
                        cb4aulaquar.Enabled = false;
                    }

                    if (sql.guarda[6].ToString() != "1" && sql.guarda[6].ToString() != cod_aula)
                    {
                        cb5aulaquar.Enabled = false;
                    }

                    if (sql.guarda[7].ToString() != "1" && sql.guarda[7].ToString() != cod_aula)
                    {
                        cb6aulaquar.Enabled = false;
                    }

                    if (sql.guarda[8].ToString() != "1" && sql.guarda[8].ToString() != cod_aula)
                    {
                        cb7aulaquar.Enabled = false;
                    }

                    if (sql.guarda[9].ToString() != "1" && sql.guarda[9].ToString() != cod_aula)
                    {
                        cb8aulaquar.Enabled = false;
                    }

                    if (sql.guarda[10].ToString() != "1" && sql.guarda[10].ToString() != cod_aula)
                    {
                        cb9aulaquar.Enabled = false;
                    }
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //pesquisa o horario de quinta do prof
                sql.pesquisar("select * from tab_dia where dia_semana = 5 and fk_prof = '" + cod_prof + "'");

                while (sql.guarda.Read())
                {
                    //indisponibiliza as aulas q o prof estiver indisponível
                    if (sql.guarda[2].ToString() != "1" && sql.guarda[2].ToString() != cod_aula)
                    {
                        cb1aulaquin.Enabled = false;
                    }

                    if (sql.guarda[3].ToString() != "1" && sql.guarda[3].ToString() != cod_aula)
                    {
                        cb2aulaquin.Enabled = false;
                    }

                    if (sql.guarda[4].ToString() != "1" && sql.guarda[4].ToString() != cod_aula)
                    {
                        cb3aulaquin.Enabled = false;
                    }

                    if (sql.guarda[5].ToString() != "1" && sql.guarda[5].ToString() != cod_aula)
                    {
                        cb4aulaquin.Enabled = false;
                    }

                    if (sql.guarda[6].ToString() != "1" && sql.guarda[6].ToString() != cod_aula)
                    {
                        cb5aulaquin.Enabled = false;
                    }

                    if (sql.guarda[7].ToString() != "1" && sql.guarda[7].ToString() != cod_aula)
                    {
                        cb6aulaquin.Enabled = false;
                    }

                    if (sql.guarda[8].ToString() != "1" && sql.guarda[8].ToString() != cod_aula)
                    {
                        cb7aulaquin.Enabled = false;
                    }

                    if (sql.guarda[9].ToString() != "1" && sql.guarda[9].ToString() != cod_aula)
                    {
                        cb8aulaquin.Enabled = false;
                    }

                    if (sql.guarda[10].ToString() != "1" && sql.guarda[10].ToString() != cod_aula)
                    {
                        cb9aulaquin.Enabled = false;
                    }
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //pesquisa o horario de sexta do prof
                sql.pesquisar("select * from tab_dia where dia_semana = 6 and fk_prof = '" + cod_prof + "'");

                while (sql.guarda.Read())
                {
                    //indisponibiliza as aulas q o prof estiver indisponível
                    if (sql.guarda[2].ToString() != "1" && sql.guarda[2].ToString() != cod_aula)
                    {
                        cb1aulasex.Enabled = false;
                    }

                    if (sql.guarda[3].ToString() != "1" && sql.guarda[3].ToString() != cod_aula)
                    {
                        cb2aulasex.Enabled = false;
                    }

                    if (sql.guarda[4].ToString() != "1" && sql.guarda[4].ToString() != cod_aula)
                    {
                        cb3aulasex.Enabled = false;
                    }

                    if (sql.guarda[5].ToString() != "1" && sql.guarda[5].ToString() != cod_aula)
                    {
                        cb4aulasex.Enabled = false;
                    }

                    if (sql.guarda[6].ToString() != "1" && sql.guarda[6].ToString() != cod_aula)
                    {
                        cb5aulasex.Enabled = false;
                    }

                    if (sql.guarda[7].ToString() != "1" && sql.guarda[7].ToString() != cod_aula)
                    {
                        cb6aulasex.Enabled = false;
                    }

                    if (sql.guarda[8].ToString() != "1" && sql.guarda[8].ToString() != cod_aula)
                    {
                        cb7aulasex.Enabled = false;
                    }

                    if (sql.guarda[9].ToString() != "1" && sql.guarda[9].ToString() != cod_aula)
                    {
                        cb8aulasex.Enabled = false;
                    }

                    if (sql.guarda[10].ToString() != "1" && sql.guarda[10].ToString() != cod_aula)
                    {
                        cb9aulasex.Enabled = false;
                    }
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();
                #endregion
            }
            else
            {
                cod_turma = null;
                txtsala.Text = "";
                txtsala.Enabled = false;
            }
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {

        }

        #region metodos das checkboxs
        private void cb1aulaseg_CheckedChanged(object sender, EventArgs e)
        {
            if (cb1aulaseg.Checked == true)
            {
                status = mudaHoraAula("2", "730", true);
            }
            else
            {
                status = mudaHoraAula("2", "730", false);
            }
            if(cb1aulaseg.Checked != status)
            {
                cb1aulaseg.Checked = status;
            }
        }

        private void cb2aulaseg_CheckedChanged(object sender, EventArgs e)
        {
            if (cb2aulaseg.Checked == true)
            {
                status = mudaHoraAula("2", "820", true);
            }
            else
            {
                status = mudaHoraAula("2", "820", false);
            }
            if (cb2aulaseg.Checked != status)
            {
                cb2aulaseg.Checked = status;
            }
        }

        private void cb3aulaseg_CheckedChanged(object sender, EventArgs e)
        {
            if (cb3aulaseg.Checked == true)
            {
                status = mudaHoraAula("2", "910", true);
            }
            else
            {
                status = mudaHoraAula("2", "910", false);
            }
            if (cb3aulaseg.Checked != status)
            {
                cb3aulaseg.Checked = status;
            }
        }

        private void cb4aulaseg_CheckedChanged(object sender, EventArgs e)
        {
            if (cb4aulaseg.Checked == true)
            {
                status = mudaHoraAula("2", "1020", true);
            }
            else
            {
                status = mudaHoraAula("2", "1020", false);
            }
            if (cb4aulaseg.Checked != status)
            {
                cb4aulaseg.Checked = status;
            }
        }

        private void cb5aulaseg_CheckedChanged(object sender, EventArgs e)
        {
            if (cb5aulaseg.Checked == true)
            {
                status = mudaHoraAula("2", "1110", true);
            }
            else
            {
                status = mudaHoraAula("2", "1110", false);
            }
            if (cb5aulaseg.Checked != status)
            {
                cb5aulaseg.Checked = status;
            }
        }

        private void cb6aulaseg_CheckedChanged(object sender, EventArgs e)
        {
            if (cb6aulaseg.Checked == true)
            {
                status = mudaHoraAula("2", "1330", true);
            }
            else
            {
                status = mudaHoraAula("2", "1330", false);
            }
            if (cb6aulaseg.Checked != status)
            {
                cb6aulaseg.Checked = status;
            }
        }

        private void cb7aulaseg_CheckedChanged(object sender, EventArgs e)
        {
            if (cb7aulaseg.Checked == true)
            {
                status = mudaHoraAula("2", "1420", true);
            }
            else
            {
                status = mudaHoraAula("2", "1420", false);
            }
            if (cb7aulaseg.Checked != status)
            {
                cb7aulaseg.Checked = status;
            }
        }

        private void cb8aulaseg_CheckedChanged(object sender, EventArgs e)
        {
            if (cb8aulaseg.Checked == true)
            {
                status =  mudaHoraAula("2", "1530", true);
            }
            else
            {
                status = mudaHoraAula("2", "1530", false);
            }
            if (cb8aulaseg.Checked != status)
            {
                cb8aulaseg.Checked = status;
            }
        }

        private void cb9aulaseg_CheckedChanged(object sender, EventArgs e)
        {
            if (cb9aulaseg.Checked == true)
            {
                status = mudaHoraAula("2", "1620", true);
            }
            else
            {
                status = mudaHoraAula("2", "1620", false);
            }
            if (cb9aulaseg.Checked != status)
            {
                cb9aulaseg.Checked = status;
            }
        }

        private void cb1aulater_CheckedChanged(object sender, EventArgs e)
        {
            if (cb1aulater.Checked == true)
            {
                status = mudaHoraAula("3", "730", true);
            }
            else
            {
                status = mudaHoraAula("3", "730", false);
            }
            if (cb1aulater.Checked != status)
            {
                cb1aulater.Checked = status;
            }
        }

        private void cb2aulater_CheckedChanged(object sender, EventArgs e)
        {
            if (cb2aulater.Checked == true)
            {
                status = mudaHoraAula("3", "820", true);
            }
            else
            {
                status = mudaHoraAula("3", "820", false);
            }
            if (cb2aulater.Checked != status)
            {
                cb2aulater.Checked = status;
            }
        }

        private void cb3aulater_CheckedChanged(object sender, EventArgs e)
        {
            if (cb3aulater.Checked == true)
            {
                status = mudaHoraAula("3", "910", true);
            }
            else
            {
                status = mudaHoraAula("3", "910", false);
            }
            if (cb3aulater.Checked != status)
            {
                cb3aulater.Checked = status;
            }
        }

        private void cb4aulater_CheckedChanged(object sender, EventArgs e)
        {
            if (cb4aulater.Checked == true)
            {
                status = mudaHoraAula("3", "1020", true);
            }
            else
            {
                status = mudaHoraAula("3", "1020", false);
            }
            if (cb4aulater.Checked != status)
            {
                cb4aulater.Checked = status;
            }
        }

        private void cb5aulater_CheckedChanged(object sender, EventArgs e)
        {
            if (cb5aulater.Checked == true)
            {
                status = mudaHoraAula("3", "1110", true);
            }
            else
            {
                status = mudaHoraAula("3", "1110", false);
            }
            if (cb5aulater.Checked != status)
            {
                cb5aulater.Checked = status;
            }
        }

        private void cb6aulater_CheckedChanged(object sender, EventArgs e)
        {
            if (cb6aulater.Checked == true)
            {
                status = mudaHoraAula("3", "1330", true);
            }
            else
            {
                status = mudaHoraAula("3", "1330", false);
            }
            if (cb6aulater.Checked != status)
            {
                cb6aulater.Checked = status;
            }
        }

        private void cb7aulater_CheckedChanged(object sender, EventArgs e)
        {
            if (cb7aulater.Checked == true)
            {
                status = mudaHoraAula("3", "1420", true);
            }
            else
            {
                status = mudaHoraAula("3", "1420", false);
            }
            if (cb7aulater.Checked != status)
            {
                cb7aulater.Checked = status;
            }
        }

        private void cb8aulater_CheckedChanged(object sender, EventArgs e)
        {
            if (cb8aulater.Checked == true)
            {
                status = mudaHoraAula("3", "1510", true);
            }
            else
            {
                status = mudaHoraAula("3", "1510", false);
            }
            if (cb8aulater.Checked != status)
            {
                cb8aulater.Checked = status;
            }
        }

        private void cb9aulater_CheckedChanged(object sender, EventArgs e)
        {
            if (cb9aulater.Checked == true)
            {
                status = mudaHoraAula("3", "1620", true);
            }
            else
            {
                status = mudaHoraAula("3", "1620", false);
            }
            if (cb9aulater.Checked != status)
            {
                cb9aulater.Checked = status;
            }
        }

        private void cb1aulaquar_CheckedChanged(object sender, EventArgs e)
        {
            if (cb1aulaquar.Checked == true)
            {
                status = mudaHoraAula("4", "730", true);
            }
            else
            {
                status = mudaHoraAula("4", "730", false);
            }
            if (cb1aulaquar.Checked != status)
            {
                cb1aulaquar.Checked = status;
            }
        }

        private void cb2aulaquar_CheckedChanged(object sender, EventArgs e)
        {
            if (cb2aulaquar.Checked == true)
            {
                status = mudaHoraAula("4", "820", true);
            }
            else
            {
                status = mudaHoraAula("4", "820", false);
            }
            if (cb2aulaquar.Checked != status)
            {
                cb2aulaquar.Checked = status;
            }
        }

        private void cb3aulaquar_CheckedChanged(object sender, EventArgs e)
        {
            if (cb3aulaquar.Checked == true)
            {
                status = mudaHoraAula("4", "910", true);
            }
            else
            {
                status = mudaHoraAula("4", "910", false);
            }
            if (cb3aulaquar.Checked != status)
            {
                cb3aulaquar.Checked = status;
            }
        }

        private void cb4aulaquar_CheckedChanged(object sender, EventArgs e)
        {
            if (cb4aulaquar.Checked == true)
            {
                status = mudaHoraAula("4", "1020", true);
            }
            else
            {
                status = mudaHoraAula("4", "1020", false);
            }
            if (cb4aulaquar.Checked != status)
            {
                cb4aulaquar.Checked = status;
            }
        }

        private void cb5aulaquar_CheckedChanged(object sender, EventArgs e)
        {
            if (cb5aulaquar.Checked == true)
            {
                status = mudaHoraAula("4", "1110", true);
            }
            else
            {
                status = mudaHoraAula("4", "1110", false);
            }
            if (cb5aulaquar.Checked != status)
            {
                cb5aulaquar.Checked = status;
            }
        }

        private void cb6aulaquar_CheckedChanged(object sender, EventArgs e)
        {
            if (cb6aulaquar.Checked == true)
            {
                status = mudaHoraAula("4", "1330", true);
            }
            else
            {
                status = mudaHoraAula("4", "1330", false);
            }
            if (cb6aulaquar.Checked != status)
            {
                cb6aulaquar.Checked = status;
            }
        }

        private void cb7aulaquar_CheckedChanged(object sender, EventArgs e)
        {
            if (cb7aulaquar.Checked == true)
            {
                status = mudaHoraAula("4", "1420", true);
            }
            else
            {
                status = mudaHoraAula("4", "1420", false);
            }
            if (cb7aulaquar.Checked != status)
            {
                cb7aulaquar.Checked = status;
            }
        }

        private void cb8aulaquar_CheckedChanged(object sender, EventArgs e)
        {
            if (cb8aulaquar.Checked == true)
            {
                status = mudaHoraAula("4", "1530", true);
            }
            else
            {
                status = mudaHoraAula("4", "1530", false);
            }
            if (cb8aulaquar.Checked != status)
            {
                cb8aulaquar.Checked = status;
            }
        }

        private void cb9aulaquar_CheckedChanged(object sender, EventArgs e)
        {
            if (cb9aulaquar.Checked == true)
            {
                status = mudaHoraAula("4", "1620", true);
            }
            else
            {
                status = mudaHoraAula("4", "1620", false);
            }
            if (cb9aulaquar.Checked != status)
            {
                cb9aulaquar.Checked = status;
            }
        }

        private void cb1aulaquin_CheckedChanged(object sender, EventArgs e)
        {
            if (cb1aulaquin.Checked == true)
            {
                status = mudaHoraAula("5", "730", true);
            }
            else
            {
                status = mudaHoraAula("5", "730", false);
            }
            if (cb1aulaquin.Checked != status)
            {
                cb1aulaquin.Checked = status;
            }
        }

        private void cb2aulaquin_CheckedChanged(object sender, EventArgs e)
        {
            if (cb2aulaquin.Checked == true)
            {
                status = mudaHoraAula("5", "820", true);
            }
            else
            {
                status = mudaHoraAula("5", "820", false);
            }
            if (cb2aulaquin.Checked != status)
            {
                cb2aulaquin.Checked = status;
            }
        }

        private void cb3aulaquin_CheckedChanged(object sender, EventArgs e)
        {
            if (cb3aulaquin.Checked == true)
            {
                status = mudaHoraAula("5", "910", true);
            }
            else
            {
                status = mudaHoraAula("5", "910", false);
            }
            if (cb3aulaquin.Checked != status)
            {
                cb3aulaquin.Checked = status;
            }
        }

        private void cb4aulaquin_CheckedChanged(object sender, EventArgs e)
        {
            if (cb4aulaquin.Checked == true)
            {
                status = mudaHoraAula("5", "1020", true);
            }
            else
            {
                status = mudaHoraAula("5", "1020", false);
            }
            if (cb4aulaquin.Checked != status)
            {
                cb4aulaquin.Checked = status;
            }
        }

        private void cb5aulaquin_CheckedChanged(object sender, EventArgs e)
        {
            if (cb5aulaquin.Checked == true)
            {
                status = mudaHoraAula("5", "1110", true);
            }
            else
            {
                status = mudaHoraAula("5", "1110", false);
            }
            if (cb5aulaquin.Checked != status)
            {
                cb5aulaquin.Checked = status;
            }
        }

        private void cb6aulaquin_CheckedChanged(object sender, EventArgs e)
        {
            if (cb6aulaquin.Checked == true)
            {
                status = mudaHoraAula("5", "1330", true);
            }
            else
            {
                status = mudaHoraAula("5", "1330", false);
            }
            if (cb6aulaquin.Checked != status)
            {
                cb6aulaquin.Checked = status;
            }
        }

        private void cb7aulaquin_CheckedChanged(object sender, EventArgs e)
        {
            if (cb7aulaquin.Checked == true)
            {
                status = mudaHoraAula("5", "1420", true);
            }
            else
            {
                status = mudaHoraAula("5", "1420", false);
            }
            if (cb7aulaquin.Checked != status)
            {
                cb7aulaquin.Checked = status;
            }
        }

        private void cb8aulaquin_CheckedChanged(object sender, EventArgs e)
        {
            if (cb8aulaquin.Checked == true)
            {
                status = mudaHoraAula("5", "1510", true);
            }
            else
            {
                status = mudaHoraAula("5", "1510", false);
            }
            if (cb8aulaquin.Checked != status)
            {
                cb8aulaquin.Checked = status;
            }
        }

        private void cb9aulaquin_CheckedChanged(object sender, EventArgs e)
        {
            if (cb9aulaquin.Checked == true)
            {
                status = mudaHoraAula("5", "1620", true);
            }
            else
            {
                status = mudaHoraAula("5", "1620", false);
            }
            if (cb9aulaquin.Checked != status)
            {
                cb9aulaquin.Checked = status;
            }
        }

        private void cb1aulasex_CheckedChanged(object sender, EventArgs e)
        {
            if (cb1aulasex.Checked == true)
            {
                status = mudaHoraAula("6", "730", true);
            }
            else
            {
                status = mudaHoraAula("6", "730", false);
            }
            if (cb1aulasex.Checked != status)
            {
                cb1aulasex.Checked = status;
            }
        }

        private void cb2aulasex_CheckedChanged(object sender, EventArgs e)
        {
            if (cb2aulasex.Checked == true)
            {
                status = mudaHoraAula("6", "820", true);
            }
            else
            {
                status = mudaHoraAula("6", "820", false);
            }
            if (cb2aulasex.Checked != status)
            {
                cb2aulasex.Checked = status;
            }
        }

        private void cb3aulasex_CheckedChanged(object sender, EventArgs e)
        {
            if (cb3aulasex.Checked == true)
            {
                status = mudaHoraAula("6", "910", true);
            }
            else
            {
                status = mudaHoraAula("6", "910", false);
            }
            if (cb3aulasex.Checked != status)
            {
                cb3aulasex.Checked = status;
            }
        }

        private void cb4aulasex_CheckedChanged(object sender, EventArgs e)
        {
            if (cb4aulasex.Checked == true)
            {
                status = mudaHoraAula("6", "1020", true);
            }
            else
            {
                status = mudaHoraAula("6", "1020", false);
            }
            if (cb4aulasex.Checked != status)
            {
                cb4aulasex.Checked = status;
            }
        }

        private void cb5aulasex_CheckedChanged(object sender, EventArgs e)
        {
            if (cb5aulasex.Checked == true)
            {
                status = mudaHoraAula("6", "1110", true);
            }
            else
            {
                status = mudaHoraAula("6", "1110", false);
            }
            if (cb5aulasex.Checked != status)
            {
                cb5aulasex.Checked = status;
            }
        }

        private void cb6aulasex_CheckedChanged(object sender, EventArgs e)
        {
            if (cb6aulasex.Checked == true)
            {
                status = mudaHoraAula("6", "1330", true);
            }
            else
            {
                status = mudaHoraAula("6", "1330", false);
            }
        }

        private void cb7aulasex_CheckedChanged(object sender, EventArgs e)
        {
            if (cb7aulasex.Checked == true)
            {
                status = mudaHoraAula("6", "1420", true);
            }
            else
            {
                status = mudaHoraAula("6", "1420", false);
            }
            if (cb7aulasex.Checked != status)
            {
                cb7aulasex.Checked = status;
            }
        }

        private void cb8aulasex_CheckedChanged(object sender, EventArgs e)
        {
            if (cb8aulasex.Checked == true)
            {
                status = mudaHoraAula("6", "1530", true);
            }
            else
            {
                status = mudaHoraAula("6", "1530", false);
            }
            if (cb8aulasex.Checked != status)
            {
                cb8aulasex.Checked = status;
            }
        }

        private void cb9aulasex_CheckedChanged(object sender, EventArgs e)
        {
            if (cb9aulasex.Checked == true)
            {
                status = mudaHoraAula("6", "1620", true);
            }
            else
            {
                status = mudaHoraAula("6", "1620", false);
            }
            if (cb9aulasex.Checked != status)
            {
                cb9aulasex.Checked = status;
            }
        }
        #endregion

        private void PreencherHorarios_Load(object sender, EventArgs e)
        {
            //limpa a cb
            cbprof.Items.Clear();

            //preencher lista de matérias disponíveis
            sql.pesquisar("select * from tab_prof where id_prof != '666' AND id_prof != '999' AND id_prof != '0'");

            //escreve um por um na combobox
            while (sql.guarda.Read())
            {
                cbprof.Items.Add(sql.guarda[0].ToString() + " - " + sql.guarda[1].ToString() + " - Pref: " + sql.guarda[4].ToString());
            }

            //fecha as conexões
            sql.guarda.Close();
            sql.olecon.Close();

            //zera as variaveis
            aulasrestantes = "0";
            cod_prof = null;
            cod_turma = null;
            cod_materia = null;
            lblaulasrest.Text = aulasrestantes;
            cbprof.Focus();
        }

        private void pbcancelar_Click(object sender, EventArgs e)
        {
            limpaCB();

            //limpa e desativa as combobox
            cbturma.Items.Clear();
            cbturma.Text = "";
            cbturma.SelectedIndex = -1;
            cbturma.Enabled = false;

            cbmateria.Items.Clear();
            cbmateria.Text = "";
            cbmateria.SelectedIndex = -1;
            cbmateria.Enabled = false;

            PreencherHorarios_Load(sender, e);

            txtsala.Text = "";
            txtsala.Enabled = false;
        }

        private void pbconfirmar_Click(object sender, EventArgs e)
        {
            //testa se todos os passos foram seguidos
            if (cbturma.SelectedIndex != -1 || cbturma.SelectedIndex != -1 || cbprof.SelectedIndex != -1)
            {
                if (lblaulasrest.Text != "0")
                {
                    //confirma se o usuário deseja deixar aulas faltando
                    DialogResult YN = MessageBox.Show("Restam " + lblaulasrest.Text + " aula(s). \nDeseja continuar?", "*** Aulas Restantes ***", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (YN != DialogResult.Yes)
                    {
                        return;
                    }
                }
                if (txtsala.Text == "")
                {
                    //confirma se o usuário deseja deixar o número da sala em branco
                    DialogResult YN = MessageBox.Show("O campo de número da sala está vazio. \nDeseja continuar?", "*** Sem Sala Especificada ***", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (YN != DialogResult.Yes)
                    {
                        return;
                    }
                }

                //faz a atualização da sala da aula aula
                sql.executarcomando("UPDATE tab_aula SET num_sala = '" + txtsala.Text + "' WHERE id_aula = " + cod_aula, " update");

                //limpa todas as aulas desse prof com essa matéria e sala
                sql.executarcomando("UPDATE `tab_dia` SET `730` = 1 WHERE `tab_dia`.`730` = " + cod_aula + ";", " atualização");
                sql.executarcomando("UPDATE `tab_dia` SET `820` = 1 WHERE `tab_dia`.`820` = " + cod_aula + ";", " atualização");
                sql.executarcomando("UPDATE `tab_dia` SET `910` = 1 WHERE `tab_dia`.`910` = " + cod_aula + ";", " atualização");
                sql.executarcomando("UPDATE `tab_dia` SET `1020` = 1 WHERE `tab_dia`.`1020` = " + cod_aula + ";", " atualização");
                sql.executarcomando("UPDATE `tab_dia` SET `1110` = 1 WHERE `tab_dia`.`1110` = " + cod_aula + ";", " atualização");
                sql.executarcomando("UPDATE `tab_dia` SET `1330` = 1 WHERE `tab_dia`.`1330` = " + cod_aula + ";", " atualização");
                sql.executarcomando("UPDATE `tab_dia` SET `1420` = 1 WHERE `tab_dia`.`1420` = " + cod_aula + ";", " atualização");
                sql.executarcomando("UPDATE `tab_dia` SET `1530` = 1 WHERE `tab_dia`.`1530` = " + cod_aula + ";", " atualização");
                sql.executarcomando("UPDATE `tab_dia` SET `1620` = 1 WHERE `tab_dia`.`1620` = " + cod_aula + ";", " atualização");

                //faz a inclusão dessas aulas no horário do professor e da turma
                for (int i = 0; i < aulas.Length / 2; i++)
                {
                    //confere se a aula não é nula
                    if (aulas[i, 1] != null)
                    {
                        sql.executarcomando("UPDATE `tab_dia` SET `" + aulas[i, 1] + "` = " + cod_aula + " WHERE `tab_dia`.`fk_prof` = " + cod_prof + " AND `tab_dia`.`dia_semana` = " + aulas[i, 0] + ";", " atualização");
                        sql.executarcomando("UPDATE `tab_dia` SET `" + aulas[i, 1] + "` = " + cod_aula + " WHERE `tab_dia`.`fk_classe` = " + cod_turma + " AND `tab_dia`.`dia_semana` = " + aulas[i, 0] + ";", " atualização");
                    }
                }

                //Da o feedback ao usuário
                MessageBox.Show("Horário salvo com sucesso!", "*** Horário Salvo ***", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                //Pede ao usuário para seguir todos os passos
                MessageBox.Show("Selecione um professor, uma matéria e uma turma!", "*** ERRO ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cbprof_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cbprof.SelectedIndex != -1)
            {
                //ativa cbmateria e a limpa
                cbmateria.Enabled = true;
                cbmateria.Items.Clear();
                cbmateria.Text = "";
                cbmateria.SelectedIndex = -1;

                cbturma.Enabled = false;
                cbturma.Items.Clear();
                cbturma.Text = "";
                cbturma.SelectedIndex = -1;

                txtsala.Text = "";
                txtsala.Enabled = false;

                limpaCB();

                //seleciona o código do prof
                string materia = cbprof.Text;
                string codigo = "";
                string letra = "";
                int index = 0;
                while (letra != " ")
                {
                    codigo += letra;
                    letra = materia.Substring(index, 1);
                    index++;
                }
                cod_prof = codigo;

                //pesquisa todas as matérias relacionadas ao prof
                sql.pesquisar("SELECT * FROM relacao_prof_materia_permanente WHERE fk_prof = " + cod_prof);

                //cria variável para contagem
                int k = 0;

                //conta
                while (sql.guarda.Read())
                {
                    k++;
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //cria vetor para armazenar código dessas matérias
                string[] codigos = new string[k];

                //pesquisa todas as matérias relacionadas ao curso
                sql.pesquisar("SELECT * FROM relacao_prof_materia_permanente WHERE fk_prof = " + cod_prof);

                //zera variavel para usar como auxiliar
                k = 0;

                //armazena codigo por codigo
                while (sql.guarda.Read())
                {
                    codigos[k] = sql.guarda[0].ToString();
                    k++;
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //cria laço de repetição para colocalas na cbmateria
                for (k = 0; k < codigos.Length; k++)
                {
                    //pesquisa a matéria
                    sql.pesquisar("SELECT * FROM tab_materia WHERE id_materia = " + codigos[k]);

                    //se achar
                    if (sql.guarda.Read())
                    {
                        //escreve na combobox
                        cbmateria.Items.Add(sql.guarda[0].ToString() + " - " + sql.guarda[1].ToString() + " - " + sql.guarda[3].ToString() + " aula(s)/semana");
                    }

                    //fecha as conexões
                    sql.guarda.Close();
                    sql.olecon.Close();
                }
            }
            else
            {
                cod_prof = null;
                btnCancelar_Click(sender, e);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {

        }

        private bool mudaHoraAula(string dia, string aula, Boolean status)
        {
            if(status == false)
            {
                for(int i = 0; i < aulas.Length/2; i++)
                {
                    if(aulas[i, 0] == dia && aulas[i, 1] == aula)
                    {
                        aulas[i, 0] = null;
                        aulas[i, 1] = null;
                        aulasrestantes = (Convert.ToInt32(aulasrestantes)+1).ToString();
                        lblaulasrest.Text = aulasrestantes;
                        i = aulas.Length;
                    }
                }
                return false;
            }
            else if(status == true && (Convert.ToInt32(aulasrestantes)-1) >= 0)
            {
                for (int i = 0; i < aulas.Length/2; i++)
                {
                    if (aulas[i, 0] == null && aulas[i, 1] == null)
                    {
                        aulas[i, 0] = dia;
                        aulas[i, 1] = aula;
                        aulasrestantes = (Convert.ToInt32(aulasrestantes) - 1).ToString();
                        lblaulasrest.Text = aulasrestantes;
                        i = aulas.Length;
                    }
                }
                return true;
            }
            else
            {
                MessageBox.Show("O total de aulas já foi atingido!", "*** ERRO ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void limpaCB()
        {
            //limpa e desativa as chackbox
            #region limpando
            cb1aulaseg.Checked = false;
            cb1aulater.Checked = false;
            cb1aulaquar.Checked = false;
            cb1aulaquin.Checked = false;
            cb1aulasex.Checked = false;

            cb2aulaseg.Checked = false;
            cb2aulater.Checked = false;
            cb2aulaquar.Checked = false;
            cb2aulaquin.Checked = false;
            cb2aulasex.Checked = false;

            cb3aulaseg.Checked = false;
            cb3aulater.Checked = false;
            cb3aulaquar.Checked = false;
            cb3aulaquin.Checked = false;
            cb3aulasex.Checked = false;

            cb4aulaseg.Checked = false;
            cb4aulater.Checked = false;
            cb4aulaquar.Checked = false;
            cb4aulaquin.Checked = false;
            cb4aulasex.Checked = false;

            cb5aulaseg.Checked = false;
            cb5aulater.Checked = false;
            cb5aulaquar.Checked = false;
            cb5aulaquin.Checked = false;
            cb5aulasex.Checked = false;

            cb6aulaseg.Checked = false;
            cb6aulater.Checked = false;
            cb6aulaquar.Checked = false;
            cb6aulaquin.Checked = false;
            cb6aulasex.Checked = false;

            cb7aulaseg.Checked = false;
            cb7aulater.Checked = false;
            cb7aulaquar.Checked = false;
            cb7aulaquin.Checked = false;
            cb7aulasex.Checked = false;

            cb8aulaseg.Checked = false;
            cb8aulater.Checked = false;
            cb8aulaquar.Checked = false;
            cb8aulaquin.Checked = false;
            cb8aulasex.Checked = false;

            cb9aulaseg.Checked = false;
            cb9aulater.Checked = false;
            cb9aulaquar.Checked = false;
            cb9aulaquin.Checked = false;
            cb9aulasex.Checked = false;
            #endregion
            #region desativando
            cb1aulaseg.Enabled = false;
            cb1aulater.Enabled = false;
            cb1aulaquar.Enabled = false;
            cb1aulaquin.Enabled = false;
            cb1aulasex.Enabled = false;

            cb2aulaseg.Enabled = false;
            cb2aulater.Enabled = false;
            cb2aulaquar.Enabled = false;
            cb2aulaquin.Enabled = false;
            cb2aulasex.Enabled = false;

            cb3aulaseg.Enabled = false;
            cb3aulater.Enabled = false;
            cb3aulaquar.Enabled = false;
            cb3aulaquin.Enabled = false;
            cb3aulasex.Enabled = false;

            cb4aulaseg.Enabled = false;
            cb4aulater.Enabled = false;
            cb4aulaquar.Enabled = false;
            cb4aulaquin.Enabled = false;
            cb4aulasex.Enabled = false;

            cb5aulaseg.Enabled = false;
            cb5aulater.Enabled = false;
            cb5aulaquar.Enabled = false;
            cb5aulaquin.Enabled = false;
            cb5aulasex.Enabled = false;

            cb6aulaseg.Enabled = false;
            cb6aulater.Enabled = false;
            cb6aulaquar.Enabled = false;
            cb6aulaquin.Enabled = false;
            cb6aulasex.Enabled = false;

            cb7aulaseg.Enabled = false;
            cb7aulater.Enabled = false;
            cb7aulaquar.Enabled = false;
            cb7aulaquin.Enabled = false;
            cb7aulasex.Enabled = false;

            cb8aulaseg.Enabled = false;
            cb8aulater.Enabled = false;
            cb8aulaquar.Enabled = false;
            cb8aulaquin.Enabled = false;
            cb8aulasex.Enabled = false;

            cb9aulaseg.Enabled = false;
            cb9aulater.Enabled = false;
            cb9aulaquar.Enabled = false;
            cb9aulaquin.Enabled = false;
            cb9aulasex.Enabled = false;
            #endregion
        }
    }
}