﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace softwarecordenacao
{
    public partial class FrmHorarioTurmas : Form
    {
        public FrmHorarioTurmas()
        {
            InitializeComponent();
        }

        private void FrmHorarioTurmas_Load(object sender, EventArgs e)
        {
            //preencher lista de turmas disponíveis
            sql.pesquisar("select * from tab_classe where id_classe != '666' AND id_classe != '999'");

            //escreve um por um na combobox
            while (sql.guarda.Read())
            {
                comboBox1.Items.Add(sql.guarda[0].ToString() + " - " + sql.guarda[1].ToString() + sql.guarda[2].ToString());
            }

            //fecha as conexões
            sql.guarda.Close();
            sql.olecon.Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBox1.SelectedIndex != -1)
            {
                //seleciona o código da turma
                string materia = comboBox1.Text;
                string codigo = "";
                string letra = "";
                int index = 0;
                while (letra != " ")
                {
                    codigo += letra;
                    letra = materia.Substring(index, 1);
                    index++;
                }
                string cod_turma = codigo;


                string[] codigo_aulas = new string[9];
                string[] codigo_profs = new string[9];
                string[] codigo_materia = new string[9];
                string[] num_salas = new string[9];
                string[] profs = new string[9];
                string[] materias = new string[9];
                string[,] segunda = new string[3, 9];
                string[,] terca = new string[3, 9];
                string[,] quarta = new string[3, 9];
                string[,] quinta = new string[3, 9];
                string[,] sexta = new string[3, 9];

                DataTable dt = new DataTable();
                dt.Columns.Add(new DataColumn("Matéria - 2ª", typeof(string)));
                dt.Columns.Add(new DataColumn("Professor - 2ª", typeof(string)));
                dt.Columns.Add(new DataColumn("Sala - 2ª", typeof(string)));

                dt.Columns.Add(new DataColumn("Matéria - 3ª", typeof(string)));
                dt.Columns.Add(new DataColumn("Professor - 3ª", typeof(string)));
                dt.Columns.Add(new DataColumn("Sala - 3ª", typeof(string)));

                dt.Columns.Add(new DataColumn("Matéria - 4ª", typeof(string)));
                dt.Columns.Add(new DataColumn("Professor - 4ª", typeof(string)));
                dt.Columns.Add(new DataColumn("Sala - 4ª", typeof(string)));

                dt.Columns.Add(new DataColumn("Matéria - 5ª", typeof(string)));
                dt.Columns.Add(new DataColumn("Professor - 5ª", typeof(string)));
                dt.Columns.Add(new DataColumn("Sala - 5ª", typeof(string)));

                dt.Columns.Add(new DataColumn("Matéria - 6ª", typeof(string)));
                dt.Columns.Add(new DataColumn("Professor - 6ª", typeof(string)));
                dt.Columns.Add(new DataColumn("Sala - 6ª", typeof(string)));


                //loop para buscar todas as aulas de cada dia da semana
                for (int s = 2; s < 7; s++)
                {
                    //faz a busca
                    sql.pesquisar("select * from tab_dia where fk_classe = '" + cod_turma + "' and dia_semana = '" + s.ToString() + "';");

                    if (sql.guarda.Read()) //ler
                    {
                        codigo_aulas[0] = sql.guarda[2].ToString();
                        codigo_aulas[1] = sql.guarda[3].ToString();
                        codigo_aulas[2] = sql.guarda[4].ToString();
                        codigo_aulas[3] = sql.guarda[5].ToString();
                        codigo_aulas[4] = sql.guarda[6].ToString();
                        codigo_aulas[5] = sql.guarda[7].ToString();
                        codigo_aulas[6] = sql.guarda[8].ToString();
                        codigo_aulas[7] = sql.guarda[9].ToString();
                        codigo_aulas[8] = sql.guarda[10].ToString();
                    }

                    //fecha as conexões
                    sql.guarda.Close();
                    sql.olecon.Close();

                    //loop para buscar cada aula individualmente
                    for (int a = 0; a < 9; a++)
                    {
                        //faz a busca
                        sql.pesquisar("select * from tab_aula where id_aula = '" + codigo_aulas[a].ToString() + "';");

                        if (sql.guarda.Read())  //se achar
                        {
                            if (sql.guarda[0].ToString() == "1" || sql.guarda[0].ToString() == "2")
                            {
                                //armazena numero da sala
                                num_salas[a] = "-";

                                //armazena codigo da turma
                                codigo_profs[a] = "-";

                                //armazena codigo da materia
                                codigo_materia[a] = "-";
                            }
                            else
                            {
                                //armazena numero da sala
                                num_salas[a] = sql.guarda[1].ToString();

                                //armazena codigo do prof
                                codigo_profs[a] = sql.guarda[2].ToString();

                                //armazena codigo da materia
                                codigo_materia[a] = sql.guarda[4].ToString();
                            }
                        }

                        //fecha as conexões
                        sql.guarda.Close();
                        sql.olecon.Close();
                    }

                    //loop para buscar cada prof individualmente
                    for (int a = 0; a < 9; a++)
                    {
                        if (codigo_profs[a].ToString() != "-")
                        {
                            //faz a busca
                            sql.pesquisar("select * from tab_prof where id_prof = " + codigo_profs[a].ToString() + ";");

                            if (sql.guarda.Read())  //se achar
                            {
                                //armazena nome da sala
                                profs[a] = sql.guarda[2].ToString();
                            }

                            //fecha as conexões
                            sql.guarda.Close();
                            sql.olecon.Close();
                        }
                        else
                        {
                            //armazena nome da sala
                            profs[a] = "-";
                        }
                    }

                    //loop para buscar cada materia
                    for (int a = 0; a < 9; a++)
                    {
                        if (codigo_materia[a].ToString() != "-")
                        {
                            //faz a busca
                            sql.pesquisar("select * from tab_materia where id_materia = '" + codigo_materia[a].ToString() + "';");

                            if (sql.guarda.Read())  //se achar
                            {
                                //armazena nome da sala
                                materias[a] = sql.guarda[2].ToString();
                            }

                            //fecha as conexões
                            sql.guarda.Close();
                            sql.olecon.Close();
                        }
                        else
                        {
                            //armazena nome da sala
                            materias[a] = "-";
                        }
                    }

                    if (s == 2)
                    {
                        for (int lk = 0; lk < 9; lk++)
                        {
                            segunda[0, lk] = materias[lk];
                        }

                        for (int lk = 0; lk < 9; lk++)
                        {
                            segunda[1, lk] = profs[lk];
                        }

                        for (int lk = 0; lk < 9; lk++)
                        {
                            segunda[2, lk] = num_salas[lk];
                        }
                    }
                    else if (s == 3)
                    {
                        for (int lk = 0; lk < 9; lk++)
                        {
                            terca[0, lk] = materias[lk];
                        }

                        for (int lk = 0; lk < 9; lk++)
                        {
                            terca[1, lk] = profs[lk];
                        }

                        for (int lk = 0; lk < 9; lk++)
                        {
                            terca[2, lk] = num_salas[lk];
                        }
                    }
                    else if (s == 4)
                    {
                        for (int lk = 0; lk < 9; lk++)
                        {
                            quarta[0, lk] = materias[lk];
                        }

                        for (int lk = 0; lk < 9; lk++)
                        {
                            quarta[1, lk] = profs[lk];
                        }

                        for (int lk = 0; lk < 9; lk++)
                        {
                            quarta[2, lk] = num_salas[lk];
                        }
                    }
                    else if (s == 5)
                    {
                        for (int lk = 0; lk < 9; lk++)
                        {
                            quinta[0, lk] = materias[lk];
                        }

                        for (int lk = 0; lk < 9; lk++)
                        {
                            quinta[1, lk] = profs[lk];
                        }

                        for (int lk = 0; lk < 9; lk++)
                        {
                            quinta[2, lk] = num_salas[lk];
                        }
                    }
                    else if (s == 6)
                    {
                        for (int lk = 0; lk < 9; lk++)
                        {
                            sexta[0, lk] = materias[lk];
                        }

                        for (int lk = 0; lk < 9; lk++)
                        {
                            sexta[1, lk] = profs[lk];
                        }

                        for (int lk = 0; lk < 9; lk++)
                        {
                            sexta[2, lk] = num_salas[lk];
                        }
                    }
                }

                for (int lk = 0; lk < 9; lk++)
                {
                    dt.Rows.Add(segunda[0, lk], segunda[1, lk], segunda[2, lk], terca[0, lk], terca[1, lk], terca[2, lk], quarta[0, lk], quarta[1, lk], quarta[2, lk], quinta[0, lk], quinta[1, lk], quinta[2, lk], sexta[0, lk], sexta[1, lk], sexta[2, lk]);
                }

                dataGridView1.DataSource = dt;
            }
        }
    }
}
