﻿namespace softwarecordenacao.Formularios
{
    partial class CadastrarCurso
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CadastrarCurso));
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pbconfirmarcadastro = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtano = new System.Windows.Forms.TextBox();
            this.txtnome = new System.Windows.Forms.TextBox();
            this.txtidcurso = new System.Windows.Forms.TextBox();
            this.lblMDisp = new System.Windows.Forms.ListBox();
            this.lblMCurso = new System.Windows.Forms.ListBox();
            this.pbsetadir = new System.Windows.Forms.PictureBox();
            this.pbsetaesq = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbconfirmarcadastro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsetadir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsetaesq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.DarkGreen;
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(128, 2);
            this.pictureBox5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(393, 151);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 32;
            this.pictureBox5.TabStop = false;
            // 
            // pbconfirmarcadastro
            // 
            this.pbconfirmarcadastro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbconfirmarcadastro.Image = ((System.Drawing.Image)(resources.GetObject("pbconfirmarcadastro.Image")));
            this.pbconfirmarcadastro.Location = new System.Drawing.Point(242, 758);
            this.pbconfirmarcadastro.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbconfirmarcadastro.Name = "pbconfirmarcadastro";
            this.pbconfirmarcadastro.Size = new System.Drawing.Size(209, 64);
            this.pbconfirmarcadastro.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbconfirmarcadastro.TabIndex = 31;
            this.pbconfirmarcadastro.TabStop = false;
            this.pbconfirmarcadastro.Click += new System.EventHandler(this.pbconfirmarcadastro_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(27, 240);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(100, 50);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 30;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(27, 300);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 51);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 29;
            this.pictureBox1.TabStop = false;
            // 
            // txtano
            // 
            this.txtano.Location = new System.Drawing.Point(150, 317);
            this.txtano.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtano.Name = "txtano";
            this.txtano.Size = new System.Drawing.Size(504, 26);
            this.txtano.TabIndex = 28;
            // 
            // txtnome
            // 
            this.txtnome.Location = new System.Drawing.Point(150, 251);
            this.txtnome.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtnome.Name = "txtnome";
            this.txtnome.Size = new System.Drawing.Size(504, 26);
            this.txtnome.TabIndex = 27;
            // 
            // txtidcurso
            // 
            this.txtidcurso.Location = new System.Drawing.Point(150, 186);
            this.txtidcurso.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtidcurso.Name = "txtidcurso";
            this.txtidcurso.Size = new System.Drawing.Size(504, 26);
            this.txtidcurso.TabIndex = 34;
            // 
            // lblMDisp
            // 
            this.lblMDisp.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblMDisp.FormattingEnabled = true;
            this.lblMDisp.ItemHeight = 20;
            this.lblMDisp.Location = new System.Drawing.Point(44, 443);
            this.lblMDisp.Name = "lblMDisp";
            this.lblMDisp.Size = new System.Drawing.Size(241, 284);
            this.lblMDisp.TabIndex = 37;
            // 
            // lblMCurso
            // 
            this.lblMCurso.BackColor = System.Drawing.Color.White;
            this.lblMCurso.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblMCurso.FormattingEnabled = true;
            this.lblMCurso.ItemHeight = 20;
            this.lblMCurso.Location = new System.Drawing.Point(386, 443);
            this.lblMCurso.Name = "lblMCurso";
            this.lblMCurso.Size = new System.Drawing.Size(241, 284);
            this.lblMCurso.TabIndex = 38;
            // 
            // pbsetadir
            // 
            this.pbsetadir.Enabled = false;
            this.pbsetadir.Image = ((System.Drawing.Image)(resources.GetObject("pbsetadir.Image")));
            this.pbsetadir.Location = new System.Drawing.Point(298, 523);
            this.pbsetadir.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbsetadir.Name = "pbsetadir";
            this.pbsetadir.Size = new System.Drawing.Size(75, 38);
            this.pbsetadir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbsetadir.TabIndex = 42;
            this.pbsetadir.TabStop = false;
            this.pbsetadir.Click += new System.EventHandler(this.pbsetadir_Click);
            // 
            // pbsetaesq
            // 
            this.pbsetaesq.Enabled = false;
            this.pbsetaesq.Image = ((System.Drawing.Image)(resources.GetObject("pbsetaesq.Image")));
            this.pbsetaesq.Location = new System.Drawing.Point(298, 616);
            this.pbsetaesq.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbsetaesq.Name = "pbsetaesq";
            this.pbsetaesq.Size = new System.Drawing.Size(75, 38);
            this.pbsetaesq.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbsetaesq.TabIndex = 41;
            this.pbsetaesq.TabStop = false;
            this.pbsetaesq.Click += new System.EventHandler(this.pbsetaesq_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.DarkGreen;
            this.pictureBox2.Location = new System.Drawing.Point(513, 2);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(165, 151);
            this.pictureBox2.TabIndex = 43;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.DarkGreen;
            this.pictureBox4.Location = new System.Drawing.Point(0, 2);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(130, 151);
            this.pictureBox4.TabIndex = 44;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(18, 177);
            this.pictureBox6.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(122, 50);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 45;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(44, 385);
            this.pictureBox7.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(242, 50);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 46;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(386, 385);
            this.pictureBox8.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(242, 50);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 47;
            this.pictureBox8.TabStop = false;
            // 
            // CadastrarCurso
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(674, 842);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pbsetadir);
            this.Controls.Add(this.pbsetaesq);
            this.Controls.Add(this.lblMCurso);
            this.Controls.Add(this.lblMDisp);
            this.Controls.Add(this.txtidcurso);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pbconfirmarcadastro);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txtano);
            this.Controls.Add(this.txtnome);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "CadastrarCurso";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastrar curso";
            this.Load += new System.EventHandler(this.CadastrarCurso_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbconfirmarcadastro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsetadir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsetaesq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pbconfirmarcadastro;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txtano;
        private System.Windows.Forms.TextBox txtnome;
        private System.Windows.Forms.TextBox txtidcurso;
        private System.Windows.Forms.ListBox lblMDisp;
        private System.Windows.Forms.ListBox lblMCurso;
        private System.Windows.Forms.PictureBox pbsetadir;
        private System.Windows.Forms.PictureBox pbsetaesq;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
    }
}