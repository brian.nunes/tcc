﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace softwarecordenacao.Formularios
{
    public partial class CadastrarCurso : Form
    {
        public CadastrarCurso()
        {
            InitializeComponent();
        }

        private void pbconfirmarcadastro_Click(object sender, EventArgs e)
        {
            //confere se foi tudo preenchido
            if (txtidcurso.Text != "" && txtnome.Text != "" && txtano.Text != "" && lblMCurso.Items.Count > 0)
            {
                //confere se o id nunca foi usado
                sql.pesquisar("select * from tab_curso where id_curso ='" + txtidcurso.Text + "';");
                if (sql.guarda.Read())
                {
                    //avisa se for o caso
                    MessageBox.Show("ID do curso já cadastrada!", "*** Erro ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //fecha as conexões
                    sql.guarda.Close();
                    sql.olecon.Close();
                }
                else
                {
                    //fecha as conexões
                    sql.guarda.Close();
                    sql.olecon.Close();
                    //confere se as senhas batem
                    try
                    {
                        //faz a inserção no banco de dados
                        sql.executarcomando("INSERT INTO `tab_curso` (`id_curso`, `nome_curso`, `ano`) VALUES ('" + txtidcurso.Text + "', '" + txtnome.Text + "', '" + txtano.Text + "')", "Inserção");

                        //faz a inserção das relações curso-materia
                        criaRelacoes();

                        //da o feed back ao usuario
                        MessageBox.Show("Cadastro feito com sucesso!", "*** Cadastro ***", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //prepara o form para um novo cadastro
                        txtidcurso.Clear();
                        txtnome.Clear();
                        txtano.Clear();
                        lblMCurso.Items.Clear();
                        lblMDisp.Items.Clear();
                        CadastrarCurso_Load(sender, e);
                        txtnome.Focus();
                    }
                    catch
                    {
                        //mensagem de erro caso ocorra algum erro coma inserção do cadastro
                        MessageBox.Show("Cadastro não pode ser efetuado!", "*** Erro ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
            {
                //avisa ao usuario que existem campos não preenchidos
                MessageBox.Show("Preencha todos os campos!", "*** Erro ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CadastrarCurso_Load(object sender, EventArgs e)
        {
            //preencher lista de matérias disponíveis
            sql.pesquisar("select * from tab_materia where id_materia != '666' AND id_materia != '999'");

            //escreve um por um na lista
            while(sql.guarda.Read())
            {
                lblMDisp.Items.Add(sql.guarda[0].ToString() + " - " + sql.guarda[1].ToString() + " - " + sql.guarda[2].ToString());
            }

            //fecha as conexões
            sql.guarda.Close();
            sql.olecon.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void criaRelacoes()
        {
            //cria vetor para código das matérias
            string[] codigos = new string[lblMCurso.Items.Count];

            for(int i = 0; i < lblMCurso.Items.Count; i++)
            {
                //seleciona o código da matéria
                string materia = lblMCurso.Items[i].ToString();
                string codigo = "";
                string letra = "";
                int index = 0;
                while (letra != " ")
                {
                    codigo += letra;
                    letra = materia.Substring(index, 1);
                    index++;
                }
                codigos[i] = codigo;
            }

            for(int i = 0; i < codigos.Length; i++)
            {
                sql.executarcomando("INSERT INTO `relacao_materia_curso` (`fk_curso`, `fk_materia`) VALUES ('" + txtidcurso.Text + "', '" + codigos[i] + "')", " inserção");
            }
        }

        private void pbsetadir_Click(object sender, EventArgs e)
        {
            //confere se há algo selecionado
            if (lblMDisp.SelectedIndex != -1)
            {
                //transfere pra lista de selecionados
                lblMCurso.Items.Add(lblMDisp.SelectedItem.ToString());

                //deleta da lista de disponíveis
                lblMDisp.Items.RemoveAt(lblMDisp.SelectedIndex);
            }
        }

        private void pbsetaesq_Click(object sender, EventArgs e)
        {
            //confere se há algo selecionado
            if (lblMCurso.SelectedIndex != -1)
            {
                //transfere pra lista de disponíveis
                lblMDisp.Items.Add(lblMCurso.SelectedItem.ToString());

                //deleta da lista de selecionados
                lblMCurso.Items.RemoveAt(lblMCurso.SelectedIndex);
            }
        }
    }
}
