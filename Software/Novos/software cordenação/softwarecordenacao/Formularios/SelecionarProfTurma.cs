﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace softwarecordenacao
{
    public partial class SelecionarProfTurma : Form
    {
        public SelecionarProfTurma()
        {
            InitializeComponent();
        }

        private void pbconfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                //cria vetor para código dos profs
                string[] codigos2 = new string[lbPerm.Items.Count];

                for (int i = 0; i < lbPerm.Items.Count; i++)
                {
                    //seleciona o código dos profs
                    string materia2 = lbPerm.Items[i].ToString();
                    string codigo2 = "";
                    string letra2 = "";
                    int index2 = 0;
                    while (letra2 != " ")
                    {
                        codigo2 += letra2;
                        letra2 = materia2.Substring(index2, 1);
                        index2++;
                    }
                    codigos2[i] = codigo2;
                }

                //seleciona o código da matéria
                string materia = cbmateria.Text;
                string codigo = "";
                string letra = "";
                int index = 0;
                while (letra != " ")
                {
                    codigo += letra;
                    letra = materia.Substring(index, 1);
                    index++;
                }
                string cod_materia = codigo;

                //seleciona o código da turma
                materia = cbturma.Text;
                codigo = "";
                letra = "";
                index = 0;
                while (letra != " ")
                {
                    codigo += letra;
                    letra = materia.Substring(index, 1);
                    index++;
                }
                string cod_turma = codigo;

                for (int i = 0; i < codigos2.Length; i++)
                {
                    sql.executarcomando("INSERT INTO `relacao_prof_classe_permanente` (`fk_materia`, `fk_prof`, `fk_classe`) VALUES (" + cod_materia + ", " + codigos2[i] + ", " + cod_turma + ")", " inserção");
                    sql.executarcomando("INSERT INTO `tab_aula` (`fk_sigla_materia`, `fk_prof`, `fk_classe`) VALUES (" + cod_materia + ", " + codigos2[i] + ", " + cod_turma + ")", " inserção");
                    sql.executarcomando("DELETE FROM `relacao_prof_classe_temporaria` where `fk_materia` = " + cod_materia + " AND `fk_prof` = " + codigos2[i] + " AND `fk_classe` = " + cod_turma, " delete");
                }

                //cria vetor para código dos profs na temporaria
                codigos2 = new string[lbTemp.Items.Count];

                for (int i = 0; i < lbTemp.Items.Count; i++)
                {
                    //seleciona o código dos profs
                    materia = lbTemp.Items[i].ToString();
                    codigo = "";
                    letra = "";
                    index = 0;
                    while (letra != " ")
                    {
                        codigo += letra;
                        letra = materia.Substring(index, 1);
                        index++;
                    }
                    codigos2[i] = codigo;
                }

                for (int i = 0; i < codigos2.Length; i++)
                {
                    //pesquisa se há relação temp do prof
                    sql.pesquisar("SELECT * FROM relacao_prof_classe_temporaria WHERE fk_prof = " + codigos2[i] + " AND fk_materia = " + cod_materia + " AND fk_classe = " + cod_turma);

                    //se não houver significa que ele foi colocado la
                    if (!sql.guarda.Read())
                    {
                        //fecha as conexões
                        sql.guarda.Close();
                        sql.olecon.Close();
                        //e neste caso sua relaçao permanente é retirada, e uma temporária é colocada no lugar
                        sql.executarcomando("INSERT INTO `relacao_prof_classe_temporaria` (`fk_materia`, `fk_prof`, `fk_classe`) VALUES ('" + cod_materia + "', '" + codigos2[i] + "', '" + cod_turma + "')", " inserção");
                        sql.executarcomando("DELETE FROM `relacao_prof_classe_permanente` where `fk_materia` = " + cod_materia + " AND `fk_prof` = " + codigos2[i] + " AND `fk_classe` = " + cod_turma, " delete");
                        SQLDeletes.DeletarAulaProfTurma(codigos2[i], cod_turma);
                    }
                    else
                    {
                        //fecha as conexões
                        sql.guarda.Close();
                        sql.olecon.Close();
                    }
                }

                //da o feed back ao usuario
                DialogResult YN = MessageBox.Show("Registro feita com sucesso! \nDeseja limpar o formulário?", "*** Registro ***", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                //testa a resposta do usuário
                if (YN == DialogResult.Yes)
                {
                    //prepara o form para um novo cadastro
                    pbcancelar_Click(sender, e);
                }
            }
            catch
            {
                //mensagem de erro caso ocorra algum erro coma inserção do registro
                MessageBox.Show("Registro não pode ser efetuado!", "*** Erro ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void SelecionarProfTurma_Load(object sender, EventArgs e)
        {
            //preencher lista de matérias disponíveis
            sql.pesquisar("select * from tab_classe where id_classe != '666' AND id_classe != '999'");

            //escreve um por um na combobox
            while (sql.guarda.Read())
            {
                cbturma.Items.Add(sql.guarda[0].ToString() + " - " + sql.guarda[1].ToString() + sql.guarda[2].ToString());
            }

            //fecha as conexões
            sql.guarda.Close();
            sql.olecon.Close();
        }

        private void cbturma_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cbturma.SelectedIndex != -1)
            {
                //ativa cbmateria e a limpa
                cbmateria.Enabled = true;
                cbmateria.Items.Clear();

                lbPerm.Items.Clear();
                lbTemp.Items.Clear();

                //seleciona o código da classe
                string materia = cbturma.Text;
                string codigo = "";
                string letra = "";
                int index = 0;
                while (letra != " ")
                {
                    codigo += letra;
                    letra = materia.Substring(index, 1);
                    index++;
                }

                //pesquisa o curso da sala
                sql.pesquisar("SELECT fk_curso FROM tab_classe WHERE id_classe = " + codigo);

                //se encontrar
                if(sql.guarda.Read())
                {
                    //armazena numa variavel
                    string cod_curso = sql.guarda[0].ToString();

                    //fecha as conexões
                    sql.guarda.Close();
                    sql.olecon.Close();

                    //pesquisa todas as matérias relacionadas ao curso
                    sql.pesquisar("SELECT * FROM relacao_materia_curso WHERE fk_curso = " + cod_curso);

                    //cria variável para contagem
                    int k = 0;

                    //conta
                    while (sql.guarda.Read())
                    {
                        k++;
                    }

                    //fecha as conexões
                    sql.guarda.Close();
                    sql.olecon.Close();

                    //cria vetor para armazenar código dessas matérias
                    string[] codigos = new string[k];

                    //pesquisa todas as matérias relacionadas ao curso
                    sql.pesquisar("SELECT * FROM relacao_materia_curso WHERE fk_curso = " + cod_curso);

                    //zera variavel para usar como auxiliar
                    k = 0;

                    //armazena codigo por codigo
                    while (sql.guarda.Read())
                    {
                        codigos[k] = sql.guarda[0].ToString();
                        k++;
                    }

                    //fecha as conexões
                    sql.guarda.Close();
                    sql.olecon.Close();

                    //cria laço de repetição para colocalas na cbmateria
                    for (k = 0; k < codigos.Length; k++)
                    {
                        //pesquisa a matéria
                        sql.pesquisar("SELECT * FROM tab_materia WHERE id_materia = " + codigos[k]);

                        //se achar
                        if(sql.guarda.Read())
                        {
                            //escreve na combobox
                            cbmateria.Items.Add(sql.guarda[0].ToString() + " - " + sql.guarda[1].ToString() + " - " + sql.guarda[3].ToString() + " aula(s)/semana");
                        }

                        //fecha as conexões
                        sql.guarda.Close();
                        sql.olecon.Close();
                    }
                }
            }
            else
            {
                pbcancelar_Click(sender, e);
            }
        }

        private void pbcancelar_Click(object sender, EventArgs e)
        {
            //limpa e desativa as listas
            lbPerm.Items.Clear();
            lbPerm.Enabled = false;
            lbTemp.Items.Clear();
            lbTemp.Enabled = false;

            //desativa os botões
            pbsetadir.Enabled = false;
            pbsetaesq.Enabled = false;

            //limpa e desativa combobox de matéria
            cbmateria.Items.Clear();
            cbmateria.SelectedIndex = -1;
            cbmateria.Enabled = false;

            //deixa a cbturma em branco e foca nela
            cbturma.Items.Clear();
            cbturma.Text = "";
            cbturma.Focus();

            //repreenche cbturma
            SelecionarProfTurma_Load(sender, e);
        }

        private void pbsetadir_Click(object sender, EventArgs e)
        {
            //confere se há algo selecionado
            if (lbTemp.SelectedIndex != -1)
            {
                //transfere pra lista de selecionados
                lbPerm.Items.Add(lbTemp.SelectedItem.ToString());

                //deleta da lista de disponíveis
                lbTemp.Items.RemoveAt(lbTemp.SelectedIndex);
            }
        }

        private void pbsetaesq_Click(object sender, EventArgs e)
        {
            //confere se há algo selecionado
            if (lbPerm.SelectedIndex != -1)
            {
                //transfere pra lista de disponíveis
                lbTemp.Items.Add(lbPerm.SelectedItem.ToString());

                //deleta da lista de selecionados
                lbPerm.Items.RemoveAt(lbPerm.SelectedIndex);
            }
        }

        private void cbmateria_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbmateria.SelectedIndex != -1)
            {
                //ativa os campos
                lbPerm.Enabled = true;
                lbTemp.Enabled = true;
                pbsetadir.Enabled = true;
                pbsetaesq.Enabled = true;

                //limpa as listas
                lbPerm.Items.Clear();
                lbTemp.Items.Clear();

                //seleciona o código da matéria
                string materia = cbmateria.Text;
                string codigo = "";
                string letra = "";
                int index = 0;
                while (letra != " ")
                {
                    codigo += letra;
                    letra = materia.Substring(index, 1);
                    index++;
                }
                string cod_materia = codigo;

                //seleciona o código da turma
                materia = cbturma.Text;
                codigo = "";
                letra = "";
                index = 0;
                while (letra != " ")
                {
                    codigo += letra;
                    letra = materia.Substring(index, 1);
                    index++;
                }
                string cod_turma = codigo;

                //pesquisa os códigos de profs que querem essa turma para essa matéria
                sql.pesquisar("SELECT * FROM relacao_prof_classe_temporaria WHERE fk_classe = " + cod_turma + " AND fk_materia = " + cod_materia);

                //cria variável para contar
                int k = 0;

                //faz a contagem
                while (sql.guarda.Read())
                {
                    k++;
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                string[] codigos = new string[k];

                //pesquisa os códigos de profs que querem essa matéria
                sql.pesquisar("SELECT * FROM relacao_prof_classe_temporaria WHERE fk_classe = " + cod_turma + " AND fk_materia = " + cod_materia);

                //zera a variavel para ser usada como auxiliar
                k = 0;

                //faz a passagem pra array
                while (sql.guarda.Read())
                {
                    codigos[k] = sql.guarda[0].ToString();
                    k++;
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //pesquisa todos os profs um por um e os lista
                for (k = 0; k < codigos.Length; k++)
                {
                    //pesquisa o professor
                    sql.pesquisar("SELECT * FROM tab_prof WHERE id_prof = " + codigos[k] + " ORDER BY preferencia");

                    if (sql.guarda.Read())
                    {
                        lbTemp.Items.Add(sql.guarda[0].ToString() + " - " + sql.guarda[1].ToString() + " - Pref: " + sql.guarda[4].ToString());
                    }

                    //fecha as conexões
                    sql.guarda.Close();
                    sql.olecon.Close();
                }




                //o mesmo procedimento para os profs que ja lecionam a matéria para a sala



                // pesquisa os códigos de profs que lecionam essa matéria
                sql.pesquisar("SELECT * FROM relacao_prof_classe_permanente WHERE fk_classe = " + cod_turma + " AND fk_materia = " + cod_materia);

                //cria variável para contar
                k = 0;

                //faz a contagem
                while (sql.guarda.Read())
                {
                    k++;
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                codigos = new string[k];

                //pesquisa os códigos de profs que querem essa matéria
                sql.pesquisar("SELECT * FROM relacao_prof_classe_permanente WHERE fk_classe = " + cod_turma + " AND fk_materia = " + cod_materia);

                //zera a variavel para ser usada como auxiliar
                k = 0;

                //faz a passagem pra array
                while (sql.guarda.Read())
                {
                    codigos[k] = sql.guarda[0].ToString();
                    k++;
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //pesquisa todos os profs um por um e os lista
                for (k = 0; k < codigos.Length; k++)
                {
                    //pesquisa o professor
                    sql.pesquisar("SELECT * FROM tab_prof WHERE id_prof = " + codigos[k]);

                    if (sql.guarda.Read())
                    {
                        lbPerm.Items.Add(sql.guarda[0].ToString() + " - " + sql.guarda[1].ToString() + " - Pref: " + sql.guarda[4].ToString());
                    }

                    //fecha as conexões
                    sql.guarda.Close();
                    sql.olecon.Close();
                }
            }
            else
            {
                //limpa as listas
                lbPerm.Items.Clear();
                lbTemp.Items.Clear();

                pbcancelar_Click(sender, e);
            }
        }
    }
}