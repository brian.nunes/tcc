﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace softwarecordenacao
{
    public partial class SelecionarProfMateria : Form
    {
        string[] codigos;
        string codigo;
        string materia;
        string letra;
        int index;
        public SelecionarProfMateria()
        {
            InitializeComponent();
        }

        private void pbconfirmar_Click(object sender, EventArgs e)
        {
            try
            {
                //cria vetor para código dos profs
                string[] codigos2 = new string[lbPerm.Items.Count];

                for (int i = 0; i < lbPerm.Items.Count; i++)
                {
                    //seleciona o código dos profs
                    materia = lbPerm.Items[i].ToString();
                    codigo = "";
                    letra = "";
                    index = 0;
                    while (letra != " ")
                    {
                        codigo += letra;
                        letra = materia.Substring(index, 1);
                        index++;
                    }
                    codigos2[i] = codigo;
                }

                //seleciona o código da matéria
                materia = cbmateria.Text;
                codigo = "";
                letra = "";
                index = 0;
                while (letra != " ")
                {
                    codigo += letra;
                    letra = materia.Substring(index, 1);
                    index++;
                }

                for (int i = 0; i < codigos2.Length; i++)
                {
                    sql.executarcomando("INSERT INTO `relacao_prof_materia_permanente` (`fk_materia`, `fk_prof`) VALUES ('" + codigo + "', '" + codigos2[i] + "')", " inserção");
                    sql.executarcomando("DELETE FROM `relacao_prof_materia_temporaria` where `fk_materia` = " + codigo + " AND `fk_prof` = " + codigos2[i], " delete");
                }

                //cria vetor para código dos profs na temporaria
                codigos2 = new string[lbTemp.Items.Count];

                for (int i = 0; i < lbTemp.Items.Count; i++)
                {
                    //seleciona o código dos profs
                    materia = lbTemp.Items[i].ToString();
                    codigo = "";
                    letra = "";
                    index = 0;
                    while (letra != " ")
                    {
                        codigo += letra;
                        letra = materia.Substring(index, 1);
                        index++;
                    }
                    codigos2[i] = codigo;
                }

                //seleciona o código da matéria
                materia = cbmateria.Text;
                codigo = "";
                letra = "";
                index = 0;
                while (letra != " ")
                {
                    codigo += letra;
                    letra = materia.Substring(index, 1);
                    index++;
                }

                for (int i = 0; i < codigos2.Length; i++)
                {
                    //pesquisa se há relação temp do prof
                    sql.pesquisar("SELECT * FROM relacao_prof_materia_temporaria WHERE fk_prof = " + codigos2[i] + " AND fk_materia = " + codigo);

                    //se não houver significa que ele foi colocado la
                    if (!sql.guarda.Read())
                    {
                        //fecha as conexões
                        sql.guarda.Close();
                        sql.olecon.Close();
                        //e neste caso sua relaçao permanente é retirada, e uma temporária é colocada no lugar
                        sql.executarcomando("INSERT INTO `relacao_prof_materia_temporaria` (`fk_materia`, `fk_prof`) VALUES ('" + codigo + "', '" + codigos2[i] + "')", " inserção");
                        sql.executarcomando("DELETE FROM `relacao_prof_materia_permanente` where `fk_materia` = " + codigo + " AND `fk_prof` = " + codigos2[i], " delete");
                    }
                    else
                    {
                        //fecha as conexões
                        sql.guarda.Close();
                        sql.olecon.Close();
                    }
                }

                //da o feed back ao usuario
                DialogResult YN = MessageBox.Show("Registro feita com sucesso! \nDeseja limpar o formulário?", "*** Registro ***", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                //testa a resposta do usuário
                if (YN == DialogResult.Yes)
                {
                    //prepara o form para um novo cadastro
                    pbcancelar_Click(sender, e);
                }
            }
            catch
            {
                //mensagem de erro caso ocorra algum erro coma inserção do registro
                MessageBox.Show("Registro não pode ser efetuado!", "*** Erro ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void SelecionarProfMateria_Load(object sender, EventArgs e)
        {
            //preencher lista de matérias disponíveis
            sql.pesquisar("select * from tab_materia where id_materia != '666' AND id_materia != '999'");

            //escreve um por um na combobox
            while (sql.guarda.Read())
            {
                cbmateria.Items.Add(sql.guarda[0].ToString() + " - " + sql.guarda[1].ToString() + " - " + sql.guarda[3].ToString() + " aula(s)/semana");
            }

            //fecha as conexões
            sql.guarda.Close();
            sql.olecon.Close();
        }

        private void pbsetadir_Click(object sender, EventArgs e)
        {
            //confere se há algo selecionado
            if (lbTemp.SelectedIndex != -1)
            {
                //transfere pra lista de selecionados
                lbPerm.Items.Add(lbTemp.SelectedItem.ToString());

                //deleta da lista de disponíveis
                lbTemp.Items.RemoveAt(lbTemp.SelectedIndex);
            }
        }

        private void pbsetaesq_Click(object sender, EventArgs e)
        {
            //confere se há algo selecionado
            if (lbPerm.SelectedIndex != -1)
            {
                //transfere pra lista de disponíveis
                lbTemp.Items.Add(lbPerm.SelectedItem.ToString());

                //deleta da lista de selecionados
                lbPerm.Items.RemoveAt(lbPerm.SelectedIndex);
            }
        }

        private void pbcancelar_Click(object sender, EventArgs e)
        {
            //limpa seleção da combobox(e consequentemente das listas)
            cbmateria.SelectedIndex = -1;

            //desativa listas e botões
            lbPerm.Enabled = false;
            lbTemp.Enabled = false;
            pbsetadir.Enabled = false;
            pbsetaesq.Enabled = false;
        }

        private void cbmateria_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cbmateria.SelectedIndex != -1)
            {
                //ativa os campos
                lbPerm.Enabled = true;
                lbTemp.Enabled = true;
                pbsetadir.Enabled = true;
                pbsetaesq.Enabled = true;

                //limpa as listas
                lbPerm.Items.Clear();
                lbTemp.Items.Clear();

                //seleciona o código da matéria
                string materia = cbmateria.Text;
                codigo = "";
                string letra = "";
                int index = 0;
                while (letra != " ")
                {
                    codigo += letra;
                    letra = materia.Substring(index, 1);
                    index++;
                }

                //pesquisa os códigos de profs que querem essa matéria
                sql.pesquisar("SELECT * FROM `relacao_prof_materia_temporaria` WHERE `relacao_prof_materia_temporaria`.`fk_materia` = " + codigo);

                //cria variável para contar
                int k = 0;

                //faz a contagem
                while(sql.guarda.Read())
                {
                    k++;
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                string[] codigos = new string[k];

                //pesquisa os códigos de profs que querem essa matéria
                sql.pesquisar("SELECT * FROM `relacao_prof_materia_temporaria` WHERE `relacao_prof_materia_temporaria`.`fk_materia` = " + codigo);

                //zera a variavel para ser usada como auxiliar
                k = 0;

                //faz a passagem pra array
                while (sql.guarda.Read())
                {
                    codigos[k] = sql.guarda[0].ToString();
                    k++;
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //pesquisa todos os profs um por um e os lista
                for(k = 0; k < codigos.Length; k++)
                {
                    //pesquisa o professor
                    sql.pesquisar("SELECT * FROM tab_prof WHERE id_prof = " + codigos[k] + " ORDER BY preferencia");

                    if(sql.guarda.Read())
                    {
                        lbTemp.Items.Add(sql.guarda[0].ToString() + " - " + sql.guarda[1].ToString() + " - Pref: " + sql.guarda[4].ToString());
                    }

                    //fecha as conexões
                    sql.guarda.Close();
                    sql.olecon.Close();
                }




                //o mesmo procedimento para os profs que ja lecionam a matéria



                // pesquisa os códigos de profs que lecionam essa matéria
                sql.pesquisar("SELECT * FROM `relacao_prof_materia_permanente` WHERE `relacao_prof_materia_permanente`.`fk_materia` = " + codigo);

                //cria variável para contar
                k = 0;

                //faz a contagem
                while (sql.guarda.Read())
                {
                    k++;
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                codigos = new string[k];

                //pesquisa os códigos de profs que querem essa matéria
                sql.pesquisar("SELECT * FROM `relacao_prof_materia_permanente` WHERE `relacao_prof_materia_permanente`.`fk_materia` = " + codigo);

                //zera a variavel para ser usada como auxiliar
                k = 0;

                //faz a passagem pra array
                while (sql.guarda.Read())
                {
                    codigos[k] = sql.guarda[1].ToString();
                    k++;
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //pesquisa todos os profs um por um e os lista
                for (k = 0; k < codigos.Length; k++)
                {
                    //pesquisa o professor
                    sql.pesquisar("SELECT * FROM tab_prof WHERE id_prof = " + codigos[k] + " ORDER BY preferencia DESC");

                    if (sql.guarda.Read())
                    {
                        lbPerm.Items.Add(sql.guarda[0].ToString() + " - " + sql.guarda[1].ToString() + " - Pref: " + sql.guarda[4].ToString());
                    }

                    //fecha as conexões
                    sql.guarda.Close();
                    sql.olecon.Close();
                }
            }
            else
            {
                //limpa as listas
                lbPerm.Items.Clear();
                lbTemp.Items.Clear();

                pbcancelar_Click(sender, e);
            }
        }
    }
}