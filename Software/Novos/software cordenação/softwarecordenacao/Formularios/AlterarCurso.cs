﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace softwarecordenacao
{
    public partial class AlterarCurso : Form
    {
        public AlterarCurso()
        {
            InitializeComponent();
        }

        private void pbconfirmaralteracoes_Click(object sender, EventArgs e)
        {
            //confere se foi tudo preenchido
            if (txtidcurso.Text != "" && cbcurso.Text != "" && numano.Text != "" && lblMCurso.Items.Count > 0)
            {
                    //fecha as conexões
                    sql.guarda.Close();
                    sql.olecon.Close();
                    //confere se as senhas batem
                    try
                    {
                        //faz a inserção no banco de dados
                        sql.executarcomando("INSERT INTO `tab_curso` (`nome_curso`, `ano`) VALUES ('" + cbcurso.Text + "', '" + numano.Text + "')", "Inserção");

                        //faz a inserção das relações curso-materia
                        criaRelacoes();

                        //da o feed back ao usuario
                        MessageBox.Show("Alteração feita com sucesso feito com sucesso!", "*** Alteração ***", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //prepara o form para um novo cadastro
                    button4_Click(sender, e);
                    }
                    catch
                    {
                        //mensagem de erro caso ocorra algum erro coma inserção do cadastro
                        MessageBox.Show("Alteração não pode ser efetuada!", "*** Erro ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            else
            {
                //avisa ao usuario que existem campos não preenchidos
                MessageBox.Show("Preencha todos os campos!", "*** Erro ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void AlterarCurso_Load(object sender, EventArgs e)
        {

            //preencher lista de cursos disponíveis
            sql.pesquisar("select * from tab_curso where id_curso != '666' AND id_curso != '999'");

            //escreve um por um na combobox
            while (sql.guarda.Read())
            {
                cbcurso.Items.Add(sql.guarda[1].ToString());
            }

            //fecha as conexões
            sql.guarda.Close();
            sql.olecon.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void criaRelacoes()
        {
            sql.executarcomando("DELETE FROM relacao_materia_curso WHERE fk_curso = '" + txtidcurso.Text + "'", " inserção");

            //cria vetor para código das matérias
            string[] codigos = new string[lblMCurso.Items.Count];

            for (int i = 0; i < lblMCurso.Items.Count; i++)
            {
                //seleciona o código da matéria
                string materia = lblMCurso.Items[i].ToString();
                string codigo = "";
                string letra = "";
                int index = 0;
                while (letra != " ")
                {
                    codigo += letra;
                    letra = materia.Substring(index, 1);
                    index++;
                }
                codigos[i] = codigo;
            }

            for (int i = 0; i < codigos.Length; i++)
            {
                sql.executarcomando("INSERT INTO `relacao_materia_curso` (`fk_curso`, `fk_materia`) VALUES ('" + txtidcurso.Text + "', '" + codigos[i] + "')", " inserção");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //confere se há algo selecionado
            if (lblMDisp.SelectedIndex != -1)
            {
                //transfere pra lista de selecionados
                lblMCurso.Items.Add(lblMDisp.SelectedItem.ToString());

                //deleta da lista de disponíveis
                lblMDisp.Items.RemoveAt(lblMDisp.SelectedIndex);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {

        }

        private void pblocalizar_Click(object sender, EventArgs e)
        {
            //testa se os campos foram preenchidos
            if (cbcurso.Text != "" && numano.Value != 0)
            {
                try
                {
                    //pesquisa o curso buscado
                    sql.pesquisar("SELECT * FROM tab_curso WHERE ano = '" + numano.Value.ToString() + "' AND nome_curso = '" + cbcurso.Text + "'");

                    //testa se encontrou
                    if (sql.guarda.Read())
                    {
                        //ativa as partes necessárias
                        lblMDisp.Enabled = true;
                        lblMCurso.Enabled = true;
                        pbsetadir.Enabled = true;
                        pbsetaesq.Enabled = true;

                        //preencha as mesmas com os dados do curso
                        txtidcurso.Text = sql.guarda[0].ToString();

                        //fecha as conexões
                        sql.guarda.Close();
                        sql.olecon.Close();

                        //preencher combobox
                        //pesquisa lista de cursos disponíveis
                        sql.pesquisar("select * from relacao_materia_curso where fk_curso = '" + txtidcurso.Text + "'");

                        //cri variavel para contagem
                        int k = 0;

                        //Container o número de respostas
                        while (sql.guarda.Read())
                        {
                            k++;
                        }

                        //fecha as conexões
                        sql.guarda.Close();
                        sql.olecon.Close();

                        //pesquisa lista de cursos disponíveis
                        sql.pesquisar("select * from relacao_materia_curso where fk_curso = '" + txtidcurso.Text + "'");

                        //instancia array para receber códigos
                        string[] codigos = new string[k];

                        //zera para servir de auxiliar
                        k = 0;

                        //escreve um por um na array de matérias do curso
                        while (sql.guarda.Read())
                        {
                            codigos[k] = sql.guarda[0].ToString();
                            k++;
                        }

                        //fecha as conexões
                        sql.guarda.Close();
                        sql.olecon.Close();

                        //limpa listboxs para que não haja duplicatas
                        lblMCurso.Items.Clear();
                        lblMDisp.Items.Clear();

                        //faz a pesquisa de matéria por matéria
                        for (int i = 0; i < codigos.Length; i++)
                        {
                            //faz a pesquisa
                            sql.pesquisar("select * from tab_materia where id_materia = '" + codigos[i] + "'");

                            if (sql.guarda.Read())
                            {
                                //adiciona na lista de matérias do curso
                                lblMCurso.Items.Add(sql.guarda[0].ToString() + " - " + sql.guarda[1].ToString() + " - " + sql.guarda[2].ToString());
                            }

                            //fecha as conexões
                            sql.guarda.Close();
                            sql.olecon.Close();
                        }

                        //preenche a lista de matérias disponíveis
                        sql.pesquisar("select * from tab_materia where id_materia != '666' AND id_materia != '999'");

                        //escreve um por um na lista
                        while (sql.guarda.Read())
                        {
                            lblMDisp.Items.Add(sql.guarda[0].ToString() + " - " + sql.guarda[1].ToString() + " - " + sql.guarda[2].ToString());
                        }

                        //fecha as conexões
                        sql.guarda.Close();
                        sql.olecon.Close();

                        //procura materia por materia aquela que já tem na lista de matérias do curso
                        for (int i = 0; i < lblMDisp.Items.Count; i++)
                        {
                            //seleciona o código da matéria
                            string curso = lblMDisp.Items[i].ToString();
                            string codigo = "";
                            string letra = "";
                            int index = 0;
                            while (letra != " ")
                            {
                                codigo += letra;
                                letra = curso.Substring(index, 1);
                                index++;
                            }

                            //compara com as matérias já do curso
                            for (int y = 0; y < codigos.Length; y++)
                            {
                                //se achar...
                                if (codigo == codigos[y])
                                {
                                    //...deleta...
                                    lblMDisp.Items.RemoveAt(i);
                                    //ajusta primeiro laço
                                    i--;
                                    //...e fecha o laço
                                    y = codigos.Length;
                                }
                            }
                        }
                    }
                    else
                    {
                        //fecha as conexões
                        sql.guarda.Close();
                        sql.olecon.Close();
                        //avisa se o curso não foi encontrado
                        MessageBox.Show("O curso '" + cbcurso.Text + " - " + numano.Text + " ano' não foi encontrado.", "*** ERRO ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        button4_Click(sender, e);
                    }
                }
                catch
                {
                    //informa que há um erro de conexão com o banco de dados
                    MessageBox.Show("Conexão falha.", "*** ERRO ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    button4_Click(sender, e);
                }
            }
            else
            {
                //pede ao usuário para preencher
                MessageBox.Show("Escreva um curso válido. \nVocê pesquisou por: " + cbcurso.Text + " - " + numano.Text, "*** ERRO ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
                button4_Click(sender, e);
            }
        }

        private void pbsetadir_Click(object sender, EventArgs e)
        {
            //confere se há algo selecionado
            if (lblMDisp.SelectedIndex != -1)
            {
                //transfere pra lista de selecionados
                lblMCurso.Items.Add(lblMDisp.SelectedItem.ToString());

                //deleta da lista de disponíveis
                lblMDisp.Items.RemoveAt(lblMDisp.SelectedIndex);
            }
        }

        private void pbsetaesq_Click(object sender, EventArgs e)
        {
            //confere se há algo selecionado
            if (lblMCurso.SelectedIndex != -1)
            {
                //transfere pra lista de disponíveis
                lblMDisp.Items.Add(lblMCurso.SelectedItem.ToString());

                //deleta da lista de selecionados
                lblMCurso.Items.RemoveAt(lblMCurso.SelectedIndex);
            }
        }

        private void pblimparformulario_Click(object sender, EventArgs e)
        {
            //limpa os campos
            cbcurso.Items.Clear();
            cbcurso.Text = "";
            txtidcurso.Clear();
            numano.Value = 1;
            lblMDisp.Items.Clear();
            lblMCurso.Items.Clear();

            //disponibiliza os campos de busca
            cbcurso.Enabled = true;
            numano.Enabled = true;

            //desativa o que não há o que ser mudado
            txtidcurso.Enabled = false;
            lblMCurso.Enabled = false;
            lblMDisp.Enabled = false;
            pbsetadir.Enabled = false;
            pbsetaesq.Enabled = false;

            //foca na combobox
            cbcurso.Focus();

            //repreenche combobox
            AlterarCurso_Load(sender, e);
        }

        private void pbdeletar_Click(object sender, EventArgs e)
        {
            if (cbcurso.Enabled == true)
            {
                //pergunta ao usuário se ele tem certeza do ato
                DialogResult YN = MessageBox.Show("Deseja mesmo excluir este curso e suas respectivas aulas?", "*** Exclusão ***", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                //confere a resposta
                if (YN == DialogResult.Yes)
                {
                    //fecha as conexões
                    sql.guarda.Close();
                    sql.olecon.Close();

                    SQLDeletes.DeletarCurso(txtidcurso.Text);

                    //retorna ao usuário o sucesso da exclusão
                    DialogResult SN = MessageBox.Show("Exclusão feita com sucesso! \nDeseja limpar o formulário?", "*** Alteração ***", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    //testa a resposta do usuário
                    if (SN == DialogResult.Yes)
                    {
                        //prepara o form para um novo cadastro
                        button4_Click(sender, e);
                    }
                }
            }
            else
            {
                //pede ao usuário localizar o curso
                MessageBox.Show("Localize o curso primeiro", "*** ERRO ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
