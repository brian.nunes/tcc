﻿namespace softwarecordenacao
{
    partial class Cordenacao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.organiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selecionarProfessoresPorMatériaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selecionarProfessoresPorTurmaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.prencherHoráriosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadasteoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastroDeMatériaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastroDeCursoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastroDeProfessorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alteraçãoEmMatériaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.alteraçãoEmTurmaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alteraçãoEmCursoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.horárioDasTurmasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.organiToolStripMenuItem,
            this.cadastrarToolStripMenuItem,
            this.aToolStripMenuItem,
            this.horárioDasTurmasToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(9, 3, 0, 3);
            this.menuStrip1.Size = new System.Drawing.Size(809, 35);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // organiToolStripMenuItem
            // 
            this.organiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selecionarProfessoresPorMatériaToolStripMenuItem,
            this.selecionarProfessoresPorTurmaToolStripMenuItem,
            this.prencherHoráriosToolStripMenuItem});
            this.organiToolStripMenuItem.Name = "organiToolStripMenuItem";
            this.organiToolStripMenuItem.Size = new System.Drawing.Size(101, 29);
            this.organiToolStripMenuItem.Text = "Organizar";
            this.organiToolStripMenuItem.Click += new System.EventHandler(this.organiToolStripMenuItem_Click);
            // 
            // selecionarProfessoresPorMatériaToolStripMenuItem
            // 
            this.selecionarProfessoresPorMatériaToolStripMenuItem.Name = "selecionarProfessoresPorMatériaToolStripMenuItem";
            this.selecionarProfessoresPorMatériaToolStripMenuItem.Size = new System.Drawing.Size(371, 30);
            this.selecionarProfessoresPorMatériaToolStripMenuItem.Text = "Selecionar professores por matéria";
            this.selecionarProfessoresPorMatériaToolStripMenuItem.Click += new System.EventHandler(this.selecionarProfessoresPorMatériaToolStripMenuItem_Click);
            // 
            // selecionarProfessoresPorTurmaToolStripMenuItem
            // 
            this.selecionarProfessoresPorTurmaToolStripMenuItem.Name = "selecionarProfessoresPorTurmaToolStripMenuItem";
            this.selecionarProfessoresPorTurmaToolStripMenuItem.Size = new System.Drawing.Size(371, 30);
            this.selecionarProfessoresPorTurmaToolStripMenuItem.Text = "Selecionar professores por turma";
            this.selecionarProfessoresPorTurmaToolStripMenuItem.Click += new System.EventHandler(this.selecionarProfessoresPorTurmaToolStripMenuItem_Click);
            // 
            // prencherHoráriosToolStripMenuItem
            // 
            this.prencherHoráriosToolStripMenuItem.Name = "prencherHoráriosToolStripMenuItem";
            this.prencherHoráriosToolStripMenuItem.Size = new System.Drawing.Size(371, 30);
            this.prencherHoráriosToolStripMenuItem.Text = "Prencher horários";
            this.prencherHoráriosToolStripMenuItem.Click += new System.EventHandler(this.prencherHoráriosToolStripMenuItem_Click);
            // 
            // cadastrarToolStripMenuItem
            // 
            this.cadastrarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadasteoToolStripMenuItem,
            this.cadastroDeMatériaToolStripMenuItem,
            this.cadastroDeCursoToolStripMenuItem,
            this.cadastroDeProfessorToolStripMenuItem});
            this.cadastrarToolStripMenuItem.Name = "cadastrarToolStripMenuItem";
            this.cadastrarToolStripMenuItem.Size = new System.Drawing.Size(99, 29);
            this.cadastrarToolStripMenuItem.Text = "Cadastrar";
            // 
            // cadasteoToolStripMenuItem
            // 
            this.cadasteoToolStripMenuItem.Name = "cadasteoToolStripMenuItem";
            this.cadasteoToolStripMenuItem.Size = new System.Drawing.Size(273, 30);
            this.cadasteoToolStripMenuItem.Text = "Cadastro de turma";
            this.cadasteoToolStripMenuItem.Click += new System.EventHandler(this.cadasteoToolStripMenuItem_Click);
            // 
            // cadastroDeMatériaToolStripMenuItem
            // 
            this.cadastroDeMatériaToolStripMenuItem.Name = "cadastroDeMatériaToolStripMenuItem";
            this.cadastroDeMatériaToolStripMenuItem.Size = new System.Drawing.Size(273, 30);
            this.cadastroDeMatériaToolStripMenuItem.Text = "Cadastro de matéria";
            this.cadastroDeMatériaToolStripMenuItem.Click += new System.EventHandler(this.cadastroDeMatériaToolStripMenuItem_Click);
            // 
            // cadastroDeCursoToolStripMenuItem
            // 
            this.cadastroDeCursoToolStripMenuItem.Name = "cadastroDeCursoToolStripMenuItem";
            this.cadastroDeCursoToolStripMenuItem.Size = new System.Drawing.Size(273, 30);
            this.cadastroDeCursoToolStripMenuItem.Text = "Cadastro de curso";
            this.cadastroDeCursoToolStripMenuItem.Click += new System.EventHandler(this.cadastroDeCursoToolStripMenuItem_Click);
            // 
            // cadastroDeProfessorToolStripMenuItem
            // 
            this.cadastroDeProfessorToolStripMenuItem.Name = "cadastroDeProfessorToolStripMenuItem";
            this.cadastroDeProfessorToolStripMenuItem.Size = new System.Drawing.Size(273, 30);
            this.cadastroDeProfessorToolStripMenuItem.Text = "Cadastro de professor";
            this.cadastroDeProfessorToolStripMenuItem.Click += new System.EventHandler(this.cadastroDeProfessorToolStripMenuItem_Click);
            // 
            // aToolStripMenuItem
            // 
            this.aToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.alteraçãoEmMatériaToolStripMenuItem,
            this.toolStripMenuItem2,
            this.alteraçãoEmTurmaToolStripMenuItem,
            this.alteraçãoEmCursoToolStripMenuItem});
            this.aToolStripMenuItem.Name = "aToolStripMenuItem";
            this.aToolStripMenuItem.Size = new System.Drawing.Size(149, 29);
            this.aToolStripMenuItem.Text = "Alterar cadastro";
            // 
            // alteraçãoEmMatériaToolStripMenuItem
            // 
            this.alteraçãoEmMatériaToolStripMenuItem.Name = "alteraçãoEmMatériaToolStripMenuItem";
            this.alteraçãoEmMatériaToolStripMenuItem.Size = new System.Drawing.Size(281, 30);
            this.alteraçãoEmMatériaToolStripMenuItem.Text = "Alteração em matéria";
            this.alteraçãoEmMatériaToolStripMenuItem.Click += new System.EventHandler(this.alteraçãoEmMatériaToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(281, 30);
            this.toolStripMenuItem2.Text = "Alteração em turma";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // alteraçãoEmTurmaToolStripMenuItem
            // 
            this.alteraçãoEmTurmaToolStripMenuItem.Name = "alteraçãoEmTurmaToolStripMenuItem";
            this.alteraçãoEmTurmaToolStripMenuItem.Size = new System.Drawing.Size(281, 30);
            this.alteraçãoEmTurmaToolStripMenuItem.Text = "Alteração em professor";
            this.alteraçãoEmTurmaToolStripMenuItem.Click += new System.EventHandler(this.alteraçãoEmTurmaToolStripMenuItem_Click);
            // 
            // alteraçãoEmCursoToolStripMenuItem
            // 
            this.alteraçãoEmCursoToolStripMenuItem.Name = "alteraçãoEmCursoToolStripMenuItem";
            this.alteraçãoEmCursoToolStripMenuItem.Size = new System.Drawing.Size(281, 30);
            this.alteraçãoEmCursoToolStripMenuItem.Text = "Alteração em curso";
            this.alteraçãoEmCursoToolStripMenuItem.Click += new System.EventHandler(this.alteraçãoEmCursoToolStripMenuItem_Click);
            // 
            // horárioDasTurmasToolStripMenuItem
            // 
            this.horárioDasTurmasToolStripMenuItem.Name = "horárioDasTurmasToolStripMenuItem";
            this.horárioDasTurmasToolStripMenuItem.Size = new System.Drawing.Size(180, 29);
            this.horárioDasTurmasToolStripMenuItem.Text = "Horário das Turmas";
            this.horárioDasTurmasToolStripMenuItem.Click += new System.EventHandler(this.horárioDasTurmasToolStripMenuItem_Click);
            // 
            // Cordenacao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(809, 692);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Cordenacao";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Home";
            this.TransparencyKey = System.Drawing.Color.Transparent;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Cordenacao_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem organiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selecionarProfessoresPorMatériaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selecionarProfessoresPorTurmaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem prencherHoráriosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadasteoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastroDeMatériaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastroDeCursoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastroDeProfessorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alteraçãoEmMatériaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem alteraçãoEmTurmaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alteraçãoEmCursoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem horárioDasTurmasToolStripMenuItem;
    }
}

