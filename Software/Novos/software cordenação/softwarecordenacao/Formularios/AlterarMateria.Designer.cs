﻿namespace softwarecordenacao.Formularios
{
    partial class AlterarMateria
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AlterarMateria));
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pbconfirmaralteracoes = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.txtsigla = new System.Windows.Forms.TextBox();
            this.txtnome = new System.Windows.Forms.TextBox();
            this.txtidmateria = new System.Windows.Forms.TextBox();
            this.numaulas = new System.Windows.Forms.NumericUpDown();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pblimparformulario = new System.Windows.Forms.PictureBox();
            this.pbdeletar = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbconfirmaralteracoes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numaulas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pblimparformulario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbdeletar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.DarkGreen;
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(99, -9);
            this.pictureBox6.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(416, 166);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 33;
            this.pictureBox6.TabStop = false;
            // 
            // pbconfirmaralteracoes
            // 
            this.pbconfirmaralteracoes.BackColor = System.Drawing.Color.White;
            this.pbconfirmaralteracoes.Image = ((System.Drawing.Image)(resources.GetObject("pbconfirmaralteracoes.Image")));
            this.pbconfirmaralteracoes.Location = new System.Drawing.Point(417, 552);
            this.pbconfirmaralteracoes.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbconfirmaralteracoes.Name = "pbconfirmaralteracoes";
            this.pbconfirmaralteracoes.Size = new System.Drawing.Size(189, 78);
            this.pbconfirmaralteracoes.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbconfirmaralteracoes.TabIndex = 32;
            this.pbconfirmaralteracoes.TabStop = false;
            this.pbconfirmaralteracoes.Click += new System.EventHandler(this.pbconfirmaralteracoes_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(16, 428);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(230, 49);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 31;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(16, 220);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(110, 49);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 30;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(16, 358);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(110, 49);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 29;
            this.pictureBox3.TabStop = false;
            // 
            // txtsigla
            // 
            this.txtsigla.Location = new System.Drawing.Point(146, 228);
            this.txtsigla.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtsigla.Name = "txtsigla";
            this.txtsigla.Size = new System.Drawing.Size(295, 26);
            this.txtsigla.TabIndex = 27;
            // 
            // txtnome
            // 
            this.txtnome.Enabled = false;
            this.txtnome.Location = new System.Drawing.Point(146, 368);
            this.txtnome.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtnome.Name = "txtnome";
            this.txtnome.Size = new System.Drawing.Size(422, 26);
            this.txtnome.TabIndex = 26;
            // 
            // txtidmateria
            // 
            this.txtidmateria.Enabled = false;
            this.txtidmateria.Location = new System.Drawing.Point(165, 302);
            this.txtidmateria.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtidmateria.Name = "txtidmateria";
            this.txtidmateria.Size = new System.Drawing.Size(403, 26);
            this.txtidmateria.TabIndex = 40;
            // 
            // numaulas
            // 
            this.numaulas.Enabled = false;
            this.numaulas.Location = new System.Drawing.Point(268, 438);
            this.numaulas.Maximum = new decimal(new int[] {
            45,
            0,
            0,
            0});
            this.numaulas.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numaulas.Name = "numaulas";
            this.numaulas.Size = new System.Drawing.Size(302, 26);
            this.numaulas.TabIndex = 41;
            this.numaulas.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(452, 206);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(118, 68);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 52;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // pblimparformulario
            // 
            this.pblimparformulario.BackColor = System.Drawing.Color.White;
            this.pblimparformulario.Image = ((System.Drawing.Image)(resources.GetObject("pblimparformulario.Image")));
            this.pblimparformulario.Location = new System.Drawing.Point(216, 552);
            this.pblimparformulario.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pblimparformulario.Name = "pblimparformulario";
            this.pblimparformulario.Size = new System.Drawing.Size(192, 78);
            this.pblimparformulario.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pblimparformulario.TabIndex = 53;
            this.pblimparformulario.TabStop = false;
            this.pblimparformulario.Click += new System.EventHandler(this.pblimparformulario_Click);
            // 
            // pbdeletar
            // 
            this.pbdeletar.BackColor = System.Drawing.Color.White;
            this.pbdeletar.Image = ((System.Drawing.Image)(resources.GetObject("pbdeletar.Image")));
            this.pbdeletar.Location = new System.Drawing.Point(16, 552);
            this.pbdeletar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbdeletar.Name = "pbdeletar";
            this.pbdeletar.Size = new System.Drawing.Size(190, 78);
            this.pbdeletar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbdeletar.TabIndex = 54;
            this.pbdeletar.TabStop = false;
            this.pbdeletar.Click += new System.EventHandler(this.pbdeletar_Click);
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(16, 289);
            this.pictureBox8.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(140, 49);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 55;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.Color.DarkGreen;
            this.pictureBox9.Location = new System.Drawing.Point(-12, -9);
            this.pictureBox9.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(112, 166);
            this.pictureBox9.TabIndex = 56;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.Color.DarkGreen;
            this.pictureBox10.Location = new System.Drawing.Point(508, -9);
            this.pictureBox10.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(112, 166);
            this.pictureBox10.TabIndex = 57;
            this.pictureBox10.TabStop = false;
            // 
            // AlterarMateria
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(616, 649);
            this.Controls.Add(this.pictureBox10);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pbdeletar);
            this.Controls.Add(this.pblimparformulario);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.numaulas);
            this.Controls.Add(this.txtidmateria);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pbconfirmaralteracoes);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.txtsigla);
            this.Controls.Add(this.txtnome);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "AlterarMateria";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Alterar matéria";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbconfirmaralteracoes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numaulas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pblimparformulario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbdeletar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pbconfirmaralteracoes;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.TextBox txtsigla;
        private System.Windows.Forms.TextBox txtnome;
        private System.Windows.Forms.TextBox txtidmateria;
        private System.Windows.Forms.NumericUpDown numaulas;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pblimparformulario;
        private System.Windows.Forms.PictureBox pbdeletar;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox10;
    }
}