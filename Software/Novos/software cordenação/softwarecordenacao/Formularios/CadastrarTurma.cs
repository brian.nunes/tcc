﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace softwarecordenacao.Formularios
{
    public partial class CadastrarTurma : Form
    {
        public CadastrarTurma()
        {
            InitializeComponent();
        }

        private void pbcadastrar_Click(object sender, EventArgs e)
        {
            //confere se foi tudo preenchido
            if (txtidturma.Text != "" && txtletra.Text != "" && txtano.Text != "" && comboBox1.Items.Count > 0)
            {
                //confere se o id, letra e numero juntos nunca foi usado
                sql.pesquisar("select * from tab_classe where id_classe ='" + txtidturma.Text + "' OR ano = '"+txtano.Text+"' AND letra = '"+txtletra.Text+"';");
                if (sql.guarda.Read())
                {
                    //avisa se for o caso
                    MessageBox.Show("ID da turma, ou classe já cadastradada!", "*** Erro ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //fecha as conexões
                    sql.guarda.Close();
                    sql.olecon.Close();
                }
                else
                {
                    //fecha as conexões
                    sql.guarda.Close();
                    sql.olecon.Close();
                    //confere se as senhas batem
                    try
                    {
                        //seleciona o código do curso
                        string curso = comboBox1.Text;
                        string codigo = "";
                        string letra = "";
                        int index = 0;
                        while (letra != " ")
                        {
                            codigo += letra;
                            letra = curso.Substring(index, 1);
                            index++;
                        }

                        //faz a inserção no banco de dados
                        sql.executarcomando("INSERT INTO `tab_classe` (`id_classe`, `letra`, `ano`, `fk_curso`) VALUES ('" + txtidturma.Text + "', '" + txtletra.Text + "', '" + txtano.Text + "', '" + codigo + "')", "Inserção");

                        //da o feed back ao usuario
                        MessageBox.Show("Cadastro feito com sucesso!", "*** Cadastro ***", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //prepara o form para um novo cadastro
                        txtidturma.Clear();
                        txtletra.Clear();
                        txtano.Clear();
                        comboBox1.Items.Clear();
                        CadastrarTurma_Load(sender, e);
                        txtidturma.Focus();
                    }
                    catch
                    {
                        //mensagem de erro caso ocorra algum erro coma inserção do cadastro
                        MessageBox.Show("Cadastro não pode ser efetuado!", "*** Erro ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
            {
                //avisa ao usuario que existem campos não preenchidos
                MessageBox.Show("Preencha todos os campos!", "*** Erro ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CadastrarTurma_Load(object sender, EventArgs e)
        {
            //preencher lista de cursos disponíveis
            sql.pesquisar("select * from tab_curso where id_curso != '666' AND id_curso != '999'");

            //escreve um por um na combobox
            while (sql.guarda.Read())
            {
                comboBox1.Items.Add(sql.guarda[0].ToString() + " - " + sql.guarda[1].ToString() + " - " + sql.guarda[2].ToString() + " ano");
            }

            //fecha as conexões
            sql.guarda.Close();
            sql.olecon.Close();
        }
    }
}
