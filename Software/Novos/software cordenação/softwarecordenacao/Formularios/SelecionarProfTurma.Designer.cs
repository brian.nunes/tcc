﻿namespace softwarecordenacao
{
    partial class SelecionarProfTurma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SelecionarProfTurma));
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pbcancelar = new System.Windows.Forms.PictureBox();
            this.pbconfirmar = new System.Windows.Forms.PictureBox();
            this.pbsetadir = new System.Windows.Forms.PictureBox();
            this.pbsetaesq = new System.Windows.Forms.PictureBox();
            this.lbPerm = new System.Windows.Forms.ListBox();
            this.lbTemp = new System.Windows.Forms.ListBox();
            this.cbturma = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.cbmateria = new System.Windows.Forms.ComboBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbcancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbconfirmar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsetadir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsetaesq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.DarkGreen;
            this.pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(79, -1);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(323, 142);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 32;
            this.pictureBox3.TabStop = false;
            // 
            // pbcancelar
            // 
            this.pbcancelar.Image = ((System.Drawing.Image)(resources.GetObject("pbcancelar.Image")));
            this.pbcancelar.Location = new System.Drawing.Point(48, 517);
            this.pbcancelar.Name = "pbcancelar";
            this.pbcancelar.Size = new System.Drawing.Size(137, 37);
            this.pbcancelar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbcancelar.TabIndex = 31;
            this.pbcancelar.TabStop = false;
            this.pbcancelar.Click += new System.EventHandler(this.pbcancelar_Click);
            // 
            // pbconfirmar
            // 
            this.pbconfirmar.Image = ((System.Drawing.Image)(resources.GetObject("pbconfirmar.Image")));
            this.pbconfirmar.Location = new System.Drawing.Point(304, 517);
            this.pbconfirmar.Name = "pbconfirmar";
            this.pbconfirmar.Size = new System.Drawing.Size(139, 37);
            this.pbconfirmar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbconfirmar.TabIndex = 30;
            this.pbconfirmar.TabStop = false;
            this.pbconfirmar.Click += new System.EventHandler(this.pbconfirmar_Click);
            // 
            // pbsetadir
            // 
            this.pbsetadir.Enabled = false;
            this.pbsetadir.Image = ((System.Drawing.Image)(resources.GetObject("pbsetadir.Image")));
            this.pbsetadir.Location = new System.Drawing.Point(210, 365);
            this.pbsetadir.Name = "pbsetadir";
            this.pbsetadir.Size = new System.Drawing.Size(65, 25);
            this.pbsetadir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbsetadir.TabIndex = 29;
            this.pbsetadir.TabStop = false;
            this.pbsetadir.Click += new System.EventHandler(this.pbsetadir_Click);
            // 
            // pbsetaesq
            // 
            this.pbsetaesq.Enabled = false;
            this.pbsetaesq.Image = ((System.Drawing.Image)(resources.GetObject("pbsetaesq.Image")));
            this.pbsetaesq.Location = new System.Drawing.Point(210, 425);
            this.pbsetaesq.Name = "pbsetaesq";
            this.pbsetaesq.Size = new System.Drawing.Size(65, 25);
            this.pbsetaesq.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbsetaesq.TabIndex = 28;
            this.pbsetaesq.TabStop = false;
            this.pbsetaesq.Click += new System.EventHandler(this.pbsetaesq_Click);
            // 
            // lbPerm
            // 
            this.lbPerm.Enabled = false;
            this.lbPerm.ForeColor = System.Drawing.Color.DarkGreen;
            this.lbPerm.FormattingEnabled = true;
            this.lbPerm.HorizontalScrollbar = true;
            this.lbPerm.Location = new System.Drawing.Point(282, 324);
            this.lbPerm.Name = "lbPerm";
            this.lbPerm.Size = new System.Drawing.Size(183, 173);
            this.lbPerm.TabIndex = 27;
            // 
            // lbTemp
            // 
            this.lbTemp.BackColor = System.Drawing.Color.White;
            this.lbTemp.Enabled = false;
            this.lbTemp.ForeColor = System.Drawing.Color.DarkGreen;
            this.lbTemp.FormattingEnabled = true;
            this.lbTemp.HorizontalScrollbar = true;
            this.lbTemp.Location = new System.Drawing.Point(31, 324);
            this.lbTemp.Name = "lbTemp";
            this.lbTemp.Size = new System.Drawing.Size(173, 173);
            this.lbTemp.TabIndex = 26;
            // 
            // cbturma
            // 
            this.cbturma.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbturma.FormattingEnabled = true;
            this.cbturma.Location = new System.Drawing.Point(123, 179);
            this.cbturma.Name = "cbturma";
            this.cbturma.Size = new System.Drawing.Size(342, 21);
            this.cbturma.TabIndex = 34;
            this.cbturma.SelectedIndexChanged += new System.EventHandler(this.cbturma_SelectedIndexChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(30, 174);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(80, 29);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 38;
            this.pictureBox1.TabStop = false;
            // 
            // cbmateria
            // 
            this.cbmateria.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbmateria.Enabled = false;
            this.cbmateria.FormattingEnabled = true;
            this.cbmateria.Location = new System.Drawing.Point(123, 218);
            this.cbmateria.Name = "cbmateria";
            this.cbmateria.Size = new System.Drawing.Size(342, 21);
            this.cbmateria.TabIndex = 40;
            this.cbmateria.SelectedIndexChanged += new System.EventHandler(this.cbmateria_SelectedIndexChanged);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(282, 272);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(183, 46);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 41;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(31, 272);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(173, 46);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 42;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(30, 215);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(80, 29);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 43;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.DarkGreen;
            this.pictureBox6.Location = new System.Drawing.Point(-3, -1);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(85, 142);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 44;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.Color.DarkGreen;
            this.pictureBox7.Location = new System.Drawing.Point(402, -1);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(100, 142);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 45;
            this.pictureBox7.TabStop = false;
            // 
            // SelecionarProfTurma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(499, 569);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.cbmateria);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.cbturma);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pbcancelar);
            this.Controls.Add(this.pbconfirmar);
            this.Controls.Add(this.pbsetadir);
            this.Controls.Add(this.pbsetaesq);
            this.Controls.Add(this.lbPerm);
            this.Controls.Add(this.lbTemp);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "SelecionarProfTurma";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Selecionar professor por turma";
            this.Load += new System.EventHandler(this.SelecionarProfTurma_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbcancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbconfirmar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsetadir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsetaesq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pbcancelar;
        private System.Windows.Forms.PictureBox pbconfirmar;
        private System.Windows.Forms.PictureBox pbsetadir;
        private System.Windows.Forms.PictureBox pbsetaesq;
        private System.Windows.Forms.ListBox lbPerm;
        private System.Windows.Forms.ListBox lbTemp;
        private System.Windows.Forms.ComboBox cbturma;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ComboBox cbmateria;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
    }
}