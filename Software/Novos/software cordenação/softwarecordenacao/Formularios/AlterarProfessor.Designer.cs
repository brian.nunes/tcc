﻿namespace softwarecordenacao.Formularios
{
    partial class AlterarProfessor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AlterarProfessor));
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pbconfirmaralteracoes = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtsigla = new System.Windows.Forms.TextBox();
            this.txtconfirmasenha = new System.Windows.Forms.TextBox();
            this.txtsenha = new System.Windows.Forms.TextBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.txtnomeprof = new System.Windows.Forms.TextBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.txtidprofessor = new System.Windows.Forms.TextBox();
            this.numpref = new System.Windows.Forms.NumericUpDown();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pblimparformulario = new System.Windows.Forms.PictureBox();
            this.pbdeletar = new System.Windows.Forms.PictureBox();
            this.pblocalizar = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbconfirmaralteracoes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numpref)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pblimparformulario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbdeletar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pblocalizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(18, 631);
            this.pictureBox7.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(150, 54);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 43;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(18, 272);
            this.pictureBox6.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(106, 54);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 42;
            this.pictureBox6.TabStop = false;
            // 
            // pbconfirmaralteracoes
            // 
            this.pbconfirmaralteracoes.BackColor = System.Drawing.Color.DarkGreen;
            this.pbconfirmaralteracoes.Image = ((System.Drawing.Image)(resources.GetObject("pbconfirmaralteracoes.Image")));
            this.pbconfirmaralteracoes.Location = new System.Drawing.Point(492, 754);
            this.pbconfirmaralteracoes.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbconfirmaralteracoes.Name = "pbconfirmaralteracoes";
            this.pbconfirmaralteracoes.Size = new System.Drawing.Size(224, 71);
            this.pbconfirmaralteracoes.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbconfirmaralteracoes.TabIndex = 39;
            this.pbconfirmaralteracoes.TabStop = false;
            this.pbconfirmaralteracoes.Click += new System.EventHandler(this.pbconfirmaralteracoes_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.DarkGreen;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(122, -3);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(495, 200);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 38;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(18, 488);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(106, 54);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 37;
            this.pictureBox1.TabStop = false;
            // 
            // txtsigla
            // 
            this.txtsigla.Location = new System.Drawing.Point(153, 289);
            this.txtsigla.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtsigla.Name = "txtsigla";
            this.txtsigla.Size = new System.Drawing.Size(372, 26);
            this.txtsigla.TabIndex = 35;
            // 
            // txtconfirmasenha
            // 
            this.txtconfirmasenha.Enabled = false;
            this.txtconfirmasenha.Location = new System.Drawing.Point(177, 571);
            this.txtconfirmasenha.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtconfirmasenha.Name = "txtconfirmasenha";
            this.txtconfirmasenha.PasswordChar = '*';
            this.txtconfirmasenha.Size = new System.Drawing.Size(492, 26);
            this.txtconfirmasenha.TabIndex = 34;
            // 
            // txtsenha
            // 
            this.txtsenha.Enabled = false;
            this.txtsenha.Location = new System.Drawing.Point(153, 503);
            this.txtsenha.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtsenha.Name = "txtsenha";
            this.txtsenha.PasswordChar = '*';
            this.txtsenha.Size = new System.Drawing.Size(516, 26);
            this.txtsenha.TabIndex = 33;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(18, 418);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(106, 51);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 44;
            this.pictureBox4.TabStop = false;
            // 
            // txtnomeprof
            // 
            this.txtnomeprof.Enabled = false;
            this.txtnomeprof.Location = new System.Drawing.Point(153, 434);
            this.txtnomeprof.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtnomeprof.Name = "txtnomeprof";
            this.txtnomeprof.Size = new System.Drawing.Size(516, 26);
            this.txtnomeprof.TabIndex = 45;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.DarkGreen;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(18, 348);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(106, 51);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 40;
            this.pictureBox2.TabStop = false;
            // 
            // txtidprofessor
            // 
            this.txtidprofessor.Enabled = false;
            this.txtidprofessor.Location = new System.Drawing.Point(153, 363);
            this.txtidprofessor.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtidprofessor.Name = "txtidprofessor";
            this.txtidprofessor.Size = new System.Drawing.Size(516, 26);
            this.txtidprofessor.TabIndex = 32;
            // 
            // numpref
            // 
            this.numpref.Enabled = false;
            this.numpref.Location = new System.Drawing.Point(177, 646);
            this.numpref.Name = "numpref";
            this.numpref.Size = new System.Drawing.Size(494, 26);
            this.numpref.TabIndex = 48;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(18, 562);
            this.pictureBox5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(150, 54);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 51;
            this.pictureBox5.TabStop = false;
            // 
            // pblimparformulario
            // 
            this.pblimparformulario.BackColor = System.Drawing.Color.DarkGreen;
            this.pblimparformulario.Image = ((System.Drawing.Image)(resources.GetObject("pblimparformulario.Image")));
            this.pblimparformulario.Location = new System.Drawing.Point(255, 754);
            this.pblimparformulario.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pblimparformulario.Name = "pblimparformulario";
            this.pblimparformulario.Size = new System.Drawing.Size(224, 71);
            this.pblimparformulario.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pblimparformulario.TabIndex = 52;
            this.pblimparformulario.TabStop = false;
            this.pblimparformulario.Click += new System.EventHandler(this.pblimparformulario_Click);
            // 
            // pbdeletar
            // 
            this.pbdeletar.BackColor = System.Drawing.Color.DarkGreen;
            this.pbdeletar.Image = ((System.Drawing.Image)(resources.GetObject("pbdeletar.Image")));
            this.pbdeletar.Location = new System.Drawing.Point(18, 754);
            this.pbdeletar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbdeletar.Name = "pbdeletar";
            this.pbdeletar.Size = new System.Drawing.Size(224, 71);
            this.pbdeletar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbdeletar.TabIndex = 53;
            this.pbdeletar.TabStop = false;
            this.pbdeletar.Click += new System.EventHandler(this.pbdeletar_Click);
            // 
            // pblocalizar
            // 
            this.pblocalizar.Image = ((System.Drawing.Image)(resources.GetObject("pblocalizar.Image")));
            this.pblocalizar.Location = new System.Drawing.Point(532, 271);
            this.pblocalizar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pblocalizar.Name = "pblocalizar";
            this.pblocalizar.Size = new System.Drawing.Size(138, 68);
            this.pblocalizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pblocalizar.TabIndex = 54;
            this.pblocalizar.TabStop = false;
            this.pblocalizar.Click += new System.EventHandler(this.pblocalizar_Click);
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.Color.DarkGreen;
            this.pictureBox11.Location = new System.Drawing.Point(606, -3);
            this.pictureBox11.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(126, 200);
            this.pictureBox11.TabIndex = 55;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackColor = System.Drawing.Color.DarkGreen;
            this.pictureBox12.Location = new System.Drawing.Point(-2, -3);
            this.pictureBox12.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(126, 200);
            this.pictureBox12.TabIndex = 56;
            this.pictureBox12.TabStop = false;
            // 
            // AlterarProfessor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(726, 843);
            this.Controls.Add(this.pictureBox12);
            this.Controls.Add(this.pictureBox11);
            this.Controls.Add(this.pblocalizar);
            this.Controls.Add(this.pbdeletar);
            this.Controls.Add(this.pblimparformulario);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.numpref);
            this.Controls.Add(this.txtnomeprof);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pbconfirmaralteracoes);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txtsigla);
            this.Controls.Add(this.txtconfirmasenha);
            this.Controls.Add(this.txtsenha);
            this.Controls.Add(this.txtidprofessor);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "AlterarProfessor";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Alterar professor";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbconfirmaralteracoes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numpref)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pblimparformulario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbdeletar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pblocalizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pbconfirmaralteracoes;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txtsigla;
        private System.Windows.Forms.TextBox txtconfirmasenha;
        private System.Windows.Forms.TextBox txtsenha;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.TextBox txtnomeprof;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox txtidprofessor;
        private System.Windows.Forms.NumericUpDown numpref;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pblimparformulario;
        private System.Windows.Forms.PictureBox pbdeletar;
        private System.Windows.Forms.PictureBox pblocalizar;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox12;
    }
}