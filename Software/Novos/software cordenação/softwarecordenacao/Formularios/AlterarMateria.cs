﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace softwarecordenacao.Formularios
{
    public partial class AlterarMateria : Form
    {
        public AlterarMateria()
        {
            InitializeComponent();
        }

        private void pbconfirmaralteracoes_Click(object sender, EventArgs e)
        {
            //confere se foi tudo preenchido
            if (txtnome.Text != "" && txtsigla.Text != "" && numaulas.Text != "" && txtidmateria.Text != "")
            {
                    //fecha as conexões
                    sql.guarda.Close();
                    sql.olecon.Close();
                    try
                    {
                        //faz a atualização no banco de dados
                        sql.executarcomando("UPDATE `tab_materia` SET `nome_materia` = '" + txtnome.Text + "', `num_aulas_semanais` = '" + numaulas.Value.ToString() + "' WHERE `tab_materia`.`id_materia` = " + txtidmateria.Text, " atualização");
                        //da o feed back ao usuario
                        DialogResult YN = MessageBox.Show("Alteração feita com sucesso! \nDeseja limpar o formulário?", "*** Alteração ***", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        //testa a resposta do usuário
                        if (YN == DialogResult.Yes)
                        {
                            //prepara o form para um novo cadastro
                            button2_Click(sender, e);
                        }
                    }
                    catch
                    {
                        //mensagem de erro caso ocorra algum erro coma inserção do cadastro
                        MessageBox.Show("Alteração não pode ser efetuado!", "*** Erro ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            //testa se o campo foi preenchiddo
            if (txtsigla.Text != "" && txtsigla.Text != "NDA" && txtsigla.Text != "IND")
            {
                try
                {
                    //pesquisa a matéria buscada
                    sql.pesquisar("SELECT * FROM tab_materia WHERE sigla_materia = '" + txtsigla.Text + "'");

                    //testa se encontrou
                    if (sql.guarda.Read())
                    {
                        //ativa todas as entradas de dados
                        txtnome.Enabled = true;
                        numaulas.Enabled = true;

                        //desativa a da sigla do prof
                        txtsigla.Enabled = false;

                        //preencha as mesmas com os dados do professor
                        txtidmateria.Text = sql.guarda[0].ToString();
                        txtnome.Text = sql.guarda[1].ToString();
                        numaulas.Value = Convert.ToInt32(sql.guarda[3]);
                    }
                    else
                    {
                        //avisa se o prof não foi encontrado
                        MessageBox.Show("Matéria não encontrada", "*** ERRO ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch
                {
                    //informa que há um erro de conexão com o banco de dados
                    MessageBox.Show("Conexão falha.", "*** ERRO ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                //pede ao usuário para preencher
                MessageBox.Show("Escreva uma sigla válida.", "*** ERRO ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            //fecha as conexões
            sql.guarda.Close();
            sql.olecon.Close();
        }

        private void pblimparformulario_Click(object sender, EventArgs e)
        {
            //limpa todos os campos
            txtsigla.Clear();
            txtidmateria.Clear();
            txtnome.Clear();
            numaulas.Value = 1;

            //foca no campo da sigla
            txtsigla.Focus();

            //abre a textbox para a sigla
            txtsigla.Enabled = true;

            //fecha todas as outras textbox
            txtidmateria.Enabled = false;
            txtnome.Enabled = false;
            numaulas.Enabled = false;
        }

        private void pbdeletar_Click(object sender, EventArgs e)
        {
            if (txtnome.Enabled == true)
            {
                //pergunta ao usuário se ele tem certeza do ato
                DialogResult YN = MessageBox.Show("Deseja mesmo excluir este registro?", "*** Exclusão ***", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                //confere a resposta
                if (YN == DialogResult.Yes)
                {
                    //fecha as conexões
                    sql.guarda.Close();
                    sql.olecon.Close();

                    SQLDeletes.DeletarMateria(txtidmateria.Text);

                    //retorna ao usuário o sucesso da exclusão
                    DialogResult SN = MessageBox.Show("Exclusão feita com sucesso! \nDeseja limpar o formulário?", "*** Alteração ***", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    //testa a resposta do usuário
                    if (SN == DialogResult.Yes)
                    {
                        //prepara o form para um novo cadastro
                        button2_Click(sender, e);
                    }
                }
            }
            else
            {
                //pede ao usuário localizar a turma
                MessageBox.Show("Localize a matéria primeiro", "*** ERRO ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}