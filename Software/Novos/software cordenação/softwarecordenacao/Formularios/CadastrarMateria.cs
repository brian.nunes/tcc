﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace softwarecordenacao.Formularios
{
    public partial class CadastrarMateria : Form
    {
        public CadastrarMateria()
        {
            InitializeComponent();
        }

        private void pbcadastrarmateria_Click(object sender, EventArgs e)
        {
            //confere se foi tudo preenchido
            if (txtidmateria.Text != "" && txtnome.Text != "" && txtsigla.Text != "" && txtnumdeaulas.Text != "")
            {
                //confere se o id ou sigla nunca foi usado
                sql.pesquisar("select * from tab_materia where id_materia ='" + txtidmateria.Text + "' OR sigla_materia = '" + txtsigla.Text + "';");
                if (sql.guarda.Read())
                {
                    //avisa se for o caso
                    MessageBox.Show("ID de matéria ou sigla já cadastrada!", "*** Erro ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //fecha as conexões
                    sql.guarda.Close();
                    sql.olecon.Close();
                }
                else
                {
                    //fecha as conexões
                    sql.guarda.Close();
                    sql.olecon.Close();
                    //confere se as senhas batem
                    try
                    {
                        //faz a inserção no banco de dados
                        sql.executarcomando("INSERT INTO `tab_materia` (`id_materia`, `nome_materia`, `sigla_materia`, `num_aulas_semanais`) VALUES ('" + txtidmateria.Text + "', '" + txtnome.Text + "', '" + txtsigla.Text + "', '" + txtnumdeaulas.Text + "')", "Inserção");
                        //da o feed back ao usuario
                        MessageBox.Show("Cadastro feito com sucesso!", "*** Cadastro ***", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //prepara o form para um novo cadastro
                        txtidmateria.Clear();
                        txtnome.Clear();
                        txtsigla.Clear();
                        txtnumdeaulas.Clear();
                        txtnome.Focus();
                    }
                    catch
                    {
                        //mensagem de erro caso ocorra algum erro coma inserção do cadastro
                        MessageBox.Show("Cadastro não pode ser efetuado!", "*** Erro ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
            {
                //avisa ao usuario que existem campos não preenchidos
                MessageBox.Show("Preencha todos os campos!", "*** Erro ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
