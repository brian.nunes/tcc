﻿using softwarecordenacao.Formularios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace softwarecordenacao
{
    public partial class Cordenacao : Form
    {
        SelecionarProfMateria FormProfMat;
        SelecionarProfTurma FormProfTurma;
        CadastrarCurso FormNovoCurso;
        CadastrarMateria FormNovaMateria;
        CadastrarTurma FormNovaTurma;
        CadastrarProfessor FormNovoProf;
        AlterarProfessor FormAlterarProf;
        AlterarMateria FormAlterarMateria;
        AlterarTurma FormAlterarTurma;
        AlterarCurso FormAlterarCurso;
        PreencherHorarios FormHorarios;
        FrmHorarioTurmas FrmHorarioTurmas;

        public CadastrarCurso FormNovoCurso1 { get => FormNovoCurso; set => FormNovoCurso = value; }

        public Cordenacao()
        {
            InitializeComponent();
        }

        private void organiToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void alteraçãoEmMatériaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (FormAlterarMateria == null || FormAlterarMateria.IsDisposed)
            {
                FormAlterarMateria = new AlterarMateria();
                FormAlterarMateria.MdiParent = this;
                FormAlterarMateria.Show();
            }
            else
            {
                FormAlterarMateria.BringToFront();
            }
        }

        private void alteraçãoEmTurmaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (FormAlterarProf == null || FormAlterarProf.IsDisposed)
            {
                FormAlterarProf = new AlterarProfessor();
                FormAlterarProf.MdiParent = this;
                FormAlterarProf.Show();
            }
            else
            {
                FormAlterarProf.BringToFront();
            }
        }

        private void selecionarProfessoresPorMatériaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (FormProfMat == null || FormProfMat.IsDisposed)
            {
                FormProfMat = new SelecionarProfMateria();
                FormProfMat.MdiParent = this;
                FormProfMat.Show();
            }
            else
            {
                FormProfMat.BringToFront();
            }
        }

        private void Cordenacao_Load(object sender, EventArgs e)
        {

        }

        private void cadasteoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (FormNovaTurma == null || FormNovaTurma.IsDisposed)
            {
                FormNovaTurma = new CadastrarTurma();
                FormNovaTurma.MdiParent = this;
                FormNovaTurma.Show();
            }
            else
            {
                FormNovaTurma.BringToFront();
            }
        }

        private void cadastroDeMatériaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (FormNovaMateria == null || FormNovaMateria.IsDisposed)
            {
                FormNovaMateria = new CadastrarMateria();
                FormNovaMateria.MdiParent = this;
                FormNovaMateria.Show();
            }
            else
            {
                FormNovaMateria.BringToFront();
            }
        }

        private void cadastroDeCursoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (FormNovoCurso == null || FormNovoCurso.IsDisposed)
            {
                FormNovoCurso = new CadastrarCurso();
                FormNovoCurso.MdiParent = this;
                FormNovoCurso.Show();
            }
            else
            {
                FormNovoCurso.BringToFront();
            }
        }

        private void cadastroDeProfessorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (FormNovoProf == null || FormNovoProf.IsDisposed)
            {
                FormNovoProf = new CadastrarProfessor();
                FormNovoProf.MdiParent = this;
                FormNovoProf.Show();
            }
            else
            {
                FormNovoProf.BringToFront();
            }
        }

        private void selecionarProfessoresPorTurmaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (FormProfTurma == null || FormProfTurma.IsDisposed)
            {
                FormProfTurma = new SelecionarProfTurma();
                FormProfTurma.MdiParent = this;
                FormProfTurma.Show();
            }
            else
            {
                FormProfTurma.BringToFront();
            }
        }

        private void prencherHoráriosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (FormHorarios == null || FormHorarios.IsDisposed)
            {
                FormHorarios = new PreencherHorarios();
                FormHorarios.MdiParent = this;
                FormHorarios.Show();
            }
            else
            {
                FormHorarios.BringToFront();
            }
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            if (FormAlterarTurma == null || FormAlterarTurma.IsDisposed)
            {
                FormAlterarTurma = new AlterarTurma();
                FormAlterarTurma.MdiParent = this;
                FormAlterarTurma.Show();
            }
            else
            {
                FormAlterarTurma.BringToFront();
            }
        }

        private void alteraçãoEmCursoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (FormAlterarCurso == null || FormAlterarCurso.IsDisposed)
            {
                FormAlterarCurso = new AlterarCurso();
                FormAlterarCurso.MdiParent = this;
                FormAlterarCurso.Show();
            }
            else
            {
                FormAlterarCurso.BringToFront();
            }
        }

        private void horárioDasTurmasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (FrmHorarioTurmas == null || FrmHorarioTurmas.IsDisposed)
            {
                FrmHorarioTurmas = new FrmHorarioTurmas();
                FrmHorarioTurmas.MdiParent = this;
                FrmHorarioTurmas.Show();
            }
            else
            {
                FrmHorarioTurmas.BringToFront();
            }
        }
    }
}
