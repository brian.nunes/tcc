﻿namespace softwarecordenacao
{
    partial class AlterarCurso
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AlterarCurso));
            this.pbconfirmaralteracoes = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtidcurso = new System.Windows.Forms.TextBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.lblMCurso = new System.Windows.Forms.ListBox();
            this.lblMDisp = new System.Windows.Forms.ListBox();
            this.numano = new System.Windows.Forms.NumericUpDown();
            this.cbcurso = new System.Windows.Forms.ComboBox();
            this.pblimparformulario = new System.Windows.Forms.PictureBox();
            this.pbdeletar = new System.Windows.Forms.PictureBox();
            this.pbsetadir = new System.Windows.Forms.PictureBox();
            this.pbsetaesq = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pblocalizar = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbconfirmaralteracoes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numano)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pblimparformulario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbdeletar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsetadir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsetaesq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pblocalizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            this.SuspendLayout();
            // 
            // pbconfirmaralteracoes
            // 
            this.pbconfirmaralteracoes.Image = ((System.Drawing.Image)(resources.GetObject("pbconfirmaralteracoes.Image")));
            this.pbconfirmaralteracoes.Location = new System.Drawing.Point(483, 838);
            this.pbconfirmaralteracoes.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbconfirmaralteracoes.Name = "pbconfirmaralteracoes";
            this.pbconfirmaralteracoes.Size = new System.Drawing.Size(225, 68);
            this.pbconfirmaralteracoes.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbconfirmaralteracoes.TabIndex = 29;
            this.pbconfirmaralteracoes.TabStop = false;
            this.pbconfirmaralteracoes.Click += new System.EventHandler(this.pbconfirmaralteracoes_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(10, 318);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(134, 51);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 28;
            this.pictureBox1.TabStop = false;
            // 
            // txtidcurso
            // 
            this.txtidcurso.Enabled = false;
            this.txtidcurso.Location = new System.Drawing.Point(153, 400);
            this.txtidcurso.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtidcurso.Name = "txtidcurso";
            this.txtidcurso.Size = new System.Drawing.Size(553, 26);
            this.txtidcurso.TabIndex = 33;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(10, 243);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(132, 51);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 45;
            this.pictureBox4.TabStop = false;
            // 
            // lblMCurso
            // 
            this.lblMCurso.BackColor = System.Drawing.Color.White;
            this.lblMCurso.Enabled = false;
            this.lblMCurso.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblMCurso.FormattingEnabled = true;
            this.lblMCurso.ItemHeight = 20;
            this.lblMCurso.Location = new System.Drawing.Point(411, 525);
            this.lblMCurso.Name = "lblMCurso";
            this.lblMCurso.Size = new System.Drawing.Size(241, 264);
            this.lblMCurso.TabIndex = 49;
            // 
            // lblMDisp
            // 
            this.lblMDisp.BackColor = System.Drawing.Color.White;
            this.lblMDisp.Enabled = false;
            this.lblMDisp.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblMDisp.FormattingEnabled = true;
            this.lblMDisp.ItemHeight = 20;
            this.lblMDisp.Location = new System.Drawing.Point(66, 525);
            this.lblMDisp.Name = "lblMDisp";
            this.lblMDisp.Size = new System.Drawing.Size(241, 264);
            this.lblMDisp.TabIndex = 48;
            // 
            // numano
            // 
            this.numano.Location = new System.Drawing.Point(153, 329);
            this.numano.Maximum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numano.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numano.Name = "numano";
            this.numano.Size = new System.Drawing.Size(388, 26);
            this.numano.TabIndex = 55;
            this.numano.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // cbcurso
            // 
            this.cbcurso.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbcurso.FormattingEnabled = true;
            this.cbcurso.Location = new System.Drawing.Point(153, 252);
            this.cbcurso.Name = "cbcurso";
            this.cbcurso.Size = new System.Drawing.Size(548, 28);
            this.cbcurso.TabIndex = 56;
            // 
            // pblimparformulario
            // 
            this.pblimparformulario.Image = ((System.Drawing.Image)(resources.GetObject("pblimparformulario.Image")));
            this.pblimparformulario.Location = new System.Drawing.Point(252, 838);
            this.pblimparformulario.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pblimparformulario.Name = "pblimparformulario";
            this.pblimparformulario.Size = new System.Drawing.Size(222, 68);
            this.pblimparformulario.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pblimparformulario.TabIndex = 57;
            this.pblimparformulario.TabStop = false;
            this.pblimparformulario.Click += new System.EventHandler(this.pblimparformulario_Click);
            // 
            // pbdeletar
            // 
            this.pbdeletar.Image = ((System.Drawing.Image)(resources.GetObject("pbdeletar.Image")));
            this.pbdeletar.Location = new System.Drawing.Point(10, 838);
            this.pbdeletar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbdeletar.Name = "pbdeletar";
            this.pbdeletar.Size = new System.Drawing.Size(232, 68);
            this.pbdeletar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbdeletar.TabIndex = 58;
            this.pbdeletar.TabStop = false;
            this.pbdeletar.Click += new System.EventHandler(this.pbdeletar_Click);
            // 
            // pbsetadir
            // 
            this.pbsetadir.Enabled = false;
            this.pbsetadir.Image = ((System.Drawing.Image)(resources.GetObject("pbsetadir.Image")));
            this.pbsetadir.Location = new System.Drawing.Point(324, 592);
            this.pbsetadir.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbsetadir.Name = "pbsetadir";
            this.pbsetadir.Size = new System.Drawing.Size(75, 38);
            this.pbsetadir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbsetadir.TabIndex = 60;
            this.pbsetadir.TabStop = false;
            this.pbsetadir.Click += new System.EventHandler(this.pbsetadir_Click);
            // 
            // pbsetaesq
            // 
            this.pbsetaesq.Enabled = false;
            this.pbsetaesq.Image = ((System.Drawing.Image)(resources.GetObject("pbsetaesq.Image")));
            this.pbsetaesq.Location = new System.Drawing.Point(324, 685);
            this.pbsetaesq.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbsetaesq.Name = "pbsetaesq";
            this.pbsetaesq.Size = new System.Drawing.Size(75, 38);
            this.pbsetaesq.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbsetaesq.TabIndex = 59;
            this.pbsetaesq.TabStop = false;
            this.pbsetaesq.Click += new System.EventHandler(this.pbsetaesq_Click);
            // 
            // pictureBox6
            // 
            this.pictureBox6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(66, 465);
            this.pictureBox6.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(242, 51);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 61;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(411, 465);
            this.pictureBox7.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(242, 51);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 62;
            this.pictureBox7.TabStop = false;
            // 
            // pblocalizar
            // 
            this.pblocalizar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pblocalizar.Image = ((System.Drawing.Image)(resources.GetObject("pblocalizar.Image")));
            this.pblocalizar.Location = new System.Drawing.Point(562, 292);
            this.pblocalizar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pblocalizar.Name = "pblocalizar";
            this.pblocalizar.Size = new System.Drawing.Size(140, 82);
            this.pblocalizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pblocalizar.TabIndex = 63;
            this.pblocalizar.TabStop = false;
            this.pblocalizar.Click += new System.EventHandler(this.pblocalizar_Click);
            // 
            // pictureBox9
            // 
            this.pictureBox9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox9.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox9.Image")));
            this.pictureBox9.Location = new System.Drawing.Point(10, 386);
            this.pictureBox9.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(132, 51);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox9.TabIndex = 64;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.DarkGreen;
            this.pictureBox5.Location = new System.Drawing.Point(-62, -11);
            this.pictureBox5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(154, 203);
            this.pictureBox5.TabIndex = 67;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.Color.DarkGreen;
            this.pictureBox10.Location = new System.Drawing.Point(614, -11);
            this.pictureBox10.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(165, 203);
            this.pictureBox10.TabIndex = 66;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.Color.DarkGreen;
            this.pictureBox11.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox11.Image")));
            this.pictureBox11.Location = new System.Drawing.Point(84, -11);
            this.pictureBox11.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(532, 203);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox11.TabIndex = 65;
            this.pictureBox11.TabStop = false;
            // 
            // AlterarCurso
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(722, 925);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox10);
            this.Controls.Add(this.pictureBox11);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.pblocalizar);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pbsetadir);
            this.Controls.Add(this.pbsetaesq);
            this.Controls.Add(this.pbdeletar);
            this.Controls.Add(this.pblimparformulario);
            this.Controls.Add(this.cbcurso);
            this.Controls.Add(this.numano);
            this.Controls.Add(this.lblMCurso);
            this.Controls.Add(this.lblMDisp);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.txtidcurso);
            this.Controls.Add(this.pbconfirmaralteracoes);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "AlterarCurso";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Alterar curso";
            this.Load += new System.EventHandler(this.AlterarCurso_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbconfirmaralteracoes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numano)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pblimparformulario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbdeletar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsetadir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsetaesq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pblocalizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pbconfirmaralteracoes;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txtidcurso;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.ListBox lblMCurso;
        private System.Windows.Forms.ListBox lblMDisp;
        private System.Windows.Forms.NumericUpDown numano;
        private System.Windows.Forms.ComboBox cbcurso;
        private System.Windows.Forms.PictureBox pblimparformulario;
        private System.Windows.Forms.PictureBox pbdeletar;
        private System.Windows.Forms.PictureBox pbsetadir;
        private System.Windows.Forms.PictureBox pbsetaesq;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pblocalizar;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox11;
    }
}