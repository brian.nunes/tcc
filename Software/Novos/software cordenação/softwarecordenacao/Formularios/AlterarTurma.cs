﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace softwarecordenacao.Formularios
{
    public partial class AlterarTurma : Form
    {
        string cod_curso;
        public AlterarTurma()
        {
            InitializeComponent();
        }

        private void pbconfirmaralteracoes_Click(object sender, EventArgs e)
        {
            //confere se foi tudo preenchido
            if (txtidturma.Text != "" && txtletra.Text != "" && numano.Text != "0" && cbcurso.Text != "")
            {
                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();
                try
                {
                    //seleciona o código do curso
                    string curso = cbcurso.Text;
                    string codigo = "";
                    string letra = "";
                    int index = 0;
                    while (letra != " ")
                    {
                        codigo += letra;
                        letra = curso.Substring(index, 1);
                        index++;
                    }

                    //faz a atualização no banco de dados
                    sql.executarcomando("UPDATE `tab_classe` SET `letra` = '" + txtletra.Text + "', `ano` = '" + numano.Value.ToString() + "', `fk_curso` = '" + codigo + "' WHERE `tab_classe`.`id_classe` = " + txtidturma.Text, " atualização");
                    
                    //da o feed back ao usuario
                    DialogResult YN = MessageBox.Show("Alteração feita com sucesso! \nDeseja limpar o formulário?", "*** Alteração ***", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                   
                    //testa a resposta do usuário
                    if (YN == DialogResult.Yes)
                    {
                        //prepara o form para um novo cadastro
                        button2_Click(sender, e);
                    }
                }
                catch
                {
                    //mensagem de erro caso ocorra algum erro coma inserção do cadastro
                    MessageBox.Show("Alteração não pode ser efetuado!", "*** Erro ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                //avisa ao usuario que existem campos não preenchidos
                MessageBox.Show("Preencha todos os campos!", "*** Erro ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void AlterarTurma_Load(object sender, EventArgs e)
        {

        }

        private void pblocalizar_Click(object sender, EventArgs e)
        {
            //testa se os campos foram preenchidos
            if (txtletra.Text != "" && numano.Value != 0)
            {
                try
                {
                    //pesquisa a sala buscado
                    sql.pesquisar("SELECT * FROM tab_classe WHERE ano = '" + numano.Value.ToString() + "' AND letra = '" + txtletra.Text + "'");

                    //testa se encontrou
                    if (sql.guarda.Read())
                    {
                        //ativa a combobox
                        cbcurso.Enabled = true;

                        //preencha as mesmas com os dados do professor
                        txtidturma.Text = sql.guarda[0].ToString();
                        cod_curso = sql.guarda[3].ToString();
                    }
                    else
                    {
                        //avisa se a sala não foi encontrada
                        MessageBox.Show("A turma '" + numano.Value.ToString() + txtletra.Text + "' não foi encontrada.", "*** ERRO ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        button2_Click(sender, e);
                    }

                    //fecha as conexões
                    sql.guarda.Close();
                    sql.olecon.Close();

                    //preencher combobox
                    //pesquisa lista de cursos disponíveis
                    sql.pesquisar("select * from tab_curso where id_curso != '666' AND id_curso != '999'");

                    //limpa combox para que não haja duplicatas
                    cbcurso.Items.Clear();

                    //escreve um por um na combobox
                    while (sql.guarda.Read())
                    {
                        cbcurso.Items.Add(sql.guarda[0].ToString() + " - " + sql.guarda[1].ToString() + " - " + sql.guarda[2].ToString() + " ano");
                    }

                    //fecha as conexões
                    sql.guarda.Close();
                    sql.olecon.Close();

                    //procura curso por curso aquele q tem o código do da sala pesquisada
                    for (int i = 0; i < cbcurso.Items.Count; i++)
                    {
                        //seleciona o código do curso
                        string curso = cbcurso.Items[i].ToString();
                        string codigo = "";
                        string letra = "";
                        int index = 0;
                        while (letra != " ")
                        {
                            codigo += letra;
                            letra = curso.Substring(index, 1);
                            index++;
                        }

                        //quando acha...
                        if (codigo == cod_curso)
                        {
                            //...o marca na combo box...
                            cbcurso.SelectedIndex = i;
                            //...e fecha o laço
                            i = cbcurso.Items.Count;
                        }
                    }
                }
                catch
                {
                    //informa que há um erro de conexão com o banco de dados
                    MessageBox.Show("Conexão falha.", "*** ERRO ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    button2_Click(sender, e);
                }
            }
            else
            {
                //pede ao usuário para preencher
                MessageBox.Show("Escreva uma turma válida. \nVocê pesquisou por: " + numano.Value.ToString() + txtletra.Text, "*** ERRO ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
                button2_Click(sender, e);
            }
        }

        private void pblimparformulario_Click(object sender, EventArgs e)
        {
            //limpa todos os campos
            cbcurso.Items.Clear();
            cbcurso.Text = "";
            txtletra.Clear();
            txtidturma.Clear();
            numano.Value = 0;

            //foca no campo do ano
            numano.Focus();

            //abre a textbox para a ano e letra da sala
            numano.Enabled = true;
            txtletra.Enabled = true;

            //fecha todas as outras textbox
            cbcurso.Enabled = false;
            txtidturma.Enabled = false;
        }

        private void pbdeletar_Click(object sender, EventArgs e)
        {
            if (cbcurso.Enabled == true)
            {
                //pergunta ao usuário se ele tem certeza do ato
                DialogResult YN = MessageBox.Show("Deseja mesmo excluir este registro?", "*** Exclusão ***", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                //confere a resposta
                if (YN == DialogResult.Yes)
                {
                    //fecha as conexões
                    sql.guarda.Close();
                    sql.olecon.Close();

                    SQLDeletes.DeletarTurma(txtidturma.Text);

                    //retorna ao usuário o sucesso da exclusão
                    DialogResult SN = MessageBox.Show("Exclusão feita com sucesso! \nDeseja limpar o formulário?", "*** Alteração ***", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    //testa a resposta do usuário
                    if (SN == DialogResult.Yes)
                    {
                        //prepara o form para um novo cadastro
                        button2_Click(sender, e);
                    }
                }
            }
            else
            {
                //pede ao usuário localizar a turma
                MessageBox.Show("Localize uma turma primeiro", "*** ERRO ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
