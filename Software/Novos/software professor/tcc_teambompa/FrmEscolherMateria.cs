﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tcc_teambompa
{
    public partial class FrmEscolherMateria : Form
    {
        //variavel para manipulação de strings
        string texto;
        //cria vetor
        int[] materiaslecionadas;
        public FrmEscolherMateria()
        {
            InitializeComponent();
        }

        private void FrmEscolherMateria_Load(object sender, EventArgs e)
        {
            //limpa listas
            lbdispM.Items.Clear();
            lbselecM.Items.Clear();
            lblecionadasM.Items.Clear();

            //inicia com número de aulas para as materias disponíveis no máximo
            sql.aulas_disp_materia = sql.aulas_disp;

            //buscar todas as matérias cadastradas
            sql.pesquisar("select * from tab_materia where id_materia != '666' and id_materia != '999';");

            while (sql.guarda.Read())  //faz a leitura dos resultados obtidos
            {
                //lista as materias uma a uma na dropdownlist
                lbdispM.Items.Add(sql.guarda[0].ToString() + " - " + sql.guarda[2].ToString() + " - " + sql.guarda[3].ToString() + " aulas/semana/turma");
            }

            //fecha as conexões
            sql.guarda.Close();
            sql.olecon.Close();

            //buscar todas as matérias previamente selecionadas
            sql.pesquisar("select * from relacao_prof_materia_temporaria where fk_prof = '" + sql.proflogado.ToString() + "';");

            int k = 0;
            while (sql.guarda.Read())  //faz a contagem dos resultados obtidos
            {
                //soma na contagem
                k++;
            }

            //fecha as conexões
            sql.guarda.Close();
            sql.olecon.Close();

            //cria vetor para as materias
            int[] materiasselecionadas = new int[k];

            //inicia variavel auxiliar
            int h = 0;

            //buscar todas as matérias previamente selecionadas
            sql.pesquisar("select * from relacao_prof_materia_temporaria where fk_prof = '" + sql.proflogado.ToString() + "';");

            while (sql.guarda.Read())  //faz a leitura dos resultados obtidos
            {
                //salva o cod da materia no vetor
                materiasselecionadas[h] = Convert.ToInt32(sql.guarda[1].ToString());
                h++;
            }

            //fecha as conexões
            sql.guarda.Close();
            sql.olecon.Close();

            //buscar todas as matérias previamente lecionadas
            sql.pesquisar("select * from relacao_prof_materia_permanente where fk_prof = '" + sql.proflogado.ToString() + "';");

            k = 0;
            while (sql.guarda.Read())  //faz a contagem dos resultados obtidos
            {
                //soma na contagem
                k++;
            }

            //fecha as conexões
            sql.guarda.Close();
            sql.olecon.Close();

            //instancia vetor para as materias
            materiaslecionadas = new int[k];

            //inicia variavel auxiliar
            h = 0;

            //buscar todas as matérias previamente selecionadas
            sql.pesquisar("select * from relacao_prof_materia_permanente where fk_prof = '" + sql.proflogado.ToString() + "';");

            while (sql.guarda.Read())  //faz a leitura dos resultados obtidos
            {
                //salva o cod da materia no vetor
                materiaslecionadas[h] = Convert.ToInt32(sql.guarda[0].ToString());
                h++;
            }

            //fecha as conexões
            sql.guarda.Close();
            sql.olecon.Close();

            //testar cada código das matérias selecionadas
            for (int i = 0; i < materiasselecionadas.Length; i++)
            {
                //em cada item da dropdownlist
                for (int j = 0; j < lbdispM.Items.Count; j++)
                {
                    texto = lbdispM.Items[j].ToString();
                    string codigo = "";
                    string letra = "";
                    int index = 0;
                    while (letra != " ")
                    {
                        codigo += letra;
                        letra = texto.Substring(index, 1);
                        index++;
                    }
                    //e deletar caso encontre
                    if (codigo == materiasselecionadas[i].ToString())
                    {
                        lbdispM.Items.RemoveAt(j);
                    }
                }
            }

            //fecha as conexões
            sql.guarda.Close();
            sql.olecon.Close();

            //testar cada código das matérias lecionadas
            for (int i = 0; i < materiaslecionadas.Length; i++)
            {
                //em cada item da lista
                for (int j = 0; j < lbdispM.Items.Count; j++)
                {
                    texto = lbdispM.Items[j].ToString();
                    string codigo = "";
                    string letra = "";
                    int index = 0;
                    while (letra != " ")
                    {
                        codigo += letra;
                        letra = texto.Substring(index, 1);
                        index++;
                    }
                    //e deletar caso encontre
                    if (codigo == materiaslecionadas[i].ToString())
                    {
                        lbdispM.Items.RemoveAt(j);
                    }
                }
            }

            //fecha as conexões
            sql.guarda.Close();
            sql.olecon.Close();

            //loop para buscar todas as matérias previamente selecionadas uma a uma
            for (int j = 0; j < materiasselecionadas.Length; j++)
            {
                //faz a busca
                sql.pesquisar("select * from tab_materia where id_materia = '" + materiasselecionadas[j].ToString() + "';");

                if (sql.guarda.Read())  //se achar
                {
                    //lista a materia na listbox
                    lbselecM.Items.Add(sql.guarda[0].ToString() + " - " + sql.guarda[2].ToString() + " - " + sql.guarda[3].ToString() + " aulas/semana/turma");

                    //diminui as aulas vagas com o numero referente à matéria
                    sql.aulas_disp_materia -= Convert.ToInt32(sql.guarda[3].ToString());
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();
            }

            //loop para buscar todas as matérias previamente lecionadas uma a uma
            for (int j = 0; j < materiaslecionadas.Length; j++)
            {
                //faz a busca
                sql.pesquisar("select * from tab_materia where id_materia = '" + materiaslecionadas[j].ToString() + "';");

                if (sql.guarda.Read())  //se achar
                {
                    //lista a materia na listbox
                    lblecionadasM.Items.Add(sql.guarda[0].ToString() + " - " + sql.guarda[2].ToString() + " - " + sql.guarda[3].ToString() + " aulas/semana/turma");

                    //diminui as aulas vagas com o numero referente à matéria
                    sql.aulas_disp_materia -= Convert.ToInt32(sql.guarda[3].ToString());
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();
            }

            //atualiza a label
            lblqtdaulasM.Text = sql.aulas_disp_materia.ToString();

        }

        private void pbsetadir_Click(object sender, EventArgs e)
        {
            //confere se há algo selecionado
            if (lbdispM.SelectedIndex != -1)
            {
                //seleciona o código da matéria escolhida
                texto = lbdispM.SelectedItem.ToString();
                string codigo = "";
                string letra = "";
                int index = 0;
                while (letra != " ")
                {
                    codigo += letra;
                    letra = texto.Substring(index, 1);
                    index++;
                }

                //faz a busca da matéria selecionada
                sql.pesquisar("select * from tab_materia where id_materia = '" + codigo + "';");

                if (sql.guarda.Read())  //faz a leitura dos resultados obtidos
                {
                    //confere se é possivel dar essas aulas
                    if (sql.aulas_disp_materia - Convert.ToInt32(sql.guarda[3].ToString()) >= 0)
                    {
                        sql.aulas_disp_materia = sql.aulas_disp_materia - Convert.ToInt32(sql.guarda[3].ToString());
                        lblqtdaulasM.Text = sql.aulas_disp_materia.ToString();
                        lbselecM.Items.Add(lbdispM.SelectedItem.ToString());
                        lbdispM.Items.RemoveAt(lbdispM.SelectedIndex);
                    }
                    else
                    {
                        MessageBox.Show("Número de aulas máxima excedido!", "*** ERRO ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            //fecha as conexões
            sql.guarda.Close();
            sql.olecon.Close();
        }

        private void pbsetaesc_Click(object sender, EventArgs e)
        {
            //confere se há algo selecionado
            if (lbselecM.SelectedIndex != -1)
            {
                //seleciona o código da matéria escolhida
                texto = lbselecM.SelectedItem.ToString();
                string codigo = "";
                string letra = "";
                int index = 0;
                while (letra != " ")
                {
                    codigo += letra;
                    letra = texto.Substring(index, 1);
                    index++;
                }

                //faz a busca da matéria selecionada
                sql.pesquisar("select num_aulas_semanais from tab_materia where id_materia = '" + codigo + "';");

                //faz a leitura dos resultados obtidos
                if (sql.guarda.Read())
                {
                    //realoca as aulas utilizadas pela matéria para as aulas disponíveis
                    sql.aulas_disp_materia = sql.aulas_disp_materia + Convert.ToInt32(sql.guarda[0].ToString());
                    //atualiza label
                    lblqtdaulasM.Text = sql.aulas_disp_materia.ToString();
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //devolve a materia para a dropdownlist
                lbdispM.Items.Add(lbselecM.SelectedItem.ToString());

                //retira ela da listbox
                lbselecM.Items.RemoveAt(lbselecM.SelectedIndex);
            }
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            //confere se há algo selecionado
            if (lblecionadasM.SelectedIndex != -1)
            {
                //seleciona o código da matéria escolhida
                texto = lblecionadasM.SelectedItem.ToString();
                string codigo = "";
                string letra = "";
                int index = 0;
                while (letra != " ")
                {
                    codigo += letra;
                    letra = texto.Substring(index, 1);
                    index++;
                }

                //faz a busca da matéria selecionada
                sql.pesquisar("select num_aulas_semanais from tab_materia where id_materia = '" + codigo + "';");

                //faz a leitura dos resultados obtidos
                if (sql.guarda.Read())
                {
                    //realoca as aulas utilizadas pela matéria para as aulas disponíveis
                    sql.aulas_disp_materia = sql.aulas_disp_materia + Convert.ToInt32(sql.guarda[0].ToString());
                    //atualiza label
                    lblqtdaulasM.Text = sql.aulas_disp_materia.ToString();
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //devolve a materia para a dropdownlist
                lbdispM.Items.Add(lblecionadasM.SelectedItem.ToString());

                //retira ela da listbox
                lblecionadasM.Items.RemoveAt(lblecionadasM.SelectedIndex);
            }
        }

        private void pictureBox12_Click(object sender, EventArgs e)
        {
            //deleta registros prévios
            sql.executarcomando("DELETE FROM `relacao_prof_materia_temporaria` WHERE `relacao_prof_materia_temporaria`.`fk_prof` = " + sql.proflogado.ToString(), "Delete ");
            //faz um loop criando uma relação para cada matéria escolhida
            foreach (string materia in lbselecM.Items)
            {
                //seleciona o código da matéria
                string codigo = "";
                string letra = "";
                int index = 0;
                while (letra != " ")
                {
                    codigo += letra;
                    letra = materia.Substring(index, 1);
                    index++;
                }

                //insere a relação para a materia
                sql.executarcomando("INSERT INTO `relacao_prof_materia_temporaria` (`fk_prof`, `fk_materia`) VALUES ('" + sql.proflogado.ToString() + "', '" + codigo + "')", "Inserção ");
            }

            //lista codigo das atuais leciondas
            string[] lecionadas = new string[lblecionadasM.Items.Count];
            for (int y = 0; y < lblecionadasM.Items.Count; y++)
            {
                //seleciona o código da matéria
                string materia = lblecionadasM.Items[y].ToString();
                string codigo = "";
                string letra = "";
                int index = 0;
                while (letra != " ")
                {
                    codigo += letra;
                    letra = materia.Substring(index, 1);
                    index++;
                }
                lecionadas[y] = codigo;
            }

            //testa os codigos q n existem mais
            foreach (int codigo in materiaslecionadas)
            {
                bool foi = false;
                foreach (string codigo2 in lecionadas)
                {
                    if (codigo.ToString() == codigo2)
                    {
                        foi = true;
                    }
                }
                //e os deleta qnd os encontra
                if (foi != true)
                {
                    sql.executarcomando("DELETE FROM `relacao_prof_materia_permanente` WHERE `relacao_prof_materia_permanente`.`fk_materia` = " + codigo.ToString() + " AND `relacao_prof_materia_permanente`.`fk_prof` = " + sql.proflogado.ToString(), "Delete ");
                    sql.executarcomando("DELETE FROM `relacao_prof_classe_temporaria` WHERE `fk_materia` = '" + codigo.ToString() + "';", " Delete");
                    sql.executarcomando("DELETE FROM `relacao_prof_classe_permanente` WHERE `fk_materia` = '" + codigo.ToString() + "';", " Delete");
                }
            }

            //da o feedback ao usuário
            MessageBox.Show("Matérias mudadas com êxito!", "*** Escolha das matérias ***", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            lbdispM.Items.Clear();
            lbselecM.Items.Clear();
            lblecionadasM.Items.Clear();

            FrmEscolherMateria_Load(sender, e);
        }
    }
}
