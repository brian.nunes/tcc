﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tcc_teambompa
{
    public partial class FmLogin : Form
    {
        public FmLogin()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //fecha a conexão evitando possiveis conflitos
            sql.olecon.Close();

            //desloga algum possível prof logado
            sql.proflogado = -1;
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            //faz a consulta dos dados inseridos
            string comandox = "select * from tab_prof where sigla='" + txt_usuario.Text + "'and senha='" + txt_senha.Text + "'";
            sql.pesquisar(comandox);

            if (sql.guarda.Read())  // se existe registro
            {
                //define código do professor logado no sistema
                sql.proflogado = Convert.ToInt32(sql.guarda[0]);

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();
                //defini a quantidade de aulas disponíveis do professor
                comandox = "select num_aula_disponivel from tab_prof where id_prof='" + sql.proflogado.ToString() + "'";
                sql.pesquisar(comandox);
                if (sql.guarda.Read())
                {
                    sql.aulas_disp = Convert.ToInt32(sql.guarda[0].ToString());
                    sql.aulas_disp_materia = sql.aulas_disp;
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                txt_usuario.Clear();
                txt_senha.Clear();
                txt_usuario.Focus();

                //instancia form principal
                FrmMdi f1 = new FrmMdi();
                //Abre form principal
                f1.Show();
                this.Hide();
            }
            else
            {
                //informa do erro
                MessageBox.Show("Login ou senha inválido(s).", "*** ERRO ***", MessageBoxButtons.OK, MessageBoxIcon.Error);

                //limpa os campos e foca na primeira text box
                txt_usuario.Text = "";
                txt_senha.Text = "";
                txt_usuario.Focus();

                //fecha as conexões
                sql.olecon.Close();
                sql.guarda.Close();
            }
        }

        private void txt_usuario_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
