﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tcc_teambompa
{
    public partial class FrmMudaSenha : Form
    {
        public FrmMudaSenha()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
        
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            //confere se senhas novas são iguais
            if (txtsenhanova.Text == txtconfirmarsenha.Text)
            {
                //pesquisa senha atual
                sql.pesquisar("select * from tab_prof where id_prof = '" + sql.proflogado.ToString() + "';");

                //confere se achou
                if (sql.guarda.Read())
                {
                    string senha = sql.guarda[3].ToString();

                    //confere se a senha atual esta correta
                    if (senha == txtsenhaatual.Text)
                    {
                        //fecha as conexões
                        sql.guarda.Close();
                        sql.olecon.Close();

                        //atualiza a senha no banco de dados
                        sql.executarcomando("UPDATE `tab_prof` SET `senha` = '" + txtsenhanova.Text + "' WHERE `tab_prof`.`id_prof` = " + sql.proflogado.ToString(), "update");

                        //da o feedback ao usuário
                        MessageBox.Show("Senha alterada com êxito!", "*** Alteração de Senha ***", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        //da o feedback ao usuário
                        MessageBox.Show("Senha antiga está incorreta!", "*** Alteração de Senha ***", MessageBoxButtons.OK, MessageBoxIcon.Error);

                        //fecha as conexões
                        sql.guarda.Close();
                        sql.olecon.Close();
                    }
                }
            }
            else
            {
                //da o feedback ao usuário
                MessageBox.Show("As senhas novas não conferem!", "*** Alteração de Senha ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
