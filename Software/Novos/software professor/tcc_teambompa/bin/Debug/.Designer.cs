﻿namespace tcc_teambompa
{
    partial class FrmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tab = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.lblfeedbackD = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.lblqtdaulas = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.cb9aulasex = new System.Windows.Forms.CheckBox();
            this.cb9aulaquin = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cb8aulaquin = new System.Windows.Forms.CheckBox();
            this.cb9aulaquar = new System.Windows.Forms.CheckBox();
            this.cb9aulater = new System.Windows.Forms.CheckBox();
            this.cb9aulaseg = new System.Windows.Forms.CheckBox();
            this.cb8aulasex = new System.Windows.Forms.CheckBox();
            this.cb8aulaquar = new System.Windows.Forms.CheckBox();
            this.cb8aulater = new System.Windows.Forms.CheckBox();
            this.cb8aulaseg = new System.Windows.Forms.CheckBox();
            this.cb7aulasex = new System.Windows.Forms.CheckBox();
            this.cb7aulaquin = new System.Windows.Forms.CheckBox();
            this.cb7aulaquar = new System.Windows.Forms.CheckBox();
            this.cb7aulater = new System.Windows.Forms.CheckBox();
            this.cb7aulaseg = new System.Windows.Forms.CheckBox();
            this.cb6aulasex = new System.Windows.Forms.CheckBox();
            this.cb6aulaquin = new System.Windows.Forms.CheckBox();
            this.cb6aulaquar = new System.Windows.Forms.CheckBox();
            this.cb6aulater = new System.Windows.Forms.CheckBox();
            this.cb6aulaseg = new System.Windows.Forms.CheckBox();
            this.cb5aulasex = new System.Windows.Forms.CheckBox();
            this.cb5aulaquin = new System.Windows.Forms.CheckBox();
            this.cb5aulaquar = new System.Windows.Forms.CheckBox();
            this.cb5aulater = new System.Windows.Forms.CheckBox();
            this.cb4aulasex = new System.Windows.Forms.CheckBox();
            this.cb5aulaseg = new System.Windows.Forms.CheckBox();
            this.cb4aulaquin = new System.Windows.Forms.CheckBox();
            this.cb4aulaquar = new System.Windows.Forms.CheckBox();
            this.cb4aulaseg = new System.Windows.Forms.CheckBox();
            this.cb4aulater = new System.Windows.Forms.CheckBox();
            this.cb3aulasex = new System.Windows.Forms.CheckBox();
            this.cb3aulaquin = new System.Windows.Forms.CheckBox();
            this.cb3aulaquar = new System.Windows.Forms.CheckBox();
            this.cb3aulater = new System.Windows.Forms.CheckBox();
            this.cb3aulaseg = new System.Windows.Forms.CheckBox();
            this.cb2aulasex = new System.Windows.Forms.CheckBox();
            this.cb2aulaquin = new System.Windows.Forms.CheckBox();
            this.cb2aulater = new System.Windows.Forms.CheckBox();
            this.cb2aulaquar = new System.Windows.Forms.CheckBox();
            this.cb1aulaquin = new System.Windows.Forms.CheckBox();
            this.cb2aulaseg = new System.Windows.Forms.CheckBox();
            this.cb1aulasex = new System.Windows.Forms.CheckBox();
            this.cb1aulaquar = new System.Windows.Forms.CheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cb1aulater = new System.Windows.Forms.CheckBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.cb1aulaseg = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblfeedback = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button5 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.button4 = new System.Windows.Forms.Button();
            this.lblfeedbackT = new System.Windows.Forms.Label();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pbsetaesc = new System.Windows.Forms.PictureBox();
            this.pbsetadir = new System.Windows.Forms.PictureBox();
            this.lblecionadasT = new System.Windows.Forms.ListBox();
            this.lbselecT = new System.Windows.Forms.ListBox();
            this.lbdispT = new System.Windows.Forms.ListBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.lblqtdaulasT = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.cbmateriaT = new System.Windows.Forms.ComboBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.lbmateriadisp = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.lblecionadasM = new System.Windows.Forms.ListBox();
            this.lbselecM = new System.Windows.Forms.ListBox();
            this.lblfeedbackM = new System.Windows.Forms.Label();
            this.lblqtdaulasM = new System.Windows.Forms.Label();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.lbdispM = new System.Windows.Forms.ListBox();
            this.lblprofnome = new System.Windows.Forms.Label();
            this.LinkButton1 = new System.Windows.Forms.LinkLabel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.tab.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsetaesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsetadir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbmateriadisp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // tab
            // 
            this.tab.Controls.Add(this.tabPage1);
            this.tab.Controls.Add(this.tabPage2);
            this.tab.Controls.Add(this.tabPage5);
            this.tab.Location = new System.Drawing.Point(2, 244);
            this.tab.Name = "tab";
            this.tab.SelectedIndex = 0;
            this.tab.Size = new System.Drawing.Size(823, 467);
            this.tab.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.White;
            this.tabPage1.Controls.Add(this.lblfeedbackD);
            this.tabPage1.Controls.Add(this.pictureBox3);
            this.tabPage1.Controls.Add(this.lblqtdaulas);
            this.tabPage1.Controls.Add(this.tableLayoutPanel1);
            this.tabPage1.Controls.Add(this.lblfeedback);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(815, 441);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Disponibilidade";
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // lblfeedbackD
            // 
            this.lblfeedbackD.AutoSize = true;
            this.lblfeedbackD.ForeColor = System.Drawing.Color.Red;
            this.lblfeedbackD.Location = new System.Drawing.Point(8, 11);
            this.lblfeedbackD.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblfeedbackD.Name = "lblfeedbackD";
            this.lblfeedbackD.Size = new System.Drawing.Size(62, 13);
            this.lblfeedbackD.TabIndex = 63;
            this.lblfeedbackD.Text = "lblfeedback";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::tcc_teambompa.Properties.Resources.confirmar;
            this.pictureBox3.Location = new System.Drawing.Point(653, 384);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(128, 39);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 62;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // lblqtdaulas
            // 
            this.lblqtdaulas.AutoSize = true;
            this.lblqtdaulas.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblqtdaulas.Location = new System.Drawing.Point(644, 9);
            this.lblqtdaulas.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblqtdaulas.Name = "lblqtdaulas";
            this.lblqtdaulas.Size = new System.Drawing.Size(149, 13);
            this.lblqtdaulas.TabIndex = 61;
            this.lblqtdaulas.Text = "Label para a qnt de aulas disp";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.tableLayoutPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.16456F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.53164F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.7373F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.23296F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16F));
            this.tableLayoutPanel1.Controls.Add(this.cb9aulasex, 5, 9);
            this.tableLayoutPanel1.Controls.Add(this.cb9aulaquin, 4, 9);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.cb8aulaquin, 4, 8);
            this.tableLayoutPanel1.Controls.Add(this.cb9aulaquar, 3, 9);
            this.tableLayoutPanel1.Controls.Add(this.cb9aulater, 2, 9);
            this.tableLayoutPanel1.Controls.Add(this.cb9aulaseg, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.cb8aulasex, 5, 8);
            this.tableLayoutPanel1.Controls.Add(this.cb8aulaquar, 3, 8);
            this.tableLayoutPanel1.Controls.Add(this.cb8aulater, 2, 8);
            this.tableLayoutPanel1.Controls.Add(this.cb8aulaseg, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.cb7aulasex, 5, 7);
            this.tableLayoutPanel1.Controls.Add(this.cb7aulaquin, 4, 7);
            this.tableLayoutPanel1.Controls.Add(this.cb7aulaquar, 3, 7);
            this.tableLayoutPanel1.Controls.Add(this.cb7aulater, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.cb7aulaseg, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.cb6aulasex, 5, 6);
            this.tableLayoutPanel1.Controls.Add(this.cb6aulaquin, 4, 6);
            this.tableLayoutPanel1.Controls.Add(this.cb6aulaquar, 3, 6);
            this.tableLayoutPanel1.Controls.Add(this.cb6aulater, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.cb6aulaseg, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.cb5aulasex, 5, 5);
            this.tableLayoutPanel1.Controls.Add(this.cb5aulaquin, 4, 5);
            this.tableLayoutPanel1.Controls.Add(this.cb5aulaquar, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.cb5aulater, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.cb4aulasex, 5, 4);
            this.tableLayoutPanel1.Controls.Add(this.cb5aulaseg, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.cb4aulaquin, 4, 4);
            this.tableLayoutPanel1.Controls.Add(this.cb4aulaquar, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.cb4aulaseg, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.cb4aulater, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.cb3aulasex, 5, 3);
            this.tableLayoutPanel1.Controls.Add(this.cb3aulaquin, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this.cb3aulaquar, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.cb3aulater, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.cb3aulaseg, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.cb2aulasex, 5, 2);
            this.tableLayoutPanel1.Controls.Add(this.cb2aulaquin, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.cb2aulater, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.cb2aulaquar, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.cb1aulaquin, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.cb2aulaseg, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.cb1aulasex, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.cb1aulaquar, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.label13, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.cb1aulater, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label14, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.label11, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label10, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label15, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.cb1aulaseg, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label12, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 9);
            this.tableLayoutPanel1.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 31);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 10;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(791, 345);
            this.tableLayoutPanel1.TabIndex = 60;
            // 
            // cb9aulasex
            // 
            this.cb9aulasex.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb9aulasex.AutoSize = true;
            this.cb9aulasex.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb9aulasex.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb9aulasex.Location = new System.Drawing.Point(663, 309);
            this.cb9aulasex.Margin = new System.Windows.Forms.Padding(2);
            this.cb9aulasex.Name = "cb9aulasex";
            this.cb9aulasex.Size = new System.Drawing.Size(125, 17);
            this.cb9aulasex.TabIndex = 56;
            this.cb9aulasex.Text = "9ª Aula";
            this.cb9aulasex.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb9aulasex.UseVisualStyleBackColor = false;
            this.cb9aulasex.CheckedChanged += new System.EventHandler(this.cb9aulasex_CheckedChanged);
            // 
            // cb9aulaquin
            // 
            this.cb9aulaquin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb9aulaquin.AutoSize = true;
            this.cb9aulaquin.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb9aulaquin.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb9aulaquin.Location = new System.Drawing.Point(535, 309);
            this.cb9aulaquin.Margin = new System.Windows.Forms.Padding(2);
            this.cb9aulaquin.Name = "cb9aulaquin";
            this.cb9aulaquin.Size = new System.Drawing.Size(123, 17);
            this.cb9aulaquin.TabIndex = 55;
            this.cb9aulaquin.Text = "9ª Aula";
            this.cb9aulaquin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb9aulaquin.UseVisualStyleBackColor = false;
            this.cb9aulaquin.CheckedChanged += new System.EventHandler(this.cb9aulaquin_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.label4.ForeColor = System.Drawing.Color.DarkGreen;
            this.label4.Location = new System.Drawing.Point(3, 137);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "10:20";
            // 
            // cb8aulaquin
            // 
            this.cb8aulaquin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb8aulaquin.AutoSize = true;
            this.cb8aulaquin.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb8aulaquin.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb8aulaquin.Location = new System.Drawing.Point(535, 275);
            this.cb8aulaquin.Margin = new System.Windows.Forms.Padding(2);
            this.cb8aulaquin.Name = "cb8aulaquin";
            this.cb8aulaquin.Size = new System.Drawing.Size(123, 17);
            this.cb8aulaquin.TabIndex = 50;
            this.cb8aulaquin.Text = "8ª Aula";
            this.cb8aulaquin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb8aulaquin.UseVisualStyleBackColor = false;
            this.cb8aulaquin.CheckedChanged += new System.EventHandler(this.cb8aulaquin_CheckedChanged);
            // 
            // cb9aulaquar
            // 
            this.cb9aulaquar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb9aulaquar.AutoSize = true;
            this.cb9aulaquar.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb9aulaquar.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb9aulaquar.Location = new System.Drawing.Point(411, 309);
            this.cb9aulaquar.Margin = new System.Windows.Forms.Padding(2);
            this.cb9aulaquar.Name = "cb9aulaquar";
            this.cb9aulaquar.Size = new System.Drawing.Size(119, 17);
            this.cb9aulaquar.TabIndex = 53;
            this.cb9aulaquar.Text = "9ª Aula";
            this.cb9aulaquar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb9aulaquar.UseVisualStyleBackColor = false;
            this.cb9aulaquar.CheckedChanged += new System.EventHandler(this.cb9aulaquar_CheckedChanged);
            // 
            // cb9aulater
            // 
            this.cb9aulater.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb9aulater.AutoSize = true;
            this.cb9aulater.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb9aulater.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb9aulater.Location = new System.Drawing.Point(285, 309);
            this.cb9aulater.Margin = new System.Windows.Forms.Padding(2);
            this.cb9aulater.Name = "cb9aulater";
            this.cb9aulater.Size = new System.Drawing.Size(121, 17);
            this.cb9aulater.TabIndex = 54;
            this.cb9aulater.Text = "9ª Aula";
            this.cb9aulater.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb9aulater.UseVisualStyleBackColor = false;
            this.cb9aulater.CheckedChanged += new System.EventHandler(this.cb9aulater_CheckedChanged);
            // 
            // cb9aulaseg
            // 
            this.cb9aulaseg.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb9aulaseg.AutoSize = true;
            this.cb9aulaseg.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb9aulaseg.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb9aulaseg.Location = new System.Drawing.Point(107, 309);
            this.cb9aulaseg.Margin = new System.Windows.Forms.Padding(2);
            this.cb9aulaseg.Name = "cb9aulaseg";
            this.cb9aulaseg.Size = new System.Drawing.Size(173, 17);
            this.cb9aulaseg.TabIndex = 52;
            this.cb9aulaseg.Text = "9ª Aula";
            this.cb9aulaseg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb9aulaseg.UseVisualStyleBackColor = false;
            this.cb9aulaseg.CheckedChanged += new System.EventHandler(this.cb9aulaseg_CheckedChanged);
            // 
            // cb8aulasex
            // 
            this.cb8aulasex.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb8aulasex.AutoSize = true;
            this.cb8aulasex.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb8aulasex.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb8aulasex.Location = new System.Drawing.Point(663, 275);
            this.cb8aulasex.Margin = new System.Windows.Forms.Padding(2);
            this.cb8aulasex.Name = "cb8aulasex";
            this.cb8aulasex.Size = new System.Drawing.Size(125, 17);
            this.cb8aulasex.TabIndex = 51;
            this.cb8aulasex.Text = "8ª Aula";
            this.cb8aulasex.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb8aulasex.UseVisualStyleBackColor = false;
            this.cb8aulasex.CheckedChanged += new System.EventHandler(this.cb8aulasex_CheckedChanged);
            // 
            // cb8aulaquar
            // 
            this.cb8aulaquar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb8aulaquar.AutoSize = true;
            this.cb8aulaquar.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb8aulaquar.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb8aulaquar.Location = new System.Drawing.Point(411, 275);
            this.cb8aulaquar.Margin = new System.Windows.Forms.Padding(2);
            this.cb8aulaquar.Name = "cb8aulaquar";
            this.cb8aulaquar.Size = new System.Drawing.Size(119, 17);
            this.cb8aulaquar.TabIndex = 50;
            this.cb8aulaquar.Text = "8ª Aula";
            this.cb8aulaquar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb8aulaquar.UseVisualStyleBackColor = false;
            this.cb8aulaquar.CheckedChanged += new System.EventHandler(this.cb8aulaquar_CheckedChanged);
            // 
            // cb8aulater
            // 
            this.cb8aulater.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb8aulater.AutoSize = true;
            this.cb8aulater.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb8aulater.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb8aulater.Location = new System.Drawing.Point(285, 275);
            this.cb8aulater.Margin = new System.Windows.Forms.Padding(2);
            this.cb8aulater.Name = "cb8aulater";
            this.cb8aulater.Size = new System.Drawing.Size(121, 17);
            this.cb8aulater.TabIndex = 49;
            this.cb8aulater.Text = "8ª Aula";
            this.cb8aulater.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb8aulater.UseVisualStyleBackColor = false;
            this.cb8aulater.CheckedChanged += new System.EventHandler(this.cb8aulater_CheckedChanged);
            // 
            // cb8aulaseg
            // 
            this.cb8aulaseg.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb8aulaseg.AutoSize = true;
            this.cb8aulaseg.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb8aulaseg.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb8aulaseg.Location = new System.Drawing.Point(107, 275);
            this.cb8aulaseg.Margin = new System.Windows.Forms.Padding(2);
            this.cb8aulaseg.Name = "cb8aulaseg";
            this.cb8aulaseg.Size = new System.Drawing.Size(173, 17);
            this.cb8aulaseg.TabIndex = 48;
            this.cb8aulaseg.Text = "8ª Aula";
            this.cb8aulaseg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb8aulaseg.UseVisualStyleBackColor = false;
            this.cb8aulaseg.CheckedChanged += new System.EventHandler(this.cb8aulaseg_CheckedChanged);
            // 
            // cb7aulasex
            // 
            this.cb7aulasex.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb7aulasex.AutoSize = true;
            this.cb7aulasex.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb7aulasex.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb7aulasex.Location = new System.Drawing.Point(663, 241);
            this.cb7aulasex.Margin = new System.Windows.Forms.Padding(2);
            this.cb7aulasex.Name = "cb7aulasex";
            this.cb7aulasex.Size = new System.Drawing.Size(125, 17);
            this.cb7aulasex.TabIndex = 47;
            this.cb7aulasex.Text = "7ª Aula";
            this.cb7aulasex.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb7aulasex.UseVisualStyleBackColor = false;
            this.cb7aulasex.CheckedChanged += new System.EventHandler(this.cb7aulasex_CheckedChanged);
            // 
            // cb7aulaquin
            // 
            this.cb7aulaquin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb7aulaquin.AutoSize = true;
            this.cb7aulaquin.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb7aulaquin.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb7aulaquin.Location = new System.Drawing.Point(535, 241);
            this.cb7aulaquin.Margin = new System.Windows.Forms.Padding(2);
            this.cb7aulaquin.Name = "cb7aulaquin";
            this.cb7aulaquin.Size = new System.Drawing.Size(123, 17);
            this.cb7aulaquin.TabIndex = 46;
            this.cb7aulaquin.Text = "7ª Aula";
            this.cb7aulaquin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb7aulaquin.UseVisualStyleBackColor = false;
            this.cb7aulaquin.CheckedChanged += new System.EventHandler(this.cb7aulaquin_CheckedChanged);
            // 
            // cb7aulaquar
            // 
            this.cb7aulaquar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb7aulaquar.AutoSize = true;
            this.cb7aulaquar.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb7aulaquar.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb7aulaquar.Location = new System.Drawing.Point(411, 241);
            this.cb7aulaquar.Margin = new System.Windows.Forms.Padding(2);
            this.cb7aulaquar.Name = "cb7aulaquar";
            this.cb7aulaquar.Size = new System.Drawing.Size(119, 17);
            this.cb7aulaquar.TabIndex = 45;
            this.cb7aulaquar.Text = "7ª Aula";
            this.cb7aulaquar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb7aulaquar.UseVisualStyleBackColor = false;
            this.cb7aulaquar.CheckedChanged += new System.EventHandler(this.cb7aulaquar_CheckedChanged);
            // 
            // cb7aulater
            // 
            this.cb7aulater.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb7aulater.AutoSize = true;
            this.cb7aulater.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb7aulater.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb7aulater.Location = new System.Drawing.Point(285, 241);
            this.cb7aulater.Margin = new System.Windows.Forms.Padding(2);
            this.cb7aulater.Name = "cb7aulater";
            this.cb7aulater.Size = new System.Drawing.Size(121, 17);
            this.cb7aulater.TabIndex = 44;
            this.cb7aulater.Text = "7ª Aula";
            this.cb7aulater.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb7aulater.UseVisualStyleBackColor = false;
            this.cb7aulater.CheckedChanged += new System.EventHandler(this.cb7aulater_CheckedChanged);
            // 
            // cb7aulaseg
            // 
            this.cb7aulaseg.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb7aulaseg.AutoSize = true;
            this.cb7aulaseg.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb7aulaseg.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb7aulaseg.Location = new System.Drawing.Point(107, 241);
            this.cb7aulaseg.Margin = new System.Windows.Forms.Padding(2);
            this.cb7aulaseg.Name = "cb7aulaseg";
            this.cb7aulaseg.Size = new System.Drawing.Size(173, 17);
            this.cb7aulaseg.TabIndex = 43;
            this.cb7aulaseg.Text = "7ª Aula";
            this.cb7aulaseg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb7aulaseg.UseVisualStyleBackColor = false;
            this.cb7aulaseg.CheckedChanged += new System.EventHandler(this.cb7aulaseg_CheckedChanged);
            // 
            // cb6aulasex
            // 
            this.cb6aulasex.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb6aulasex.AutoSize = true;
            this.cb6aulasex.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb6aulasex.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb6aulasex.Location = new System.Drawing.Point(663, 207);
            this.cb6aulasex.Margin = new System.Windows.Forms.Padding(2);
            this.cb6aulasex.Name = "cb6aulasex";
            this.cb6aulasex.Size = new System.Drawing.Size(125, 17);
            this.cb6aulasex.TabIndex = 42;
            this.cb6aulasex.Text = "6ª Aula";
            this.cb6aulasex.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb6aulasex.UseVisualStyleBackColor = false;
            this.cb6aulasex.CheckedChanged += new System.EventHandler(this.cb6aulasex_CheckedChanged);
            // 
            // cb6aulaquin
            // 
            this.cb6aulaquin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb6aulaquin.AutoSize = true;
            this.cb6aulaquin.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb6aulaquin.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb6aulaquin.Location = new System.Drawing.Point(535, 207);
            this.cb6aulaquin.Margin = new System.Windows.Forms.Padding(2);
            this.cb6aulaquin.Name = "cb6aulaquin";
            this.cb6aulaquin.Size = new System.Drawing.Size(123, 17);
            this.cb6aulaquin.TabIndex = 41;
            this.cb6aulaquin.Text = "6ª Aula";
            this.cb6aulaquin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb6aulaquin.UseVisualStyleBackColor = false;
            this.cb6aulaquin.CheckedChanged += new System.EventHandler(this.cb6aulaquin_CheckedChanged);
            // 
            // cb6aulaquar
            // 
            this.cb6aulaquar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb6aulaquar.AutoSize = true;
            this.cb6aulaquar.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb6aulaquar.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb6aulaquar.Location = new System.Drawing.Point(411, 207);
            this.cb6aulaquar.Margin = new System.Windows.Forms.Padding(2);
            this.cb6aulaquar.Name = "cb6aulaquar";
            this.cb6aulaquar.Size = new System.Drawing.Size(119, 17);
            this.cb6aulaquar.TabIndex = 40;
            this.cb6aulaquar.Text = "6ª Aula";
            this.cb6aulaquar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb6aulaquar.UseVisualStyleBackColor = false;
            this.cb6aulaquar.CheckedChanged += new System.EventHandler(this.cb6aulaquar_CheckedChanged);
            // 
            // cb6aulater
            // 
            this.cb6aulater.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb6aulater.AutoSize = true;
            this.cb6aulater.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb6aulater.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb6aulater.Location = new System.Drawing.Point(285, 207);
            this.cb6aulater.Margin = new System.Windows.Forms.Padding(2);
            this.cb6aulater.Name = "cb6aulater";
            this.cb6aulater.Size = new System.Drawing.Size(121, 17);
            this.cb6aulater.TabIndex = 39;
            this.cb6aulater.Text = "6ª Aula";
            this.cb6aulater.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb6aulater.UseVisualStyleBackColor = false;
            this.cb6aulater.CheckedChanged += new System.EventHandler(this.cb6aulater_CheckedChanged);
            // 
            // cb6aulaseg
            // 
            this.cb6aulaseg.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb6aulaseg.AutoSize = true;
            this.cb6aulaseg.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb6aulaseg.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb6aulaseg.Location = new System.Drawing.Point(107, 207);
            this.cb6aulaseg.Margin = new System.Windows.Forms.Padding(2);
            this.cb6aulaseg.Name = "cb6aulaseg";
            this.cb6aulaseg.Size = new System.Drawing.Size(173, 17);
            this.cb6aulaseg.TabIndex = 38;
            this.cb6aulaseg.Text = "6ª Aula";
            this.cb6aulaseg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb6aulaseg.UseVisualStyleBackColor = false;
            this.cb6aulaseg.CheckedChanged += new System.EventHandler(this.cb6aulaseg_CheckedChanged);
            // 
            // cb5aulasex
            // 
            this.cb5aulasex.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb5aulasex.AutoSize = true;
            this.cb5aulasex.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb5aulasex.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb5aulasex.Location = new System.Drawing.Point(663, 173);
            this.cb5aulasex.Margin = new System.Windows.Forms.Padding(2);
            this.cb5aulasex.Name = "cb5aulasex";
            this.cb5aulasex.Size = new System.Drawing.Size(125, 17);
            this.cb5aulasex.TabIndex = 37;
            this.cb5aulasex.Text = "5ª Aula";
            this.cb5aulasex.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb5aulasex.UseVisualStyleBackColor = false;
            this.cb5aulasex.CheckedChanged += new System.EventHandler(this.cb5aulasex_CheckedChanged);
            // 
            // cb5aulaquin
            // 
            this.cb5aulaquin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb5aulaquin.AutoSize = true;
            this.cb5aulaquin.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb5aulaquin.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb5aulaquin.Location = new System.Drawing.Point(535, 173);
            this.cb5aulaquin.Margin = new System.Windows.Forms.Padding(2);
            this.cb5aulaquin.Name = "cb5aulaquin";
            this.cb5aulaquin.Size = new System.Drawing.Size(123, 17);
            this.cb5aulaquin.TabIndex = 36;
            this.cb5aulaquin.Text = "5ª Aula";
            this.cb5aulaquin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb5aulaquin.UseVisualStyleBackColor = false;
            this.cb5aulaquin.CheckedChanged += new System.EventHandler(this.cb5aulaquin_CheckedChanged);
            // 
            // cb5aulaquar
            // 
            this.cb5aulaquar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb5aulaquar.AutoSize = true;
            this.cb5aulaquar.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb5aulaquar.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb5aulaquar.Location = new System.Drawing.Point(411, 173);
            this.cb5aulaquar.Margin = new System.Windows.Forms.Padding(2);
            this.cb5aulaquar.Name = "cb5aulaquar";
            this.cb5aulaquar.Size = new System.Drawing.Size(119, 17);
            this.cb5aulaquar.TabIndex = 35;
            this.cb5aulaquar.Text = "5ª Aula";
            this.cb5aulaquar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb5aulaquar.UseVisualStyleBackColor = false;
            this.cb5aulaquar.CheckedChanged += new System.EventHandler(this.cb5aulaquar_CheckedChanged);
            // 
            // cb5aulater
            // 
            this.cb5aulater.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb5aulater.AutoSize = true;
            this.cb5aulater.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb5aulater.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb5aulater.Location = new System.Drawing.Point(285, 173);
            this.cb5aulater.Margin = new System.Windows.Forms.Padding(2);
            this.cb5aulater.Name = "cb5aulater";
            this.cb5aulater.Size = new System.Drawing.Size(121, 17);
            this.cb5aulater.TabIndex = 34;
            this.cb5aulater.Text = "5ª Aula";
            this.cb5aulater.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb5aulater.UseVisualStyleBackColor = false;
            this.cb5aulater.CheckedChanged += new System.EventHandler(this.cb5aulater_CheckedChanged);
            // 
            // cb4aulasex
            // 
            this.cb4aulasex.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb4aulasex.AutoSize = true;
            this.cb4aulasex.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb4aulasex.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb4aulasex.Location = new System.Drawing.Point(663, 139);
            this.cb4aulasex.Margin = new System.Windows.Forms.Padding(2);
            this.cb4aulasex.Name = "cb4aulasex";
            this.cb4aulasex.Size = new System.Drawing.Size(125, 17);
            this.cb4aulasex.TabIndex = 32;
            this.cb4aulasex.Text = "4ª Aula";
            this.cb4aulasex.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb4aulasex.UseVisualStyleBackColor = false;
            this.cb4aulasex.CheckedChanged += new System.EventHandler(this.cb4aulasex_CheckedChanged);
            // 
            // cb5aulaseg
            // 
            this.cb5aulaseg.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb5aulaseg.AutoSize = true;
            this.cb5aulaseg.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb5aulaseg.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb5aulaseg.Location = new System.Drawing.Point(107, 173);
            this.cb5aulaseg.Margin = new System.Windows.Forms.Padding(2);
            this.cb5aulaseg.Name = "cb5aulaseg";
            this.cb5aulaseg.Size = new System.Drawing.Size(173, 17);
            this.cb5aulaseg.TabIndex = 33;
            this.cb5aulaseg.Text = "5ª Aula";
            this.cb5aulaseg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb5aulaseg.UseVisualStyleBackColor = false;
            this.cb5aulaseg.CheckedChanged += new System.EventHandler(this.cb5aulaseg_CheckedChanged);
            // 
            // cb4aulaquin
            // 
            this.cb4aulaquin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb4aulaquin.AutoSize = true;
            this.cb4aulaquin.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb4aulaquin.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb4aulaquin.Location = new System.Drawing.Point(535, 139);
            this.cb4aulaquin.Margin = new System.Windows.Forms.Padding(2);
            this.cb4aulaquin.Name = "cb4aulaquin";
            this.cb4aulaquin.Size = new System.Drawing.Size(123, 17);
            this.cb4aulaquin.TabIndex = 31;
            this.cb4aulaquin.Text = "4ª Aula";
            this.cb4aulaquin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb4aulaquin.UseVisualStyleBackColor = false;
            this.cb4aulaquin.CheckedChanged += new System.EventHandler(this.cb4aulaquin_CheckedChanged);
            // 
            // cb4aulaquar
            // 
            this.cb4aulaquar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb4aulaquar.AutoSize = true;
            this.cb4aulaquar.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb4aulaquar.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb4aulaquar.Location = new System.Drawing.Point(411, 139);
            this.cb4aulaquar.Margin = new System.Windows.Forms.Padding(2);
            this.cb4aulaquar.Name = "cb4aulaquar";
            this.cb4aulaquar.Size = new System.Drawing.Size(119, 17);
            this.cb4aulaquar.TabIndex = 30;
            this.cb4aulaquar.Text = "4ª Aula";
            this.cb4aulaquar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb4aulaquar.UseVisualStyleBackColor = false;
            this.cb4aulaquar.CheckedChanged += new System.EventHandler(this.cb4aulaquar_CheckedChanged);
            // 
            // cb4aulaseg
            // 
            this.cb4aulaseg.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb4aulaseg.AutoSize = true;
            this.cb4aulaseg.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb4aulaseg.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb4aulaseg.Location = new System.Drawing.Point(107, 139);
            this.cb4aulaseg.Margin = new System.Windows.Forms.Padding(2);
            this.cb4aulaseg.Name = "cb4aulaseg";
            this.cb4aulaseg.Size = new System.Drawing.Size(173, 17);
            this.cb4aulaseg.TabIndex = 28;
            this.cb4aulaseg.Text = "4ª Aula";
            this.cb4aulaseg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb4aulaseg.UseVisualStyleBackColor = false;
            this.cb4aulaseg.CheckedChanged += new System.EventHandler(this.cb4aulaseg_CheckedChanged);
            // 
            // cb4aulater
            // 
            this.cb4aulater.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb4aulater.AutoSize = true;
            this.cb4aulater.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb4aulater.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb4aulater.Location = new System.Drawing.Point(285, 139);
            this.cb4aulater.Margin = new System.Windows.Forms.Padding(2);
            this.cb4aulater.Name = "cb4aulater";
            this.cb4aulater.Size = new System.Drawing.Size(121, 17);
            this.cb4aulater.TabIndex = 29;
            this.cb4aulater.Text = "4ª Aula";
            this.cb4aulater.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb4aulater.UseVisualStyleBackColor = false;
            this.cb4aulater.CheckedChanged += new System.EventHandler(this.cb4aulater_CheckedChanged);
            // 
            // cb3aulasex
            // 
            this.cb3aulasex.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb3aulasex.AutoSize = true;
            this.cb3aulasex.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb3aulasex.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb3aulasex.Location = new System.Drawing.Point(663, 105);
            this.cb3aulasex.Margin = new System.Windows.Forms.Padding(2);
            this.cb3aulasex.Name = "cb3aulasex";
            this.cb3aulasex.Size = new System.Drawing.Size(125, 17);
            this.cb3aulasex.TabIndex = 27;
            this.cb3aulasex.Text = "3ª Aula";
            this.cb3aulasex.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb3aulasex.UseVisualStyleBackColor = false;
            this.cb3aulasex.CheckedChanged += new System.EventHandler(this.cb3aulasex_CheckedChanged);
            // 
            // cb3aulaquin
            // 
            this.cb3aulaquin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb3aulaquin.AutoSize = true;
            this.cb3aulaquin.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb3aulaquin.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb3aulaquin.Location = new System.Drawing.Point(535, 105);
            this.cb3aulaquin.Margin = new System.Windows.Forms.Padding(2);
            this.cb3aulaquin.Name = "cb3aulaquin";
            this.cb3aulaquin.Size = new System.Drawing.Size(123, 17);
            this.cb3aulaquin.TabIndex = 26;
            this.cb3aulaquin.Text = "3ª Aula";
            this.cb3aulaquin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb3aulaquin.UseVisualStyleBackColor = false;
            this.cb3aulaquin.CheckedChanged += new System.EventHandler(this.cb3aulaquin_CheckedChanged);
            // 
            // cb3aulaquar
            // 
            this.cb3aulaquar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb3aulaquar.AutoSize = true;
            this.cb3aulaquar.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb3aulaquar.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb3aulaquar.Location = new System.Drawing.Point(411, 105);
            this.cb3aulaquar.Margin = new System.Windows.Forms.Padding(2);
            this.cb3aulaquar.Name = "cb3aulaquar";
            this.cb3aulaquar.Size = new System.Drawing.Size(119, 17);
            this.cb3aulaquar.TabIndex = 25;
            this.cb3aulaquar.Text = "3ª Aula";
            this.cb3aulaquar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb3aulaquar.UseVisualStyleBackColor = false;
            this.cb3aulaquar.CheckedChanged += new System.EventHandler(this.cb3aulaquar_CheckedChanged);
            // 
            // cb3aulater
            // 
            this.cb3aulater.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb3aulater.AutoSize = true;
            this.cb3aulater.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb3aulater.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb3aulater.Location = new System.Drawing.Point(285, 105);
            this.cb3aulater.Margin = new System.Windows.Forms.Padding(2);
            this.cb3aulater.Name = "cb3aulater";
            this.cb3aulater.Size = new System.Drawing.Size(121, 17);
            this.cb3aulater.TabIndex = 24;
            this.cb3aulater.Text = "3ª Aula";
            this.cb3aulater.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb3aulater.UseVisualStyleBackColor = false;
            this.cb3aulater.CheckedChanged += new System.EventHandler(this.cb3aulater_CheckedChanged);
            // 
            // cb3aulaseg
            // 
            this.cb3aulaseg.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb3aulaseg.AutoSize = true;
            this.cb3aulaseg.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb3aulaseg.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb3aulaseg.Location = new System.Drawing.Point(107, 105);
            this.cb3aulaseg.Margin = new System.Windows.Forms.Padding(2);
            this.cb3aulaseg.Name = "cb3aulaseg";
            this.cb3aulaseg.Size = new System.Drawing.Size(173, 17);
            this.cb3aulaseg.TabIndex = 23;
            this.cb3aulaseg.Text = "3ª Aula";
            this.cb3aulaseg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb3aulaseg.UseVisualStyleBackColor = false;
            this.cb3aulaseg.CheckedChanged += new System.EventHandler(this.cb3aulaseg_CheckedChanged);
            // 
            // cb2aulasex
            // 
            this.cb2aulasex.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb2aulasex.AutoSize = true;
            this.cb2aulasex.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb2aulasex.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb2aulasex.Location = new System.Drawing.Point(663, 71);
            this.cb2aulasex.Margin = new System.Windows.Forms.Padding(2);
            this.cb2aulasex.Name = "cb2aulasex";
            this.cb2aulasex.Size = new System.Drawing.Size(125, 17);
            this.cb2aulasex.TabIndex = 22;
            this.cb2aulasex.Text = "2ª Aula";
            this.cb2aulasex.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb2aulasex.UseVisualStyleBackColor = false;
            this.cb2aulasex.CheckedChanged += new System.EventHandler(this.cb2aulasex_CheckedChanged);
            // 
            // cb2aulaquin
            // 
            this.cb2aulaquin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb2aulaquin.AutoSize = true;
            this.cb2aulaquin.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb2aulaquin.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb2aulaquin.Location = new System.Drawing.Point(535, 71);
            this.cb2aulaquin.Margin = new System.Windows.Forms.Padding(2);
            this.cb2aulaquin.Name = "cb2aulaquin";
            this.cb2aulaquin.Size = new System.Drawing.Size(123, 17);
            this.cb2aulaquin.TabIndex = 21;
            this.cb2aulaquin.Text = "2ª Aula";
            this.cb2aulaquin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb2aulaquin.UseVisualStyleBackColor = false;
            this.cb2aulaquin.CheckedChanged += new System.EventHandler(this.cb2aulaquin_CheckedChanged);
            // 
            // cb2aulater
            // 
            this.cb2aulater.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb2aulater.AutoSize = true;
            this.cb2aulater.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb2aulater.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb2aulater.Location = new System.Drawing.Point(285, 71);
            this.cb2aulater.Margin = new System.Windows.Forms.Padding(2);
            this.cb2aulater.Name = "cb2aulater";
            this.cb2aulater.Size = new System.Drawing.Size(121, 17);
            this.cb2aulater.TabIndex = 20;
            this.cb2aulater.Text = "2ª Aula";
            this.cb2aulater.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb2aulater.UseVisualStyleBackColor = false;
            this.cb2aulater.CheckedChanged += new System.EventHandler(this.cb2aulater_CheckedChanged);
            // 
            // cb2aulaquar
            // 
            this.cb2aulaquar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb2aulaquar.AutoSize = true;
            this.cb2aulaquar.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb2aulaquar.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb2aulaquar.Location = new System.Drawing.Point(411, 71);
            this.cb2aulaquar.Margin = new System.Windows.Forms.Padding(2);
            this.cb2aulaquar.Name = "cb2aulaquar";
            this.cb2aulaquar.Size = new System.Drawing.Size(119, 17);
            this.cb2aulaquar.TabIndex = 20;
            this.cb2aulaquar.Text = "2ª Aula";
            this.cb2aulaquar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb2aulaquar.UseVisualStyleBackColor = false;
            this.cb2aulaquar.CheckedChanged += new System.EventHandler(this.cb2aulaquar_CheckedChanged);
            // 
            // cb1aulaquin
            // 
            this.cb1aulaquin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb1aulaquin.AutoSize = true;
            this.cb1aulaquin.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb1aulaquin.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb1aulaquin.Location = new System.Drawing.Point(535, 37);
            this.cb1aulaquin.Margin = new System.Windows.Forms.Padding(2);
            this.cb1aulaquin.Name = "cb1aulaquin";
            this.cb1aulaquin.Size = new System.Drawing.Size(123, 17);
            this.cb1aulaquin.TabIndex = 17;
            this.cb1aulaquin.Text = "1ª Aula";
            this.cb1aulaquin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb1aulaquin.UseVisualStyleBackColor = false;
            this.cb1aulaquin.CheckedChanged += new System.EventHandler(this.cb1aulaquin_CheckedChanged);
            // 
            // cb2aulaseg
            // 
            this.cb2aulaseg.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb2aulaseg.AutoSize = true;
            this.cb2aulaseg.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb2aulaseg.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb2aulaseg.Location = new System.Drawing.Point(107, 71);
            this.cb2aulaseg.Margin = new System.Windows.Forms.Padding(2);
            this.cb2aulaseg.Name = "cb2aulaseg";
            this.cb2aulaseg.Size = new System.Drawing.Size(173, 17);
            this.cb2aulaseg.TabIndex = 19;
            this.cb2aulaseg.Text = "2ª Aula";
            this.cb2aulaseg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb2aulaseg.UseVisualStyleBackColor = false;
            this.cb2aulaseg.CheckedChanged += new System.EventHandler(this.cb2aulaseg_CheckedChanged);
            // 
            // cb1aulasex
            // 
            this.cb1aulasex.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb1aulasex.AutoSize = true;
            this.cb1aulasex.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb1aulasex.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb1aulasex.Location = new System.Drawing.Point(663, 37);
            this.cb1aulasex.Margin = new System.Windows.Forms.Padding(2);
            this.cb1aulasex.Name = "cb1aulasex";
            this.cb1aulasex.Size = new System.Drawing.Size(125, 17);
            this.cb1aulasex.TabIndex = 18;
            this.cb1aulasex.Text = "1ª Aula";
            this.cb1aulasex.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb1aulasex.UseVisualStyleBackColor = false;
            this.cb1aulasex.CheckedChanged += new System.EventHandler(this.cb1aulasex_CheckedChanged);
            // 
            // cb1aulaquar
            // 
            this.cb1aulaquar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb1aulaquar.AutoSize = true;
            this.cb1aulaquar.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb1aulaquar.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb1aulaquar.Location = new System.Drawing.Point(411, 37);
            this.cb1aulaquar.Margin = new System.Windows.Forms.Padding(2);
            this.cb1aulaquar.Name = "cb1aulaquar";
            this.cb1aulaquar.Size = new System.Drawing.Size(119, 17);
            this.cb1aulaquar.TabIndex = 16;
            this.cb1aulaquar.Text = "1ª Aula";
            this.cb1aulaquar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb1aulaquar.UseVisualStyleBackColor = false;
            this.cb1aulaquar.CheckedChanged += new System.EventHandler(this.cb1aulaquar_CheckedChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.label13.ForeColor = System.Drawing.Color.DarkGreen;
            this.label13.Location = new System.Drawing.Point(535, 1);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(38, 13);
            this.label13.TabIndex = 12;
            this.label13.Text = "Quinta";
            // 
            // cb1aulater
            // 
            this.cb1aulater.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb1aulater.AutoSize = true;
            this.cb1aulater.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb1aulater.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb1aulater.Location = new System.Drawing.Point(285, 37);
            this.cb1aulater.Margin = new System.Windows.Forms.Padding(2);
            this.cb1aulater.Name = "cb1aulater";
            this.cb1aulater.Size = new System.Drawing.Size(121, 17);
            this.cb1aulater.TabIndex = 15;
            this.cb1aulater.Text = "1ª Aula";
            this.cb1aulater.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb1aulater.UseVisualStyleBackColor = false;
            this.cb1aulater.CheckedChanged += new System.EventHandler(this.cb1aulater_CheckedChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.DarkGreen;
            this.label14.Location = new System.Drawing.Point(663, 1);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(34, 13);
            this.label14.TabIndex = 13;
            this.label14.Text = "Sexta";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.label11.ForeColor = System.Drawing.Color.DarkGreen;
            this.label11.Location = new System.Drawing.Point(285, 1);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(35, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Terça";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.label10.ForeColor = System.Drawing.Color.DarkGreen;
            this.label10.Location = new System.Drawing.Point(107, 1);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(50, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "Segunda";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.label7.ForeColor = System.Drawing.Color.DarkGreen;
            this.label7.Location = new System.Drawing.Point(3, 239);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "14:20";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.label8.ForeColor = System.Drawing.Color.DarkGreen;
            this.label8.Location = new System.Drawing.Point(3, 273);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(34, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "15:30";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.label6.ForeColor = System.Drawing.Color.DarkGreen;
            this.label6.Location = new System.Drawing.Point(3, 205);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "13:30";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.label2.ForeColor = System.Drawing.Color.DarkGreen;
            this.label2.Location = new System.Drawing.Point(3, 35);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "7:30";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.label5.ForeColor = System.Drawing.Color.DarkGreen;
            this.label5.Location = new System.Drawing.Point(3, 171);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "11:10";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.label3.ForeColor = System.Drawing.Color.DarkGreen;
            this.label3.Location = new System.Drawing.Point(3, 103);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "9:10";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.label15.ForeColor = System.Drawing.Color.DarkGreen;
            this.label15.Location = new System.Drawing.Point(3, 69);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(28, 13);
            this.label15.TabIndex = 1;
            this.label15.Text = "8:20";
            // 
            // cb1aulaseg
            // 
            this.cb1aulaseg.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cb1aulaseg.AutoSize = true;
            this.cb1aulaseg.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.cb1aulaseg.ForeColor = System.Drawing.Color.DarkGreen;
            this.cb1aulaseg.Location = new System.Drawing.Point(107, 37);
            this.cb1aulaseg.Margin = new System.Windows.Forms.Padding(2);
            this.cb1aulaseg.Name = "cb1aulaseg";
            this.cb1aulaseg.Size = new System.Drawing.Size(173, 17);
            this.cb1aulaseg.TabIndex = 14;
            this.cb1aulaseg.Text = "1ª Aula";
            this.cb1aulaseg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.cb1aulaseg.UseVisualStyleBackColor = false;
            this.cb1aulaseg.CheckedChanged += new System.EventHandler(this.cb1aulaseg_CheckedChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.label12.ForeColor = System.Drawing.Color.DarkGreen;
            this.label12.Location = new System.Drawing.Point(411, 1);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(39, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "Quarta";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.label9.ForeColor = System.Drawing.Color.DarkGreen;
            this.label9.Location = new System.Drawing.Point(3, 307);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(34, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "16:20";
            // 
            // lblfeedback
            // 
            this.lblfeedback.AutoSize = true;
            this.lblfeedback.ForeColor = System.Drawing.Color.Red;
            this.lblfeedback.Location = new System.Drawing.Point(16, 280);
            this.lblfeedback.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblfeedback.Name = "lblfeedback";
            this.lblfeedback.Size = new System.Drawing.Size(0, 13);
            this.lblfeedback.TabIndex = 59;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button5);
            this.tabPage2.Controls.Add(this.dataGridView1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(815, 441);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Horario";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(686, 394);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(100, 36);
            this.button5.TabIndex = 11;
            this.button5.Text = "Atualizar";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.MediumSeaGreen;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(9, 15);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(800, 373);
            this.dataGridView1.TabIndex = 10;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.tabControl1);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(815, 441);
            this.tabPage5.TabIndex = 2;
            this.tabPage5.Text = "Escolher";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(809, 432);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.button4);
            this.tabPage6.Controls.Add(this.lblfeedbackT);
            this.tabPage6.Controls.Add(this.pictureBox12);
            this.tabPage6.Controls.Add(this.pictureBox11);
            this.tabPage6.Controls.Add(this.pbsetaesc);
            this.tabPage6.Controls.Add(this.pbsetadir);
            this.tabPage6.Controls.Add(this.lblecionadasT);
            this.tabPage6.Controls.Add(this.lbselecT);
            this.tabPage6.Controls.Add(this.lbdispT);
            this.tabPage6.Controls.Add(this.pictureBox8);
            this.tabPage6.Controls.Add(this.pictureBox7);
            this.tabPage6.Controls.Add(this.lblqtdaulasT);
            this.tabPage6.Controls.Add(this.pictureBox6);
            this.tabPage6.Controls.Add(this.cbmateriaT);
            this.tabPage6.Controls.Add(this.pictureBox5);
            this.tabPage6.Controls.Add(this.pictureBox4);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(801, 406);
            this.tabPage6.TabIndex = 0;
            this.tabPage6.Text = "Escolher turma";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(657, 198);
            this.button4.Margin = new System.Windows.Forms.Padding(2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(138, 40);
            this.button4.TabIndex = 18;
            this.button4.Text = "Cancelar";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // lblfeedbackT
            // 
            this.lblfeedbackT.AutoSize = true;
            this.lblfeedbackT.ForeColor = System.Drawing.Color.Red;
            this.lblfeedbackT.Location = new System.Drawing.Point(691, 304);
            this.lblfeedbackT.Name = "lblfeedbackT";
            this.lblfeedbackT.Size = new System.Drawing.Size(69, 13);
            this.lblfeedbackT.TabIndex = 15;
            this.lblfeedbackT.Text = "lblfeedbackT";
            this.lblfeedbackT.Click += new System.EventHandler(this.lblfeedbackT_Click);
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = global::tcc_teambompa.Properties.Resources.confirmar;
            this.pictureBox12.Location = new System.Drawing.Point(657, 146);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(138, 37);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox12.TabIndex = 14;
            this.pictureBox12.TabStop = false;
            this.pictureBox12.Click += new System.EventHandler(this.pictureBox12_Click);
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = global::tcc_teambompa.Properties.Resources.turmas_lecionadas;
            this.pictureBox11.Location = new System.Drawing.Point(473, 84);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(173, 37);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox11.TabIndex = 13;
            this.pictureBox11.TabStop = false;
            // 
            // pbsetaesc
            // 
            this.pbsetaesc.Image = global::tcc_teambompa.Properties.Resources.seta_esquerda;
            this.pbsetaesc.Location = new System.Drawing.Point(191, 179);
            this.pbsetaesc.Name = "pbsetaesc";
            this.pbsetaesc.Size = new System.Drawing.Size(50, 23);
            this.pbsetaesc.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbsetaesc.TabIndex = 12;
            this.pbsetaesc.TabStop = false;
            this.pbsetaesc.Click += new System.EventHandler(this.pbsetaesc_Click);
            // 
            // pbsetadir
            // 
            this.pbsetadir.Image = global::tcc_teambompa.Properties.Resources.seta_direita;
            this.pbsetadir.Location = new System.Drawing.Point(191, 226);
            this.pbsetadir.Name = "pbsetadir";
            this.pbsetadir.Size = new System.Drawing.Size(50, 25);
            this.pbsetadir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbsetadir.TabIndex = 11;
            this.pbsetadir.TabStop = false;
            this.pbsetadir.Click += new System.EventHandler(this.pbsetadir_Click);
            // 
            // lblecionadasT
            // 
            this.lblecionadasT.ForeColor = System.Drawing.Color.Green;
            this.lblecionadasT.FormattingEnabled = true;
            this.lblecionadasT.Location = new System.Drawing.Point(473, 131);
            this.lblecionadasT.Name = "lblecionadasT";
            this.lblecionadasT.Size = new System.Drawing.Size(173, 186);
            this.lblecionadasT.TabIndex = 9;
            // 
            // lbselecT
            // 
            this.lbselecT.ForeColor = System.Drawing.Color.Green;
            this.lbselecT.FormattingEnabled = true;
            this.lbselecT.Location = new System.Drawing.Point(250, 131);
            this.lbselecT.Name = "lbselecT";
            this.lbselecT.Size = new System.Drawing.Size(173, 186);
            this.lbselecT.TabIndex = 8;
            // 
            // lbdispT
            // 
            this.lbdispT.ForeColor = System.Drawing.Color.Green;
            this.lbdispT.FormattingEnabled = true;
            this.lbdispT.Location = new System.Drawing.Point(8, 131);
            this.lbdispT.Name = "lbdispT";
            this.lbdispT.Size = new System.Drawing.Size(173, 186);
            this.lbdispT.TabIndex = 7;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = global::tcc_teambompa.Properties.Resources.Excluir;
            this.pictureBox8.Location = new System.Drawing.Point(657, 254);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(138, 39);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 6;
            this.pictureBox8.TabStop = false;
            this.pictureBox8.Click += new System.EventHandler(this.pictureBox8_Click);
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::tcc_teambompa.Properties.Resources.turmas_selecionadas;
            this.pictureBox7.Location = new System.Drawing.Point(250, 84);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(173, 37);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 5;
            this.pictureBox7.TabStop = false;
            // 
            // lblqtdaulasT
            // 
            this.lblqtdaulasT.AutoSize = true;
            this.lblqtdaulasT.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lblqtdaulasT.Location = new System.Drawing.Point(501, 25);
            this.lblqtdaulasT.Name = "lblqtdaulasT";
            this.lblqtdaulasT.Size = new System.Drawing.Size(64, 13);
            this.lblqtdaulasT.TabIndex = 4;
            this.lblqtdaulasT.Text = "lblqtdaulasT";
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::tcc_teambompa.Properties.Resources.qtd_de_aula;
            this.pictureBox6.Location = new System.Drawing.Point(375, 16);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(109, 31);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 3;
            this.pictureBox6.TabStop = false;
            // 
            // cbmateriaT
            // 
            this.cbmateriaT.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbmateriaT.FormattingEnabled = true;
            this.cbmateriaT.Location = new System.Drawing.Point(105, 22);
            this.cbmateriaT.Name = "cbmateriaT";
            this.cbmateriaT.Size = new System.Drawing.Size(245, 21);
            this.cbmateriaT.TabIndex = 2;
            this.cbmateriaT.SelectedIndexChanged += new System.EventHandler(this.cbmateriaT_SelectedIndexChanged);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::tcc_teambompa.Properties.Resources.turmadisponivel;
            this.pictureBox5.Location = new System.Drawing.Point(8, 84);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(173, 37);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 1;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::tcc_teambompa.Properties.Resources.materia;
            this.pictureBox4.Location = new System.Drawing.Point(6, 16);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(93, 32);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 0;
            this.pictureBox4.TabStop = false;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.pictureBox13);
            this.tabPage7.Controls.Add(this.pictureBox14);
            this.tabPage7.Controls.Add(this.button3);
            this.tabPage7.Controls.Add(this.button2);
            this.tabPage7.Controls.Add(this.button1);
            this.tabPage7.Controls.Add(this.lbmateriadisp);
            this.tabPage7.Controls.Add(this.pictureBox10);
            this.tabPage7.Controls.Add(this.pictureBox9);
            this.tabPage7.Controls.Add(this.lblecionadasM);
            this.tabPage7.Controls.Add(this.lbselecM);
            this.tabPage7.Controls.Add(this.lblfeedbackM);
            this.tabPage7.Controls.Add(this.lblqtdaulasM);
            this.tabPage7.Controls.Add(this.pictureBox19);
            this.tabPage7.Controls.Add(this.pictureBox18);
            this.tabPage7.Controls.Add(this.pictureBox16);
            this.tabPage7.Controls.Add(this.lbdispM);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(801, 406);
            this.tabPage7.TabIndex = 1;
            this.tabPage7.Text = "Escolher materia";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // pictureBox13
            // 
            this.pictureBox13.Image = global::tcc_teambompa.Properties.Resources.seta_esquerda;
            this.pictureBox13.Location = new System.Drawing.Point(209, 196);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(50, 23);
            this.pictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox13.TabIndex = 21;
            this.pictureBox13.TabStop = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.Image = global::tcc_teambompa.Properties.Resources.seta_direita;
            this.pictureBox14.Location = new System.Drawing.Point(209, 251);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(50, 25);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox14.TabIndex = 20;
            this.pictureBox14.TabStop = false;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(402, 45);
            this.button3.Margin = new System.Windows.Forms.Padding(2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(32, 25);
            this.button3.TabIndex = 19;
            this.button3.Text = "vol";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(402, 5);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(32, 25);
            this.button2.TabIndex = 18;
            this.button2.Text = "escolher";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(662, 218);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(116, 37);
            this.button1.TabIndex = 17;
            this.button1.Text = "Cancelar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lbmateriadisp
            // 
            this.lbmateriadisp.Image = global::tcc_teambompa.Properties.Resources.turmas_lecionadas;
            this.lbmateriadisp.Location = new System.Drawing.Point(466, 102);
            this.lbmateriadisp.Name = "lbmateriadisp";
            this.lbmateriadisp.Size = new System.Drawing.Size(179, 34);
            this.lbmateriadisp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.lbmateriadisp.TabIndex = 16;
            this.lbmateriadisp.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = global::tcc_teambompa.Properties.Resources.turmas_selecionadas;
            this.pictureBox10.Location = new System.Drawing.Point(267, 102);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(179, 34);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox10.TabIndex = 15;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = global::tcc_teambompa.Properties.Resources.turmadisponivel;
            this.pictureBox9.Location = new System.Drawing.Point(21, 102);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(180, 34);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox9.TabIndex = 14;
            this.pictureBox9.TabStop = false;
            // 
            // lblecionadasM
            // 
            this.lblecionadasM.ForeColor = System.Drawing.Color.Green;
            this.lblecionadasM.FormattingEnabled = true;
            this.lblecionadasM.Location = new System.Drawing.Point(466, 142);
            this.lblecionadasM.Name = "lblecionadasM";
            this.lblecionadasM.Size = new System.Drawing.Size(179, 199);
            this.lblecionadasM.TabIndex = 13;
            this.lblecionadasM.SelectedIndexChanged += new System.EventHandler(this.lblecionadasM_SelectedIndexChanged);
            // 
            // lbselecM
            // 
            this.lbselecM.ForeColor = System.Drawing.Color.Green;
            this.lbselecM.FormattingEnabled = true;
            this.lbselecM.Location = new System.Drawing.Point(267, 142);
            this.lbselecM.Name = "lbselecM";
            this.lbselecM.Size = new System.Drawing.Size(179, 199);
            this.lbselecM.TabIndex = 12;
            // 
            // lblfeedbackM
            // 
            this.lblfeedbackM.AutoSize = true;
            this.lblfeedbackM.ForeColor = System.Drawing.Color.Red;
            this.lblfeedbackM.Location = new System.Drawing.Point(683, 328);
            this.lblfeedbackM.Name = "lblfeedbackM";
            this.lblfeedbackM.Size = new System.Drawing.Size(71, 13);
            this.lblfeedbackM.TabIndex = 10;
            this.lblfeedbackM.Text = "lblfeedbackM";
            // 
            // lblqtdaulasM
            // 
            this.lblqtdaulasM.AutoSize = true;
            this.lblqtdaulasM.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lblqtdaulasM.Location = new System.Drawing.Point(258, 31);
            this.lblqtdaulasM.Name = "lblqtdaulasM";
            this.lblqtdaulasM.Size = new System.Drawing.Size(66, 13);
            this.lblqtdaulasM.TabIndex = 6;
            this.lblqtdaulasM.Text = "lblqtdaulasM";
            // 
            // pictureBox19
            // 
            this.pictureBox19.Image = global::tcc_teambompa.Properties.Resources.confirmar;
            this.pictureBox19.Location = new System.Drawing.Point(662, 169);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(116, 37);
            this.pictureBox19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox19.TabIndex = 9;
            this.pictureBox19.TabStop = false;
            this.pictureBox19.Click += new System.EventHandler(this.pictureBox19_Click);
            // 
            // pictureBox18
            // 
            this.pictureBox18.Image = global::tcc_teambompa.Properties.Resources.Excluir;
            this.pictureBox18.Location = new System.Drawing.Point(663, 265);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(116, 37);
            this.pictureBox18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox18.TabIndex = 8;
            this.pictureBox18.TabStop = false;
            this.pictureBox18.Click += new System.EventHandler(this.pictureBox18_Click);
            // 
            // pictureBox16
            // 
            this.pictureBox16.Image = global::tcc_teambompa.Properties.Resources.qtddisponivel;
            this.pictureBox16.Location = new System.Drawing.Point(21, 20);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(222, 34);
            this.pictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox16.TabIndex = 5;
            this.pictureBox16.TabStop = false;
            // 
            // lbdispM
            // 
            this.lbdispM.ForeColor = System.Drawing.Color.Green;
            this.lbdispM.FormattingEnabled = true;
            this.lbdispM.Location = new System.Drawing.Point(21, 142);
            this.lbdispM.Name = "lbdispM";
            this.lbdispM.Size = new System.Drawing.Size(179, 199);
            this.lbdispM.TabIndex = 4;
            // 
            // lblprofnome
            // 
            this.lblprofnome.AutoSize = true;
            this.lblprofnome.ForeColor = System.Drawing.Color.Green;
            this.lblprofnome.Location = new System.Drawing.Point(3, 228);
            this.lblprofnome.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblprofnome.Name = "lblprofnome";
            this.lblprofnome.Size = new System.Drawing.Size(32, 13);
            this.lblprofnome.TabIndex = 12;
            this.lblprofnome.Text = "Olá, !";
            // 
            // LinkButton1
            // 
            this.LinkButton1.AutoSize = true;
            this.LinkButton1.LinkColor = System.Drawing.Color.Green;
            this.LinkButton1.Location = new System.Drawing.Point(763, 228);
            this.LinkButton1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LinkButton1.Name = "LinkButton1";
            this.LinkButton1.Size = new System.Drawing.Size(40, 13);
            this.LinkButton1.TabIndex = 1;
            this.LinkButton1.TabStop = true;
            this.LinkButton1.Text = "Logout";
            this.LinkButton1.VisitedLinkColor = System.Drawing.Color.Green;
            this.LinkButton1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.DarkGreen;
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Image = global::tcc_teambompa.Properties.Resources.Tempus;
            this.pictureBox2.Location = new System.Drawing.Point(-1, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(822, 223);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 10;
            this.pictureBox2.TabStop = false;
            // 
            // FrmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(823, 701);
            this.Controls.Add(this.LinkButton1);
            this.Controls.Add(this.lblprofnome);
            this.Controls.Add(this.tab);
            this.Controls.Add(this.pictureBox2);
            this.Name = "FrmPrincipal";
            this.Text = "Tempus";
            this.Load += new System.EventHandler(this.alterar_Load);
            this.tab.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsetaesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbsetadir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbmateriadisp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tab;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label lblprofnome;
        private System.Windows.Forms.LinkLabel LinkButton1;
        private System.Windows.Forms.Label lblfeedback;
        private System.Windows.Forms.Label lblqtdaulas;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label lblfeedbackT;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pbsetaesc;
        private System.Windows.Forms.PictureBox pbsetadir;
        private System.Windows.Forms.ListBox lblecionadasT;
        private System.Windows.Forms.ListBox lbselecT;
        private System.Windows.Forms.ListBox lbdispT;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Label lblqtdaulasT;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.ComboBox cbmateriaT;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.ListBox lbdispM;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.Label lblqtdaulasM;
        private System.Windows.Forms.Label lblfeedbackM;
        private System.Windows.Forms.PictureBox lbmateriadisp;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.ListBox lblecionadasM;
        private System.Windows.Forms.ListBox lbselecM;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.CheckBox cb9aulasex;
        private System.Windows.Forms.CheckBox cb9aulaquin;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox cb8aulaquin;
        private System.Windows.Forms.CheckBox cb9aulaquar;
        private System.Windows.Forms.CheckBox cb9aulater;
        private System.Windows.Forms.CheckBox cb9aulaseg;
        private System.Windows.Forms.CheckBox cb8aulasex;
        private System.Windows.Forms.CheckBox cb8aulaquar;
        private System.Windows.Forms.CheckBox cb8aulater;
        private System.Windows.Forms.CheckBox cb8aulaseg;
        private System.Windows.Forms.CheckBox cb7aulasex;
        private System.Windows.Forms.CheckBox cb7aulaquin;
        private System.Windows.Forms.CheckBox cb7aulaquar;
        private System.Windows.Forms.CheckBox cb7aulater;
        private System.Windows.Forms.CheckBox cb7aulaseg;
        private System.Windows.Forms.CheckBox cb6aulasex;
        private System.Windows.Forms.CheckBox cb6aulaquin;
        private System.Windows.Forms.CheckBox cb6aulaquar;
        private System.Windows.Forms.CheckBox cb6aulater;
        private System.Windows.Forms.CheckBox cb6aulaseg;
        private System.Windows.Forms.CheckBox cb5aulasex;
        private System.Windows.Forms.CheckBox cb5aulaquin;
        private System.Windows.Forms.CheckBox cb5aulaquar;
        private System.Windows.Forms.CheckBox cb5aulater;
        private System.Windows.Forms.CheckBox cb4aulasex;
        private System.Windows.Forms.CheckBox cb5aulaseg;
        private System.Windows.Forms.CheckBox cb4aulaquin;
        private System.Windows.Forms.CheckBox cb4aulaquar;
        private System.Windows.Forms.CheckBox cb4aulaseg;
        private System.Windows.Forms.CheckBox cb4aulater;
        private System.Windows.Forms.CheckBox cb3aulasex;
        private System.Windows.Forms.CheckBox cb3aulaquin;
        private System.Windows.Forms.CheckBox cb3aulaquar;
        private System.Windows.Forms.CheckBox cb3aulater;
        private System.Windows.Forms.CheckBox cb3aulaseg;
        private System.Windows.Forms.CheckBox cb2aulasex;
        private System.Windows.Forms.CheckBox cb2aulaquin;
        private System.Windows.Forms.CheckBox cb2aulater;
        private System.Windows.Forms.CheckBox cb2aulaquar;
        private System.Windows.Forms.CheckBox cb1aulaquin;
        private System.Windows.Forms.CheckBox cb2aulaseg;
        private System.Windows.Forms.CheckBox cb1aulasex;
        private System.Windows.Forms.CheckBox cb1aulaquar;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox cb1aulater;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.CheckBox cb1aulaseg;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label lblfeedbackD;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox14;
    }
}