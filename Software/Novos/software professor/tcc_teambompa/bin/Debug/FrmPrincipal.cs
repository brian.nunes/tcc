﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tcc_teambompa
{
    public partial class FrmPrincipal : Form
    {
        //declara vetor para armazenar disponibilidade de cada dia
        Boolean[,] semana = { {/*segunda*/ true, true, true, true, true, true, true, true, true },
                               {/*terca*/ true, true, true, true, true, true, true, true, true },
                               {/*quarta*/ true, true, true, true, true, true, true, true, true },
                               {/*quinta*/  true, true, true, true, true, true, true, true, true },
                               {/*sexta*/ true, true, true, true, true, true, true, true, true }};

        //declara vetor para armazenar a nova disponibilidade de cada dia
        Boolean[,] nova_disp = { {/*segunda*/ true, true, true, true, true, true, true, true, true },
                               {/*terca*/ true, true, true, true, true, true, true, true, true },
                               {/*quarta*/ true, true, true, true, true, true, true, true, true },
                               {/*quinta*/  true, true, true, true, true, true, true, true, true },
                               {/*sexta*/ true, true, true, true, true, true, true, true, true }};

        //declara variavel para trabalhar
        int nova_aulas_disp = sql.aulas_disp;
        //variavel para manipulação de strings
        string texto;
        //cria vetor
        int[] materiaslecionadas;
        int[] turmaslecionadas;
        public FrmPrincipal()
        {
            InitializeComponent();
        }

        private void alterar_Load(object sender, EventArgs e)
        {
            //fecha a conexão evitando possiveis conflitos
            sql.olecon.Close();

            //busca informações do prof logado
            sql.pesquisar("select nome from tab_prof where id_prof=" + sql.proflogado.ToString());

            if (sql.guarda.Read())//se ha um logado
            {
                //coloca o nome do professor na label
                lblprofnome.Text = "Olá, " + sql.guarda[0].ToString() + "!";
                //ativa o linkbutton para deslogar
                LinkButton1.Visible = true;
                LinkButton1.Enabled = true;
                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();
            }

        //buscar atual disposição do horario do professor
        for (int j = 0; j < 5; j++)//para cada dia da semana
        {

            sql.pesquisar("select * from tab_dia where fk_prof='" + sql.proflogado.ToString() + "' and dia_semana = '" + (j + 2).ToString() + "';");//executa a query de busca

            if (sql.guarda.Read())  //faz a leitura dos resultados obtidos
            {
                //analisa onde estão as aulas indisponíveis e guarda em variáveis
                for (int i = 0; i < 9; i++)//testa aula por aula
                {
                    if (sql.guarda[i + 2].ToString() != "1")//caso o valor 1(aula disponível) não seja encontrado...
                    {
                        semana[j, i] = false;//...a disponibilidade da variavel é setada para false
                    }
                }
            }
            //fecha as conexões
            sql.guarda.Close();
            sql.olecon.Close();
        }

        //defini nova disponibilidade igual a atual, para ser alterada em seguida
        for (int j = 0; j < 5; j++)
        {
            for (int i = 0; i < 9; i++)
            {
                nova_disp[j, i] = semana[j, i];
            }
        }


            //define os status das combobox apartir da variavel "nova_disp"
            #region Muda CB por CB

            cb1aulaseg.Checked = nova_disp[0, 0];
            cb2aulaseg.Checked = nova_disp[0, 1];
            cb3aulaseg.Checked = nova_disp[0, 2];
            cb4aulaseg.Checked = nova_disp[0, 3];
            cb5aulaseg.Checked = nova_disp[0, 4];
            cb6aulaseg.Checked = nova_disp[0, 5];
            cb7aulaseg.Checked = nova_disp[0, 6];
            cb8aulaseg.Checked = nova_disp[0, 7];
            cb9aulaseg.Checked = nova_disp[0, 8];

            cb1aulater.Checked = nova_disp[1, 0];
            cb2aulater.Checked = nova_disp[1, 1];
            cb3aulater.Checked = nova_disp[1, 2];
            cb4aulater.Checked = nova_disp[1, 3];
            cb5aulater.Checked = nova_disp[1, 4];
            cb6aulater.Checked = nova_disp[1, 5];
            cb7aulater.Checked = nova_disp[1, 6];
            cb8aulater.Checked = nova_disp[1, 7];
            cb9aulater.Checked = nova_disp[1, 8];

            cb1aulaquar.Checked = nova_disp[2, 0];
            cb2aulaquar.Checked = nova_disp[2, 1];
            cb3aulaquar.Checked = nova_disp[2, 2];
            cb4aulaquar.Checked = nova_disp[2, 3];
            cb5aulaquar.Checked = nova_disp[2, 4];
            cb6aulaquar.Checked = nova_disp[2, 5];
            cb7aulaquar.Checked = nova_disp[2, 6];
            cb8aulaquar.Checked = nova_disp[2, 7];
            cb9aulaquar.Checked = nova_disp[2, 8];

            cb1aulaquin.Checked = nova_disp[3, 0];
            cb2aulaquin.Checked = nova_disp[3, 1];
            cb3aulaquin.Checked = nova_disp[3, 2];
            cb4aulaquin.Checked = nova_disp[3, 3];
            cb5aulaquin.Checked = nova_disp[3, 4];
            cb6aulaquin.Checked = nova_disp[3, 5];
            cb7aulaquin.Checked = nova_disp[3, 6];
            cb8aulaquin.Checked = nova_disp[3, 7];
            cb9aulaquin.Checked = nova_disp[3, 8];

            cb1aulasex.Checked = nova_disp[4, 0];
            cb2aulasex.Checked = nova_disp[4, 1];
            cb3aulasex.Checked = nova_disp[4, 2];
            cb4aulasex.Checked = nova_disp[4, 3];
            cb5aulasex.Checked = nova_disp[4, 4];
            cb6aulasex.Checked = nova_disp[4, 5];
            cb7aulasex.Checked = nova_disp[4, 6];
            cb8aulasex.Checked = nova_disp[4, 7];
            cb9aulasex.Checked = nova_disp[4, 8];

            #endregion

            //atualiza label com número de aulas livres
            attnumaulas();

            //definir o numero de aulas disponiveis na coluna da tab prof
            sql.aulas_disp = nova_aulas_disp;

            //fecha as conexões
            sql.guarda.Close();
            sql.olecon.Close();
        }

        //método para deslogar e sair do programa
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //desloga o prof e fecha o form
            sql.proflogado = -1;
            this.Close();
            //aq o login voltaria
            Application.Exit();
        }

        //atualiza o número de aulas na label
        private void attnumaulas()
        {
            //defini as quantidade de aulas disponiveis na label
            lblqtdaulas.Text = nova_aulas_disp.ToString();
        }

        //método feito por cada combobox mudando o status de disponibilidade de uma determinada aula em um determinado dia
        private void mudaDisp(Boolean status, int dia, int aula)
        {
            if (nova_disp[dia, aula] != status)
            {
                nova_disp[dia, aula] = status;
                if (status == true)
                {
                    nova_aulas_disp++;
                }
                else
                {
                    nova_aulas_disp--;
                }
                attnumaulas();
            }
        }

        /*atualizar a variave de aulas disponiveis sempre que uma checkbox foi marcada ou desmarcada
        atualizar variavel do dia da nova diponibilidade com o valor da checkbox referente ao dia*/
        #region cbs
        private void cb1aulaseg_CheckedChanged(object sender, EventArgs e)
        {
            if (cb1aulaseg.Checked == true)
            {
                mudaDisp(true, 0, 0);
            }
            else
            {
                mudaDisp(false, 0, 0);
            }
        }

        private void cb2aulaseg_CheckedChanged(object sender, EventArgs e)
        {
            if (cb2aulaseg.Checked == true)
            {
                mudaDisp(true, 0, 1);
            }
            else
            {
                mudaDisp(false, 0, 1);
            }
        }

        private void cb3aulaseg_CheckedChanged(object sender, EventArgs e)
        {
            if (cb3aulaseg.Checked == true)
            {
                mudaDisp(true, 0, 2);
            }
            else
            {
                mudaDisp(false, 0, 2);
            }
        }

        private void cb4aulaseg_CheckedChanged(object sender, EventArgs e)
        {
            if (cb4aulaseg.Checked == true)
            {
                mudaDisp(true, 0, 3);
            }
            else
            {
                mudaDisp(false, 0, 3);
            }
        }

        private void cb5aulaseg_CheckedChanged(object sender, EventArgs e)
        {
            if (cb5aulaseg.Checked == true)
            {
                mudaDisp(true, 0, 4);
            }
            else
            {
                mudaDisp(false, 0, 4);
            }
        }

        private void cb6aulaseg_CheckedChanged(object sender, EventArgs e)
        {
            if (cb6aulaseg.Checked == true)
            {
                mudaDisp(true, 0, 5);
            }
            else
            {
                mudaDisp(false, 0, 5);
            }
        }

        private void cb7aulaseg_CheckedChanged(object sender, EventArgs e)
        {
            if (cb7aulaseg.Checked == true)
            {
                mudaDisp(true, 0, 6);
            }
            else
            {
                mudaDisp(false, 0, 6);
            }
        }

        private void cb8aulaseg_CheckedChanged(object sender, EventArgs e)
        {
            if (cb8aulaseg.Checked == true)
            {
                mudaDisp(true, 0, 7);
            }
            else
            {
                mudaDisp(false, 0, 7);
            }
        }

        private void cb9aulaseg_CheckedChanged(object sender, EventArgs e)
        {
            if (cb9aulaseg.Checked == true)
            {
                mudaDisp(true, 0, 8);
            }
            else
            {
                mudaDisp(false, 0, 8);
            }
        }

        private void cb1aulater_CheckedChanged(object sender, EventArgs e)
        {
            if (cb1aulater.Checked == true)
            {
                mudaDisp(true, 1, 0);
            }
            else
            {
                mudaDisp(false, 1, 0);
            }
        }

        private void cb2aulater_CheckedChanged(object sender, EventArgs e)
        {
            if (cb2aulater.Checked == true)
            {
                mudaDisp(true, 1, 1);
            }
            else
            {
                mudaDisp(false, 1, 1);
            }
        }

        private void cb3aulater_CheckedChanged(object sender, EventArgs e)
        {
            if (cb3aulater.Checked == true)
            {
                mudaDisp(true, 1, 2);
            }
            else
            {
                mudaDisp(false, 1, 2);
            }
        }

        private void cb4aulater_CheckedChanged(object sender, EventArgs e)
        {
            if (cb4aulater.Checked == true)
            {
                mudaDisp(true, 1, 3);
            }
            else
            {
                mudaDisp(false, 1, 3);
            }
        }

        private void cb5aulater_CheckedChanged(object sender, EventArgs e)
        {
            if (cb5aulater.Checked == true)
            {
                mudaDisp(true, 1, 4);
            }
            else
            {
                mudaDisp(false, 1, 4);
            }
        }

        private void cb6aulater_CheckedChanged(object sender, EventArgs e)
        {
            if (cb6aulater.Checked == true)
            {
                mudaDisp(true, 1, 5);
            }
            else
            {
                mudaDisp(false, 1, 5);
            }
        }

        private void cb7aulater_CheckedChanged(object sender, EventArgs e)
        {
            if (cb7aulater.Checked == true)
            {
                mudaDisp(true, 1, 6);
            }
            else
            {
                mudaDisp(false, 1, 6);
            }
        }

        private void cb8aulater_CheckedChanged(object sender, EventArgs e)
        {
            if (cb8aulater.Checked == true)
            {
                mudaDisp(true, 1, 7);
            }
            else
            {
                mudaDisp(false, 1, 7);
            }
        }

        private void cb9aulater_CheckedChanged(object sender, EventArgs e)
        {
            if (cb9aulater.Checked == true)
            {
                mudaDisp(true, 1, 8);
            }
            else
            {
                mudaDisp(false, 1, 8);
            }
        }

        private void cb1aulaquar_CheckedChanged(object sender, EventArgs e)
        {
            if (cb1aulaquar.Checked == true)
            {
                mudaDisp(true, 2, 0);
            }
            else
            {
                mudaDisp(false, 2, 0);
            }
        }

        private void cb2aulaquar_CheckedChanged(object sender, EventArgs e)
        {
            if (cb2aulaquar.Checked == true)
            {
                mudaDisp(true, 2, 1);
            }
            else
            {
                mudaDisp(false, 2, 1);
            }
        }

        private void cb3aulaquar_CheckedChanged(object sender, EventArgs e)
        {
            if (cb3aulaquar.Checked == true)
            {
                mudaDisp(true, 2, 2);
            }
            else
            {
                mudaDisp(false, 2, 2);
            }
        }

        private void cb4aulaquar_CheckedChanged(object sender, EventArgs e)
        {
            if (cb4aulaquar.Checked == true)
            {
                mudaDisp(true, 2, 3);
            }
            else
            {
                mudaDisp(false, 2, 3);
            }
        }

        private void cb5aulaquar_CheckedChanged(object sender, EventArgs e)
        {
            if (cb5aulaquar.Checked == true)
            {
                mudaDisp(true, 2, 4);
            }
            else
            {
                mudaDisp(false, 2, 4);
            }
        }

        private void cb6aulaquar_CheckedChanged(object sender, EventArgs e)
        {
            if (cb6aulaquar.Checked == true)
            {
                mudaDisp(true, 2, 5);
            }
            else
            {
                mudaDisp(false, 2, 5);
            }
        }

        private void cb7aulaquar_CheckedChanged(object sender, EventArgs e)
        {
            if (cb7aulaquar.Checked == true)
            {
                mudaDisp(true, 2, 6);
            }
            else
            {
                mudaDisp(false, 2, 6);
            }
        }

        private void cb8aulaquar_CheckedChanged(object sender, EventArgs e)
        {
            if (cb8aulaquar.Checked == true)
            {
                mudaDisp(true, 2, 7);
            }
            else
            {
                mudaDisp(false, 2, 7);
            }
        }

        private void cb9aulaquar_CheckedChanged(object sender, EventArgs e)
        {
            if (cb9aulaquar.Checked == true)
            {
                mudaDisp(true, 2, 8);
            }
            else
            {
                mudaDisp(false, 2, 8);
            }
        }

        private void cb1aulaquin_CheckedChanged(object sender, EventArgs e)
        {
            if (cb1aulaquin.Checked == true)
            {
                mudaDisp(true, 3, 0);
            }
            else
            {
                mudaDisp(false, 3, 0);
            }
        }

        private void cb2aulaquin_CheckedChanged(object sender, EventArgs e)
        {
            if (cb2aulaquin.Checked == true)
            {
                mudaDisp(true, 3, 1);
            }
            else
            {
                mudaDisp(false, 3, 1);
            }
        }

        private void cb3aulaquin_CheckedChanged(object sender, EventArgs e)
        {
            if (cb3aulaquin.Checked == true)
            {
                mudaDisp(true, 3, 2);
            }
            else
            {
                mudaDisp(false, 3, 2);
            }
        }

        private void cb4aulaquin_CheckedChanged(object sender, EventArgs e)
        {
            if (cb4aulaquin.Checked == true)
            {
                mudaDisp(true, 3, 3);
            }
            else
            {
                mudaDisp(false, 3, 3);
            }
        }

        private void cb5aulaquin_CheckedChanged(object sender, EventArgs e)
        {
            if (cb5aulaquin.Checked == true)
            {
                mudaDisp(true, 3, 4);
            }
            else
            {
                mudaDisp(false, 3, 4);
            }
        }

        private void cb6aulaquin_CheckedChanged(object sender, EventArgs e)
        {
            if (cb6aulaquin.Checked == true)
            {
                mudaDisp(true, 3, 5);
            }
            else
            {
                mudaDisp(false, 3, 5);
            }
        }

        private void cb7aulaquin_CheckedChanged(object sender, EventArgs e)
        {
            if (cb7aulaquin.Checked == true)
            {
                mudaDisp(true, 3, 6);
            }
            else
            {
                mudaDisp(false, 3, 6);
            }
        }

        private void cb8aulaquin_CheckedChanged(object sender, EventArgs e)
        {
            if (cb8aulaquin.Checked == true)
            {
                mudaDisp(true, 3, 7);
            }
            else
            {
                mudaDisp(false, 3, 7);
            }
        }

        private void cb9aulaquin_CheckedChanged(object sender, EventArgs e)
        {
            if (cb9aulaquin.Checked == true)
            {
                mudaDisp(true, 3, 8);
            }
            else
            {
                mudaDisp(false, 3, 8);
            }
        }

        private void cb1aulasex_CheckedChanged(object sender, EventArgs e)
        {
            if (cb1aulasex.Checked == true)
            {
                mudaDisp(true, 4, 0);
            }
            else
            {
                mudaDisp(false, 4, 0);
            }
        }

        private void cb2aulasex_CheckedChanged(object sender, EventArgs e)
        {
            if (cb2aulasex.Checked == true)
            {
                mudaDisp(true, 4, 1);
            }
            else
            {
                mudaDisp(false, 4, 1);
            }
        }

        private void cb3aulasex_CheckedChanged(object sender, EventArgs e)
        {
            if (cb3aulasex.Checked == true)
            {
                mudaDisp(true, 4, 2);
            }
            else
            {
                mudaDisp(false, 4, 2);
            }
        }

        private void cb4aulasex_CheckedChanged(object sender, EventArgs e)
        {
            if (cb4aulasex.Checked == true)
            {
                mudaDisp(true, 4, 3);
            }
            else
            {
                mudaDisp(false, 4, 3);
            }
        }

        private void cb5aulasex_CheckedChanged(object sender, EventArgs e)
        {
            if (cb5aulasex.Checked == true)
            {
                mudaDisp(true, 4, 4);
            }
            else
            {
                mudaDisp(false, 4, 4);
            }
        }

        private void cb6aulasex_CheckedChanged(object sender, EventArgs e)
        {
            if (cb6aulasex.Checked == true)
            {
                mudaDisp(true, 4, 5);
            }
            else
            {
                mudaDisp(false, 4, 5);
            }
        }

        private void cb7aulasex_CheckedChanged(object sender, EventArgs e)
        {
            if (cb7aulasex.Checked == true)
            {
                mudaDisp(true, 4, 6);
            }
            else
            {
                mudaDisp(false, 4, 6);
            }
        }

        private void cb8aulasex_CheckedChanged(object sender, EventArgs e)
        {
            if (cb8aulasex.Checked == true)
            {
                mudaDisp(true, 4, 7);
            }
            else
            {
                mudaDisp(false, 4, 7);
            }
        }

        private void cb9aulasex_CheckedChanged(object sender, EventArgs e)
        {
            if (cb9aulasex.Checked == true)
            {
                mudaDisp(true, 4, 8);
            }
            else
            {
                mudaDisp(false, 4, 8);
            }
        }
        #endregion

        private void lblfeedbackT_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //limpa listas
            lbdispM.Items.Clear();
            lbselecM.Items.Clear();
            lblecionadasM.Items.Clear();

            //inicia com número de aulas para as materias disponíveis no máximo
            sql.aulas_disp_materia = sql.aulas_disp;

            //buscar todas as matérias cadastradas
            sql.pesquisar("select * from tab_materia where id_materia != '666' and id_materia != '999';");

            while (sql.guarda.Read())  //faz a leitura dos resultados obtidos
            {
                //lista as materias uma a uma na dropdownlist
                lbdispM.Items.Add(sql.guarda[0].ToString() + " - " + sql.guarda[2].ToString() + " - " + sql.guarda[3].ToString() + " aulas/semana/turma");
            }

            //fecha as conexões
            sql.guarda.Close();
            sql.olecon.Close();

            //buscar todas as matérias previamente selecionadas
            sql.pesquisar("select * from relacao_prof_materia_temporaria where fk_prof = '" + sql.proflogado.ToString() + "';");

            int k = 0;
            while (sql.guarda.Read())  //faz a contagem dos resultados obtidos
            {
                //soma na contagem
                k++;
            }

            //fecha as conexões
            sql.guarda.Close();
            sql.olecon.Close();

            //cria vetor para as materias
            int[] materiasselecionadas = new int[k];

            //inicia variavel auxiliar
            int h = 0;

            //buscar todas as matérias previamente selecionadas
            sql.pesquisar("select * from relacao_prof_materia_temporaria where fk_prof = '" + sql.proflogado.ToString() + "';");

            while (sql.guarda.Read())  //faz a leitura dos resultados obtidos
            {
                //salva o cod da materia no vetor
                materiasselecionadas[h] = Convert.ToInt32(sql.guarda[1].ToString());
                h++;
            }

            //fecha as conexões
            sql.guarda.Close();
            sql.olecon.Close();

            //buscar todas as matérias previamente lecionadas
            sql.pesquisar("select * from relacao_prof_materia_permanente where fk_prof = '" + sql.proflogado.ToString() + "';");

            k = 0;
            while (sql.guarda.Read())  //faz a contagem dos resultados obtidos
            {
                //soma na contagem
                k++;
            }

            //fecha as conexões
            sql.guarda.Close();
            sql.olecon.Close();

            //instancia vetor para as materias
            materiaslecionadas = new int[k];

            //inicia variavel auxiliar
            h = 0;

            //buscar todas as matérias previamente selecionadas
            sql.pesquisar("select * from relacao_prof_materia_permanente where fk_prof = '" + sql.proflogado.ToString() + "';");

            while (sql.guarda.Read())  //faz a leitura dos resultados obtidos
            {
                //salva o cod da materia no vetor
                materiaslecionadas[h] = Convert.ToInt32(sql.guarda[1].ToString());
                h++;
            }

            //fecha as conexões
            sql.guarda.Close();
            sql.olecon.Close();

            //testar cada código das matérias selecionadas
            for (int i = 0; i < materiasselecionadas.Length; i++)
            {
                //em cada item da dropdownlist
                for (int j = 0; j < lbdispM.Items.Count; j++)
                {
                    texto = lbdispM.Items[j].ToString();
                    string codigo = "";
                    string letra = "";
                    int index = 0;
                    while (letra != " ")
                    {
                        codigo += letra;
                        letra = texto.Substring(index, 1);
                        index++;
                    }
                    //e deletar caso encontre
                    if (codigo == materiasselecionadas[i].ToString())
                    {
                        lbdispM.Items.RemoveAt(j);
                    }
                }
            }

            //fecha as conexões
            sql.guarda.Close();
            sql.olecon.Close();

            //testar cada código das matérias lecionadas
            for (int i = 0; i < materiaslecionadas.Length; i++)
            {
                //em cada item da lista
                for (int j = 0; j < lbdispM.Items.Count; j++)
                {
                    texto = lbdispM.Items[j].ToString();
                    string codigo = "";
                    string letra = "";
                    int index = 0;
                    while (letra != " ")
                    {
                        codigo += letra;
                        letra = texto.Substring(index, 1);
                        index++;
                    }
                    //e deletar caso encontre
                    if (codigo == materiaslecionadas[i].ToString())
                    {
                        lbdispM.Items.RemoveAt(j);
                    }
                }
            }

            //fecha as conexões
            sql.guarda.Close();
            sql.olecon.Close();

            //loop para buscar todas as matérias previamente selecionadas uma a uma
            for (int j = 0; j < materiasselecionadas.Length; j++)
            {
                //faz a busca
                sql.pesquisar("select * from tab_materia where id_materia = '" + materiasselecionadas[j].ToString() + "';");

                if (sql.guarda.Read())  //se achar
                {
                    //lista a materia na listbox
                    lbselecM.Items.Add(sql.guarda[0].ToString() + " - " + sql.guarda[2].ToString() + " - " + sql.guarda[3].ToString() + " aulas/semana/turma");

                    //diminui as aulas vagas com o numero referente à matéria
                    sql.aulas_disp_materia -= Convert.ToInt32(sql.guarda[3].ToString());
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();
            }

            //loop para buscar todas as matérias previamente lecionadas uma a uma
            for (int j = 0; j < materiaslecionadas.Length; j++)
            {
                //faz a busca
                sql.pesquisar("select * from tab_materia where id_materia = '" + materiaslecionadas[j].ToString() + "';");

                if (sql.guarda.Read())  //se achar
                {
                    //lista a materia na listbox
                    lblecionadasM.Items.Add(sql.guarda[0].ToString() + " - " + sql.guarda[2].ToString() + " - " + sql.guarda[3].ToString() + " aulas/semana/turma");

                    //diminui as aulas vagas com o numero referente à matéria
                    sql.aulas_disp_materia -= Convert.ToInt32(sql.guarda[3].ToString());
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();
            }

            //atualiza a label
            lblqtdaulasM.Text = sql.aulas_disp_materia.ToString();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            //seleciona o código da matéria escolhida
            texto = lbdispM.SelectedItem.ToString();
            string codigo = "";
            string letra = "";
            int index = 0;
            while (letra != " ")
            {
                codigo += letra;
                letra = texto.Substring(index, 1);
                index++;
            }

            //faz a busca da matéria selecionada
            sql.pesquisar("select * from tab_materia where id_materia = '" + codigo + "';");

            if (sql.guarda.Read())  //faz a leitura dos resultados obtidos
            {
                //confere se é possivel dar essas aulas
                if (sql.aulas_disp_materia - Convert.ToInt32(sql.guarda[3].ToString()) >= 0)
                {
                    sql.aulas_disp_materia = sql.aulas_disp_materia - Convert.ToInt32(sql.guarda[3].ToString());
                    lblqtdaulasM.Text = sql.aulas_disp_materia.ToString();
                    lbselecM.Items.Add(lbdispM.SelectedItem.ToString());
                    lblfeedbackM.Text = "";
                    lbdispM.Items.RemoveAt(lbdispM.SelectedIndex);
                }
                else
                {
                    lblfeedbackM.Text = "Número de aulas máxima excedido!";
                }
            }

            //fecha as conexões
            sql.guarda.Close();
            sql.olecon.Close();
        }

        private void pictureBox19_Click(object sender, EventArgs e)
        {
            //deleta registros prévios
            sql.executarcomando("DELETE FROM `relacao_prof_materia_temporaria` WHERE `relacao_prof_materia_temporaria`.`fk_prof` = " + sql.proflogado.ToString(), "Delete ");
            //faz um loop criando uma relação para cada matéria escolhida
            foreach (string materia in lbselecM.Items)
            {
                //seleciona o código da matéria
                string codigo = "";
                string letra = "";
                int index = 0;
                while (letra != " ")
                {
                    codigo += letra;
                    letra = materia.Substring(index, 1);
                    index++;
                }

                //insere a relação para a materia
                sql.executarcomando("INSERT INTO `relacao_prof_materia_temporaria` (`fk_prof`, `fk_materia`) VALUES ('" + sql.proflogado.ToString() + "', '" + codigo + "')", "Inserção ");
            }

            //lista codigo das atuais leciondas
            string[] lecionadas = new string[lblecionadasM.Items.Count];
            for(int y = 0; y < lblecionadasM.Items.Count; y++)
            {
                //seleciona o código da matéria
                string materia = lblecionadasM.Items[y].ToString();
                string codigo = "";
                string letra = "";
                int index = 0;
                while (letra != " ")
                {
                    codigo += letra;
                    letra = materia.Substring(index, 1);
                    index++;
                }
                lecionadas[y] = codigo;
            }

            //testa os codigos q n existem mais
            foreach(int codigo in materiaslecionadas)
            {
                bool foi = false;
                foreach(string codigo2 in lecionadas)
                {
                    if(codigo.ToString() == codigo2)
                    {
                        foi = true;
                    }
                }
                //e os deleta qnd os encontra
                if(foi != true)
                {
                    sql.executarcomando("DELETE FROM `relacao_prof_materia_permanente` WHERE `relacao_prof_materia_permanente`.`fk_materia` = " + codigo.ToString() + " AND `relacao_prof_materia_permanente`.`fk_prof` = " + sql.proflogado.ToString(), "Delete ");
                }
            }

            //ao fim da o feeddback ao usuario
            lblfeedbackM.Text = "Atualização com êxito";
        }

        private void pictureBox18_Click(object sender, EventArgs e)
        {
            //confere se há algo selecionado
            if (lblecionadasM.SelectedIndex != -1)
            {
                //seleciona o código da matéria escolhida
                texto = lblecionadasM.SelectedItem.ToString();
                string codigo = "";
                string letra = "";
                int index = 0;
                while (letra != " ")
                {
                    codigo += letra;
                    letra = texto.Substring(index, 1);
                    index++;
                }

                //faz a busca da matéria selecionada
                sql.pesquisar("select num_aulas_semanais from tab_materia where id_materia = '" + codigo + "';");

                //faz a leitura dos resultados obtidos
                if (sql.guarda.Read())
                {
                    //realoca as aulas utilizadas pela matéria para as aulas disponíveis
                    sql.aulas_disp_materia = sql.aulas_disp_materia + Convert.ToInt32(sql.guarda[0].ToString());
                    //atualiza label
                    lblqtdaulasM.Text = sql.aulas_disp_materia.ToString();
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //devolve a materia para a dropdownlist
                lbdispM.Items.Add(lblecionadasM.SelectedItem.ToString());

                //retira ela da listbox
                lblecionadasM.Items.RemoveAt(lblecionadasM.SelectedIndex);

                //da um feedback ao usuario pela label
                lblfeedbackM.Text = "Matéria removida.";
            }
        }


        //método de atualização da nova disponibilidade no banco de dados
        private void pictureBox3_Click(object sender, EventArgs e)
        {
            //atualizar variavel que armazena a nova disponibidade
            #region Métodos dos CBs

            cb1aulaseg_CheckedChanged(sender, e);
            cb2aulaseg_CheckedChanged(sender, e);
            cb3aulaseg_CheckedChanged(sender, e);
            cb4aulaseg_CheckedChanged(sender, e);
            cb5aulaseg_CheckedChanged(sender, e);
            cb6aulaseg_CheckedChanged(sender, e);
            cb7aulaseg_CheckedChanged(sender, e);
            cb8aulaseg_CheckedChanged(sender, e);
            cb9aulaseg_CheckedChanged(sender, e);

            cb1aulater_CheckedChanged(sender, e);
            cb2aulater_CheckedChanged(sender, e);
            cb3aulater_CheckedChanged(sender, e);
            cb4aulater_CheckedChanged(sender, e);
            cb5aulater_CheckedChanged(sender, e);
            cb6aulater_CheckedChanged(sender, e);
            cb7aulater_CheckedChanged(sender, e);
            cb8aulater_CheckedChanged(sender, e);
            cb9aulater_CheckedChanged(sender, e);

            cb1aulaquar_CheckedChanged(sender, e);
            cb2aulaquar_CheckedChanged(sender, e);
            cb3aulaquar_CheckedChanged(sender, e);
            cb4aulaquar_CheckedChanged(sender, e);
            cb5aulaquar_CheckedChanged(sender, e);
            cb6aulaquar_CheckedChanged(sender, e);
            cb7aulaquar_CheckedChanged(sender, e);
            cb8aulaquar_CheckedChanged(sender, e);
            cb9aulaquar_CheckedChanged(sender, e);

            cb1aulaquin_CheckedChanged(sender, e);
            cb2aulaquin_CheckedChanged(sender, e);
            cb3aulaquin_CheckedChanged(sender, e);
            cb4aulaquin_CheckedChanged(sender, e);
            cb5aulaquin_CheckedChanged(sender, e);
            cb6aulaquin_CheckedChanged(sender, e);
            cb7aulaquin_CheckedChanged(sender, e);
            cb8aulaquin_CheckedChanged(sender, e);
            cb9aulaquin_CheckedChanged(sender, e);

            cb1aulasex_CheckedChanged(sender, e);
            cb2aulasex_CheckedChanged(sender, e);
            cb3aulasex_CheckedChanged(sender, e);
            cb4aulasex_CheckedChanged(sender, e);
            cb5aulasex_CheckedChanged(sender, e);
            cb6aulasex_CheckedChanged(sender, e);
            cb7aulasex_CheckedChanged(sender, e);
            cb8aulasex_CheckedChanged(sender, e);
            cb9aulasex_CheckedChanged(sender, e);
            #endregion

            //atualiza o campo do professor
            sql.aulas_disp = nova_aulas_disp;
            string comandox = "UPDATE `tab_prof` SET `num_aula_disponivel` = '" + sql.aulas_disp.ToString() + "' WHERE `tab_prof`.`id_prof` = '" + sql.proflogado.ToString() + "';";
            sql.executarcomando(comandox, "Atualização ");

            //fecha as conexões
            sql.guarda.Close();
            sql.olecon.Close();

            //fazer o update de dia por dia nas 5 tabelas do banco de dados
            for (int j = 0; j < 5; j++)
            {
                string[] aula = new string[9];
                for(int p = 0; p < 9; p++)
                {
                    if(nova_disp[j, p] == false)
                    {
                        aula[p] = "2";
                    }
                    else
                    {
                        aula[p] = "1";
                    }
                }

                //formula e executa comando
                comandox = "UPDATE `tab_dia` SET `730` = '"+ aula[0] +"', `820` = '"+ aula[1] + "', `910` = '"+ aula[2] + "', `1020` = '"+ aula[3] + "', `1110` = '"+ aula[4] + "', `1330` = '"+ aula[5] + "', `1420` = '"+ aula[6] + "', `1530` = '"+ aula[7] + "', `1620` = '"+ aula[8] + "' WHERE `tab_dia`.`fk_prof` = "+sql.proflogado.ToString()+" AND dia_semana = "+(j+2).ToString()+"";
                sql.executarcomando(comandox, "Atualização ");

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();
            }

            lblfeedbackD.Text = "Atualização feita com sucesso";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //confere se há algo selecionado
            if (lbselecM.SelectedIndex != -1)
            {
                //seleciona o código da matéria escolhida
                texto = lbselecM.SelectedItem.ToString();
                string codigo = "";
                string letra = "";
                int index = 0;
                while (letra != " ")
                {
                    codigo += letra;
                    letra = texto.Substring(index, 1);
                    index++;
                }

                //faz a busca da matéria selecionada
                sql.pesquisar("select num_aulas_semanais from tab_materia where id_materia = '" + codigo + "';");

                //faz a leitura dos resultados obtidos
                if (sql.guarda.Read())
                {
                    //realoca as aulas utilizadas pela matéria para as aulas disponíveis
                    sql.aulas_disp_materia = sql.aulas_disp_materia + Convert.ToInt32(sql.guarda[0].ToString());
                    //atualiza label
                    lblqtdaulasM.Text = sql.aulas_disp_materia.ToString();
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //devolve a materia para a dropdownlist
                lbdispM.Items.Add(lbselecM.SelectedItem.ToString());

                //retira ela da listbox
                lbselecM.Items.RemoveAt(lbselecM.SelectedIndex);

                //da um feedback ao usuario pela label
                lblfeedbackM.Text = "Matéria removida.";
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            cbmateriaT.Items.Clear();

            //inicia com número de aulas para as materias disponíveis no máximo
            sql.aulas_disp_turma = sql.aulas_disp;

            //faz busca de matérias
            //buscar todas as matérias lecionadas do prof
            sql.pesquisar("select * from relacao_prof_materia_permanente where fk_prof='" + sql.proflogado + "';");

            int h = 0;

            while (sql.guarda.Read())  //faz a contagem dos resultados obtidos
            {
                h++;
            }

            int[] materiaslec = new int[h];
            h = 0;

            //fecha as conexões
            sql.guarda.Close();
            sql.olecon.Close();

            //buscar todas as matérias lecionadas do prof
            sql.pesquisar("select * from relacao_prof_materia_permanente where fk_prof='" + sql.proflogado + "';");

            while (sql.guarda.Read())  //faz a contagem dos resultados obtidos
            {
                materiaslec[h] = Convert.ToInt32(sql.guarda[0]);
            }

            //fecha as conexões
            sql.guarda.Close();
            sql.olecon.Close();


            for (h = 0; h < materiaslec.Length; h++)
            {
                //buscar todas as matérias lecionadas do prof
                sql.pesquisar("select * from tab_materia where id_materia='" + materiaslec[h].ToString() + "';");

                while (sql.guarda.Read())  //faz a leitura dos resultados obtidos
                {
                    //lista as materias uma a uma na dropdownlist
                    cbmateriaT.Items.Add(sql.guarda[0].ToString() + " - " + sql.guarda[2].ToString() + " - " + sql.guarda[3].ToString() + " aulas semanais");
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();
            }

            atualizarlists();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string[] codigo_aulas = new string[9];
            string[] codigo_classe = new string[9];
            string[] codigo_materia = new string[9];
            string[] num_salas = new string[9];
            string[] classes = new string[9];
            string[] materias = new string[9];
            string[,] segunda = new string[3, 9];
            string[,] terca = new string[3, 9];
            string[,] quarta = new string[3, 9];
            string[,] quinta = new string[3, 9];
            string[,] sexta = new string[3, 9];

            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Matéria2", typeof(string)));
            dt.Columns.Add(new DataColumn("Classe2", typeof(string)));
            dt.Columns.Add(new DataColumn("NumSala2", typeof(string)));

            dt.Columns.Add(new DataColumn("Matéria3", typeof(string)));
            dt.Columns.Add(new DataColumn("Classe3", typeof(string)));
            dt.Columns.Add(new DataColumn("NumSala3", typeof(string)));

            dt.Columns.Add(new DataColumn("Matéria4", typeof(string)));
            dt.Columns.Add(new DataColumn("Classe4", typeof(string)));
            dt.Columns.Add(new DataColumn("NumSala4", typeof(string)));

            dt.Columns.Add(new DataColumn("Matéria5", typeof(string)));
            dt.Columns.Add(new DataColumn("Classe5", typeof(string)));
            dt.Columns.Add(new DataColumn("NumSala5", typeof(string)));

            dt.Columns.Add(new DataColumn("Matéria6", typeof(string)));
            dt.Columns.Add(new DataColumn("Classe6", typeof(string)));
            dt.Columns.Add(new DataColumn("NumSala6", typeof(string)));


            //loop para buscar todas as aulas de cada dia da semana
            for (int s = 2; s < 7; s++)
            {
                //faz a busca
                sql.pesquisar("select * from tab_dia where fk_prof = '" + sql.proflogado.ToString() + "' and dia_semana = '" + s.ToString() + "';");

                if(sql.guarda.Read()) //ler
                {
                    codigo_aulas[0] = sql.guarda[2].ToString();
                    codigo_aulas[1] = sql.guarda[3].ToString();
                    codigo_aulas[2] = sql.guarda[4].ToString();
                    codigo_aulas[3] = sql.guarda[5].ToString();
                    codigo_aulas[4] = sql.guarda[6].ToString();
                    codigo_aulas[5] = sql.guarda[7].ToString();
                    codigo_aulas[6] = sql.guarda[8].ToString();
                    codigo_aulas[7] = sql.guarda[9].ToString();
                    codigo_aulas[8] = sql.guarda[10].ToString();
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //loop para buscar cada aula individualmente
                for (int a = 0; a < 9; a++)
                {
                    //faz a busca
                    sql.pesquisar("select * from tab_aula where id_aula = '" + codigo_aulas[a].ToString() + "';");

                    if (sql.guarda.Read())  //se achar
                    {
                        //armazena numero da sala
                        num_salas[a] = sql.guarda[1].ToString();

                        //armazena codigo da sala
                        codigo_classe[a] = sql.guarda[3].ToString();

                        //armazena codigo da materia
                        codigo_materia[a] = sql.guarda[4].ToString();
                    }

                    //fecha as conexões
                    sql.guarda.Close();
                    sql.olecon.Close();
                }

                //loop para buscar cada classe individualmente
                for (int a = 0; a < 9; a++)
                {
                    //faz a busca
                    sql.pesquisar("select * from tab_classe where id_classe = " + codigo_classe[a].ToString() + ";");

                    if (sql.guarda.Read())  //se achar
                    {
                        //armazena nome da sala
                        classes[a] = sql.guarda[2].ToString() + sql.guarda[3].ToString();
                    }

                    //fecha as conexões
                    sql.guarda.Close();
                    sql.olecon.Close();
                }

                //loop para buscar cada materia
                for (int a = 0; a < 9; a++)
                {
                    //faz a busca
                    sql.pesquisar("select * from tab_materia where id_materia = '" + codigo_materia[a].ToString() + "';");

                    if (sql.guarda.Read())  //se achar
                    {
                        //armazena nome da sala
                        materias[a] = sql.guarda[2].ToString();
                    }

                    //fecha as conexões
                    sql.guarda.Close();
                    sql.olecon.Close();
                }

                if(s == 2)
                {
                    for (int lk = 0; lk < 9; lk++)
                    {
                        segunda[0, lk] = materias[lk];
                    }

                    for (int lk = 0; lk < 9; lk++)
                    {
                        segunda[1, lk] = classes[lk];
                    }

                    for (int lk = 0; lk < 9; lk++)
                    {
                        segunda[2, lk] = num_salas[lk];
                    }
                }
                else if(s == 3)
                {
                    for (int lk = 0; lk < 9; lk++)
                    {
                        terca[0, lk] = materias[lk];
                    }

                    for (int lk = 0; lk < 9; lk++)
                    {
                        terca[1, lk] = classes[lk];
                    }

                    for (int lk = 0; lk < 9; lk++)
                    {
                        terca[2, lk] = num_salas[lk];
                    }
                }
                else if (s == 4)
                {
                    for (int lk = 0; lk < 9; lk++)
                    {
                        quarta[0, lk] = materias[lk];
                    }

                    for (int lk = 0; lk < 9; lk++)
                    {
                        quarta[1, lk] = classes[lk];
                    }

                    for (int lk = 0; lk < 9; lk++)
                    {
                        quarta[2, lk] = num_salas[lk];
                    }
                }
                else if (s == 5)
                {
                    for (int lk = 0; lk < 9; lk++)
                    {
                        quinta[0, lk] = materias[lk];
                    }

                    for (int lk = 0; lk < 9; lk++)
                    {
                        quinta[1, lk] = classes[lk];
                    }

                    for (int lk = 0; lk < 9; lk++)
                    {
                        quinta[2, lk] = num_salas[lk];
                    }
                }
                else if (s == 6)
                {
                    for (int lk = 0; lk < 9; lk++)
                    {
                        sexta[0, lk] = materias[lk];
                    }

                    for (int lk = 0; lk < 9; lk++)
                    {
                        sexta[1, lk] = classes[lk];
                    }

                    for (int lk = 0; lk < 9; lk++)
                    {
                        sexta[2, lk] = num_salas[lk];
                    }
                }
            }

            for (int lk = 0; lk < 9; lk++)
            {
                dt.Rows.Add(segunda[0, lk], segunda[1, lk], segunda[2, lk], terca[0, lk], terca[1, lk], terca[2, lk], quarta[0, lk], quarta[1, lk], quarta[2, lk], quinta[0, lk], quinta[1, lk], quinta[2, lk], sexta[0, lk], sexta[1, lk], sexta[2, lk]);
            }

            dataGridView1.DataSource = dt;
        }

        private void pbsetaesc_Click(object sender, EventArgs e)
        {
            //confere se há algo selecionado
            if (lbselecT.SelectedIndex != -1)
            {
                //seleciona o código da matéria escolhida
                texto = cbmateriaT.SelectedItem.ToString();
                string codigo = "";
                string letra = "";
                int index = 0;
                while (letra != " ")
                {
                    codigo += letra;
                    letra = texto.Substring(index, 1);
                    index++;
                }

                //faz a busca da matéria selecionada
                sql.pesquisar("select num_aulas_semanais from tab_materia where id_materia = '" + codigo + "';");

                //faz a leitura dos resultados obtidos
                if (sql.guarda.Read())
                {
                    //realoca as aulas utilizadas pela matéria para as aulas disponíveis
                    sql.aulas_disp_turma = sql.aulas_disp_turma + Convert.ToInt32(sql.guarda[0].ToString());
                    //atualiza label
                    lblqtdaulasT.Text = sql.aulas_disp_materia.ToString();
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //devolve a materia para a dropdownlist
                lbdispT.Items.Add(lbselecT.SelectedItem.ToString());

                //retira ela da listbox
                lbselecT.Items.RemoveAt(lbselecT.SelectedIndex);

                //da um feedback ao usuario pela label
                lblfeedbackT.Text = "Matéria removida.";
            }
        }

        private void pbsetadir_Click(object sender, EventArgs e)
        {
            if (lbdispT.SelectedIndex != -1)
            {
                //seleciona o código da matéria escolhida
                texto = cbmateriaT.SelectedItem.ToString();
                string codigo = "";
                string letra = "";
                int index = 0;
                while (letra != " ")
                {
                    codigo += letra;
                    letra = texto.Substring(index, 1);
                    index++;
                }

                //faz a busca da matéria selecionada
                sql.pesquisar("select * from tab_materia where id_materia = '" + codigo + "';");

                if (sql.guarda.Read())  //faz a leitura dos resultados obtidos
                {
                    //confere se é possivel dar essas aulas
                    if (sql.aulas_disp_turma - Convert.ToInt32(sql.guarda[3].ToString()) >= 0)
                    {
                        sql.aulas_disp_turma = sql.aulas_disp_turma - Convert.ToInt32(sql.guarda[3].ToString());
                        lblqtdaulasT.Text = sql.aulas_disp_turma.ToString();
                        lbselecT.Items.Add(lbdispT.SelectedItem.ToString());
                        lblfeedbackT.Text = "";
                        lbdispT.Items.RemoveAt(lbdispT.SelectedIndex);
                    }
                    else
                    {
                        lblfeedbackT.Text = "Número de aulas máxima excedido!";
                    }
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();
            }
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            //confere se há algo selecionado
            if (lblecionadasT.SelectedIndex != -1)
            {
                //seleciona o código da matéria escolhida
                texto = cbmateriaT.SelectedItem.ToString();
                string codigo = "";
                string letra = "";
                int index = 0;
                while (letra != " ")
                {
                    codigo += letra;
                    letra = texto.Substring(index, 1);
                    index++;
                }

                //faz a busca da matéria selecionada
                sql.pesquisar("select num_aulas_semanais from tab_materia where id_materia = '" + codigo + "';");

                //faz a leitura dos resultados obtidos
                if (sql.guarda.Read())
                {
                    //realoca as aulas utilizadas pela matéria para as aulas disponíveis
                    sql.aulas_disp_turma = sql.aulas_disp_turma + Convert.ToInt32(sql.guarda[0].ToString());
                    //atualiza label
                    lblqtdaulasT.Text = sql.aulas_disp_turma.ToString();
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //devolve a materia para a dropdownlist
                lbdispT.Items.Add(lblecionadasT.SelectedItem.ToString());

                //retira ela da listbox
                lblecionadasT.Items.RemoveAt(lblecionadasT.SelectedIndex);

                //da um feedback ao usuario pela label
                lblfeedbackT.Text = "Matéria removida.";
            }
        }

        private void pictureBox12_Click(object sender, EventArgs e)
        {
            //deleta registros prévios
            sql.executarcomando("DELETE FROM `relacao_prof_classe_temporaria` WHERE `relacao_prof_classe_temporaria`.`fk_prof` = " + sql.proflogado.ToString(), "Delete ");
            //faz um loop criando uma relação para cada matéria escolhida
            foreach (string turma in lbselecT.Items)
            {
                //seleciona o código da matéria
                string codigo = "";
                string letra = "";
                int index = 0;
                while (letra != " ")
                {
                    codigo += letra;
                    letra = turma.Substring(index, 1);
                    index++;
                }

                texto = cbmateriaT.SelectedItem.ToString();
                string codigo2 = "";
                letra = "";
                index = 0;
                while (letra != " ")
                {
                    codigo2 += letra;
                    letra = texto.Substring(index, 1);
                    index++;
                }

                //insere a relação para a materia
                sql.executarcomando("INSERT INTO `relacao_prof_classe_temporaria` (`fk_prof`, `fk_classe`, 'fk_materia') VALUES ('" + sql.proflogado.ToString() + "', '" + codigo + "', '" + codigo2 +  "')", "Inserção ");
            }

            //lista codigo das atuais leciondas
            string[] lecionadas = new string[lblecionadasT.Items.Count];
            for (int y = 0; y < lblecionadasT.Items.Count; y++)
            {
                //seleciona o código da matéria
                string turma = lblecionadasT.Items[y].ToString();
                string codigo = "";
                string letra = "";
                int index = 0;
                while (letra != " ")
                {
                    codigo += letra;
                    letra = turma.Substring(index, 1);
                    index++;
                }
                lecionadas[y] = codigo;
            }

            //testa os codigos q n existem mais
            foreach (int codigo in turmaslecionadas)
            {
                bool foi = false;
                foreach (string codigo2 in lecionadas)
                {
                    if (codigo.ToString() == codigo2)
                    {
                        foi = true;
                    }
                }
                //e os deleta qnd os encontra
                if (foi != true)
                {
                    texto = cbmateriaT.SelectedItem.ToString();
                    string codigo2 = "";
                    string letra = "";
                    int index = 0;
                    while (letra != " ")
                    {
                        codigo2 += letra;
                        letra = texto.Substring(index, 1);
                        index++;
                    }
                    sql.executarcomando("DELETE FROM `relacao_prof_classe_permanente` WHERE `relacao_prof_classe_permanente`.`fk_classe` = " + codigo.ToString() + " AND `relacao_prof_classe_permanente`.`fk_prof` = " + sql.proflogado.ToString() + " AND `relacao_prof_classe_permanente`.`fk_materia` = " + codigo2, "Delete ");
                }
            }

            //ao fim da o feeddback ao usuario
            lblfeedbackT.Text = "Atualização com êxito";
        }

        private void cbmateriaT_SelectedIndexChanged(object sender, EventArgs e)
        {
            atualizarlists();
        }

        private void atualizarlists()
        {
            if (cbmateriaT.SelectedIndex != -1)
            {
                //limpa listas
                lbdispT.Items.Clear();
                lbselecT.Items.Clear();
                lblecionadasT.Items.Clear();

                //busca matéria selecionada
                texto = cbmateriaT.Items[cbmateriaT.SelectedIndex].ToString();
                string codigo = "";
                string letra = "";
                int index = 0;
                while (letra != " ")
                {
                    codigo += letra;
                    letra = texto.Substring(index, 1);
                    index++;
                }

                int m = 0;

                //buscar todas os cursos cadastradas que tenham a materia
                sql.pesquisar("select * from relacao_materia_curso where fk_materia = '" + codigo.ToString() + "';");

                while (sql.guarda.Read())
                {
                    m++;
                }

                int[] curso = new int[m];
                m = 0;

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //buscar todas os cursos cadastradas que tenham a materia
                sql.pesquisar("select * from relacao_materia_curso where fk_materia = '" + codigo.ToString() + "';");

                while (sql.guarda.Read())
                {
                    curso[m] = Convert.ToInt32(sql.guarda[1]);
                    m++;
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //buscar todas as turmas cadastradas que sejam do curso
                for (m = 0; m < curso.Length; m++)
                {
                    sql.pesquisar("select * from tab_classe where fk_curso = '" + curso[m].ToString() + "';");

                    while (sql.guarda.Read())  //faz a leitura dos resultados obtidos
                    {
                        //lista as turmas uma a uma na dropdownlist
                        lbdispT.Items.Add(sql.guarda[0].ToString() + " - " + sql.guarda[1].ToString() + sql.guarda[2].ToString());
                    }
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //buscar todas as turmas previamente selecionadas
                sql.pesquisar("select * from relacao_prof_classe_temporaria where fk_prof = '" + sql.proflogado.ToString() + "';");

                int k = 0;
                while (sql.guarda.Read())  //faz a contagem dos resultados obtidos
                {
                    //soma na contagem
                    k++;
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //cria vetor para as materias
                int[] turmasselecionadas = new int[k];

                //inicia variavel auxiliar
                int h = 0;

                //buscar todas as turmas previamente selecionadas
                sql.pesquisar("select * from relacao_prof_classe_temporaria where fk_prof = '" + sql.proflogado.ToString() + "';");

                while (sql.guarda.Read())  //faz a leitura dos resultados obtidos
                {
                    //salva o cod da materia no vetor
                    turmasselecionadas[h] = Convert.ToInt32(sql.guarda[1].ToString());
                    h++;
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //buscar todas as turmas previamente lecionadas
                sql.pesquisar("select * from relacao_prof_classe_permanente where fk_prof = '" + sql.proflogado.ToString() + "';");

                k = 0;
                while (sql.guarda.Read())  //faz a contagem dos resultados obtidos
                {
                    //soma na contagem
                    k++;
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //instancia vetor para as materias
                turmaslecionadas = new int[k];

                //inicia variavel auxiliar
                h = 0;

                //buscar todas as matérias previamente selecionadas
                sql.pesquisar("select * from relacao_prof_classe_permanente where fk_prof = '" + sql.proflogado.ToString() + "';");

                while (sql.guarda.Read())  //faz a leitura dos resultados obtidos
                {
                    //salva o cod da materia no vetor
                    turmaslecionadas[h] = Convert.ToInt32(sql.guarda[1].ToString());
                    h++;
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //testar cada código das turmas selecionadas
                for (int i = 0; i < turmasselecionadas.Length; i++)
                {
                    //em cada item da dropdownlist
                    for (int j = 0; j < lbdispT.Items.Count; j++)
                    {
                        texto = lbdispT.Items[j].ToString();
                        codigo = "";
                        letra = "";
                        index = 0;
                        while (letra != " ")
                        {
                            codigo += letra;
                            letra = texto.Substring(index, 1);
                            index++;
                        }
                        //e deletar caso encontre
                        if (codigo == turmasselecionadas[i].ToString())
                        {
                            lbdispT.Items.RemoveAt(j);
                        }
                    }
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //testar cada código das matérias lecionadas
                for (int i = 0; i < turmaslecionadas.Length; i++)
                {
                    //em cada item da lista
                    for (int j = 0; j < lbdispT.Items.Count; j++)
                    {
                        texto = lbdispT.Items[j].ToString();
                        codigo = "";
                        letra = "";
                        index = 0;
                        while (letra != " ")
                        {
                            codigo += letra;
                            letra = texto.Substring(index, 1);
                            index++;
                        }
                        //e deletar caso encontre
                        if (codigo == turmaslecionadas[i].ToString())
                        {
                            lbdispT.Items.RemoveAt(j);
                        }
                    }
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //loop para buscar todas as turmass previamente selecionadas uma a uma
                for (int j = 0; j < turmasselecionadas.Length; j++)
                {
                    //faz a busca
                    sql.pesquisar("select * from tab_classe where id_classe = '" + turmasselecionadas[j].ToString() + "';");

                    if (sql.guarda.Read())  //se achar
                    {
                        //lista a materia na listbox
                        lbselecT.Items.Add(sql.guarda[0].ToString() + " - " + sql.guarda[1].ToString() + sql.guarda[2].ToString());

                        //diminui as aulas vagas com o numero referente à matéria
                        sql.aulas_disp_turma -= Convert.ToInt32(sql.guarda[3].ToString());
                    }

                    //fecha as conexões
                    sql.guarda.Close();
                    sql.olecon.Close();
                }

                //loop para buscar todas as turmas previamente lecionadas uma a uma
                for (int j = 0; j < turmaslecionadas.Length; j++)
                {
                    //faz a busca
                    sql.pesquisar("select * from tab_classe where id_classe = '" + turmaslecionadas[j].ToString() + "';");

                    if (sql.guarda.Read())  //se achar
                    {
                        //lista a materia na listbox
                        lblecionadasT.Items.Add(sql.guarda[0].ToString() + " - " + sql.guarda[1].ToString() + sql.guarda[2].ToString());

                        //diminui as aulas vagas com o numero referente à matéria
                        sql.aulas_disp_turma -= Convert.ToInt32(sql.guarda[3].ToString());
                    }

                    //fecha as conexões
                    sql.guarda.Close();
                    sql.olecon.Close();
                }

                //atualiza a label
                lblqtdaulasT.Text = sql.aulas_disp_turma.ToString();
            }
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void lblecionadasM_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
