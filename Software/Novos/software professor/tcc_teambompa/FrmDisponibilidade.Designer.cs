﻿namespace tcc_teambompa
{
    partial class FrmDisponibilidade
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDisponibilidade));
            this.lblqtdaulas = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.cb9aulasex = new System.Windows.Forms.CheckBox();
            this.cb9aulaquin = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cb8aulaquin = new System.Windows.Forms.CheckBox();
            this.cb9aulaquar = new System.Windows.Forms.CheckBox();
            this.cb9aulater = new System.Windows.Forms.CheckBox();
            this.cb9aulaseg = new System.Windows.Forms.CheckBox();
            this.cb8aulasex = new System.Windows.Forms.CheckBox();
            this.cb8aulaquar = new System.Windows.Forms.CheckBox();
            this.cb8aulater = new System.Windows.Forms.CheckBox();
            this.cb8aulaseg = new System.Windows.Forms.CheckBox();
            this.cb7aulasex = new System.Windows.Forms.CheckBox();
            this.cb7aulaquin = new System.Windows.Forms.CheckBox();
            this.cb7aulaquar = new System.Windows.Forms.CheckBox();
            this.cb7aulater = new System.Windows.Forms.CheckBox();
            this.cb7aulaseg = new System.Windows.Forms.CheckBox();
            this.cb6aulasex = new System.Windows.Forms.CheckBox();
            this.cb6aulaquin = new System.Windows.Forms.CheckBox();
            this.cb6aulaquar = new System.Windows.Forms.CheckBox();
            this.cb6aulater = new System.Windows.Forms.CheckBox();
            this.cb6aulaseg = new System.Windows.Forms.CheckBox();
            this.cb5aulasex = new System.Windows.Forms.CheckBox();
            this.cb5aulaquin = new System.Windows.Forms.CheckBox();
            this.cb5aulaquar = new System.Windows.Forms.CheckBox();
            this.cb5aulater = new System.Windows.Forms.CheckBox();
            this.cb4aulasex = new System.Windows.Forms.CheckBox();
            this.cb5aulaseg = new System.Windows.Forms.CheckBox();
            this.cb4aulaquin = new System.Windows.Forms.CheckBox();
            this.cb4aulaquar = new System.Windows.Forms.CheckBox();
            this.cb4aulaseg = new System.Windows.Forms.CheckBox();
            this.cb4aulater = new System.Windows.Forms.CheckBox();
            this.cb3aulasex = new System.Windows.Forms.CheckBox();
            this.cb3aulaquin = new System.Windows.Forms.CheckBox();
            this.cb3aulaquar = new System.Windows.Forms.CheckBox();
            this.cb3aulater = new System.Windows.Forms.CheckBox();
            this.cb3aulaseg = new System.Windows.Forms.CheckBox();
            this.cb2aulasex = new System.Windows.Forms.CheckBox();
            this.cb2aulaquin = new System.Windows.Forms.CheckBox();
            this.cb2aulater = new System.Windows.Forms.CheckBox();
            this.cb2aulaquar = new System.Windows.Forms.CheckBox();
            this.cb1aulaquin = new System.Windows.Forms.CheckBox();
            this.cb2aulaseg = new System.Windows.Forms.CheckBox();
            this.cb1aulasex = new System.Windows.Forms.CheckBox();
            this.cb1aulaquar = new System.Windows.Forms.CheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cb1aulater = new System.Windows.Forms.CheckBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.cb1aulaseg = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // lblqtdaulas
            // 
            this.lblqtdaulas.AutoSize = true;
            this.lblqtdaulas.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblqtdaulas.Location = new System.Drawing.Point(350, 260);
            this.lblqtdaulas.Name = "lblqtdaulas";
            this.lblqtdaulas.Size = new System.Drawing.Size(0, 20);
            this.lblqtdaulas.TabIndex = 65;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.LightGreen;
            this.tableLayoutPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.83508F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.83508F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.83508F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.82457F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.83508F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.83508F));
            this.tableLayoutPanel1.Controls.Add(this.cb9aulasex, 5, 9);
            this.tableLayoutPanel1.Controls.Add(this.cb9aulaquin, 4, 9);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.cb8aulaquin, 4, 8);
            this.tableLayoutPanel1.Controls.Add(this.cb9aulaquar, 3, 9);
            this.tableLayoutPanel1.Controls.Add(this.cb9aulater, 2, 9);
            this.tableLayoutPanel1.Controls.Add(this.cb9aulaseg, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.cb8aulasex, 5, 8);
            this.tableLayoutPanel1.Controls.Add(this.cb8aulaquar, 3, 8);
            this.tableLayoutPanel1.Controls.Add(this.cb8aulater, 2, 8);
            this.tableLayoutPanel1.Controls.Add(this.cb8aulaseg, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.cb7aulasex, 5, 7);
            this.tableLayoutPanel1.Controls.Add(this.cb7aulaquin, 4, 7);
            this.tableLayoutPanel1.Controls.Add(this.cb7aulaquar, 3, 7);
            this.tableLayoutPanel1.Controls.Add(this.cb7aulater, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.cb7aulaseg, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.cb6aulasex, 5, 6);
            this.tableLayoutPanel1.Controls.Add(this.cb6aulaquin, 4, 6);
            this.tableLayoutPanel1.Controls.Add(this.cb6aulaquar, 3, 6);
            this.tableLayoutPanel1.Controls.Add(this.cb6aulater, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.cb6aulaseg, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.cb5aulasex, 5, 5);
            this.tableLayoutPanel1.Controls.Add(this.cb5aulaquin, 4, 5);
            this.tableLayoutPanel1.Controls.Add(this.cb5aulaquar, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.cb5aulater, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.cb4aulasex, 5, 4);
            this.tableLayoutPanel1.Controls.Add(this.cb5aulaseg, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.cb4aulaquin, 4, 4);
            this.tableLayoutPanel1.Controls.Add(this.cb4aulaquar, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.cb4aulaseg, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.cb4aulater, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.cb3aulasex, 5, 3);
            this.tableLayoutPanel1.Controls.Add(this.cb3aulaquin, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this.cb3aulaquar, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.cb3aulater, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.cb3aulaseg, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.cb2aulasex, 5, 2);
            this.tableLayoutPanel1.Controls.Add(this.cb2aulaquin, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.cb2aulater, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.cb2aulaquar, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.cb1aulaquin, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.cb2aulaseg, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.cb1aulasex, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.cb1aulaquar, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.label13, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.cb1aulater, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label14, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.label11, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label10, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label15, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.cb1aulaseg, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label12, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 9);
            this.tableLayoutPanel1.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(34, 300);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 10;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1155, 531);
            this.tableLayoutPanel1.TabIndex = 64;
            // 
            // cb9aulasex
            // 
            this.cb9aulasex.AutoSize = true;
            this.cb9aulasex.BackColor = System.Drawing.Color.Transparent;
            this.cb9aulasex.ForeColor = System.Drawing.Color.Black;
            this.cb9aulasex.Location = new System.Drawing.Point(962, 481);
            this.cb9aulasex.Name = "cb9aulasex";
            this.cb9aulasex.Size = new System.Drawing.Size(86, 24);
            this.cb9aulasex.TabIndex = 56;
            this.cb9aulasex.Text = "9ª Aula";
            this.cb9aulasex.UseVisualStyleBackColor = false;
            this.cb9aulasex.CheckedChanged += new System.EventHandler(this.cb9aulasex_CheckedChanged);
            // 
            // cb9aulaquin
            // 
            this.cb9aulaquin.AutoSize = true;
            this.cb9aulaquin.BackColor = System.Drawing.Color.Transparent;
            this.cb9aulaquin.ForeColor = System.Drawing.Color.Black;
            this.cb9aulaquin.Location = new System.Drawing.Point(768, 481);
            this.cb9aulaquin.Name = "cb9aulaquin";
            this.cb9aulaquin.Size = new System.Drawing.Size(86, 24);
            this.cb9aulaquin.TabIndex = 55;
            this.cb9aulaquin.Text = "9ª Aula";
            this.cb9aulaquin.UseVisualStyleBackColor = false;
            this.cb9aulaquin.CheckedChanged += new System.EventHandler(this.cb9aulaquin_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(4, 213);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "10:20";
            // 
            // cb8aulaquin
            // 
            this.cb8aulaquin.AutoSize = true;
            this.cb8aulaquin.BackColor = System.Drawing.Color.Transparent;
            this.cb8aulaquin.ForeColor = System.Drawing.Color.Black;
            this.cb8aulaquin.Location = new System.Drawing.Point(768, 428);
            this.cb8aulaquin.Name = "cb8aulaquin";
            this.cb8aulaquin.Size = new System.Drawing.Size(86, 24);
            this.cb8aulaquin.TabIndex = 50;
            this.cb8aulaquin.Text = "8ª Aula";
            this.cb8aulaquin.UseVisualStyleBackColor = false;
            this.cb8aulaquin.CheckedChanged += new System.EventHandler(this.cb8aulaquin_CheckedChanged);
            // 
            // cb9aulaquar
            // 
            this.cb9aulaquar.AutoSize = true;
            this.cb9aulaquar.BackColor = System.Drawing.Color.Transparent;
            this.cb9aulaquar.ForeColor = System.Drawing.Color.Black;
            this.cb9aulaquar.Location = new System.Drawing.Point(586, 481);
            this.cb9aulaquar.Name = "cb9aulaquar";
            this.cb9aulaquar.Size = new System.Drawing.Size(86, 24);
            this.cb9aulaquar.TabIndex = 53;
            this.cb9aulaquar.Text = "9ª Aula";
            this.cb9aulaquar.UseVisualStyleBackColor = false;
            this.cb9aulaquar.CheckedChanged += new System.EventHandler(this.cb9aulaquar_CheckedChanged);
            // 
            // cb9aulater
            // 
            this.cb9aulater.AutoSize = true;
            this.cb9aulater.BackColor = System.Drawing.Color.Transparent;
            this.cb9aulater.ForeColor = System.Drawing.Color.Black;
            this.cb9aulater.Location = new System.Drawing.Point(392, 481);
            this.cb9aulater.Name = "cb9aulater";
            this.cb9aulater.Size = new System.Drawing.Size(86, 24);
            this.cb9aulater.TabIndex = 54;
            this.cb9aulater.Text = "9ª Aula";
            this.cb9aulater.UseVisualStyleBackColor = false;
            this.cb9aulater.CheckedChanged += new System.EventHandler(this.cb9aulater_CheckedChanged);
            // 
            // cb9aulaseg
            // 
            this.cb9aulaseg.AutoSize = true;
            this.cb9aulaseg.BackColor = System.Drawing.Color.Transparent;
            this.cb9aulaseg.ForeColor = System.Drawing.Color.Black;
            this.cb9aulaseg.Location = new System.Drawing.Point(198, 481);
            this.cb9aulaseg.Name = "cb9aulaseg";
            this.cb9aulaseg.Size = new System.Drawing.Size(86, 24);
            this.cb9aulaseg.TabIndex = 52;
            this.cb9aulaseg.Text = "9ª Aula";
            this.cb9aulaseg.UseVisualStyleBackColor = false;
            this.cb9aulaseg.CheckedChanged += new System.EventHandler(this.cb9aulaseg_CheckedChanged);
            // 
            // cb8aulasex
            // 
            this.cb8aulasex.AutoSize = true;
            this.cb8aulasex.BackColor = System.Drawing.Color.Transparent;
            this.cb8aulasex.ForeColor = System.Drawing.Color.Black;
            this.cb8aulasex.Location = new System.Drawing.Point(962, 428);
            this.cb8aulasex.Name = "cb8aulasex";
            this.cb8aulasex.Size = new System.Drawing.Size(86, 24);
            this.cb8aulasex.TabIndex = 51;
            this.cb8aulasex.Text = "8ª Aula";
            this.cb8aulasex.UseVisualStyleBackColor = false;
            this.cb8aulasex.CheckedChanged += new System.EventHandler(this.cb8aulasex_CheckedChanged);
            // 
            // cb8aulaquar
            // 
            this.cb8aulaquar.AutoSize = true;
            this.cb8aulaquar.BackColor = System.Drawing.Color.Transparent;
            this.cb8aulaquar.ForeColor = System.Drawing.Color.Black;
            this.cb8aulaquar.Location = new System.Drawing.Point(586, 428);
            this.cb8aulaquar.Name = "cb8aulaquar";
            this.cb8aulaquar.Size = new System.Drawing.Size(86, 24);
            this.cb8aulaquar.TabIndex = 50;
            this.cb8aulaquar.Text = "8ª Aula";
            this.cb8aulaquar.UseVisualStyleBackColor = false;
            this.cb8aulaquar.CheckedChanged += new System.EventHandler(this.cb8aulaquar_CheckedChanged);
            // 
            // cb8aulater
            // 
            this.cb8aulater.AutoSize = true;
            this.cb8aulater.BackColor = System.Drawing.Color.Transparent;
            this.cb8aulater.ForeColor = System.Drawing.Color.Black;
            this.cb8aulater.Location = new System.Drawing.Point(392, 428);
            this.cb8aulater.Name = "cb8aulater";
            this.cb8aulater.Size = new System.Drawing.Size(86, 24);
            this.cb8aulater.TabIndex = 49;
            this.cb8aulater.Text = "8ª Aula";
            this.cb8aulater.UseVisualStyleBackColor = false;
            this.cb8aulater.CheckedChanged += new System.EventHandler(this.cb8aulater_CheckedChanged);
            // 
            // cb8aulaseg
            // 
            this.cb8aulaseg.AutoSize = true;
            this.cb8aulaseg.BackColor = System.Drawing.Color.Transparent;
            this.cb8aulaseg.ForeColor = System.Drawing.Color.Black;
            this.cb8aulaseg.Location = new System.Drawing.Point(198, 428);
            this.cb8aulaseg.Name = "cb8aulaseg";
            this.cb8aulaseg.Size = new System.Drawing.Size(86, 24);
            this.cb8aulaseg.TabIndex = 48;
            this.cb8aulaseg.Text = "8ª Aula";
            this.cb8aulaseg.UseVisualStyleBackColor = false;
            this.cb8aulaseg.CheckedChanged += new System.EventHandler(this.cb8aulaseg_CheckedChanged);
            // 
            // cb7aulasex
            // 
            this.cb7aulasex.AutoSize = true;
            this.cb7aulasex.BackColor = System.Drawing.Color.Transparent;
            this.cb7aulasex.ForeColor = System.Drawing.Color.Black;
            this.cb7aulasex.Location = new System.Drawing.Point(962, 375);
            this.cb7aulasex.Name = "cb7aulasex";
            this.cb7aulasex.Size = new System.Drawing.Size(86, 24);
            this.cb7aulasex.TabIndex = 47;
            this.cb7aulasex.Text = "7ª Aula";
            this.cb7aulasex.UseVisualStyleBackColor = false;
            this.cb7aulasex.CheckedChanged += new System.EventHandler(this.cb7aulasex_CheckedChanged);
            // 
            // cb7aulaquin
            // 
            this.cb7aulaquin.AutoSize = true;
            this.cb7aulaquin.BackColor = System.Drawing.Color.Transparent;
            this.cb7aulaquin.ForeColor = System.Drawing.Color.Black;
            this.cb7aulaquin.Location = new System.Drawing.Point(768, 375);
            this.cb7aulaquin.Name = "cb7aulaquin";
            this.cb7aulaquin.Size = new System.Drawing.Size(86, 24);
            this.cb7aulaquin.TabIndex = 46;
            this.cb7aulaquin.Text = "7ª Aula";
            this.cb7aulaquin.UseVisualStyleBackColor = false;
            this.cb7aulaquin.CheckedChanged += new System.EventHandler(this.cb7aulaquin_CheckedChanged);
            // 
            // cb7aulaquar
            // 
            this.cb7aulaquar.AutoSize = true;
            this.cb7aulaquar.BackColor = System.Drawing.Color.Transparent;
            this.cb7aulaquar.ForeColor = System.Drawing.Color.Black;
            this.cb7aulaquar.Location = new System.Drawing.Point(586, 375);
            this.cb7aulaquar.Name = "cb7aulaquar";
            this.cb7aulaquar.Size = new System.Drawing.Size(86, 24);
            this.cb7aulaquar.TabIndex = 45;
            this.cb7aulaquar.Text = "7ª Aula";
            this.cb7aulaquar.UseVisualStyleBackColor = false;
            this.cb7aulaquar.CheckedChanged += new System.EventHandler(this.cb7aulaquar_CheckedChanged);
            // 
            // cb7aulater
            // 
            this.cb7aulater.AutoSize = true;
            this.cb7aulater.BackColor = System.Drawing.Color.Transparent;
            this.cb7aulater.ForeColor = System.Drawing.Color.Black;
            this.cb7aulater.Location = new System.Drawing.Point(392, 375);
            this.cb7aulater.Name = "cb7aulater";
            this.cb7aulater.Size = new System.Drawing.Size(86, 24);
            this.cb7aulater.TabIndex = 44;
            this.cb7aulater.Text = "7ª Aula";
            this.cb7aulater.UseVisualStyleBackColor = false;
            this.cb7aulater.CheckedChanged += new System.EventHandler(this.cb7aulater_CheckedChanged);
            // 
            // cb7aulaseg
            // 
            this.cb7aulaseg.AutoSize = true;
            this.cb7aulaseg.BackColor = System.Drawing.Color.Transparent;
            this.cb7aulaseg.ForeColor = System.Drawing.Color.Black;
            this.cb7aulaseg.Location = new System.Drawing.Point(198, 375);
            this.cb7aulaseg.Name = "cb7aulaseg";
            this.cb7aulaseg.Size = new System.Drawing.Size(86, 24);
            this.cb7aulaseg.TabIndex = 43;
            this.cb7aulaseg.Text = "7ª Aula";
            this.cb7aulaseg.UseVisualStyleBackColor = false;
            this.cb7aulaseg.CheckedChanged += new System.EventHandler(this.cb7aulaseg_CheckedChanged);
            // 
            // cb6aulasex
            // 
            this.cb6aulasex.AutoSize = true;
            this.cb6aulasex.BackColor = System.Drawing.Color.Transparent;
            this.cb6aulasex.ForeColor = System.Drawing.Color.Black;
            this.cb6aulasex.Location = new System.Drawing.Point(962, 322);
            this.cb6aulasex.Name = "cb6aulasex";
            this.cb6aulasex.Size = new System.Drawing.Size(86, 24);
            this.cb6aulasex.TabIndex = 42;
            this.cb6aulasex.Text = "6ª Aula";
            this.cb6aulasex.UseVisualStyleBackColor = false;
            this.cb6aulasex.CheckedChanged += new System.EventHandler(this.cb6aulasex_CheckedChanged);
            // 
            // cb6aulaquin
            // 
            this.cb6aulaquin.AutoSize = true;
            this.cb6aulaquin.BackColor = System.Drawing.Color.Transparent;
            this.cb6aulaquin.ForeColor = System.Drawing.Color.Black;
            this.cb6aulaquin.Location = new System.Drawing.Point(768, 322);
            this.cb6aulaquin.Name = "cb6aulaquin";
            this.cb6aulaquin.Size = new System.Drawing.Size(86, 24);
            this.cb6aulaquin.TabIndex = 41;
            this.cb6aulaquin.Text = "6ª Aula";
            this.cb6aulaquin.UseVisualStyleBackColor = false;
            this.cb6aulaquin.CheckedChanged += new System.EventHandler(this.cb6aulaquin_CheckedChanged);
            // 
            // cb6aulaquar
            // 
            this.cb6aulaquar.AutoSize = true;
            this.cb6aulaquar.BackColor = System.Drawing.Color.Transparent;
            this.cb6aulaquar.ForeColor = System.Drawing.Color.Black;
            this.cb6aulaquar.Location = new System.Drawing.Point(586, 322);
            this.cb6aulaquar.Name = "cb6aulaquar";
            this.cb6aulaquar.Size = new System.Drawing.Size(86, 24);
            this.cb6aulaquar.TabIndex = 40;
            this.cb6aulaquar.Text = "6ª Aula";
            this.cb6aulaquar.UseVisualStyleBackColor = false;
            this.cb6aulaquar.CheckedChanged += new System.EventHandler(this.cb6aulaquar_CheckedChanged);
            // 
            // cb6aulater
            // 
            this.cb6aulater.AutoSize = true;
            this.cb6aulater.BackColor = System.Drawing.Color.Transparent;
            this.cb6aulater.ForeColor = System.Drawing.Color.Black;
            this.cb6aulater.Location = new System.Drawing.Point(392, 322);
            this.cb6aulater.Name = "cb6aulater";
            this.cb6aulater.Size = new System.Drawing.Size(86, 24);
            this.cb6aulater.TabIndex = 39;
            this.cb6aulater.Text = "6ª Aula";
            this.cb6aulater.UseVisualStyleBackColor = false;
            this.cb6aulater.CheckedChanged += new System.EventHandler(this.cb6aulater_CheckedChanged);
            // 
            // cb6aulaseg
            // 
            this.cb6aulaseg.AutoSize = true;
            this.cb6aulaseg.BackColor = System.Drawing.Color.Transparent;
            this.cb6aulaseg.ForeColor = System.Drawing.Color.Black;
            this.cb6aulaseg.Location = new System.Drawing.Point(198, 322);
            this.cb6aulaseg.Name = "cb6aulaseg";
            this.cb6aulaseg.Size = new System.Drawing.Size(86, 24);
            this.cb6aulaseg.TabIndex = 38;
            this.cb6aulaseg.Text = "6ª Aula";
            this.cb6aulaseg.UseVisualStyleBackColor = false;
            this.cb6aulaseg.CheckedChanged += new System.EventHandler(this.cb6aulaseg_CheckedChanged);
            // 
            // cb5aulasex
            // 
            this.cb5aulasex.AutoSize = true;
            this.cb5aulasex.BackColor = System.Drawing.Color.Transparent;
            this.cb5aulasex.ForeColor = System.Drawing.Color.Black;
            this.cb5aulasex.Location = new System.Drawing.Point(962, 269);
            this.cb5aulasex.Name = "cb5aulasex";
            this.cb5aulasex.Size = new System.Drawing.Size(86, 24);
            this.cb5aulasex.TabIndex = 37;
            this.cb5aulasex.Text = "5ª Aula";
            this.cb5aulasex.UseVisualStyleBackColor = false;
            this.cb5aulasex.CheckedChanged += new System.EventHandler(this.cb5aulasex_CheckedChanged);
            // 
            // cb5aulaquin
            // 
            this.cb5aulaquin.AutoSize = true;
            this.cb5aulaquin.BackColor = System.Drawing.Color.Transparent;
            this.cb5aulaquin.ForeColor = System.Drawing.Color.Black;
            this.cb5aulaquin.Location = new System.Drawing.Point(768, 269);
            this.cb5aulaquin.Name = "cb5aulaquin";
            this.cb5aulaquin.Size = new System.Drawing.Size(86, 24);
            this.cb5aulaquin.TabIndex = 36;
            this.cb5aulaquin.Text = "5ª Aula";
            this.cb5aulaquin.UseVisualStyleBackColor = false;
            this.cb5aulaquin.CheckedChanged += new System.EventHandler(this.cb5aulaquin_CheckedChanged);
            // 
            // cb5aulaquar
            // 
            this.cb5aulaquar.AutoSize = true;
            this.cb5aulaquar.BackColor = System.Drawing.Color.Transparent;
            this.cb5aulaquar.ForeColor = System.Drawing.Color.Black;
            this.cb5aulaquar.Location = new System.Drawing.Point(586, 269);
            this.cb5aulaquar.Name = "cb5aulaquar";
            this.cb5aulaquar.Size = new System.Drawing.Size(86, 24);
            this.cb5aulaquar.TabIndex = 35;
            this.cb5aulaquar.Text = "5ª Aula";
            this.cb5aulaquar.UseVisualStyleBackColor = false;
            this.cb5aulaquar.CheckedChanged += new System.EventHandler(this.cb5aulaquar_CheckedChanged);
            // 
            // cb5aulater
            // 
            this.cb5aulater.AutoSize = true;
            this.cb5aulater.BackColor = System.Drawing.Color.Transparent;
            this.cb5aulater.ForeColor = System.Drawing.Color.Black;
            this.cb5aulater.Location = new System.Drawing.Point(392, 269);
            this.cb5aulater.Name = "cb5aulater";
            this.cb5aulater.Size = new System.Drawing.Size(86, 24);
            this.cb5aulater.TabIndex = 34;
            this.cb5aulater.Text = "5ª Aula";
            this.cb5aulater.UseVisualStyleBackColor = false;
            this.cb5aulater.CheckedChanged += new System.EventHandler(this.cb5aulater_CheckedChanged);
            // 
            // cb4aulasex
            // 
            this.cb4aulasex.AutoSize = true;
            this.cb4aulasex.BackColor = System.Drawing.Color.Transparent;
            this.cb4aulasex.ForeColor = System.Drawing.Color.Black;
            this.cb4aulasex.Location = new System.Drawing.Point(962, 216);
            this.cb4aulasex.Name = "cb4aulasex";
            this.cb4aulasex.Size = new System.Drawing.Size(86, 24);
            this.cb4aulasex.TabIndex = 32;
            this.cb4aulasex.Text = "4ª Aula";
            this.cb4aulasex.UseVisualStyleBackColor = false;
            this.cb4aulasex.CheckedChanged += new System.EventHandler(this.cb4aulasex_CheckedChanged);
            // 
            // cb5aulaseg
            // 
            this.cb5aulaseg.AutoSize = true;
            this.cb5aulaseg.BackColor = System.Drawing.Color.Transparent;
            this.cb5aulaseg.ForeColor = System.Drawing.Color.Black;
            this.cb5aulaseg.Location = new System.Drawing.Point(198, 269);
            this.cb5aulaseg.Name = "cb5aulaseg";
            this.cb5aulaseg.Size = new System.Drawing.Size(86, 24);
            this.cb5aulaseg.TabIndex = 33;
            this.cb5aulaseg.Text = "5ª Aula";
            this.cb5aulaseg.UseVisualStyleBackColor = false;
            this.cb5aulaseg.CheckedChanged += new System.EventHandler(this.cb5aulaseg_CheckedChanged);
            // 
            // cb4aulaquin
            // 
            this.cb4aulaquin.AutoSize = true;
            this.cb4aulaquin.BackColor = System.Drawing.Color.Transparent;
            this.cb4aulaquin.ForeColor = System.Drawing.Color.Black;
            this.cb4aulaquin.Location = new System.Drawing.Point(768, 216);
            this.cb4aulaquin.Name = "cb4aulaquin";
            this.cb4aulaquin.Size = new System.Drawing.Size(86, 24);
            this.cb4aulaquin.TabIndex = 31;
            this.cb4aulaquin.Text = "4ª Aula";
            this.cb4aulaquin.UseVisualStyleBackColor = false;
            this.cb4aulaquin.CheckedChanged += new System.EventHandler(this.cb4aulaquin_CheckedChanged);
            // 
            // cb4aulaquar
            // 
            this.cb4aulaquar.AutoSize = true;
            this.cb4aulaquar.BackColor = System.Drawing.Color.Transparent;
            this.cb4aulaquar.ForeColor = System.Drawing.Color.Black;
            this.cb4aulaquar.Location = new System.Drawing.Point(586, 216);
            this.cb4aulaquar.Name = "cb4aulaquar";
            this.cb4aulaquar.Size = new System.Drawing.Size(86, 24);
            this.cb4aulaquar.TabIndex = 30;
            this.cb4aulaquar.Text = "4ª Aula";
            this.cb4aulaquar.UseVisualStyleBackColor = false;
            this.cb4aulaquar.CheckedChanged += new System.EventHandler(this.cb4aulaquar_CheckedChanged);
            // 
            // cb4aulaseg
            // 
            this.cb4aulaseg.AutoSize = true;
            this.cb4aulaseg.BackColor = System.Drawing.Color.Transparent;
            this.cb4aulaseg.ForeColor = System.Drawing.Color.Black;
            this.cb4aulaseg.Location = new System.Drawing.Point(198, 216);
            this.cb4aulaseg.Name = "cb4aulaseg";
            this.cb4aulaseg.Size = new System.Drawing.Size(86, 24);
            this.cb4aulaseg.TabIndex = 28;
            this.cb4aulaseg.Text = "4ª Aula";
            this.cb4aulaseg.UseVisualStyleBackColor = false;
            this.cb4aulaseg.CheckedChanged += new System.EventHandler(this.cb4aulaseg_CheckedChanged);
            // 
            // cb4aulater
            // 
            this.cb4aulater.AutoSize = true;
            this.cb4aulater.BackColor = System.Drawing.Color.Transparent;
            this.cb4aulater.ForeColor = System.Drawing.Color.Black;
            this.cb4aulater.Location = new System.Drawing.Point(392, 216);
            this.cb4aulater.Name = "cb4aulater";
            this.cb4aulater.Size = new System.Drawing.Size(86, 24);
            this.cb4aulater.TabIndex = 29;
            this.cb4aulater.Text = "4ª Aula";
            this.cb4aulater.UseVisualStyleBackColor = false;
            this.cb4aulater.CheckedChanged += new System.EventHandler(this.cb4aulater_CheckedChanged);
            // 
            // cb3aulasex
            // 
            this.cb3aulasex.AutoSize = true;
            this.cb3aulasex.BackColor = System.Drawing.Color.Transparent;
            this.cb3aulasex.ForeColor = System.Drawing.Color.Black;
            this.cb3aulasex.Location = new System.Drawing.Point(962, 163);
            this.cb3aulasex.Name = "cb3aulasex";
            this.cb3aulasex.Size = new System.Drawing.Size(86, 24);
            this.cb3aulasex.TabIndex = 27;
            this.cb3aulasex.Text = "3ª Aula";
            this.cb3aulasex.UseVisualStyleBackColor = false;
            this.cb3aulasex.CheckedChanged += new System.EventHandler(this.cb3aulasex_CheckedChanged);
            // 
            // cb3aulaquin
            // 
            this.cb3aulaquin.AutoSize = true;
            this.cb3aulaquin.BackColor = System.Drawing.Color.Transparent;
            this.cb3aulaquin.ForeColor = System.Drawing.Color.Black;
            this.cb3aulaquin.Location = new System.Drawing.Point(768, 163);
            this.cb3aulaquin.Name = "cb3aulaquin";
            this.cb3aulaquin.Size = new System.Drawing.Size(86, 24);
            this.cb3aulaquin.TabIndex = 26;
            this.cb3aulaquin.Text = "3ª Aula";
            this.cb3aulaquin.UseVisualStyleBackColor = false;
            this.cb3aulaquin.CheckedChanged += new System.EventHandler(this.cb3aulaquin_CheckedChanged);
            // 
            // cb3aulaquar
            // 
            this.cb3aulaquar.AutoSize = true;
            this.cb3aulaquar.BackColor = System.Drawing.Color.Transparent;
            this.cb3aulaquar.ForeColor = System.Drawing.Color.Black;
            this.cb3aulaquar.Location = new System.Drawing.Point(586, 163);
            this.cb3aulaquar.Name = "cb3aulaquar";
            this.cb3aulaquar.Size = new System.Drawing.Size(86, 24);
            this.cb3aulaquar.TabIndex = 25;
            this.cb3aulaquar.Text = "3ª Aula";
            this.cb3aulaquar.UseVisualStyleBackColor = false;
            this.cb3aulaquar.CheckedChanged += new System.EventHandler(this.cb3aulaquar_CheckedChanged);
            // 
            // cb3aulater
            // 
            this.cb3aulater.AutoSize = true;
            this.cb3aulater.BackColor = System.Drawing.Color.Transparent;
            this.cb3aulater.ForeColor = System.Drawing.Color.Black;
            this.cb3aulater.Location = new System.Drawing.Point(392, 163);
            this.cb3aulater.Name = "cb3aulater";
            this.cb3aulater.Size = new System.Drawing.Size(86, 24);
            this.cb3aulater.TabIndex = 24;
            this.cb3aulater.Text = "3ª Aula";
            this.cb3aulater.UseVisualStyleBackColor = false;
            this.cb3aulater.CheckedChanged += new System.EventHandler(this.cb3aulater_CheckedChanged);
            // 
            // cb3aulaseg
            // 
            this.cb3aulaseg.AutoSize = true;
            this.cb3aulaseg.BackColor = System.Drawing.Color.Transparent;
            this.cb3aulaseg.ForeColor = System.Drawing.Color.Black;
            this.cb3aulaseg.Location = new System.Drawing.Point(198, 163);
            this.cb3aulaseg.Name = "cb3aulaseg";
            this.cb3aulaseg.Size = new System.Drawing.Size(86, 24);
            this.cb3aulaseg.TabIndex = 23;
            this.cb3aulaseg.Text = "3ª Aula";
            this.cb3aulaseg.UseVisualStyleBackColor = false;
            this.cb3aulaseg.CheckedChanged += new System.EventHandler(this.cb3aulaseg_CheckedChanged);
            // 
            // cb2aulasex
            // 
            this.cb2aulasex.AutoSize = true;
            this.cb2aulasex.BackColor = System.Drawing.Color.Transparent;
            this.cb2aulasex.ForeColor = System.Drawing.Color.Black;
            this.cb2aulasex.Location = new System.Drawing.Point(962, 110);
            this.cb2aulasex.Name = "cb2aulasex";
            this.cb2aulasex.Size = new System.Drawing.Size(86, 24);
            this.cb2aulasex.TabIndex = 22;
            this.cb2aulasex.Text = "2ª Aula";
            this.cb2aulasex.UseVisualStyleBackColor = false;
            this.cb2aulasex.CheckedChanged += new System.EventHandler(this.cb2aulasex_CheckedChanged);
            // 
            // cb2aulaquin
            // 
            this.cb2aulaquin.AutoSize = true;
            this.cb2aulaquin.BackColor = System.Drawing.Color.Transparent;
            this.cb2aulaquin.ForeColor = System.Drawing.Color.Black;
            this.cb2aulaquin.Location = new System.Drawing.Point(768, 110);
            this.cb2aulaquin.Name = "cb2aulaquin";
            this.cb2aulaquin.Size = new System.Drawing.Size(86, 24);
            this.cb2aulaquin.TabIndex = 21;
            this.cb2aulaquin.Text = "2ª Aula";
            this.cb2aulaquin.UseVisualStyleBackColor = false;
            this.cb2aulaquin.CheckedChanged += new System.EventHandler(this.cb2aulaquin_CheckedChanged);
            // 
            // cb2aulater
            // 
            this.cb2aulater.AutoSize = true;
            this.cb2aulater.BackColor = System.Drawing.Color.Transparent;
            this.cb2aulater.ForeColor = System.Drawing.Color.Black;
            this.cb2aulater.Location = new System.Drawing.Point(392, 110);
            this.cb2aulater.Name = "cb2aulater";
            this.cb2aulater.Size = new System.Drawing.Size(86, 24);
            this.cb2aulater.TabIndex = 20;
            this.cb2aulater.Text = "2ª Aula";
            this.cb2aulater.UseVisualStyleBackColor = false;
            this.cb2aulater.CheckedChanged += new System.EventHandler(this.cb2aulater_CheckedChanged);
            // 
            // cb2aulaquar
            // 
            this.cb2aulaquar.AutoSize = true;
            this.cb2aulaquar.BackColor = System.Drawing.Color.Transparent;
            this.cb2aulaquar.ForeColor = System.Drawing.Color.Black;
            this.cb2aulaquar.Location = new System.Drawing.Point(586, 110);
            this.cb2aulaquar.Name = "cb2aulaquar";
            this.cb2aulaquar.Size = new System.Drawing.Size(86, 24);
            this.cb2aulaquar.TabIndex = 20;
            this.cb2aulaquar.Text = "2ª Aula";
            this.cb2aulaquar.UseVisualStyleBackColor = false;
            this.cb2aulaquar.CheckedChanged += new System.EventHandler(this.cb2aulaquar_CheckedChanged);
            // 
            // cb1aulaquin
            // 
            this.cb1aulaquin.AutoSize = true;
            this.cb1aulaquin.BackColor = System.Drawing.Color.Transparent;
            this.cb1aulaquin.ForeColor = System.Drawing.Color.Black;
            this.cb1aulaquin.Location = new System.Drawing.Point(768, 57);
            this.cb1aulaquin.Name = "cb1aulaquin";
            this.cb1aulaquin.Size = new System.Drawing.Size(86, 24);
            this.cb1aulaquin.TabIndex = 17;
            this.cb1aulaquin.Text = "1ª Aula";
            this.cb1aulaquin.UseVisualStyleBackColor = false;
            this.cb1aulaquin.CheckedChanged += new System.EventHandler(this.cb1aulaquin_CheckedChanged);
            // 
            // cb2aulaseg
            // 
            this.cb2aulaseg.AutoSize = true;
            this.cb2aulaseg.BackColor = System.Drawing.Color.Transparent;
            this.cb2aulaseg.ForeColor = System.Drawing.Color.Black;
            this.cb2aulaseg.Location = new System.Drawing.Point(198, 110);
            this.cb2aulaseg.Name = "cb2aulaseg";
            this.cb2aulaseg.Size = new System.Drawing.Size(86, 24);
            this.cb2aulaseg.TabIndex = 19;
            this.cb2aulaseg.Text = "2ª Aula";
            this.cb2aulaseg.UseVisualStyleBackColor = false;
            this.cb2aulaseg.CheckedChanged += new System.EventHandler(this.cb2aulaseg_CheckedChanged);
            // 
            // cb1aulasex
            // 
            this.cb1aulasex.AutoSize = true;
            this.cb1aulasex.BackColor = System.Drawing.Color.Transparent;
            this.cb1aulasex.ForeColor = System.Drawing.Color.Black;
            this.cb1aulasex.Location = new System.Drawing.Point(962, 57);
            this.cb1aulasex.Name = "cb1aulasex";
            this.cb1aulasex.Size = new System.Drawing.Size(86, 24);
            this.cb1aulasex.TabIndex = 18;
            this.cb1aulasex.Text = "1ª Aula";
            this.cb1aulasex.UseVisualStyleBackColor = false;
            this.cb1aulasex.CheckedChanged += new System.EventHandler(this.cb1aulasex_CheckedChanged);
            // 
            // cb1aulaquar
            // 
            this.cb1aulaquar.AutoSize = true;
            this.cb1aulaquar.BackColor = System.Drawing.Color.Transparent;
            this.cb1aulaquar.ForeColor = System.Drawing.Color.Black;
            this.cb1aulaquar.Location = new System.Drawing.Point(586, 57);
            this.cb1aulaquar.Name = "cb1aulaquar";
            this.cb1aulaquar.Size = new System.Drawing.Size(86, 24);
            this.cb1aulaquar.TabIndex = 16;
            this.cb1aulaquar.Text = "1ª Aula";
            this.cb1aulaquar.UseVisualStyleBackColor = false;
            this.cb1aulaquar.CheckedChanged += new System.EventHandler(this.cb1aulaquar_CheckedChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(768, 1);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 20);
            this.label13.TabIndex = 12;
            this.label13.Text = "Quinta";
            // 
            // cb1aulater
            // 
            this.cb1aulater.AutoSize = true;
            this.cb1aulater.BackColor = System.Drawing.Color.Transparent;
            this.cb1aulater.ForeColor = System.Drawing.Color.Black;
            this.cb1aulater.Location = new System.Drawing.Point(392, 57);
            this.cb1aulater.Name = "cb1aulater";
            this.cb1aulater.Size = new System.Drawing.Size(86, 24);
            this.cb1aulater.TabIndex = 15;
            this.cb1aulater.Text = "1ª Aula";
            this.cb1aulater.UseVisualStyleBackColor = false;
            this.cb1aulater.CheckedChanged += new System.EventHandler(this.cb1aulater_CheckedChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(962, 1);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(50, 20);
            this.label14.TabIndex = 13;
            this.label14.Text = "Sexta";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(392, 1);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(49, 20);
            this.label11.TabIndex = 10;
            this.label11.Text = "Terça";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(198, 1);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 20);
            this.label10.TabIndex = 9;
            this.label10.Text = "Segunda";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(4, 372);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 20);
            this.label7.TabIndex = 6;
            this.label7.Text = "14:20";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(4, 319);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 20);
            this.label6.TabIndex = 5;
            this.label6.Text = "13:30";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(4, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "7:30";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(4, 266);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "11:10";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(4, 160);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "9:10";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(4, 107);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(40, 20);
            this.label15.TabIndex = 1;
            this.label15.Text = "8:20";
            // 
            // cb1aulaseg
            // 
            this.cb1aulaseg.AutoSize = true;
            this.cb1aulaseg.BackColor = System.Drawing.Color.Transparent;
            this.cb1aulaseg.ForeColor = System.Drawing.Color.Black;
            this.cb1aulaseg.Location = new System.Drawing.Point(198, 57);
            this.cb1aulaseg.Name = "cb1aulaseg";
            this.cb1aulaseg.Size = new System.Drawing.Size(86, 24);
            this.cb1aulaseg.TabIndex = 14;
            this.cb1aulaseg.Text = "1ª Aula";
            this.cb1aulaseg.UseVisualStyleBackColor = false;
            this.cb1aulaseg.CheckedChanged += new System.EventHandler(this.cb1aulaseg_CheckedChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(586, 1);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(58, 20);
            this.label12.TabIndex = 11;
            this.label12.Text = "Quarta";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(4, 425);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 20);
            this.label8.TabIndex = 7;
            this.label8.Text = "15:30";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(4, 478);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 20);
            this.label9.TabIndex = 8;
            this.label9.Text = "16:20";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(971, 839);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(218, 63);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 66;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(34, 243);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(286, 49);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 86;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.DarkGreen;
            this.pictureBox5.Image = global::tcc_teambompa.Properties.Resources.Tempus;
            this.pictureBox5.Location = new System.Drawing.Point(256, -3);
            this.pictureBox5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(651, 236);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 87;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.DarkGreen;
            this.pictureBox1.Location = new System.Drawing.Point(902, -3);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(330, 236);
            this.pictureBox1.TabIndex = 88;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.DarkGreen;
            this.pictureBox4.Location = new System.Drawing.Point(0, -3);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(262, 236);
            this.pictureBox4.TabIndex = 89;
            this.pictureBox4.TabStop = false;
            // 
            // FrmDisponibilidade
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1222, 914);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.lblqtdaulas);
            this.Controls.Add(this.tableLayoutPanel1);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "FrmDisponibilidade";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "p";
            this.Load += new System.EventHandler(this.FrmDisponibilidade_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label lblqtdaulas;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.CheckBox cb9aulasex;
        private System.Windows.Forms.CheckBox cb9aulaquin;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox cb8aulaquin;
        private System.Windows.Forms.CheckBox cb9aulaquar;
        private System.Windows.Forms.CheckBox cb9aulater;
        private System.Windows.Forms.CheckBox cb9aulaseg;
        private System.Windows.Forms.CheckBox cb8aulasex;
        private System.Windows.Forms.CheckBox cb8aulaquar;
        private System.Windows.Forms.CheckBox cb8aulater;
        private System.Windows.Forms.CheckBox cb8aulaseg;
        private System.Windows.Forms.CheckBox cb7aulasex;
        private System.Windows.Forms.CheckBox cb7aulaquin;
        private System.Windows.Forms.CheckBox cb7aulaquar;
        private System.Windows.Forms.CheckBox cb7aulater;
        private System.Windows.Forms.CheckBox cb7aulaseg;
        private System.Windows.Forms.CheckBox cb6aulasex;
        private System.Windows.Forms.CheckBox cb6aulaquin;
        private System.Windows.Forms.CheckBox cb6aulaquar;
        private System.Windows.Forms.CheckBox cb6aulater;
        private System.Windows.Forms.CheckBox cb6aulaseg;
        private System.Windows.Forms.CheckBox cb5aulasex;
        private System.Windows.Forms.CheckBox cb5aulaquin;
        private System.Windows.Forms.CheckBox cb5aulaquar;
        private System.Windows.Forms.CheckBox cb5aulater;
        private System.Windows.Forms.CheckBox cb4aulasex;
        private System.Windows.Forms.CheckBox cb5aulaseg;
        private System.Windows.Forms.CheckBox cb4aulaquin;
        private System.Windows.Forms.CheckBox cb4aulaquar;
        private System.Windows.Forms.CheckBox cb4aulaseg;
        private System.Windows.Forms.CheckBox cb4aulater;
        private System.Windows.Forms.CheckBox cb3aulasex;
        private System.Windows.Forms.CheckBox cb3aulaquin;
        private System.Windows.Forms.CheckBox cb3aulaquar;
        private System.Windows.Forms.CheckBox cb3aulater;
        private System.Windows.Forms.CheckBox cb3aulaseg;
        private System.Windows.Forms.CheckBox cb2aulasex;
        private System.Windows.Forms.CheckBox cb2aulaquin;
        private System.Windows.Forms.CheckBox cb2aulater;
        private System.Windows.Forms.CheckBox cb2aulaquar;
        private System.Windows.Forms.CheckBox cb1aulaquin;
        private System.Windows.Forms.CheckBox cb2aulaseg;
        private System.Windows.Forms.CheckBox cb1aulasex;
        private System.Windows.Forms.CheckBox cb1aulaquar;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox cb1aulater;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.CheckBox cb1aulaseg;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox4;
    }
}