﻿namespace tcc_teambompa
{
    partial class FrmMudaSenha
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMudaSenha));
            this.txtsenhaatual = new System.Windows.Forms.TextBox();
            this.txtsenhanova = new System.Windows.Forms.TextBox();
            this.txtconfirmarsenha = new System.Windows.Forms.TextBox();
            this.pbconfirmar = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbconfirmar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.SuspendLayout();
            // 
            // txtsenhaatual
            // 
            this.txtsenhaatual.Location = new System.Drawing.Point(236, 251);
            this.txtsenhaatual.Name = "txtsenhaatual";
            this.txtsenhaatual.PasswordChar = '*';
            this.txtsenhaatual.Size = new System.Drawing.Size(322, 26);
            this.txtsenhaatual.TabIndex = 4;
            // 
            // txtsenhanova
            // 
            this.txtsenhanova.Location = new System.Drawing.Point(236, 323);
            this.txtsenhanova.Name = "txtsenhanova";
            this.txtsenhanova.PasswordChar = '*';
            this.txtsenhanova.Size = new System.Drawing.Size(322, 26);
            this.txtsenhanova.TabIndex = 5;
            // 
            // txtconfirmarsenha
            // 
            this.txtconfirmarsenha.Location = new System.Drawing.Point(236, 398);
            this.txtconfirmarsenha.Name = "txtconfirmarsenha";
            this.txtconfirmarsenha.PasswordChar = '*';
            this.txtconfirmarsenha.Size = new System.Drawing.Size(322, 26);
            this.txtconfirmarsenha.TabIndex = 6;
            // 
            // pbconfirmar
            // 
            this.pbconfirmar.Image = ((System.Drawing.Image)(resources.GetObject("pbconfirmar.Image")));
            this.pbconfirmar.Location = new System.Drawing.Point(290, 491);
            this.pbconfirmar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbconfirmar.Name = "pbconfirmar";
            this.pbconfirmar.Size = new System.Drawing.Size(192, 65);
            this.pbconfirmar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbconfirmar.TabIndex = 9;
            this.pbconfirmar.TabStop = false;
            this.pbconfirmar.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(20, 391);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(184, 45);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 10;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(20, 246);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(184, 43);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 11;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(20, 320);
            this.pictureBox5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(184, 40);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 12;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.DarkGreen;
            this.pictureBox6.Image = global::tcc_teambompa.Properties.Resources.Tempus;
            this.pictureBox6.Location = new System.Drawing.Point(-3, 2);
            this.pictureBox6.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(597, 209);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 13;
            this.pictureBox6.TabStop = false;
            // 
            // FrmMudaSenha
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(592, 585);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pbconfirmar);
            this.Controls.Add(this.txtconfirmarsenha);
            this.Controls.Add(this.txtsenhanova);
            this.Controls.Add(this.txtsenhaatual);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FrmMudaSenha";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mudar senha";
            ((System.ComponentModel.ISupportInitialize)(this.pbconfirmar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txtsenhaatual;
        private System.Windows.Forms.TextBox txtsenhanova;
        private System.Windows.Forms.TextBox txtconfirmarsenha;
        private System.Windows.Forms.PictureBox pbconfirmar;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
    }
}