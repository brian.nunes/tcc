﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tcc_teambompa
{

    public partial class FrmDisponibilidade : Form
    {
        //declara vetor para armazenar disponibilidade de cada dia
        Boolean[,] semana = { {/*segunda*/ true, true, true, true, true, true, true, true, true },
                               {/*terca*/ true, true, true, true, true, true, true, true, true },
                               {/*quarta*/ true, true, true, true, true, true, true, true, true },
                               {/*quinta*/  true, true, true, true, true, true, true, true, true },
                               {/*sexta*/ true, true, true, true, true, true, true, true, true }};

        Boolean[,] semana_aulas = { {/*segunda*/ true, true, true, true, true, true, true, true, true },
                               {/*terca*/ true, true, true, true, true, true, true, true, true },
                               {/*quarta*/ true, true, true, true, true, true, true, true, true },
                               {/*quinta*/  true, true, true, true, true, true, true, true, true },
                               {/*sexta*/ true, true, true, true, true, true, true, true, true }};

        //declara vetor para armazenar a nova disponibilidade de cada dia
        Boolean[,] nova_disp = { {/*segunda*/ true, true, true, true, true, true, true, true, true },
                               {/*terca*/ true, true, true, true, true, true, true, true, true },
                               {/*quarta*/ true, true, true, true, true, true, true, true, true },
                               {/*quinta*/  true, true, true, true, true, true, true, true, true },
                               {/*sexta*/ true, true, true, true, true, true, true, true, true }};
        //int pras aulas disponíveis
        int nova_aulas_disp = sql.aulas_disp;
        public FrmDisponibilidade()
        {
            InitializeComponent();
        }

        private void FrmDisponibilidade_Load(object sender, EventArgs e)
        {
            //buscar atual disposição do horario do professor
            for (int j = 0; j < 5; j++)//para cada dia da semana
            {

                sql.pesquisar("select * from tab_dia where fk_prof='" + sql.proflogado.ToString() + "' and dia_semana = '" + (j + 2).ToString() + "';");//executa a query de busca

                if (sql.guarda.Read())  //faz a leitura dos resultados obtidos
                {
                    //analisa onde estão as aulas indisponíveis e guarda em variáveis
                    for (int i = 0; i < 9; i++)//testa aula por aula
                    {
                        if (sql.guarda[i + 2].ToString() != "1")//caso o valor 1(aula disponível) não seja encontrado...
                        {
                            semana[j, i] = false;//...a disponibilidade da variavel é setada para false
                        }
                    }

                    //analisa onde estão as aulas indisponíveis e guarda em variáveis
                    for (int i = 0; i < 9; i++)//testa aula por aula
                    {
                        if (sql.guarda[i + 2].ToString() != "1" && sql.guarda[i + 2].ToString() != "2")//caso o valor 1(aula disponível) não seja encontrado...
                        {
                            semana_aulas[j, i] = false;//...a disponibilidade da variavel é setada para false
                        }
                    }
                }
                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();
            }

            //defini nova disponibilidade igual a atual, para ser alterada em seguida
            for (int j = 0; j < 5; j++)
            {
                for (int i = 0; i < 9; i++)
                {
                    nova_disp[j, i] = semana[j, i];
                }
            }


            //define os status das combobox apartir da variavel "nova_disp"
            #region Muda CB por CB

            cb1aulaseg.Checked = nova_disp[0, 0];
            cb2aulaseg.Checked = nova_disp[0, 1];
            cb3aulaseg.Checked = nova_disp[0, 2];
            cb4aulaseg.Checked = nova_disp[0, 3];
            cb5aulaseg.Checked = nova_disp[0, 4];
            cb6aulaseg.Checked = nova_disp[0, 5];
            cb7aulaseg.Checked = nova_disp[0, 6];
            cb8aulaseg.Checked = nova_disp[0, 7];
            cb9aulaseg.Checked = nova_disp[0, 8];

            cb1aulater.Checked = nova_disp[1, 0];
            cb2aulater.Checked = nova_disp[1, 1];
            cb3aulater.Checked = nova_disp[1, 2];
            cb4aulater.Checked = nova_disp[1, 3];
            cb5aulater.Checked = nova_disp[1, 4];
            cb6aulater.Checked = nova_disp[1, 5];
            cb7aulater.Checked = nova_disp[1, 6];
            cb8aulater.Checked = nova_disp[1, 7];
            cb9aulater.Checked = nova_disp[1, 8];

            cb1aulaquar.Checked = nova_disp[2, 0];
            cb2aulaquar.Checked = nova_disp[2, 1];
            cb3aulaquar.Checked = nova_disp[2, 2];
            cb4aulaquar.Checked = nova_disp[2, 3];
            cb5aulaquar.Checked = nova_disp[2, 4];
            cb6aulaquar.Checked = nova_disp[2, 5];
            cb7aulaquar.Checked = nova_disp[2, 6];
            cb8aulaquar.Checked = nova_disp[2, 7];
            cb9aulaquar.Checked = nova_disp[2, 8];

            cb1aulaquin.Checked = nova_disp[3, 0];
            cb2aulaquin.Checked = nova_disp[3, 1];
            cb3aulaquin.Checked = nova_disp[3, 2];
            cb4aulaquin.Checked = nova_disp[3, 3];
            cb5aulaquin.Checked = nova_disp[3, 4];
            cb6aulaquin.Checked = nova_disp[3, 5];
            cb7aulaquin.Checked = nova_disp[3, 6];
            cb8aulaquin.Checked = nova_disp[3, 7];
            cb9aulaquin.Checked = nova_disp[3, 8];

            cb1aulasex.Checked = nova_disp[4, 0];
            cb2aulasex.Checked = nova_disp[4, 1];
            cb3aulasex.Checked = nova_disp[4, 2];
            cb4aulasex.Checked = nova_disp[4, 3];
            cb5aulasex.Checked = nova_disp[4, 4];
            cb6aulasex.Checked = nova_disp[4, 5];
            cb7aulasex.Checked = nova_disp[4, 6];
            cb8aulasex.Checked = nova_disp[4, 7];
            cb9aulasex.Checked = nova_disp[4, 8];

            #endregion

            //define o estado das combobox apartir da variavel "semana_aulas"
            #region Muda CB por CB

            cb1aulaseg.Enabled = semana_aulas[0, 0];
            cb2aulaseg.Enabled = semana_aulas[0, 1];
            cb3aulaseg.Enabled = semana_aulas[0, 2];
            cb4aulaseg.Enabled = semana_aulas[0, 3];
            cb5aulaseg.Enabled = semana_aulas[0, 4];
            cb6aulaseg.Enabled = semana_aulas[0, 5];
            cb7aulaseg.Enabled = semana_aulas[0, 6];
            cb8aulaseg.Enabled = semana_aulas[0, 7];
            cb9aulaseg.Enabled = semana_aulas[0, 8];

            cb1aulater.Enabled = semana_aulas[1, 0];
            cb2aulater.Enabled = semana_aulas[1, 1];
            cb3aulater.Enabled = semana_aulas[1, 2];
            cb4aulater.Enabled = semana_aulas[1, 3];
            cb5aulater.Enabled = semana_aulas[1, 4];
            cb6aulater.Enabled = semana_aulas[1, 5];
            cb7aulater.Enabled = semana_aulas[1, 6];
            cb8aulater.Enabled = semana_aulas[1, 7];
            cb9aulater.Enabled = semana_aulas[1, 8];

            cb1aulaquar.Enabled = semana_aulas[2, 0];
            cb2aulaquar.Enabled = semana_aulas[2, 1];
            cb3aulaquar.Enabled = semana_aulas[2, 2];
            cb4aulaquar.Enabled = semana_aulas[2, 3];
            cb5aulaquar.Enabled = semana_aulas[2, 4];
            cb6aulaquar.Enabled = semana_aulas[2, 5];
            cb7aulaquar.Enabled = semana_aulas[2, 6];
            cb8aulaquar.Enabled = semana_aulas[2, 7];
            cb9aulaquar.Enabled = semana_aulas[2, 8];

            cb1aulaquin.Enabled = semana_aulas[3, 0];
            cb2aulaquin.Enabled = semana_aulas[3, 1];
            cb3aulaquin.Enabled = semana_aulas[3, 2];
            cb4aulaquin.Enabled = semana_aulas[3, 3];
            cb5aulaquin.Enabled = semana_aulas[3, 4];
            cb6aulaquin.Enabled = semana_aulas[3, 5];
            cb7aulaquin.Enabled = semana_aulas[3, 6];
            cb8aulaquin.Enabled = semana_aulas[3, 7];
            cb9aulaquin.Enabled = semana_aulas[3, 8];

            cb1aulasex.Enabled = semana_aulas[4, 0];
            cb2aulasex.Enabled = semana_aulas[4, 1];
            cb3aulasex.Enabled = semana_aulas[4, 2];
            cb4aulasex.Enabled = semana_aulas[4, 3];
            cb5aulasex.Enabled = semana_aulas[4, 4];
            cb6aulasex.Enabled = semana_aulas[4, 5];
            cb7aulasex.Enabled = semana_aulas[4, 6];
            cb8aulasex.Enabled = semana_aulas[4, 7];
            cb9aulasex.Enabled = semana_aulas[4, 8];

            #endregion

            //atualiza label com número de aulas livres
            attnumaulas();

            //definir o numero de aulas disponiveis na coluna da tab prof
            sql.aulas_disp = nova_aulas_disp;

            //fecha as conexões
            sql.guarda.Close();
            sql.olecon.Close();
        }
        private void mudaDisp(Boolean status, int dia, int aula)
        {
                if (nova_disp[dia, aula] != status)
                {
                    nova_disp[dia, aula] = status;
                    if (status == true)
                    {
                        nova_aulas_disp++;
                    }
                    else
                    {
                        nova_aulas_disp--;
                    }
                    attnumaulas();
                }
        }
        private void attnumaulas()
        {
            //defini as quantidade de aulas disponiveis na label
            lblqtdaulas.Text = nova_aulas_disp.ToString();
        }
        private void pictureBox3_Click(object sender, EventArgs e)
        {
            //atualizar variavel que armazena a nova disponibidade
            #region Métodos dos CBs
            cb1aulaseg_CheckedChanged(sender, e);
            cb2aulaseg_CheckedChanged(sender, e);
            cb3aulaseg_CheckedChanged(sender, e);
            cb4aulaseg_CheckedChanged(sender, e);
            cb5aulaseg_CheckedChanged(sender, e);
            cb6aulaseg_CheckedChanged(sender, e);
            cb7aulaseg_CheckedChanged(sender, e);
            cb8aulaseg_CheckedChanged(sender, e);
            cb9aulaseg_CheckedChanged(sender, e);

            cb1aulater_CheckedChanged(sender, e);
            cb2aulater_CheckedChanged(sender, e);
            cb3aulater_CheckedChanged(sender, e);
            cb4aulater_CheckedChanged(sender, e);
            cb5aulater_CheckedChanged(sender, e);
            cb6aulater_CheckedChanged(sender, e);
            cb7aulater_CheckedChanged(sender, e);
            cb8aulater_CheckedChanged(sender, e);
            cb9aulater_CheckedChanged(sender, e);

            cb1aulaquar_CheckedChanged(sender, e);
            cb2aulaquar_CheckedChanged(sender, e);
            cb3aulaquar_CheckedChanged(sender, e);
            cb4aulaquar_CheckedChanged(sender, e);
            cb5aulaquar_CheckedChanged(sender, e);
            cb6aulaquar_CheckedChanged(sender, e);
            cb7aulaquar_CheckedChanged(sender, e);
            cb8aulaquar_CheckedChanged(sender, e);
            cb9aulaquar_CheckedChanged(sender, e);

            cb1aulaquin_CheckedChanged(sender, e);
            cb2aulaquin_CheckedChanged(sender, e);
            cb3aulaquin_CheckedChanged(sender, e);
            cb4aulaquin_CheckedChanged(sender, e);
            cb5aulaquin_CheckedChanged(sender, e);
            cb6aulaquin_CheckedChanged(sender, e);
            cb7aulaquin_CheckedChanged(sender, e);
            cb8aulaquin_CheckedChanged(sender, e);
            cb9aulaquin_CheckedChanged(sender, e);

            cb1aulasex_CheckedChanged(sender, e);
            cb2aulasex_CheckedChanged(sender, e);
            cb3aulasex_CheckedChanged(sender, e);
            cb4aulasex_CheckedChanged(sender, e);
            cb5aulasex_CheckedChanged(sender, e);
            cb6aulasex_CheckedChanged(sender, e);
            cb7aulasex_CheckedChanged(sender, e);
            cb8aulasex_CheckedChanged(sender, e);
            cb9aulasex_CheckedChanged(sender, e);
            #endregion

            //atualiza o campo do professor
            sql.aulas_disp = nova_aulas_disp;
            string comandox = "UPDATE `tab_prof` SET `num_aula_disponivel` = '" + sql.aulas_disp.ToString() + "' WHERE `tab_prof`.`id_prof` = '" + sql.proflogado.ToString() + "';";
            sql.executarcomando(comandox, "Atualização ");

            //fecha as conexões
            sql.guarda.Close();
            sql.olecon.Close();

            //fazer o update de dia por dia nas 5 tabelas do banco de dados
            for (int j = 0; j < 5; j++)
            {
                string[] aula = new string[9];
                for (int p = 0; p < 9; p++)
                {
                    if (nova_disp[j, p] == false)
                    {
                        aula[p] = "2";
                    }
                    else
                    {
                        aula[p] = "1";
                    }
                }

                //formula e executa comando
                comandox = "UPDATE `tab_dia` SET `730` = '" + aula[0] + "', `820` = '" + aula[1] + "', `910` = '" + aula[2] + "', `1020` = '" + aula[3] + "', `1110` = '" + aula[4] + "', `1330` = '" + aula[5] + "', `1420` = '" + aula[6] + "', `1530` = '" + aula[7] + "', `1620` = '" + aula[8] + "' WHERE `tab_dia`.`fk_prof` = " + sql.proflogado.ToString() + " AND dia_semana = " + (j + 2).ToString() + "";
                sql.executarcomando(comandox, "Atualização ");

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();
            }

            MessageBox.Show("Atualização feita com sucesso!", "*** Atualização ***", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /*atualizar a variave de aulas disponiveis sempre que uma checkbox foi marcada ou desmarcada
atualizar variavel do dia da nova diponibilidade com o valor da checkbox referente ao dia*/
        #region cbs
        private void cb1aulaseg_CheckedChanged(object sender, EventArgs e)
        {
            if (cb1aulaseg.Checked == true)
            {
                mudaDisp(true, 0, 0);
            }
            else
            {
                mudaDisp(false, 0, 0);
            }
        }

        private void cb2aulaseg_CheckedChanged(object sender, EventArgs e)
        {
            if (cb2aulaseg.Checked == true)
            {
                mudaDisp(true, 0, 1);
            }
            else
            {
                mudaDisp(false, 0, 1);
            }
        }

        private void cb3aulaseg_CheckedChanged(object sender, EventArgs e)
        {
            if (cb3aulaseg.Checked == true)
            {
                mudaDisp(true, 0, 2);
            }
            else
            {
                mudaDisp(false, 0, 2);
            }
        }

        private void cb4aulaseg_CheckedChanged(object sender, EventArgs e)
        {
            if (cb4aulaseg.Checked == true)
            {
                mudaDisp(true, 0, 3);
            }
            else
            {
                mudaDisp(false, 0, 3);
            }
        }

        private void cb5aulaseg_CheckedChanged(object sender, EventArgs e)
        {
            if (cb5aulaseg.Checked == true)
            {
                mudaDisp(true, 0, 4);
            }
            else
            {
                mudaDisp(false, 0, 4);
            }
        }

        private void cb6aulaseg_CheckedChanged(object sender, EventArgs e)
        {
            if (cb6aulaseg.Checked == true)
            {
                mudaDisp(true, 0, 5);
            }
            else
            {
                mudaDisp(false, 0, 5);
            }
        }

        private void cb7aulaseg_CheckedChanged(object sender, EventArgs e)
        {
            if (cb7aulaseg.Checked == true)
            {
                mudaDisp(true, 0, 6);
            }
            else
            {
                mudaDisp(false, 0, 6);
            }
        }

        private void cb8aulaseg_CheckedChanged(object sender, EventArgs e)
        {
            if (cb8aulaseg.Checked == true)
            {
                mudaDisp(true, 0, 7);
            }
            else
            {
                mudaDisp(false, 0, 7);
            }
        }

        private void cb9aulaseg_CheckedChanged(object sender, EventArgs e)
        {
            if (cb9aulaseg.Checked == true)
            {
                mudaDisp(true, 0, 8);
            }
            else
            {
                mudaDisp(false, 0, 8);
            }
        }

        private void cb1aulater_CheckedChanged(object sender, EventArgs e)
        {
            if (cb1aulater.Checked == true)
            {
                mudaDisp(true, 1, 0);
            }
            else
            {
                mudaDisp(false, 1, 0);
            }
        }

        private void cb2aulater_CheckedChanged(object sender, EventArgs e)
        {
            if (cb2aulater.Checked == true)
            {
                mudaDisp(true, 1, 1);
            }
            else
            {
                mudaDisp(false, 1, 1);
            }
        }

        private void cb3aulater_CheckedChanged(object sender, EventArgs e)
        {
            if (cb3aulater.Checked == true)
            {
                mudaDisp(true, 1, 2);
            }
            else
            {
                mudaDisp(false, 1, 2);
            }
        }

        private void cb4aulater_CheckedChanged(object sender, EventArgs e)
        {
            if (cb4aulater.Checked == true)
            {
                mudaDisp(true, 1, 3);
            }
            else
            {
                mudaDisp(false, 1, 3);
            }
        }

        private void cb5aulater_CheckedChanged(object sender, EventArgs e)
        {
            if (cb5aulater.Checked == true)
            {
                mudaDisp(true, 1, 4);
            }
            else
            {
                mudaDisp(false, 1, 4);
            }
        }

        private void cb6aulater_CheckedChanged(object sender, EventArgs e)
        {
            if (cb6aulater.Checked == true)
            {
                mudaDisp(true, 1, 5);
            }
            else
            {
                mudaDisp(false, 1, 5);
            }
        }

        private void cb7aulater_CheckedChanged(object sender, EventArgs e)
        {
            if (cb7aulater.Checked == true)
            {
                mudaDisp(true, 1, 6);
            }
            else
            {
                mudaDisp(false, 1, 6);
            }
        }

        private void cb8aulater_CheckedChanged(object sender, EventArgs e)
        {
            if (cb8aulater.Checked == true)
            {
                mudaDisp(true, 1, 7);
            }
            else
            {
                mudaDisp(false, 1, 7);
            }
        }

        private void cb9aulater_CheckedChanged(object sender, EventArgs e)
        {
            if (cb9aulater.Checked == true)
            {
                mudaDisp(true, 1, 8);
            }
            else
            {
                mudaDisp(false, 1, 8);
            }
        }

        private void cb1aulaquar_CheckedChanged(object sender, EventArgs e)
        {
            if (cb1aulaquar.Checked == true)
            {
                mudaDisp(true, 2, 0);
            }
            else
            {
                mudaDisp(false, 2, 0);
            }
        }

        private void cb2aulaquar_CheckedChanged(object sender, EventArgs e)
        {
            if (cb2aulaquar.Checked == true)
            {
                mudaDisp(true, 2, 1);
            }
            else
            {
                mudaDisp(false, 2, 1);
            }
        }

        private void cb3aulaquar_CheckedChanged(object sender, EventArgs e)
        {
            if (cb3aulaquar.Checked == true)
            {
                mudaDisp(true, 2, 2);
            }
            else
            {
                mudaDisp(false, 2, 2);
            }
        }

        private void cb4aulaquar_CheckedChanged(object sender, EventArgs e)
        {
            if (cb4aulaquar.Checked == true)
            {
                mudaDisp(true, 2, 3);
            }
            else
            {
                mudaDisp(false, 2, 3);
            }
        }

        private void cb5aulaquar_CheckedChanged(object sender, EventArgs e)
        {
            if (cb5aulaquar.Checked == true)
            {
                mudaDisp(true, 2, 4);
            }
            else
            {
                mudaDisp(false, 2, 4);
            }
        }

        private void cb6aulaquar_CheckedChanged(object sender, EventArgs e)
        {
            if (cb6aulaquar.Checked == true)
            {
                mudaDisp(true, 2, 5);
            }
            else
            {
                mudaDisp(false, 2, 5);
            }
        }

        private void cb7aulaquar_CheckedChanged(object sender, EventArgs e)
        {
            if (cb7aulaquar.Checked == true)
            {
                mudaDisp(true, 2, 6);
            }
            else
            {
                mudaDisp(false, 2, 6);
            }
        }

        private void cb8aulaquar_CheckedChanged(object sender, EventArgs e)
        {
            if (cb8aulaquar.Checked == true)
            {
                mudaDisp(true, 2, 7);
            }
            else
            {
                mudaDisp(false, 2, 7);
            }
        }

        private void cb9aulaquar_CheckedChanged(object sender, EventArgs e)
        {
            if (cb9aulaquar.Checked == true)
            {
                mudaDisp(true, 2, 8);
            }
            else
            {
                mudaDisp(false, 2, 8);
            }
        }

        private void cb1aulaquin_CheckedChanged(object sender, EventArgs e)
        {
            if (cb1aulaquin.Checked == true)
            {
                mudaDisp(true, 3, 0);
            }
            else
            {
                mudaDisp(false, 3, 0);
            }
        }

        private void cb2aulaquin_CheckedChanged(object sender, EventArgs e)
        {
            if (cb2aulaquin.Checked == true)
            {
                mudaDisp(true, 3, 1);
            }
            else
            {
                mudaDisp(false, 3, 1);
            }
        }

        private void cb3aulaquin_CheckedChanged(object sender, EventArgs e)
        {
            if (cb3aulaquin.Checked == true)
            {
                mudaDisp(true, 3, 2);
            }
            else
            {
                mudaDisp(false, 3, 2);
            }
        }

        private void cb4aulaquin_CheckedChanged(object sender, EventArgs e)
        {
            if (cb4aulaquin.Checked == true)
            {
                mudaDisp(true, 3, 3);
            }
            else
            {
                mudaDisp(false, 3, 3);
            }
        }

        private void cb5aulaquin_CheckedChanged(object sender, EventArgs e)
        {
            if (cb5aulaquin.Checked == true)
            {
                mudaDisp(true, 3, 4);
            }
            else
            {
                mudaDisp(false, 3, 4);
            }
        }

        private void cb6aulaquin_CheckedChanged(object sender, EventArgs e)
        {
            if (cb6aulaquin.Checked == true)
            {
                mudaDisp(true, 3, 5);
            }
            else
            {
                mudaDisp(false, 3, 5);
            }
        }

        private void cb7aulaquin_CheckedChanged(object sender, EventArgs e)
        {
            if (cb7aulaquin.Checked == true)
            {
                mudaDisp(true, 3, 6);
            }
            else
            {
                mudaDisp(false, 3, 6);
            }
        }

        private void cb8aulaquin_CheckedChanged(object sender, EventArgs e)
        {
            if (cb8aulaquin.Checked == true)
            {
                mudaDisp(true, 3, 7);
            }
            else
            {
                mudaDisp(false, 3, 7);
            }
        }

        private void cb9aulaquin_CheckedChanged(object sender, EventArgs e)
        {
            if (cb9aulaquin.Checked == true)
            {
                mudaDisp(true, 3, 8);
            }
            else
            {
                mudaDisp(false, 3, 8);
            }
        }

        private void cb1aulasex_CheckedChanged(object sender, EventArgs e)
        {
            if (cb1aulasex.Checked == true)
            {
                mudaDisp(true, 4, 0);
            }
            else
            {
                mudaDisp(false, 4, 0);
            }
        }

        private void cb2aulasex_CheckedChanged(object sender, EventArgs e)
        {
            if (cb2aulasex.Checked == true)
            {
                mudaDisp(true, 4, 1);
            }
            else
            {
                mudaDisp(false, 4, 1);
            }
        }

        private void cb3aulasex_CheckedChanged(object sender, EventArgs e)
        {
            if (cb3aulasex.Checked == true)
            {
                mudaDisp(true, 4, 2);
            }
            else
            {
                mudaDisp(false, 4, 2);
            }
        }

        private void cb4aulasex_CheckedChanged(object sender, EventArgs e)
        {
            if (cb4aulasex.Checked == true)
            {
                mudaDisp(true, 4, 3);
            }
            else
            {
                mudaDisp(false, 4, 3);
            }
        }

        private void cb5aulasex_CheckedChanged(object sender, EventArgs e)
        {
            if (cb5aulasex.Checked == true)
            {
                mudaDisp(true, 4, 4);
            }
            else
            {
                mudaDisp(false, 4, 4);
            }
        }

        private void cb6aulasex_CheckedChanged(object sender, EventArgs e)
        {
            if (cb6aulasex.Checked == true)
            {
                mudaDisp(true, 4, 5);
            }
            else
            {
                mudaDisp(false, 4, 5);
            }
        }

        private void cb7aulasex_CheckedChanged(object sender, EventArgs e)
        {
            if (cb7aulasex.Checked == true)
            {
                mudaDisp(true, 4, 6);
            }
            else
            {
                mudaDisp(false, 4, 6);
            }
        }

        private void cb8aulasex_CheckedChanged(object sender, EventArgs e)
        {
            if (cb8aulasex.Checked == true)
            {
                mudaDisp(true, 4, 7);
            }
            else
            {
                mudaDisp(false, 4, 7);
            }
        }

        private void cb9aulasex_CheckedChanged(object sender, EventArgs e)
        {
            if (cb9aulasex.Checked == true)
            {
                mudaDisp(true, 4, 8);
            }
            else
            {
                mudaDisp(false, 4, 8);
            }
        }
        #endregion
    }
}
