﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tcc_teambompa
{
    public partial class FrmMdi : Form
    {
        FrmDisponibilidade FrmDisp;
        FrmEscolherMateria FrmEscolherMateria;
        FrmEscolherTurma FrmEscolherTurma;
        FrmHorario FrmHorario;
        FrmMudaSenha FrmMudaSenha;

        public FrmMdi()
        {
            InitializeComponent();
        }

        private void disponibilidadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (FrmDisp == null || FrmDisp.IsDisposed)
            {
                FrmDisp = new FrmDisponibilidade();
                FrmDisp.MdiParent = this;
                FrmDisp.Show();
            }
            else
            {
                FrmDisp.BringToFront();
            }
        }

        private void horárioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (FrmHorario == null || FrmHorario.IsDisposed)
            {
                FrmHorario = new FrmHorario();
                FrmHorario.MdiParent = this;
                FrmHorario.Show();
            }
            else
            {
                FrmHorario.BringToFront();
            }
        }

        private void escolherTurmaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (FrmEscolherTurma == null || FrmEscolherTurma.IsDisposed)
            {
                FrmEscolherTurma = new FrmEscolherTurma();
                FrmEscolherTurma.MdiParent = this;
                FrmEscolherTurma.Show();
            }
            else
            {
                FrmEscolherTurma.BringToFront();
            }
        }

        private void escolherMatériaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (FrmEscolherMateria == null || FrmEscolherMateria.IsDisposed)
            {
                FrmEscolherMateria = new FrmEscolherMateria();
                FrmEscolherMateria.MdiParent = this;
                FrmEscolherMateria.Show();
            }
            else
            {
                FrmEscolherMateria.BringToFront();
            }
        }

        private void btnlogout_Click(object sender, EventArgs e)
        {
            //Confirma se o usuário deseja mesmo sair
            DialogResult YN = MessageBox.Show("Deseja mesmo sair?", "*** Saindo ***", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if(YN == DialogResult.Yes)
            {
                //desloga o prof e fecha o form
                sql.proflogado = -1;
                this.Close();
                //aq o login volta
                DevolveForm.MainMenu.Show();
            }
        }

        private void mudarSenhaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (FrmMudaSenha == null || FrmMudaSenha.IsDisposed)
            {
                FrmMudaSenha = new FrmMudaSenha();
                FrmMudaSenha.MdiParent = this;
                FrmMudaSenha.Show();
            }
            else
            {
                FrmMudaSenha.BringToFront();
            }
        }

        private void FrmMdi_FormClosing(object sender, FormClosingEventArgs e)
        {
            //aq o login volta
            DevolveForm.MainMenu.Show();
        }
    }
}
