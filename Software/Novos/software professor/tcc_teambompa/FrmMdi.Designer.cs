﻿namespace tcc_teambompa
{
    partial class FrmMdi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.disponibilidadeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.horárioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.escolherToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.escolherTurmaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.escolherMatériaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mudarSenhaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnlogout = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.disponibilidadeToolStripMenuItem,
            this.horárioToolStripMenuItem,
            this.escolherToolStripMenuItem,
            this.mudarSenhaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(9, 3, 0, 3);
            this.menuStrip1.Size = new System.Drawing.Size(1290, 35);
            this.menuStrip1.TabIndex = 71;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // disponibilidadeToolStripMenuItem
            // 
            this.disponibilidadeToolStripMenuItem.Name = "disponibilidadeToolStripMenuItem";
            this.disponibilidadeToolStripMenuItem.Size = new System.Drawing.Size(148, 29);
            this.disponibilidadeToolStripMenuItem.Text = "Disponibilidade";
            this.disponibilidadeToolStripMenuItem.Click += new System.EventHandler(this.disponibilidadeToolStripMenuItem_Click);
            // 
            // horárioToolStripMenuItem
            // 
            this.horárioToolStripMenuItem.Name = "horárioToolStripMenuItem";
            this.horárioToolStripMenuItem.Size = new System.Drawing.Size(84, 29);
            this.horárioToolStripMenuItem.Text = "Horário";
            this.horárioToolStripMenuItem.Click += new System.EventHandler(this.horárioToolStripMenuItem_Click);
            // 
            // escolherToolStripMenuItem
            // 
            this.escolherToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.escolherTurmaToolStripMenuItem,
            this.escolherMatériaToolStripMenuItem});
            this.escolherToolStripMenuItem.Name = "escolherToolStripMenuItem";
            this.escolherToolStripMenuItem.Size = new System.Drawing.Size(89, 29);
            this.escolherToolStripMenuItem.Text = "Escolher";
            // 
            // escolherTurmaToolStripMenuItem
            // 
            this.escolherTurmaToolStripMenuItem.Name = "escolherTurmaToolStripMenuItem";
            this.escolherTurmaToolStripMenuItem.Size = new System.Drawing.Size(225, 30);
            this.escolherTurmaToolStripMenuItem.Text = "Escolher Turma";
            this.escolherTurmaToolStripMenuItem.Click += new System.EventHandler(this.escolherTurmaToolStripMenuItem_Click);
            // 
            // escolherMatériaToolStripMenuItem
            // 
            this.escolherMatériaToolStripMenuItem.Name = "escolherMatériaToolStripMenuItem";
            this.escolherMatériaToolStripMenuItem.Size = new System.Drawing.Size(225, 30);
            this.escolherMatériaToolStripMenuItem.Text = "Escolher Matéria";
            this.escolherMatériaToolStripMenuItem.Click += new System.EventHandler(this.escolherMatériaToolStripMenuItem_Click);
            // 
            // mudarSenhaToolStripMenuItem
            // 
            this.mudarSenhaToolStripMenuItem.Name = "mudarSenhaToolStripMenuItem";
            this.mudarSenhaToolStripMenuItem.Size = new System.Drawing.Size(129, 29);
            this.mudarSenhaToolStripMenuItem.Text = "Mudar Senha";
            this.mudarSenhaToolStripMenuItem.Click += new System.EventHandler(this.mudarSenhaToolStripMenuItem_Click);
            // 
            // btnlogout
            // 
            this.btnlogout.Location = new System.Drawing.Point(509, 0);
            this.btnlogout.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnlogout.Name = "btnlogout";
            this.btnlogout.Size = new System.Drawing.Size(112, 35);
            this.btnlogout.TabIndex = 73;
            this.btnlogout.Text = "  Log Out";
            this.btnlogout.UseVisualStyleBackColor = true;
            this.btnlogout.Click += new System.EventHandler(this.btnlogout_Click);
            // 
            // FrmMdi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1290, 829);
            this.Controls.Add(this.btnlogout);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "FrmMdi";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Home";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMdi_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem disponibilidadeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem horárioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem escolherToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem escolherTurmaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem escolherMatériaToolStripMenuItem;
        private System.Windows.Forms.Button btnlogout;
        private System.Windows.Forms.ToolStripMenuItem mudarSenhaToolStripMenuItem;
    }
}