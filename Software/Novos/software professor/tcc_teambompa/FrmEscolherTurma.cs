﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tcc_teambompa
{
    public partial class FrmEscolherTurma : Form
    {
        //variavel para manipulação de strings
        string texto;
        int[] turmaslecionadas;
        int num_aulas;
        public FrmEscolherTurma()
        {
            InitializeComponent();
        }

        private void FrmEscolherTurma_Load(object sender, EventArgs e)
        {
            cbmateriaT.Items.Clear();

            //inicia com número de aulas para as materias disponíveis no máximo
            sql.aulas_disp_turma = sql.aulas_disp;

            //faz busca de matérias
            //buscar todas as matérias lecionadas do prof
            sql.pesquisar("select * from relacao_prof_materia_permanente where fk_prof='" + sql.proflogado + "';");

            int h = 0;

            while (sql.guarda.Read())  //faz a contagem dos resultados obtidos
            {
                h++;
            }

            int[] materiaslec = new int[h];
            h = 0;

            //fecha as conexões
            sql.guarda.Close();
            sql.olecon.Close();

            //buscar todas as matérias lecionadas do prof
            sql.pesquisar("select * from relacao_prof_materia_permanente where fk_prof='" + sql.proflogado + "';");

            while (sql.guarda.Read())  //faz a contagem dos resultados obtidos
            {
                materiaslec[h] = Convert.ToInt32(sql.guarda[0]);
                h++;
            }

            //fecha as conexões
            sql.guarda.Close();
            sql.olecon.Close();


            for (h = 0; h < materiaslec.Length; h++)
            {
                //buscar todas as matérias lecionadas do prof
                sql.pesquisar("select * from tab_materia where id_materia='" + materiaslec[h].ToString() + "';");

                while (sql.guarda.Read())  //faz a leitura dos resultados obtidos
                {
                    //lista as materias uma a uma na dropdownlist
                    cbmateriaT.Items.Add(sql.guarda[0].ToString() + " - " + sql.guarda[2].ToString() + " - " + sql.guarda[3].ToString() + " aulas semanais");
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();
            }

            //tira das aulas disponíveis do professor cada aula q ele da (ou quer dar) para cada uma dessas matérias
            for (h = 0; h < materiaslec.Length; h++)
            {
                //buscar todas as matérias lecionadas do prof
                sql.pesquisar("select * from tab_materia where id_materia='" + materiaslec[h].ToString() + "';");

                num_aulas = 0;

                if (sql.guarda.Read())
                {
                    num_aulas = Convert.ToInt32(sql.guarda[3].ToString());
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //buscar todas as relacões temporarias
                sql.pesquisar("select * from relacao_prof_classe_temporaria where fk_materia='" + materiaslec[h].ToString() + "' and fk_prof = '" + sql.proflogado + "';");

                int y = 0;

                //conta qnts tem
                while(sql.guarda.Read())
                {
                    y++;
                }

                //subtrai do total
                sql.aulas_disp_turma -= num_aulas * y;

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //buscar todas as relacões permanentes
                sql.pesquisar("select * from relacao_prof_classe_permanente where fk_materia='" + materiaslec[h].ToString() + "' and fk_prof = '" + sql.proflogado + "';");

                y = 0;

                //conta qnts tem
                while (sql.guarda.Read())
                {
                    y++;
                }

                //subtrai do total
                sql.aulas_disp_turma -= num_aulas * y;

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();
            }

            atualizarlists();
        }

        private void cbmateriaT_SelectedIndexChanged(object sender, EventArgs e)
        {
            atualizarlists();
        }

        private void atualizarlists()
        {
            if (cbmateriaT.SelectedIndex != -1)
            {
                //limpa listas
                lbdispT.Items.Clear();
                lbselecT.Items.Clear();
                lblecionadasT.Items.Clear();

                //busca matéria selecionada
                texto = cbmateriaT.Items[cbmateriaT.SelectedIndex].ToString();
                string codigo = "";
                string letra = "";
                int index = 0;
                while (letra != " ")
                {
                    codigo += letra;
                    letra = texto.Substring(index, 1);
                    index++;
                }

                int m = 0;

                //buscar todas os cursos cadastradas que tenham a materia
                sql.pesquisar("select * from relacao_materia_curso where fk_materia = '" + codigo.ToString() + "';");

                while (sql.guarda.Read())
                {
                    m++;
                }

                int[] curso = new int[m];
                m = 0;

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //buscar todas os cursos cadastradas que tenham a materia
                sql.pesquisar("select * from relacao_materia_curso where fk_materia = '" + codigo.ToString() + "';");

                while (sql.guarda.Read())
                {
                    curso[m] = Convert.ToInt32(sql.guarda[1]);
                    m++;
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //buscar todas as turmas cadastradas que sejam do curso
                for (m = 0; m < curso.Length; m++)
                {
                    sql.pesquisar("select * from tab_classe where fk_curso = '" + curso[m].ToString() + "';");

                    while (sql.guarda.Read())  //faz a leitura dos resultados obtidos
                    {
                        //lista as turmas uma a uma na dropdownlist
                        lbdispT.Items.Add(sql.guarda[0].ToString() + " - " + sql.guarda[1].ToString() + sql.guarda[2].ToString());
                    }

                    //fecha as conexões
                    sql.guarda.Close();
                    sql.olecon.Close();
                }

                //busca matéria selecionada
                texto = cbmateriaT.Items[cbmateriaT.SelectedIndex].ToString();
                codigo = "";
                letra = "";
                index = 0;
                while (letra != " ")
                {
                    codigo += letra;
                    letra = texto.Substring(index, 1);
                    index++;
                }

                //buscar todas as turmas previamente selecionadas
                sql.pesquisar("select * from relacao_prof_classe_temporaria where fk_prof = '" + sql.proflogado.ToString() + "' and fk_materia = '" + codigo + "';");

                int k = 0;
                while (sql.guarda.Read())  //faz a contagem dos resultados obtidos
                {
                    //soma na contagem
                    k++;
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //cria vetor para as materias
                int[] turmasselecionadas = new int[k];

                //inicia variavel auxiliar
                int h = 0;

                //buscar todas as turmas previamente selecionadas
                sql.pesquisar("select * from relacao_prof_classe_temporaria where fk_prof = '" + sql.proflogado.ToString() + "' and fk_materia= '" + codigo + "';");

                while (sql.guarda.Read())  //faz a leitura dos resultados obtidos
                {
                    //salva o cod da materia no vetor
                    turmasselecionadas[h] = Convert.ToInt32(sql.guarda[1].ToString());
                    h++;
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //buscar todas as turmas previamente lecionadas
                sql.pesquisar("select * from relacao_prof_classe_permanente where fk_prof = '" + sql.proflogado.ToString() + "' and fk_materia = '" + codigo + "';");

                k = 0;
                while (sql.guarda.Read())  //faz a contagem dos resultados obtidos
                {
                    //soma na contagem
                    k++;
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //instancia vetor para as materias
                turmaslecionadas = new int[k];

                //inicia variavel auxiliar
                h = 0;

                //buscar todas as matérias previamente lecionadas
                sql.pesquisar("select * from relacao_prof_classe_permanente where fk_prof = '" + sql.proflogado.ToString() + "' and fk_materia = '" + codigo + "';");

                while (sql.guarda.Read())  //faz a leitura dos resultados obtidos
                {
                    //salva o cod da materia no vetor
                    turmaslecionadas[h] = Convert.ToInt32(sql.guarda[1].ToString());
                    h++;
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //testar cada código das turmas selecionadas
                for (int i = 0; i < turmasselecionadas.Length; i++)
                {
                    //em cada item da dropdownlist
                    for (int j = 0; j < lbdispT.Items.Count; j++)
                    {
                        texto = lbdispT.Items[j].ToString();
                        codigo = "";
                        letra = "";
                        index = 0;
                        while (letra != " ")
                        {
                            codigo += letra;
                            letra = texto.Substring(index, 1);
                            index++;
                        }
                        //e deletar caso encontre
                        if (codigo == turmasselecionadas[i].ToString())
                        {
                            lbdispT.Items.RemoveAt(j);
                        }
                    }
                }

                //testar cada código das matérias lecionadas
                for (int i = 0; i < turmaslecionadas.Length; i++)
                {
                    //em cada item da lista
                    for (int j = 0; j < lbdispT.Items.Count; j++)
                    {
                        texto = lbdispT.Items[j].ToString();
                        codigo = "";
                        letra = "";
                        index = 0;
                        while (letra != " ")
                        {
                            codigo += letra;
                            letra = texto.Substring(index, 1);
                            index++;
                        }
                        //e deletar caso encontre
                        if (codigo == turmaslecionadas[i].ToString())
                        {
                            lbdispT.Items.RemoveAt(j);
                        }
                    }
                }

                //loop para buscar todas as turmass previamente selecionadas uma a uma
                for (int j = 0; j < turmasselecionadas.Length; j++)
                {
                    //faz a busca
                    sql.pesquisar("select * from tab_classe where id_classe = '" + turmasselecionadas[j].ToString() + "';");

                    if (sql.guarda.Read())  //se achar
                    {
                        //lista a materia na listbox
                        lbselecT.Items.Add(sql.guarda[0].ToString() + " - " + sql.guarda[1].ToString() + sql.guarda[2].ToString());

                        //fecha as conexões
                        sql.guarda.Close();
                        sql.olecon.Close();
                    }

                    //fecha as conexões
                    sql.guarda.Close();
                    sql.olecon.Close();
                }

                //loop para buscar todas as turmas previamente lecionadas uma a uma
                for (int j = 0; j < turmaslecionadas.Length; j++)
                {
                    //faz a busca
                    sql.pesquisar("select * from tab_classe where id_classe = '" + turmaslecionadas[j].ToString() + "';");

                    if (sql.guarda.Read())  //se achar
                    {
                        //lista a materia na listbox
                        lblecionadasT.Items.Add(sql.guarda[0].ToString() + " - " + sql.guarda[1].ToString() + sql.guarda[2].ToString());

                        //fecha as conexões
                        sql.guarda.Close();
                        sql.olecon.Close();
                    }

                    //fecha as conexões
                    sql.guarda.Close();
                    sql.olecon.Close();
                }
            }
            lblqtdaulasT.Text = sql.aulas_disp_turma.ToString();
        }

        private void pbsetaesc_Click(object sender, EventArgs e)
        {
            //confere se há algo selecionado
            if (lbselecT.SelectedIndex != -1)
            {
                //seleciona o código da matéria escolhida
                texto = cbmateriaT.SelectedItem.ToString();
                string codigo = "";
                string letra = "";
                int index = 0;
                while (letra != " ")
                {
                    codigo += letra;
                    letra = texto.Substring(index, 1);
                    index++;
                }

                //faz a busca da matéria selecionada
                sql.pesquisar("select num_aulas_semanais from tab_materia where id_materia = '" + codigo + "';");

                //faz a leitura dos resultados obtidos
                if (sql.guarda.Read())
                {
                    //realoca as aulas utilizadas pela matéria para as aulas disponíveis
                    sql.aulas_disp_turma = sql.aulas_disp_turma + Convert.ToInt32(sql.guarda[0].ToString());
                    //atualiza label
                    lblqtdaulasT.Text = sql.aulas_disp_turma.ToString();
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //devolve a materia para a dropdownlist
                lbdispT.Items.Add(lbselecT.SelectedItem.ToString());

                //retira ela da listbox
                lbselecT.Items.RemoveAt(lbselecT.SelectedIndex);
            }
        }

        private void pbsetadir_Click(object sender, EventArgs e)
        {
            if (lbdispT.SelectedIndex != -1)
            {
                //seleciona o código da matéria escolhida
                texto = cbmateriaT.SelectedItem.ToString();
                string codigo = "";
                string letra = "";
                int index = 0;
                while (letra != " ")
                {
                    codigo += letra;
                    letra = texto.Substring(index, 1);
                    index++;
                }

                //faz a busca da matéria selecionada
                sql.pesquisar("select * from tab_materia where id_materia = '" + codigo + "';");

                if (sql.guarda.Read())  //faz a leitura dos resultados obtidos
                {
                    //confere se é possivel dar essas aulas
                    if (sql.aulas_disp_turma - Convert.ToInt32(sql.guarda[3].ToString()) >= 0)
                    {
                        sql.aulas_disp_turma = sql.aulas_disp_turma - Convert.ToInt32(sql.guarda[3].ToString());
                        lblqtdaulasT.Text = sql.aulas_disp_turma.ToString();
                        lbselecT.Items.Add(lbdispT.SelectedItem.ToString());
                        lbdispT.Items.RemoveAt(lbdispT.SelectedIndex);
                    }
                    else
                    {
                        MessageBox.Show("Número de aulas máxima excedido!", "*** ERRO ***", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            lbdispT.Items.Clear();
            lbselecT.Items.Clear();
            lblecionadasT.Items.Clear();
            cbmateriaT.Items.Clear();
            cbmateriaT.Text = "";

            FrmEscolherTurma_Load(sender, e);
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            //confere se há algo selecionado
            if (lblecionadasT.SelectedIndex != -1)
            {
                //seleciona o código da matéria escolhida
                texto = cbmateriaT.SelectedItem.ToString();
                string codigo = "";
                string letra = "";
                int index = 0;
                while (letra != " ")
                {
                    codigo += letra;
                    letra = texto.Substring(index, 1);
                    index++;
                }

                //faz a busca da matéria selecionada
                sql.pesquisar("select num_aulas_semanais from tab_materia where id_materia = '" + codigo + "';");

                //faz a leitura dos resultados obtidos
                if (sql.guarda.Read())
                {
                    //realoca as aulas utilizadas pela matéria para as aulas disponíveis
                    sql.aulas_disp_turma = sql.aulas_disp_turma + Convert.ToInt32(sql.guarda[0].ToString());
                    //atualiza label
                    lblqtdaulasT.Text = sql.aulas_disp_turma.ToString();
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //devolve a materia para a dropdownlist
                lbdispT.Items.Add(lblecionadasT.SelectedItem.ToString());

                //retira ela da listbox
                lblecionadasT.Items.RemoveAt(lblecionadasT.SelectedIndex);
            }
        }

        private void pictureBox12_Click(object sender, EventArgs e)
        {
            //seleciona o código da matéria escolhida
            texto = cbmateriaT.SelectedItem.ToString();
            string codigo = "";
            string letra = "";
            int index = 0;
            while (letra != " ")
            {
                codigo += letra;
                letra = texto.Substring(index, 1);
                index++;
            }
            //deleta registros prévios
            sql.executarcomando("DELETE FROM `relacao_prof_classe_temporaria` WHERE `relacao_prof_classe_temporaria`.`fk_prof` = " + sql.proflogado.ToString() + " and fk_materia = " + codigo, "Delete ");
            //faz um loop criando uma relação para cada matéria escolhida
            foreach (string turma in lbselecT.Items)
            {
                //seleciona o código da matéria
                codigo = "";
                letra = "";
                index = 0;
                while (letra != " ")
                {
                    codigo += letra;
                    letra = turma.Substring(index, 1);
                    index++;
                }

                texto = cbmateriaT.SelectedItem.ToString();
                string codigo2 = "";
                letra = "";
                index = 0;
                while (letra != " ")
                {
                    codigo2 += letra;
                    letra = texto.Substring(index, 1);
                    index++;
                }

                //fecha as conexões
                sql.guarda.Close();
                sql.olecon.Close();

                //insere a relação para a materia
                sql.executarcomando("INSERT INTO `relacao_prof_classe_temporaria` (`fk_prof`, `fk_classe`, `fk_materia`) VALUES (" + sql.proflogado.ToString() + ", " + codigo + ", " + codigo2 + ")", "Inserção ");
            }

            //lista codigo das atuais leciondas
            string[] lecionadas = new string[lblecionadasT.Items.Count];
            for (int y = 0; y < lblecionadasT.Items.Count; y++)
            {
                //seleciona o código da matéria
                string turma = lblecionadasT.Items[y].ToString();
                codigo = "";
                letra = "";
                index = 0;
                while (letra != " ")
                {
                    codigo += letra;
                    letra = turma.Substring(index, 1);
                    index++;
                }
                lecionadas[y] = codigo;
            }

            //testa os codigos q n existem mais
            foreach (int codigo2 in turmaslecionadas)
            {
                bool foi = false;
                foreach (string codigo3 in lecionadas)
                {
                    if (codigo2.ToString() == codigo3)
                    {
                        foi = true;
                    }
                }
                //e os deleta qnd os encontra
                if (foi != true)
                {
                    texto = cbmateriaT.SelectedItem.ToString();
                    string codigo3 = "";
                    letra = "";
                    index = 0;
                    while (letra != " ")
                    {
                        codigo3 += letra;
                        letra = texto.Substring(index, 1);
                        index++;
                    }
                    sql.executarcomando("DELETE FROM `relacao_prof_classe_permanente` WHERE `relacao_prof_classe_permanente`.`fk_classe` = " + codigo.ToString() + " AND `relacao_prof_classe_permanente`.`fk_prof` = " + sql.proflogado.ToString() + " AND `relacao_prof_classe_permanente`.`fk_materia` = " + codigo2, "Delete ");
                }
            }

            //ao fim da o feeddback ao usuario
            MessageBox.Show("Atualização com êxito", "*** Atualização ***", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
