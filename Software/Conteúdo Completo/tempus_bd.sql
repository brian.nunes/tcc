-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 14-Jul-2018 às 15:32
-- Versão do servidor: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tempus_bd`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `relacao_materia_curso`
--

DROP TABLE IF EXISTS `relacao_materia_curso`;
CREATE TABLE IF NOT EXISTS `relacao_materia_curso` (
  `fk_materia` int(11) NOT NULL,
  `fk_curso` int(11) NOT NULL,
  PRIMARY KEY (`fk_materia`,`fk_curso`),
  KEY `fk_tab_materia_has_tab_curso_tab_curso1_idx` (`fk_curso`),
  KEY `fk_tab_materia_has_tab_curso_tab_materia1_idx` (`fk_materia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `relacao_materia_curso`
--

INSERT INTO `relacao_materia_curso` (`fk_materia`, `fk_curso`) VALUES
(1, 1),
(2, 1),
(3, 1),
(1, 2),
(2, 2),
(4, 2),
(1, 3),
(2, 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `relacao_prof_classe_permanente`
--

DROP TABLE IF EXISTS `relacao_prof_classe_permanente`;
CREATE TABLE IF NOT EXISTS `relacao_prof_classe_permanente` (
  `fk_prof` int(11) NOT NULL DEFAULT '0',
  `fk_classe` int(11) NOT NULL,
  `fk_materia` int(11) NOT NULL,
  PRIMARY KEY (`fk_prof`,`fk_classe`,`fk_materia`),
  KEY `fk_tab_prof_has_tab_classe_tab_classe1_idx` (`fk_classe`),
  KEY `fk_tab_prof_has_tab_classe_tab_prof1_idx` (`fk_prof`),
  KEY `fk_relacao_prof_classe_tab_materia1_idx` (`fk_materia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `relacao_prof_classe_permanente`
--

INSERT INTO `relacao_prof_classe_permanente` (`fk_prof`, `fk_classe`, `fk_materia`) VALUES
(1, 1, 2),
(1, 1, 3),
(2, 1, 1),
(2, 2, 1),
(3, 2, 4),
(1, 3, 2),
(3, 3, 4),
(2, 4, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `relacao_prof_classe_temporaria`
--

DROP TABLE IF EXISTS `relacao_prof_classe_temporaria`;
CREATE TABLE IF NOT EXISTS `relacao_prof_classe_temporaria` (
  `fk_prof` int(11) NOT NULL,
  `fk_classe` int(11) NOT NULL,
  `fk_materia` int(11) NOT NULL,
  PRIMARY KEY (`fk_prof`,`fk_classe`,`fk_materia`),
  KEY `fk_tab_prof_has_tab_classe_tab_classe2_idx` (`fk_classe`),
  KEY `fk_tab_prof_has_tab_classe_tab_prof2_idx` (`fk_prof`),
  KEY `fk_relacao_prof_classe_temporaria_tab_materia1_idx` (`fk_materia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `relacao_prof_materia_permanente`
--

DROP TABLE IF EXISTS `relacao_prof_materia_permanente`;
CREATE TABLE IF NOT EXISTS `relacao_prof_materia_permanente` (
  `fk_materia` int(11) NOT NULL,
  `fk_prof` int(11) NOT NULL,
  PRIMARY KEY (`fk_materia`,`fk_prof`),
  KEY `fk_tab_materia_has_tab_prof_tab_prof1_idx` (`fk_prof`),
  KEY `fk_tab_materia_has_tab_prof_tab_materia1_idx` (`fk_materia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `relacao_prof_materia_permanente`
--

INSERT INTO `relacao_prof_materia_permanente` (`fk_materia`, `fk_prof`) VALUES
(2, 1),
(3, 1),
(1, 2),
(4, 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `relacao_prof_materia_temporaria`
--

DROP TABLE IF EXISTS `relacao_prof_materia_temporaria`;
CREATE TABLE IF NOT EXISTS `relacao_prof_materia_temporaria` (
  `fk_prof` int(11) NOT NULL,
  `fk_materia` int(11) NOT NULL,
  PRIMARY KEY (`fk_prof`,`fk_materia`),
  KEY `fk_tab_prof_has_tab_materia_tab_materia1_idx` (`fk_materia`),
  KEY `fk_tab_prof_has_tab_materia_tab_prof1_idx` (`fk_prof`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `relacao_prof_materia_temporaria`
--

INSERT INTO `relacao_prof_materia_temporaria` (`fk_prof`, `fk_materia`) VALUES
(3, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tab_aula`
--

DROP TABLE IF EXISTS `tab_aula`;
CREATE TABLE IF NOT EXISTS `tab_aula` (
  `id_aula` int(11) NOT NULL AUTO_INCREMENT COMMENT 'caso 0, vaga;caso 1, impossibilitado.',
  `num_sala` varchar(5) DEFAULT NULL,
  `fk_prof` int(11) NOT NULL DEFAULT '0',
  `fk_classe` int(11) NOT NULL DEFAULT '0',
  `fk_sigla_materia` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_aula`),
  KEY `fk_tab_aula_tab_prof1_idx` (`fk_prof`),
  KEY `fk_tab_aula_tab_classe1_idx` (`fk_classe`),
  KEY `fk_tab_aula_tab_materia1_idx` (`fk_sigla_materia`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tab_aula`
--

INSERT INTO `tab_aula` (`id_aula`, `num_sala`, `fk_prof`, `fk_classe`, `fk_sigla_materia`) VALUES
(1, 'VAG', 666, 666, 666),
(2, 'IND', 999, 999, 999),
(4, NULL, 2, 4, 1),
(5, NULL, 2, 1, 1),
(6, '261.5', 1, 1, 3),
(7, '351', 1, 3, 2),
(8, '255', 1, 1, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tab_classe`
--

DROP TABLE IF EXISTS `tab_classe`;
CREATE TABLE IF NOT EXISTS `tab_classe` (
  `id_classe` int(11) NOT NULL,
  `ano` int(11) NOT NULL,
  `letra` varchar(1) NOT NULL,
  `fk_curso` int(11) NOT NULL,
  PRIMARY KEY (`id_classe`),
  KEY `fk_tab_classe_tab_curso1_idx` (`fk_curso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tab_classe`
--

INSERT INTO `tab_classe` (`id_classe`, `ano`, `letra`, `fk_curso`) VALUES
(1, 3, 'E', 1),
(2, 3, 'M', 2),
(3, 3, 'N', 2),
(4, 3, 'A', 3),
(666, 666, 'Z', 666),
(999, 999, 'Z', 999);

--
-- Acionadores `tab_classe`
--
DROP TRIGGER IF EXISTS `add_dias_classe`;
DELIMITER $$
CREATE TRIGGER `add_dias_classe` AFTER INSERT ON `tab_classe` FOR EACH ROW BEGIN
INSERT INTO tab_dia VALUES (null, 2, 1, 1,1,1, 1, 1,1, 1, 1,666,new.id_classe);
INSERT INTO tab_dia VALUES (null, 3, 1, 1,1,1, 1, 1,1, 1, 1,666,new.id_classe);
INSERT INTO tab_dia VALUES (null, 4, 1, 1,1,1, 1, 1,1, 1, 1,666,new.id_classe);
INSERT INTO tab_dia VALUES (null, 5, 1, 1,1,1, 1, 1,1, 1, 1,666,new.id_classe);
INSERT INTO tab_dia VALUES (null, 6, 1, 1,1,1, 1, 1,1, 1, 1,666,new.id_classe);
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tab_curso`
--

DROP TABLE IF EXISTS `tab_curso`;
CREATE TABLE IF NOT EXISTS `tab_curso` (
  `id_curso` int(11) NOT NULL,
  `nome_curso` varchar(25) NOT NULL,
  `ano` int(11) NOT NULL,
  PRIMARY KEY (`id_curso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tab_curso`
--

INSERT INTO `tab_curso` (`id_curso`, `nome_curso`, `ano`) VALUES
(1, 'Informática', 3),
(2, 'Química', 3),
(3, 'Ensino Médio', 3),
(666, 'VAGA', 666),
(999, 'INDISPONIVEL', 999);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tab_dia`
--

DROP TABLE IF EXISTS `tab_dia`;
CREATE TABLE IF NOT EXISTS `tab_dia` (
  `id_dia` int(11) NOT NULL AUTO_INCREMENT,
  `dia_semana` int(11) NOT NULL,
  `730` int(11) NOT NULL DEFAULT '0',
  `820` int(11) NOT NULL DEFAULT '0',
  `910` int(11) NOT NULL DEFAULT '0',
  `1020` int(11) NOT NULL DEFAULT '0',
  `1110` int(11) NOT NULL DEFAULT '0',
  `1330` int(11) NOT NULL DEFAULT '0',
  `1420` int(11) NOT NULL DEFAULT '0',
  `1530` int(11) NOT NULL DEFAULT '0',
  `1620` int(11) NOT NULL DEFAULT '0',
  `fk_prof` int(11) NOT NULL DEFAULT '0',
  `fk_classe` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_dia`),
  KEY `fk_tab_dia_tab_aula1_idx` (`730`),
  KEY `fk_tab_dia_tab_aula2_idx` (`820`),
  KEY `fk_tab_dia_tab_aula3_idx` (`910`),
  KEY `fk_tab_dia_tab_aula4_idx` (`1020`),
  KEY `fk_tab_dia_tab_aula5_idx` (`1110`),
  KEY `fk_tab_dia_tab_aula6_idx` (`1330`),
  KEY `fk_tab_dia_tab_aula7_idx` (`1420`),
  KEY `fk_tab_dia_tab_aula8_idx` (`1530`),
  KEY `fk_tab_dia_tab_aula9_idx` (`1620`),
  KEY `fk_tab_dia_tab_prof1_idx` (`fk_prof`),
  KEY `fk_tab_dia_tab_classe1_idx` (`fk_classe`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tab_dia`
--

INSERT INTO `tab_dia` (`id_dia`, `dia_semana`, `730`, `820`, `910`, `1020`, `1110`, `1330`, `1420`, `1530`, `1620`, `fk_prof`, `fk_classe`) VALUES
(71, 2, 8, 8, 8, 8, 1, 6, 6, 6, 1, 1, 999),
(72, 3, 7, 7, 7, 7, 1, 2, 2, 2, 2, 1, 999),
(73, 4, 1, 1, 1, 1, 1, 2, 2, 2, 2, 1, 999),
(74, 5, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 999),
(75, 6, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 999),
(76, 2, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 999),
(77, 3, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 999),
(78, 4, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 999),
(79, 5, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 999),
(80, 6, 2, 2, 2, 2, 2, 1, 1, 1, 1, 2, 999),
(81, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 999),
(82, 3, 1, 1, 1, 1, 1, 2, 2, 2, 2, 3, 999),
(83, 4, 1, 1, 1, 1, 1, 2, 2, 2, 2, 3, 999),
(84, 5, 1, 1, 1, 1, 1, 2, 2, 2, 2, 3, 999),
(85, 6, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 999),
(86, 2, 8, 8, 8, 8, 1, 6, 6, 6, 1, 666, 1),
(87, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 666, 1),
(88, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 666, 1),
(89, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 666, 1),
(90, 6, 1, 1, 1, 1, 1, 1, 1, 1, 1, 666, 1),
(91, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 666, 2),
(92, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 666, 2),
(93, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 666, 2),
(94, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 666, 2),
(95, 6, 1, 1, 1, 1, 1, 1, 1, 1, 1, 666, 2),
(96, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 666, 3),
(97, 3, 7, 7, 7, 7, 1, 1, 1, 1, 1, 666, 3),
(98, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 666, 3),
(99, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 666, 3),
(100, 6, 1, 1, 1, 1, 1, 1, 1, 1, 1, 666, 3),
(101, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 666, 4),
(102, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 666, 4),
(103, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 666, 4),
(104, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 666, 4),
(105, 6, 1, 1, 1, 1, 1, 1, 1, 1, 1, 666, 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tab_materia`
--

DROP TABLE IF EXISTS `tab_materia`;
CREATE TABLE IF NOT EXISTS `tab_materia` (
  `id_materia` int(11) NOT NULL,
  `nome_materia` varchar(50) NOT NULL,
  `sigla_materia` varchar(3) NOT NULL,
  `num_aulas_semanais` int(11) NOT NULL,
  PRIMARY KEY (`id_materia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tab_materia`
--

INSERT INTO `tab_materia` (`id_materia`, `nome_materia`, `sigla_materia`, `num_aulas_semanais`) VALUES
(1, 'Português', 'LPC', 4),
(2, 'Matemática', 'MAT', 4),
(3, 'Programação de Computadores', 'PRC', 3),
(4, 'Química Ambiental', 'QUA', 2),
(666, 'VAGA', 'NDA', 0),
(999, 'INDISPONIVEL', 'IND', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tab_prof`
--

DROP TABLE IF EXISTS `tab_prof`;
CREATE TABLE IF NOT EXISTS `tab_prof` (
  `id_prof` int(11) NOT NULL,
  `nome` varchar(25) NOT NULL,
  `sigla` char(3) NOT NULL,
  `senha` varchar(16) NOT NULL,
  `preferencia` int(11) NOT NULL DEFAULT '0',
  `num_aula_disponivel` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_prof`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tab_prof`
--

INSERT INTO `tab_prof` (`id_prof`, `nome`, `sigla`, `senha`, `preferencia`, `num_aula_disponivel`) VALUES
(1, 'Brian Andrade Nunes', 'BAN', '123', 13, 32),
(2, 'Gustavo Monteiro Teixeira', 'GMT', '123', 11, 14),
(3, 'Rafael Silva Picolli', 'RSP', '123', 10, 24),
(666, 'VAGA', 'NDA', 'senhasenha', 0, 0),
(999, 'INDISPONIVEL', 'IND', 'senhasenhasenha', 0, 0);

--
-- Acionadores `tab_prof`
--
DROP TRIGGER IF EXISTS `add_dias_prof`;
DELIMITER $$
CREATE TRIGGER `add_dias_prof` AFTER INSERT ON `tab_prof` FOR EACH ROW BEGIN
INSERT INTO tab_dia VALUES (null, 2, 2, 2,2,2, 2, 2,2, 2, 2,NEW.id_prof,999);
INSERT INTO tab_dia VALUES (null, 3, 2, 2,2,2, 2, 2,2, 2, 2,NEW.id_prof,999);
INSERT INTO tab_dia VALUES (null, 4, 2, 2,2,2, 2, 2,2, 2, 2,NEW.id_prof,999);
INSERT INTO tab_dia VALUES (null, 5, 2, 2,2,2, 2, 2,2, 2, 2,NEW.id_prof,999);
INSERT INTO tab_dia VALUES (null, 6, 2, 2,2,2, 2, 2,2, 2, 2,NEW.id_prof,999);
END
$$
DELIMITER ;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `tab_aula`
--
ALTER TABLE `tab_aula`
  ADD CONSTRAINT `fk_tab_aula_tab_classe1` FOREIGN KEY (`fk_classe`) REFERENCES `tab_classe` (`id_classe`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tab_aula_tab_materia1` FOREIGN KEY (`fk_sigla_materia`) REFERENCES `tab_materia` (`id_materia`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tab_aula_tab_prof1` FOREIGN KEY (`fk_prof`) REFERENCES `tab_prof` (`id_prof`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `tab_classe`
--
ALTER TABLE `tab_classe`
  ADD CONSTRAINT `fk_tab_classe_tab_curso1` FOREIGN KEY (`fk_curso`) REFERENCES `tab_curso` (`id_curso`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
